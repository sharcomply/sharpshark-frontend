import i18n from "i18next";
import Backend from "i18next-http-backend";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";
import { getLanguage } from "helpers/localizationLanguageStorage";

i18n
    // Подключение бэкенда i18next
    .use(Backend)
    // Автоматическое определение языка
    .use(LanguageDetector)
    // модуль инициализации
    .use(initReactI18next)
    .init({
        // Стандартный язык
        fallbackLng: getLanguage() || "en",
        debug: false,
        // Распознавание и кэширование языковых кук
        detection: {
            order: ["queryString", "cookie"],
            cache: ["cookie"],
        },
        backend: {
            crossDomain: true,
            loadPath: `https://d1k1dlls6f6nna.cloudfront.net/locales/{{lng}}.json`,
        },
        interpolation: {
            escapeValue: false,
        },
    });

i18n.on("failedLoading", function (lng, ns, msg) {
    console.log(lng, ns, msg);
    i18n.loadLanguages(["English"]);
    i18n.changeLanguage("English")
});

export default i18n;
