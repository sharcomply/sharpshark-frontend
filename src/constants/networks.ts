export enum ChainId {
    GOERLI_TESTNET = 5,
    ETH_MAINNET = 1,
    POLYGON_MAINNET = 137,
    BSC_MAINNET = 56,
    POLYGON_TESTNET = 80001,
    BSC_TESTNET = 97,
}

export const NetworksList = [
    {
        label: "Goerli Test Network",
        value: ChainId.GOERLI_TESTNET.toString(),
        symbol: "eth",
        chainId: '0x5'
    },
    {
        label: "BNB Test Network",
        value: ChainId.BSC_TESTNET.toString(),
        symbol: "bsc",
        chainId: "0x61",
    },
    {
        label: "Polygon Mainnet",
        value: ChainId.POLYGON_MAINNET.toString(),
        symbol: "polygon",
        chainId: "0x89",
    },
    // {
    //     label: "Polygon Testnet",
    //     value: ChainId.POLYGON_TESTNET.toString(),
    //     symbol: "polygon",
    //     chainId: "0x13881",
    // },
    {
        label: "Near Network",
        value: "near",
        symbol: "near",
    },
];

export const getNetworkById = (id?: string) => {
    return NetworksList.find((item) => item.value === id);
};

export const getNetworkBySymbol = (symbol?: string) => {
    return NetworksList.find((item) => item.symbol === symbol);
};
