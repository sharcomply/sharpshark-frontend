
export const viewMethods = [
    "nft_token",
    "nft_is_approved",
    "nft_total_supply",
    "nft_tokens",
    "nft_tokens_for_owner",
    "nft_supply_for_owner",
    "nft_metadata",
    "current_owner",
    "minter",
    "metaDataHash",
    "contentHash",
    "createdDate",
    "expiredDate",
    "licenseHash",
    "parentTokenId",
    "version",
    "get_token_info"
]

export const changeMethods = [
    "pay_request",
    "pay_monitoring_request",
    "set_public_key",
    "set_transfers_allowed",
    "nft_mint",
    "set_sale",
    "buy_cert",
    "nft_transfer",
    "nft_transfer_call",
    "nft_resolve_transfer",
    "nft_approve",
    "nft_revoke",
    "nft_revoke_all",
    "set_license_hash"
]

