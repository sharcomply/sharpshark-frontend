import { ChainId } from './networks';

export type ContractAddress = {
  [chainId in ChainId]?: string;
};

export const ContractAddressBook: ContractAddress = {
  [ChainId.GOERLI_TESTNET]: '0x7AE5A0760544e70384C26c3b6597201E7695Cba2',
  [ChainId.BSC_TESTNET]: '0x0D050682FCDA9B1F0F45A3C41b1b93eB436DE6c4',
  [ChainId.POLYGON_MAINNET]: '0x22B8f00Ae3793625a078Cd191f74e4cCd2a82ee5',

  // [ChainId.ETH_MAINNET]: '0x957a8342ADa13145517979861D759bFa1CEBCD7C',
  // [ChainId.BSC_MAINNET]: '0xb6D3d9FC8CC300b162049c6f78016a614c0349a1',
  // [ChainId.POLYGON_MAINNET]: '0x679647EE70f5d3dc8155B67FaAb6d886a4648dCC',
};
