import { ethers } from 'ethers';
import { ChainId } from './networks';

const env = process.env

export const NEAR_CONTRACT = "b3e315b307957a852ec0cbb855d26f1f1d3a7de5c86e5f7d6f272f9b5c4ebbf6"


export type ProviderAddress = {
    [chainId in ChainId]?: ethers.providers.Provider;
};

export const ProviderAddressBook: ProviderAddress = {
    [ChainId.GOERLI_TESTNET]: new ethers.providers.AlchemyProvider('goerli', env.REACT_APP_GOERLI_ALCHEMY_API_KEY),
    [ChainId.BSC_TESTNET]: new ethers.providers.JsonRpcProvider('https://data-seed-prebsc-2-s2.binance.org:8545/'),
    [ChainId.POLYGON_MAINNET]: new ethers.providers.AlchemyProvider('matic', env.REACT_APP_POLYGON_ALCHEMY_API_KEY),
};
