import ModalContainer from "features/Modals";
import Routes from "features/Routes";
import "assets/styles/global.css";
import ApplicationLoader from "features/ApplicationLoader";
import { Providers } from "providers";
import { useLocation, useSearchParams } from "react-router-dom";
import { useEffect } from "react";
import { useCrossTabState } from "hooks/useCrossTbState";


export default function App() {
    // Это нужно для того что бы закрыть страницу после оплаты. Страница оплаты создается в новой вкладке.

    let { search } = useLocation();
    const [searchParams, setSearchParams] = useSearchParams();
    const [, setName] = useCrossTabState("stripeLicence", "");
    const [, setNearName] = useCrossTabState("nearLicence", "");
    const [, setNearLicenceName] = useCrossTabState("setNearLicence", "");
    const [, setNearConnectToPay] = useCrossTabState("nearConnectToPay", "");
    const [, setNearConnect] = useCrossTabState("nearConnect", "");
    const [, setNearErrorCode] = useCrossTabState("errorCode", "");
    const [, setNearStartMonitoringSuccess] = useCrossTabState("nearMonitoringSuccess", "");

    useEffect(() => {
        if (search) {
            setSearchParams(search);
            if (searchParams.get("stripePayLicenceSucces")) {
                setName(searchParams.get("stripePayLicenceSucces")?.toString());
            }
            if (searchParams.get("nearPayLicenceSucces") && !searchParams.get("errorCode")) {
                setNearName(searchParams.get("nearPayLicenceSucces")?.toString());
            }
            if (searchParams.get("nearAddLicenceSuccess")) {
                setNearLicenceName(searchParams.get("nearAddLicenceSuccess")?.toString());
            }
            if (searchParams.get("nearConnectToPay")) {
                setNearConnectToPay(searchParams.get("nearConnectToPay")?.toString());
            }
            if (searchParams.get("nearConnect")) {
                setNearConnect(searchParams.get("nearConnect")?.toString());
            }
            if (searchParams.get("errorCode")) {
                setNearErrorCode(searchParams.get("errorCode")?.toString());
            }
            if (searchParams.get("nearMonitoringSuccess") && !searchParams.get("errorCode")) {
                setNearStartMonitoringSuccess(searchParams.get("nearMonitoringSuccess")?.toString());
            }
            window.close();
        }
    }, [search, setSearchParams, searchParams, setName, setNearName, setNearLicenceName, setNearConnect, setNearConnectToPay]);
    return (
        <Providers>
            <ApplicationLoader />
            <ModalContainer />
            <Routes />
        </Providers>
    );
}
