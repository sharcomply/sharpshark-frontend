import { useTranslation } from "react-i18next";

export function useAlertData(page: string, step?: number) {
    const { t } = useTranslation();
    switch (page) {
        case "Website_Owners_Level":
            switch (step) {
                case 0:
                    return {
                        title: t("modals.info.collecting_evidence_alert_title"),
                        text: t("modals.info.collecting_evidence_alert_text"),
                    };
                case 1:
                    return {
                        title: t("modals.info.generating_claim_alert_title"),
                        text: t("modals.info.generating_claim_alert_text"),
                    };
                case 2:
                    return {
                        title: t("modals.info.sending_alert_title"),
                        text: t("modals.info.sending_alert_text"),
                    };
                default:
                    console.error(`don't found step ${step} for dispute page`);
                    return {
                        title: t("modals.info.basic_alert_title"),
                        text: t("modals.info.basic_alert_text"),
                    };
            }
        case "All_draft":
            return {
                title: t("modals.info.all_draft_alert_title"),
                text: t("modals.info.all_draft_alert_text"),
            };
        default:
            console.error(`don't found page ${page}`);
            return {
                title: t("modals.info.basic_alert_title"),
                text: t("modals.info.basic_alert_text"),
            };
    }
}
