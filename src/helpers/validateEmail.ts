export const validateEmail = (value: string) => {
    return /^\S+@\S+$/.test(value);
};
