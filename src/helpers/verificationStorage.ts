const storage = window.localStorage;

const getVerificationStorage = () => {
  return storage.getItem("verificationStorage");
}

const setVerificationStorage = (token: string) => {
  storage.setItem("verificationStorage", token);
}

const removeVerificationStorage = () => {
  storage.removeItem("verificationStorage");
}

export { getVerificationStorage, setVerificationStorage, removeVerificationStorage };