
interface ItemDate {
    updatedAt: string;
    date?: string;
    notTrackingDate?: string;
    protectedDate?: string;
}

export interface sortedListTypes {
    today: any[],
    week: any[],
    month: any[],
    lastMonth: any[],
    earlier: any[]
}

type objectKeyType = 'updatedAt' | 'date' | 'notTrackingDate' | 'protectedDate'

export function getListByDate<T>(items: (T & ItemDate)[], objectKey: objectKeyType = 'updatedAt') {
    const currentDate = new Date();
    const currentDay = currentDate.getDay();
    const currentMonth = currentDate.getMonth();

    const Lists: sortedListTypes =  {
        today: [],
        week: [],
        month: [],
        lastMonth: [],
        earlier: [],
    }

    const sorted = items.sort((a, b) => {
        const dateA = new Date(a[objectKey]! || a.date!)
        const dateB = new Date(b[objectKey]! || a.date!)

        return dateA < dateB ? 1 : -1
    })

    sorted.forEach((item) => {
        const date = new Date(item.updatedAt! || item.date!);
        const day = date.getDay();
        const month = date.getMonth();

        if (month === currentMonth - 1) {
            return Lists.month.push(item);
        }
        if (month === currentMonth && day === currentDay) {
            return Lists.today.push(item);
        }
        if (month === currentMonth && currentDay - day <= 7) {
            return Lists.week.push(item);
        }
        if (month === currentMonth) {
            return Lists.lastMonth.push(item);
        }

        Lists.earlier.push(item);
    })

    return Lists
}

export const compareRows = (comparingKey: string, a: any, b: any): number => {
    const date1 = new Date(a[comparingKey]);
    const date2 = new Date(b[comparingKey]);
    if (date1 < date2) {
        return 1;
    }
    if (date1 > date2) {
        return -1;
    }
    return 0;
};
