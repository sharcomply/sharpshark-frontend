const storage = window.localStorage;

const getWelcomeWatched = () => {
  return storage.getItem('welcome-watched');
}

const setWelcomeWatched = (state: string) => {
  storage.setItem('welcome-watched', state);
}

const removeWelcomeWatched = () => {
  storage.removeItem('welcome-watched');
}

export {
  getWelcomeWatched,
  setWelcomeWatched,
  removeWelcomeWatched
};
