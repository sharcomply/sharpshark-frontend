export const getClaimText = (demands: string | undefined, disput: any, t:any) => {
    const dateLocal = new Date();

    const UTC = () => {
        if (dateLocal.getTimezoneOffset() <= 0) {
            return `+${Math.abs(dateLocal.getTimezoneOffset() / 60)}`;
        }
        if (dateLocal.getTimezoneOffset() > 0) {
            return `-${Math.abs(dateLocal.getTimezoneOffset() / 60)}`;
        }
    };
    const date = dateLocal.toLocaleTimeString().slice(0, -3) + " UTC " + UTC();

    switch (disput.status) {
        case "In Progress: Website Owner":
        case "Draft: Website Owner":
            switch (demands) {
                case "Remove":
                    return (
                        `<div>${t("dispute.claim_text.hello")}</div> </br>
                        <p>${t("dispute.claim_text.there_is_material")} ${disput.alertId.scanId.similarContent.map((item: any) => `${item.url}`)} 
                        ${t("dispute.claim_text.website_owner.breaches_rules_law")}:
                        <a style="text-decoration: underline; color: blue" href="https://wipolex.wipo.int/en/text/283702"> ${t("dispute.claim_text.article_6")} </a>
                        ${t("dispute.claim_text.berne_convention")}
                        <a style="text-decoration: underline; color: blue" href='https://docs.google.com/spreadsheets/d/1HUtEb4LlqrPasemLBnCtyXRjm87aFLokJYGvYCXrNSk/edit?usp=sharing'>${t("dispute.claim_text.rule_of_the_current_legislation")}</a>
                        ${t("dispute.claim_text.your_country")}</br></br>
                        ${t("dispute.claim_text.evidence.evidence_title")}:</br>
                        <ul>
                        ${disput.publicationQuestionLinks.length !== 0 && ` 
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.link_publication_in_question_title")} ${disput.publicationQuestionLinks}
                        </li>
                        `}
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.time_of_case_title")}: ${date}
                        </li>
                        ${disput.publicationQuestionScreenshots.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.screenshot_publication_title")}: ${disput.publicationQuestionScreenshots.map((item: string) => `</br>${item}`)}
                        </li>
                        `}
                        ${disput.publicationQuestionWebarchiveLinks.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.publication_question_archive_title")}: ${disput.publicationQuestionWebarchiveLinks} </br>
                        </li>
                        `}
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.demands_title")}: ${disput.demands} </br>
                        </li>
                        <li style="margin-left: 32px" type="disc">
                         ${t("dispute.claim_text.evidence.certificate_link_title")}: </br>
                        ${window.location.origin + `/certificate/${disput.documentId.tokenId}`}
                        </li>
                        ${disput.publicationLinks.length !== 0 && ` 
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.publication_link_title")}: ${disput.publicationLinks}
                        </li>
                        `}
                        ${disput.publicationLinks.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.publication_link_archive_title")}: ${disput.publicationWebarchiveLinks}
                        </li>
                        `}
                        ${disput.draftsScreenshots.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.screenshots_your_drafts_title")}: ${disput.draftsScreenshots.map((item: string) => `</br>${item}`)}
                        </li>
                        `}
                        </ul>
                        </br>
                        ${t("dispute.claim_text.website_owner.only_remove")}</br></br>
                        ${t("dispute.claim_text.note")}</br></br></br>
                        ${t("dispute.claim_text.kind_regards")}
                        </p>`
                    );
                case "Pay and/or credit":
                    return (
                        `<div>${t("dispute.claim_text.hello")}</div> </br>
                        <p>${t("dispute.claim_text.there_is_material")} ${disput.alertId.scanId.similarContent.map((item: any) => `${item.url}`)} 
                        ${t("dispute.claim_text.website_owner.breaches_rules_law")}:
                        <a style="text-decoration: underline; color: blue" href="https://wipolex.wipo.int/en/text/283702"> ${t("dispute.claim_text.article_6")} </a>
                        ${t("dispute.claim_text.berne_convention")}
                        <a style="text-decoration: underline; color: blue" href='https://docs.google.com/spreadsheets/d/1HUtEb4LlqrPasemLBnCtyXRjm87aFLokJYGvYCXrNSk/edit?usp=sharing'>${t("dispute.claim_text.rule_of_the_current_legislation")}</a>
                        ${t("dispute.claim_text.your_country")}</br></br>
                        ${t("dispute.claim_text.evidence.evidence_title")}:</br>
                        <ul>
                        ${disput.publicationQuestionLinks.length !== 0 && ` 
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.link_publication_in_question_title")} ${disput.publicationQuestionLinks}
                        </li>
                        `}
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.time_of_case_title")}: ${date}
                        </li>
                        ${disput.publicationQuestionScreenshots.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.screenshot_publication_title")}: ${disput.publicationQuestionScreenshots.map((item: string) => `</br>${item}`)}
                        </li>
                        `}
                        ${disput.publicationQuestionWebarchiveLinks.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.publication_question_archive_title")}: ${disput.publicationQuestionWebarchiveLinks} </br>
                        </li>
                        `}
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.demands_title")}: ${disput.demands} </br>
                        </li>
                        <li style="margin-left: 32px" type="disc">
                         ${t("dispute.claim_text.evidence.certificate_link_title")}: </br>
                        ${window.location.origin + `/certificate/${disput.documentId.tokenId}`}
                        </li>
                        ${disput.publicationLinks.length !== 0 && ` 
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.publication_link_title")}: ${disput.publicationLinks}
                        </li>
                        `}
                        ${disput.publicationLinks.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.publication_link_archive_title")}: ${disput.publicationWebarchiveLinks}
                        </li>
                        `}
                        ${disput.draftsScreenshots.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.screenshots_your_drafts_title")}: ${disput.draftsScreenshots.map((item: string) => `</br>${item}`)}
                        </li>
                        `}
                        </ul>
                        </br>
                        ${t("dispute.claim_text.website_owner.only_pay", {publicationLinks: `${disput.alertId.scanId.similarContent.map((item: any) => `${item.url}`)}` })}</br></br>
                        ${t("dispute.claim_text.note")}</br></br></br>
                        ${t("dispute.claim_text.kind_regards")}
                        </p>`
                    );
                case "Pay and remove":
                    return (
                        `<div>${t("dispute.claim_text.hello")}</div> </br>
                        <p>${t("dispute.claim_text.there_is_material")} ${disput.alertId.scanId.similarContent.map((item: any) => `${item.url}`)} 
                        ${t("dispute.claim_text.website_owner.breaches_rules_law")}:
                        <a style="text-decoration: underline; color: blue" href="https://wipolex.wipo.int/en/text/283702"> ${t("dispute.claim_text.article_6")} </a>
                        ${t("dispute.claim_text.berne_convention")}
                        <a style="text-decoration: underline; color: blue" href='https://docs.google.com/spreadsheets/d/1HUtEb4LlqrPasemLBnCtyXRjm87aFLokJYGvYCXrNSk/edit?usp=sharing'>${t("dispute.claim_text.rule_of_the_current_legislation")}</a>
                        ${t("dispute.claim_text.your_country")}</br></br>
                        ${t("dispute.claim_text.evidence.evidence_title")}:</br>
                        <ul>
                        ${disput.publicationQuestionLinks.length !== 0 && ` 
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.link_publication_in_question_title")} ${disput.publicationQuestionLinks}
                        </li>
                        `}
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.time_of_case_title")}: ${date}
                        </li>
                        ${disput.publicationQuestionScreenshots.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.screenshot_publication_title")}: ${disput.publicationQuestionScreenshots.map((item: string) => `</br>${item}`)}
                        </li>
                        `}
                        ${disput.publicationQuestionWebarchiveLinks.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.publication_question_archive_title")}: ${disput.publicationQuestionWebarchiveLinks} </br>
                        </li>
                        `}
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.demands_title")}: ${disput.demands} </br>
                        </li>
                        <li style="margin-left: 32px" type="disc">
                         ${t("dispute.claim_text.evidence.certificate_link_title")}: </br>
                        ${window.location.origin + `/certificate/${disput.documentId.tokenId}`}
                        </li>
                        ${disput.publicationLinks.length !== 0 && ` 
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.publication_link_title")}: ${disput.publicationLinks}
                        </li>
                        `}
                        ${disput.publicationLinks.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.publication_link_archive_title")}: ${disput.publicationWebarchiveLinks}
                        </li>
                        `}
                        ${disput.draftsScreenshots.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.screenshots_your_drafts_title")}: ${disput.draftsScreenshots.map((item: string) => `</br>${item}`)}
                        </li>
                        `}
                        </ul>
                        </br>
                        ${t("dispute.claim_text.website_owner.pay_and_remove", {publicationLinks: `${disput.alertId.scanId.similarContent.map((item: any) => `${item.url}`)}` })}</br></br>
                        ${t("dispute.claim_text.note")}</br></br></br>
                        ${t("dispute.claim_text.kind_regards")}
                        </p>`
                    );
                case "Pay and attribute":
                    return (
                        `<div>${t("dispute.claim_text.hello")}</div> </br>
                        <p>${t("dispute.claim_text.there_is_material")} ${disput.alertId.scanId.similarContent.map((item: any) => `${item.url}`)} 
                        ${t("dispute.claim_text.website_owner.breaches_rules_law")}:
                        <a style="text-decoration: underline; color: blue" href="https://wipolex.wipo.int/en/text/283702"> ${t("dispute.claim_text.article_6")} </a>
                        ${t("dispute.claim_text.berne_convention")}
                        <a style="text-decoration: underline; color: blue" href='https://docs.google.com/spreadsheets/d/1HUtEb4LlqrPasemLBnCtyXRjm87aFLokJYGvYCXrNSk/edit?usp=sharing'>${t("dispute.claim_text.rule_of_the_current_legislation")}</a>
                        ${t("dispute.claim_text.your_country")}</br></br>
                        ${t("dispute.claim_text.evidence.evidence_title")}:</br>
                        <ul>
                        ${disput.publicationQuestionLinks.length !== 0 && ` 
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.link_publication_in_question_title")} ${disput.publicationQuestionLinks}
                        </li>
                        `}
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.time_of_case_title")}: ${date}
                        </li>
                        ${disput.publicationQuestionScreenshots.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.screenshot_publication_title")}: ${disput.publicationQuestionScreenshots.map((item: string) => `</br>${item}`)}
                        </li>
                        `}
                        ${disput.publicationQuestionWebarchiveLinks.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.publication_question_archive_title")}: ${disput.publicationQuestionWebarchiveLinks} </br>
                        </li>
                        `}
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.demands_title")}: ${disput.demands} </br>
                        </li>
                        <li style="margin-left: 32px" type="disc">
                         ${t("dispute.claim_text.evidence.certificate_link_title")}: </br>
                        ${window.location.origin + `/certificate/${disput.documentId.tokenId}`}
                        </li>
                        ${disput.publicationLinks.length !== 0 && ` 
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.publication_link_title")}: ${disput.publicationLinks}
                        </li>
                        `}
                        ${disput.publicationLinks.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.publication_link_archive_title")}: ${disput.publicationWebarchiveLinks}
                        </li>
                        `}
                        ${disput.draftsScreenshots.length !== 0 && `
                        <li style="margin-left: 32px" type="disc">
                        ${t("dispute.claim_text.evidence.screenshots_your_drafts_title")}: ${disput.draftsScreenshots.map((item: string) => `</br>${item}`)}
                        </li>
                        `}
                        </ul>
                        </br>
                        ${t("dispute.claim_text.website_owner.pay_and_attribute", {
                            publicationLinks: `${disput.alertId.scanId.similarContent.map((item: any) => `${item.url}`)}`,
                            certificateLink: `${window.location.origin}` + `/certificate/${disput.documentId.tokenId}`
                        })}</br></br>
                        })}</br></br>
                        ${t("dispute.claim_text.note")}</br></br></br>
                        ${t("dispute.claim_text.kind_regards")}
                        </p>`
                    );                        
                default:
                    return "";
            }
        case "Claim Sent: Website Owner":
        case "Draft: Provider":
        case "In Progress: Provider":
            return (
                `<div>${t("dispute.claim_text.hello")}</div> </br>
                <p>${t("dispute.claim_text.there_is_material")} ${disput.alertId.scanId.similarContent.map((item: any) => `${item.url}`)} 
                ${t("dispute.claim_text.provider.breaches_rules_law")}:
                <a style="text-decoration: underline; color: blue" href="https://wipolex.wipo.int/en/text/283702"> ${t("dispute.claim_text.article_6")} </a>
                ${t("dispute.claim_text.berne_convention")}
                <a style="text-decoration: underline; color: blue" href='https://docs.google.com/spreadsheets/d/1HUtEb4LlqrPasemLBnCtyXRjm87aFLokJYGvYCXrNSk/edit?usp=sharing'>${t("dispute.claim_text.rule_of_the_current_legislation")}</a>
                ${t("dispute.claim_text.your_country")}</br></br>
                ${t("dispute.claim_text.evidence.evidence_title")}:</br>
                <ul>
                ${disput.publicationQuestionLinks.length !== 0 && ` 
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.link_publication_in_question_title")} ${disput.publicationQuestionLinks}
                </li>
                `}
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.time_of_case_title")}: ${date}
                </li>
                ${disput.publicationQuestionScreenshots.length !== 0 && `
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.screenshot_publication_title")}: ${disput.publicationQuestionScreenshots.map((item: string) => `</br>${item}`)}
                </li>
                `}
                ${disput.publicationQuestionWebarchiveLinks.length !== 0 && `
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.publication_question_archive_title")}: ${disput.publicationQuestionWebarchiveLinks} </br>
                </li>
                `}
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.demands_title")}: ${disput.demands} </br>
                </li>
                <li style="margin-left: 32px" type="disc">
                 ${t("dispute.claim_text.evidence.certificate_link_title")}: </br>
                ${window.location.origin + `/certificate/${disput.documentId.tokenId}`}
                </li>
                ${disput.publicationLinks.length !== 0 && ` 
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.publication_link_title")}: ${disput.publicationLinks}
                </li>
                `}
                ${disput.publicationLinks.length !== 0 && `
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.publication_link_archive_title")}: ${disput.publicationWebarchiveLinks}
                </li>
                `}
                ${disput.draftsScreenshots.length !== 0 && `
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.screenshots_your_drafts_title")}: ${disput.draftsScreenshots.map((item: string) => `</br>${item}`)}
                </li>
                `}
                </ul>
                </br>
                ${t("dispute.claim_text.provider.only_remove")}</br></br>
                ${t("dispute.claim_text.note")}</br></br></br>
                ${t("dispute.claim_text.kind_regards")}
                </p>`
            );
        case "Claim Sent: Provider":
        case "Draft: Google":
        case "In Progress: Google":
        case "Claim Sent: Google":
        case "Closed":
        case "Under Court":
            return (
                `<div>${t("dispute.claim_text.hello")}</div> </br>
                <p>${t("dispute.claim_text.there_is_material")} ${disput.alertId.scanId.similarContent.map((item: any) => `${item.url}`)} 
                ${t("dispute.claim_text.search_engine.breaches_rules_law")}:
                <a style="text-decoration: underline; color: blue" href="https://wipolex.wipo.int/en/text/283702"> ${t("dispute.claim_text.article_6")} </a>
                ${t("dispute.claim_text.berne_convention")}
                <a style="text-decoration: underline; color: blue" href='https://docs.google.com/spreadsheets/d/1HUtEb4LlqrPasemLBnCtyXRjm87aFLokJYGvYCXrNSk/edit?usp=sharing'>${t("dispute.claim_text.rule_of_the_current_legislation")}</a>
                ${t("dispute.claim_text.your_country")}</br></br>
                ${t("dispute.claim_text.evidence.evidence_title")}:</br>
                <ul>
                ${disput.publicationQuestionLinks.length !== 0 && ` 
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.link_publication_in_question_title")} ${disput.publicationQuestionLinks}
                </li>
                `}
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.time_of_case_title")}: ${date}
                </li>
                ${disput.publicationQuestionScreenshots.length !== 0 && `
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.screenshot_publication_title")}: ${disput.publicationQuestionScreenshots.map((item: string) => `</br>${item}`)}
                </li>
                `}
                ${disput.publicationQuestionWebarchiveLinks.length !== 0 && `
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.publication_question_archive_title")}: ${disput.publicationQuestionWebarchiveLinks} </br>
                </li>
                `}
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.demands_title")}: ${disput.demands} </br>
                </li>
                <li style="margin-left: 32px" type="disc">
                 ${t("dispute.claim_text.evidence.certificate_link_title")}: </br>
                ${window.location.origin + `/certificate/${disput.documentId.tokenId}`}
                </li>
                ${disput.publicationLinks.length !== 0 && ` 
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.publication_link_title")}: ${disput.publicationLinks}
                </li>
                `}
                ${disput.publicationLinks.length !== 0 && `
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.publication_link_archive_title")}: ${disput.publicationWebarchiveLinks}
                </li>
                `}
                ${disput.draftsScreenshots.length !== 0 && `
                <li style="margin-left: 32px" type="disc">
                ${t("dispute.claim_text.evidence.screenshots_your_drafts_title")}: ${disput.draftsScreenshots.map((item: string) => `</br>${item}`)}
                </li>
                `}
                </ul>
                </br>
                ${t("dispute.claim_text.search_engine.only_remove")}</br></br>
                ${t("dispute.claim_text.note")}</br></br></br>
                ${t("dispute.claim_text.kind_regards")}
                </p>`
            );
        default:
            return "";
    }
};


