const storage = window.localStorage;
// const sessionStorage = window.sessionStorage;

const getToken = () => {
    return storage.getItem("token");
    // return sessionStorage.getItem("token");
};

const setToken = (token: string) => {
    storage.setItem("token", token);
    // sessionStorage.setItem("token", token);
};

const removeToken = () => {
    storage.removeItem("token");
    // sessionStorage.removeItem("token");
};

export { getToken, setToken, removeToken };
