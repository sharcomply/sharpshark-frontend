const storage = window.localStorage;

const getLanguage = () => {

    if (!storage.getItem("localization-language")) {
        storage.setItem("localization-language", "English");
    }

    return storage.getItem("localization-language");
}

const setLanguage = (language: string) => {
    storage.setItem("localization-language", language || "English");
}

// setLanguage("en")

export {getLanguage, setLanguage};