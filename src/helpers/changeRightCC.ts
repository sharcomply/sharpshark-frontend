const changeRightCC = (value: string) => {
  let html = "";
  switch (value) {
    case "Time Machine © 2022 by H. G. Wells is licensed under CC0 1.0: https://creativecommons.org/publicdomain/zero/1.0/":
      html = ` <span>
             
                  CC 1.0
                  <img
                      style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
                      src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"
                  >
                      <img
                          style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
                          src="https://mirrors.creativecommons.org/presskit/icons/zero.svg?ref=chooser-v1"
                      ></img>
                  </img>
            
          </span>
          <br/>
          <br/>
          <span>Creative Commons 1.0 Universal</span>
          <br/>
          <br/>
          <span>It’s CC0 public domain, anyone can distribute, remix, adapt, and build upon the material in any medium or format, even for commercial purposes. </span>
          `;

      break;
    case "Time Machine © 2022 by H. G. Wells is licensed under CC BY 4.0: https://creativecommons.org/licenses/by/4.0/":
      html = `<span>
                    CC BY 4.0
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1">
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1">
              </span>
              <br/>
              <br/>
              <span>Creative Commons Attribution 4.0 International</span>
              <br/>
              <br/>
              <span>Reusers shall give credit to the creator and can distribute, remix, adapt, and build upon the material in any medium or format, even for commercial purposes.  </span>
             

              `;

      break;

    case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/":
      html = `
              <span>        
                    CC BY-SA 4.0
                    <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1">
                    <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1">
                    <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1">
               
              </span>

              <br/>
              <br/>
              <span>Creative Commons Attribution-ShareAlike 4.0 International</span>
              <br/>
              <br/>
              <span>Reusers shall give credit to the creator and can distribute, remix, adapt, and build upon the material in any medium or format, even for commercial purposes. They must license the modified material under identical terms.</span>
              `;

      break;

    case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-ND 4.0: https://creativecommons.org/licenses/by-nd/4.0/":
      html = `
              <span>
                  CC BY-ND 4.0
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1">
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1">
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nd.svg?ref=chooser-v1">
              </span>
              <br/>
              <br/>
              <span>Creative Commons Attribution-NoDerivatives 4.0 International</span>
              <br/>
              <br/>
              <span>Reusers shall give credit to the creator and can copy and distribute the material in any medium or format in unadapted form only, even for commercial purposes.</span>
                  `;

      break;

    case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC 4.0: https://creativecommons.org/licenses/by-nc/4.0/":
      html = `
              <span>        
                  CC BY-NC 4.0
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1">
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1">
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1">
                </span> 
                <br/>
                <br/>
                <span>Creative Commons Attribution-NonCommercial 4.0 International</span>
                <br/>
                <br/>
                <span>Reusers shall give credit to the creator and can distribute, remix, adapt, and build upon the material in any medium or format, for noncommercial purposes only.</span>
                  `;

      break;

    case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/":
      html = `
              <span>
                  CC BY-NC-SA 4.0
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1">
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1">
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1">
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1">
              </span>
              <br/>
              <br/>
              <span>Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</span>
              <br/>
              <br/>
              <span>Reusers shall give credit to the creator and can distribute, remix, adapt, and build upon the material in any medium or format, for noncommercial purposes only. They must license the modified material under identical terms.</span>

                  `;

      break;
    case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-ND 4.0: https://creativecommons.org/licenses/by-nc-nd/4.0/":
      html = `<span>
                  CC BY-NC-ND 4.0
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1">
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1">
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1">
                  <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nd.svg?ref=chooser-v1">
              <span>
              <br/>
              <br/>
              <span>Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International</span>
              <br/>
              <br/>
              <span>Reusers shall give credit to the creator and can copy and distribute the material in any medium or format in unadapted form and for noncommercial purposes only.</span>

                  `;

      break;
    default:
      break;
  }
  return html;
};

export default changeRightCC

export const changeRightCCName = (value: string) => {
  let html = "";
  switch (value) {
    case "Time Machine © 2022 by H. G. Wells is licensed under CC0 1.0: https://creativecommons.org/publicdomain/zero/1.0/":
      html = ` 
        <span>Time Machine © 2022 by H. G. Wells <br/> 
        is licensed under 
        <a
          href="http://creativecommons.org/publicdomain/zero/1.0?ref=chooser-v1"
          target="_blank"
          rel="license noopener noreferrer"
          style="display:inline-block; text-decoration:underline"
        >
          CC 1.0
        </a>
      </span>
      `;

      break;
    case "Time Machine © 2022 by H. G. Wells is licensed under CC BY 4.0: https://creativecommons.org/licenses/by/4.0/":
      html = `
        <span>Time Machine © 2022 by H. G. Wells <br/> is licensed under 
          <a href="http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;text-decoration:underline">
            CC BY 4.0
          </a>  
        </span> `;

      break;

    case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/":
      html = `
      <span>Time Machine © 2022 by H. G. Wells <br/> is licensed under 
        <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;text-decoration:underline">
          CC BY-SA 4.0
        </a>
      </span>`;

      break;

    case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-ND 4.0: https://creativecommons.org/licenses/by-nd/4.0/":
      html = `
      <span>Time Machine © 2022 by H. G. Wells <br/> is licensed under
        <a href="http://creativecommons.org/licenses/by-nd/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;text-decoration:underline">
          CC BY-ND 4.0
        </a>
      </span>`;

      break;

    case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC 4.0: https://creativecommons.org/licenses/by-nc/4.0/":
      html = ` 
      <span>Time Machine © 2022 by H. G. Wells <br/> is licensed under 
        <a href="http://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;text-decoration:underline">
          CC BY-NC 4.0
        </a>
      </span>`;

      break;

    case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/":
      html = `
      <span>Time Machine © 2022 by H. G. Wells <br/> is licensed under  
        <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block; text-decoration:underline">
          CC BY-NC-SA 4.0
        </a>
      </span>`;

      break;
    case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-ND 4.0: https://creativecommons.org/licenses/by-nc-nd/4.0/":
      html = `
        <span>Time Machine © 2022 by H. G. Wells <br/> is licensed under 
          <a href="http://creativecommons.org/licenses/by-nc-nd/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;text-decoration:underline">
            CC BY-NC-ND 4.0
          </a>
        </span>`;

      break;
    default:
      break;
  }
  return html;
};