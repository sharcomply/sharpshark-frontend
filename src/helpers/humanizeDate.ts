const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

export const humanizeDate = (string?: string, divider = " ") => {
    if (!string)
        return {
            fullDate: "",
            shortDate: "",
        };

    const date = new Date(string);

    const UTC = () => {
        if (date.getTimezoneOffset() <= 0) {
            return `+${Math.abs(date.getTimezoneOffset() / 60)}`;
        }
        if (date.getTimezoneOffset() > 0) {
            return `-${Math.abs(date.getTimezoneOffset() / 60)}`;
        }
    };

    const day = ("0" + date.getDate()).slice(-2);
    const month = months[date.getMonth()];
    const year = date.getFullYear();
    const hours = ("0" + date.getHours()).slice(-2);
    const minutes = ("0" + date.getMinutes()).slice(-2);

    return {
        fullDate: `${day} ${month} ${year} ${divider} ${hours}:${minutes} UTC${UTC()}`,
        shortDate: `${day} ${month} ${divider} ${hours}:${minutes}`,
        mini: `${day} ${month}`,
    };
};
