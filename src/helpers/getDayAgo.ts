import dayjs from "dayjs"
import relativeTime from "dayjs/plugin/relativeTime"

export const getDayAgo = (date?: string) => {
    if (!date) return ""

    dayjs.extend(relativeTime)

    return dayjs(date).fromNow()
}
