export const fakeRequest = () => {
  const ms = 200 + Math.floor(Math.random() * 3000);

  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
};
