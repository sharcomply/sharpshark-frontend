import {TFunction} from "react-i18next";

type Demands = "R" | "P" | "PR" | "PA";

const demandsByShortName: Record<string, Demands> = {
  Remove: "R",
  "Pay and/or credit": "P",
  "Pay and remove": "PR",
  "Pay and attribute": "PA",
};

interface DisputDetails {
  publicationQuestionScreenshots?: (string | File)[];
  publicationQuestionLinks: string[];
  publicationQuestionWebarchiveLinks?: string[];
  publicationLinks: string[];
  publicationWebarchiveLinks: string[];
  draftsScreenshots?: (string | File)[];
  token_id: string;
  similarContent: Alert["scanId"]["similarContent"];
  discoveredDate: string;
}

interface ClaimTextGeneratorConstructor {
  t: TFunction<"translation", undefined>;
  demand: keyof typeof demandsByShortName;
  details: DisputDetails;
}

const getImage = (image: File | string) => {
  if (typeof image === "string") return image;
  if (image instanceof File) return URL.createObjectURL(image);
  return image;
};

export default class ClaimTextGenerator {
  private t: TFunction<"translation", undefined>;
  private demand: Demands;
  disputDetails: DisputDetails;

  constructor({t, demand, details}: ClaimTextGeneratorConstructor) {
    this.t = t;
    this.demand = demandsByShortName[demand];
    this.disputDetails = details;
  }

  public setDisputeDetails = (details: DisputDetails) => {
    this.disputDetails = details

    console.log(`DETAILS`, this.disputDetails)
  }

  private async getQuestionImages() {
    if (this.disputDetails.publicationQuestionScreenshots && this.disputDetails.publicationQuestionScreenshots.length > 0) {
      let screenShots = this.disputDetails.publicationQuestionScreenshots
      const images: any = []
      const getImg = (image: string | File) => {
        return new Promise((resolve) => {
          const img = document.createElement('img')
          img.src = getImage(image)
          img.onload = () => {
            const width = 600
            const ratio = img.naturalWidth / img.naturalHeight
            const height = width / ratio
            img.width = width
            img.height = height
            const div = document.createElement("div")
            div.appendChild(img)
            const elem = `${div.innerHTML} <br/> <br/>`
            return resolve(elem)
          }
        })
      }
      for(let i = 0; i < screenShots.length; i++) {
        const source = screenShots[i]
        const res = await getImg(source)
        images.push(res)
      }
      console.log(images)
      const render = images.join('')

      console.log(`render`, render)
      return `
                ${this.t("dispute.claim_text.evidence.screenshots_of_the_question_publication")} <br /> <br/>
                ${render}
                `;
    }
    return "";
  }

  private getPublicationOnQuestionLink() {
    if (this.disputDetails?.publicationQuestionLinks?.length > 0 && !!this.disputDetails.discoveredDate) {
      return ` ${this.t("dispute.claim_text.evidence.publication_in_question")}: ${this.disputDetails.publicationQuestionLinks.map(
        (item: string) => `<a style="text-decoration: underline; color: blue" href="${item}"> ${item}</a>`
      )}
            <br /> 
        ${this.t("dispute.claim_text.evidence.time_of_case_title")} ${this.disputDetails.discoveredDate} 
            <br />`;
    }

    return "";
  }

  private getPublicationQuestionWebArchive() {
    if (this.disputDetails?.publicationQuestionWebarchiveLinks && this.disputDetails?.publicationQuestionWebarchiveLinks?.length > 0) {
      return `
            ${this.t("dispute.claim_text.evidence.publication_question_archive_title")}: ${this.disputDetails.publicationQuestionWebarchiveLinks.map(
        (link) => `<a style="text-decoration: underline; color: blue" href="${link}"> ${link}</a>`
      )}
            <br />
            `;
    }

    return "";
  }

  private getCertificate() {
    return `
        ${this.t("dispute.claim_text.evidence.certificate_link_title")}: <br/> <a style="text-decoration: underline; color: blue" href="${
      window.location.origin + `/certificate/${this.disputDetails.token_id}`
    }">${window.location.origin + `/certificate/${this.disputDetails.token_id}`}</a>
        </br>
        `;
  }

  private getLinkToOriginal() {
    if (!this.disputDetails.publicationLinks || this.disputDetails.publicationLinks.length < 1) return "";
    return `
        ${this.t("dispute.claim_text.evidence.link_original_publication")} ${this.disputDetails.publicationLinks.map(
      (link) => `<a style="text-decoration: underline; color: blue" href="${link}"> ${link}</a>`
    )}
        <br/>
        `;
  }

  private getLinkToOriginalWebarchive() {
    if (!this.disputDetails.publicationWebarchiveLinks || this.disputDetails.publicationWebarchiveLinks.length < 1) return "";
    return `
        ${this.t("dispute.claim_text.evidence.link_original_on_webarchive")} ${this.disputDetails.publicationWebarchiveLinks.map(
      (link) => `<a style="text-decoration: underline; color: blue" href="${link}"> ${link}</a>`
    )}
        <br/>
        `;
  }

  private async getDraftsImages() {
    console.log(`DRAFT SCREENs`, this.disputDetails.draftsScreenshots)
    if (this.disputDetails.draftsScreenshots && this.disputDetails.draftsScreenshots.length > 0) {
      let screenShots = this.disputDetails.draftsScreenshots
      const images: any = []
      const getImg = (image: string | File) => {
        return new Promise((resolve) => {
          const img = document.createElement('img')
          img.src = getImage(image)
          img.onload = () => {
            const width = 600
            const ratio = img.naturalWidth / img.naturalHeight
            const height = width / ratio
            img.width = width
            img.height = height
            const div = document.createElement("div")
            div.appendChild(img)
            const elem = `${div.innerHTML} <br/> <br/>`
            return resolve(elem)
          }
        })
      }
      for(let i = 0; i < screenShots.length; i++) {
        const source = screenShots[i]
        const res = await getImg(source)
        images.push(res)
      }
      console.log(images)
      const render = images.join('')

      console.log(`render`, render)
      return `
                ${this.t("dispute.claim_text.evidence.drafts_screenshots")} <br /> <br/>
                ${render}
                `;
    }
    return "";
  }

  private async getOptionalParams() {
    const draftScreenShots = await this.getDraftsImages()
    const questionScreenshots = await this.getQuestionImages()
    console.log(`draftscreens`, draftScreenShots)
    return `
        ${this.t("dispute.claim_text.evidence.evidence_title")}:</br>
        ${this.getPublicationOnQuestionLink()}
        ${questionScreenshots}
        ${this.getPublicationQuestionWebArchive()}
        ${this.getCertificate()}
        ${this.getLinkToOriginal()}
        ${this.getLinkToOriginalWebarchive()}
        ${draftScreenShots}
        `;
  }

  private async getBaseText() {
    const optionalParams = await this.getOptionalParams()
    console.log(this.disputDetails.similarContent)
    const similarContent = `${this.t("dispute.claim_text.there_is_material")} ${this.disputDetails?.similarContent?.map((item: any) => `<a style="text-decoration: underline; color: blue" href="${item.url}">${item.url}</a>`)}` || ''
    return `
        <div>${this.t("dispute.claim_text.hello")}</div> </br> </br>
         ${similarContent}
        ${this.t("dispute.claim_text.website_owner.breaches_rules_law")}:
        <a style="text-decoration: underline; color: blue" href="https://wipolex.wipo.int/en/text/283702"> ${this.t("dispute.claim_text.article_6")} </a>
        ${this.t("dispute.claim_text.berne_convention")}
        <a style="text-decoration: underline; color: blue" href='https://docs.google.com/spreadsheets/d/1HUtEb4LlqrPasemLBnCtyXRjm87aFLokJYGvYCXrNSk/edit?usp=sharing'>${this.t(
      "dispute.claim_text.rule_of_the_current_legislation"
    )}</a>
        ${this.t("dispute.claim_text.your_country")}</br></br>
        ${optionalParams}
        `;
  }

  private getNote() {
    return `${this.t("dispute.claim_text.note")}</br></br>`;
  }

  private getOwnerTextByDemand() {
    switch (this.demand) {
      case "P":
        return this.getOwnerPText();
      case "R":
        return this.getOwnerRText();
      case "PR":
        return this.getOwnerPRText();
      case "PA":
        return this.getOwnerPAText();
      default:
        return "";
    }
  }

  private getProviderTextByDemand() {
    switch (this.demand) {
      case "R":
        return this.getProviderRText();
      default:
        return "";
    }
  }

  private getGoogleTextByDemand() {
    switch (this.demand) {
      case "R":
        return this.getGoogleRText();
      default:
        return "";
    }
  }

  private getRegards() {
    return `${this.t("dispute.claim_text.kind_regards")}`;
  }

  private getOwnerPText() {
    return `
        <br/>
        ${this.t("dispute.claim_text.website_owner.only_pay", {
      link: this.disputDetails.similarContent.map((item) => `<a style="text-decoration: underline; color: blue" href="${item.url}"> ${item.url}</a>`),
    })}</br></br>
        `;
  }

  private getOwnerRText() {
    return `
        <br/>
        ${this.t("dispute.claim_text.website_owner.only_remove")}</br></br>
        `;
  }

  private getOwnerPRText() {
    return `
        </br>
        ${this.t("dispute.claim_text.website_owner.pay_and_remove", {
      link: this.disputDetails.similarContent.map((item) => `<a style="text-decoration: underline; color: blue" href="${item.url}"> ${item.url}</a>`),
    })}</br></br>
        `;
  }

  private getOwnerPAText() {
    return `
        </br>
        ${this.t("dispute.claim_text.website_owner.pay_and_attribute", {
      publicationLink: this.disputDetails.similarContent.map((item) => `${item.url}`),
      certificateLink: `<a style="text-decoration: underline; color: blue" href="${
        window.location.origin + `/certificate/${this.disputDetails.token_id}`
      }">${window.location.origin + `/certificate/${this.disputDetails.token_id}`}</a>`,
    })}</br></br>
        `;
  }

  private getProviderRText() {
    return `
        </br>
        ${this.t("dispute.claim_text.provider.only_remove")}</br></br>
        `;
  }

  private getGoogleRText() {
    return `
        </br>
        ${this.t("dispute.claim_text.search_engine.only_remove")}</br></br>
        `;
  }

  public async getOwnerClaimText() {
    const baseText = await this.getBaseText()
    console.log(`BASE TEXT`, baseText)
    return `
        ${baseText}
        ${this.getOwnerTextByDemand()}
        ${this.getNote()}
        ${this.getRegards()}
        `;
  }

  public async getProviderClaimText() {
    const baseText = await this.getBaseText()
    return `
        ${baseText}
        ${this.getProviderTextByDemand()}
        ${this.getNote()}
        ${this.getRegards()}
        `;
  }

  public async  getGoogleClaimText() {
    const baseText = await this.getBaseText()
    return `
        ${baseText}
        ${this.getGoogleTextByDemand()}
        ${this.getNote()}
        ${this.getRegards()}
        `;
  }
}

