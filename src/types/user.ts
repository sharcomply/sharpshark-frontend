export interface User {
    _id: string;
    firstName: string;
    lastName: string;
    address: string;
    isVerification: boolean;
    networkType: string;
    nonce: string;
    email: string;
    isEmailVerified: boolean;
    disputesPrefillEmail?: string;
    country?: string
}
