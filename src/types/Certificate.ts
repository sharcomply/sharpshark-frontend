export interface CertificateTypes {
    title?: string;
    author?: Array<string>;
    cachedSource?: string;
    usageRights?: string;
    updatedAt?: string;
    createdAt?: string;
    historyQR?: string;
    historyLink?: string;
    ipfsQR?: string;
    contentIpfsLink?: string;
    text?: string | undefined;
    type?: string | undefined;
    price?: string;
    isSell?: boolean;
    metadataHash: string;
    contentHash: string;
    licenseHash: string;
    tokenId: string;
    versions: Array<CertificateVersion>;
    otherVersion: Draft[];
    currentVersion: CertificateVersion;
    minter: string;
    historyTransactionHash: string;
}

export type CertificateVersion = {
    title: string;
    version: string;
    tokenId: string;
    date?: string;
};
