import styled from "@emotion/styled";

// Props
type InputProps = {
    radius?: string;
};

// Styling
export const Input = styled.input<InputProps>`
    display: flex;
    font-size: 14px;
    font-weight: 400;
    background: #fcfdfe;
    border: 1px solid #f0f1f7;
    color: #9fa2b4;
    line-height: 1.5;
    min-height: 42px;
    outline: none;
    padding: 10px 16px;
    width: 100%;
    transition: border-color 0.2s ease-in-out, background-color 0.2s ease-in-out, color 0.2s ease-in-out;

    &[type="password"] {
        padding-right: 48px;
    }

    ${({ radius }) => `
        border-radius: ${radius || `8px`};
    `}

    &:hover,
    &:focus {
        color: #252733;
        border-color: #3751ff;
    }

    &:disabled {
        background-color: #e9ecef;
    }

    &::placeholder {
        color: rgb(75, 80, 109, 0.5);
    }
`;

export const RadioInput = styled.input`
    -webkit-appearance: radio;
    accent-color: #4700e5;
    width: 20px;
    height: 20px;

    &: focus {
        outline: none;
    }
`;
