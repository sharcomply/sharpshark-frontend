import styled from "@emotion/styled";
import { Select, Tooltip } from "@mantine/core";
import { ReactNode } from "react";
import { Control, Controller } from "react-hook-form";
import { Icon } from "@iconify/react";

// Props
interface SelectFieldProps {
    name: string;
    label?: string;
    data: any;
    placeholder?: string;
    description?: string | ReactNode;
    errorText?: string;
    control: Control;
    children?: ReactNode;
    helpText?: string;
}

// Component
export default function SelectField({ label, description, errorText, control, name, placeholder, data, helpText }: SelectFieldProps) {
    return (
        <>
            <Field>
                {label &&
                    <Label>
                        {label}
                        {helpText &&
                            <Tooltip
                                label={`${helpText}`}
                                wrapLines
                                width={250}
                                position={'top'}
                                placement={'start'}
                                color={"gray"}
                                withArrow
                            >
                                <Icon icon={"ic:round-help"} color="#dadada" width="16" height="16" />
                            </Tooltip>
                        }
                    </Label>
                }
                {description && <LabelText>{description}</LabelText>}
                <Controller render={({ field }) => <Select {...field} placeholder={placeholder} data={data} />} name={name} control={control} defaultValue="" />
            </Field>
            {errorText && <ErrorText>{errorText}</ErrorText>}
        </>
    );
}

// Styling
const Field = styled.div`
    display: flex;
    flex-direction: column;
    margin-bottom: 1.5rem;
    position: relative;
`;

const Label = styled.label`
    display: flex;
    align-items: center;
    margin-bottom: 6px;
    font-size: 12px;
    text-transform: uppercase;
    line-height: 15px;
    transition: color 0.2s ease-in-out;
    font-weight: 700;
    color: #0a001e;
`;

const LabelText = styled.label`
    display: flex;
    gap: 4px;
    margin-bottom: 6px;
    font-size: 14px;
    text-transform: none;
    line-height: 15px;
    transition: color 0.2s ease-in-out;
    font-weight: 400;
    color: #666892;

    & .link {
        cursor: pointer;
        text-decoration: underline;
    }
`;

const ErrorText = styled.span`
    font-weight: 500;
    font-size: 16px;
    line-height: 1;
    letter-spacing: 0.3px;
    color: #252733;
    justify-content: space-between;
    align-items: flex-start;
    background: #fff2f3;
    display: flex;
    background-color: #fff2f8;
    overflow: hidden;
    position: relative;
    padding: 0.75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 0.25rem;
    flex-direction: row;
    flex-wrap: wrap;
`;
