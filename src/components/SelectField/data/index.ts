interface Props {
  value: string;
  label: string;
}

export const country: Props[] = [
  {
    value: "US",
    label: "United States"
  },
  {
    value: "ES",
    label: "Country selector with all the countries, without sanctioned countries"
  }

]

export const language: Props[] = [
  {
    value: "EN",
    label: "English"
  },
  {
    value: "ES",
    label: "Spanish"
  }

]


