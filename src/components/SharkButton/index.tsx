import { css } from "@emotion/react";
import styled from "@emotion/styled";
import { Group, GroupProps, Loader } from "@mantine/core";
import { PropsWithChildren, useCallback } from "react";

// Props
type ButtonProps<T extends React.ElementType> = GroupProps & {
    variant: "filled" | "outlined";
    buttonType: "primary" | "secondary";
    width?: number | string;
    as?: T;
    disabled?: boolean;
    loading?: boolean;
    onClick?: () => void;
    size?: "small" | "medium";
};

// Component
export function SharkButton<T extends React.ElementType = "button">(
    props: PropsWithChildren<ButtonProps<T> & Omit<React.ComponentPropsWithoutRef<T>, keyof ButtonProps<T>>>
) {
    const { children, variant, buttonType, as = "button", width, onClick, disabled, loading, size = "medium", ...rest } = props;

    const Root = ButtonRoot.withComponent(as as any);

    const loaderColor = variant === "filled" ? "#ffffff" : "#4700e5";

    const onClickHandler = useCallback(() => {
        if (disabled || loading) return;

        onClick && onClick();
    }, [disabled, loading, onClick]);

    return (
        <Root
            onClick={onClickHandler}
            align={"center"}
            style={{ width, display: "flex", justifyContent: "center", cursor: !disabled ? "pointer" : "auto" }}
            {...rest}
            variant={variant}
            buttonType={buttonType}
            disabled={disabled}
            size={size}
            className="shark-button"
        >
            <Group spacing={16} sx={{ position: "relative", justifyContent: "center", height: "100%" }}>
                {loading && (
                    <Group sx={{ position: "absolute", inset: 0, zIndex: 10, justifyContent: "center" }}>
                        <Loader color={loaderColor} size="sm" />
                    </Group>
                )}

                <Group sx={{ opacity: loading ? 0 : 1, height: "100%" }}>{children}</Group>
            </Group>
        </Root>
    );
}

// Styling
const FilledButton = css`
    background-color: #4700e5;
    box-shadow: 0px 4px 12px rgba(55, 81, 255, 0.24);
    color: #ffffff;

    &:hover {
        background-color: #1c00a6;
        box-shadow: 0px 6px 24px rgba(37, 61, 217, 0.32);
        color: #ffffff;
    }

    &:active {
        background-color: #4700e5;
        box-shadow: 0px 2px 6px rgba(37, 61, 217, 0.32);
        color: #ffffff;
    }

    &:disabled {
        background-color: #b8bde0;
        box-shadow: none;
        color: #ffffff;
    }
`;

const PrimaryOutlinedButton = css`
    border: 1px solid #4700e5;
    background-color: transparent;
    color: #4700e5;

    &:hover {
        border: 1px solid #1c00a6;
        background-color: transparent;
        color: #1c00a6;
    }

    &:active {
        background-color: #4700e5;
        color: #ffffff;
    }

    &:disabled {
        border: 1px solid #b8bde0;
        background-color: transparent;
        color: #b8bde0;
    }
`;

const SecondaryOutlinedButton = css`
    background-color: #fcfdfe;
    border: 1px solid #dddeee;
    color: #666892;

    &:hover {
        background-color: #fcfdfe;
        border: 1px solid #1c00a6;
        color: #1c00a6;
    }

    &:active {
        background-color: #fcfdfe;
        border: 1px solid #dddeee;
        color: #4700e5;
    }

    &:disabled {
        background-color: #fcfdfe;
        border: 1px solid #dddeee;
        color: #b8bde0;
    }
`;

const PrimaryMedium = css`
    height: 42px;
    padding-left: 24px;
    padding-right: 24px;
`;

const PrimarySmall = css`
    height: 36px;
    padding-left: 12px;
    padding-right: 12px;
`;

const SecondaryMedium = css`
    height: 42px;
    padding-left: 16px;
    padding-right: 16px;
`;

const SecondarySmall = css`
    height: 30px;
    padding-left: 8px;
    padding-right: 8px;
`;

const ButtonRoot = styled(Group)<Pick<ButtonProps<"button">, "variant" | "size" | "buttonType">>`
    border-radius: 4px;
    font-size: 14px;
    line-height: 20px;
    font-weight: 500;
    align-items: center;

    & {
        transition-timing-function: ease-in-out;
        transition-duration: 150ms;
        transition-property: background-color, color, border, box-shadow;
    }

    ${({ variant }) => !!(variant === "filled") && FilledButton};
    ${({ variant, buttonType }) => !!(variant === "outlined" && buttonType === "primary") && PrimaryOutlinedButton};
    ${({ variant, buttonType }) => !!(variant === "outlined" && buttonType === "secondary") && SecondaryOutlinedButton};

    ${({ buttonType, size }) => !!(buttonType === "primary" && size === "medium") && PrimaryMedium};
    ${({ buttonType, size }) => !!(buttonType === "primary" && size === "small") && PrimarySmall};
    ${({ buttonType, size }) => !!(buttonType === "secondary" && size === "medium") && SecondaryMedium};
    ${({ buttonType, size }) => !!(buttonType === "secondary" && size === "small") && SecondarySmall};

    ${({ px }) =>
        !!px &&
        `
        padding-left: ${px}px;
        padding-right: ${px}px;
    `}

    ${({ py }) =>
        !!py &&
        `
        padding-top: ${py}px;
        padding-bottom: ${py}px;
    `}
`;
