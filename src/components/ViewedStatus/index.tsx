import styled from "@emotion/styled";
import { Group } from "@mantine/core";

interface ViewedStatusProps {
    isRead?: boolean;
    length?: number;
    onlyCircle?: boolean;
}

const ViewedStatus = ({ isRead, length, onlyCircle }: ViewedStatusProps) => {
    return (
        <Root>
            {onlyCircle && (
                <>
                    {!isRead && (
                        <Group pl={16} align="center" sx={{ height: 54 }}>
                            <Circle />
                        </Group>
                    )}
                </>
            )}
            {!onlyCircle && (
                <>
                    {!isRead && (
                        <Group pl={16} align="center" sx={{ height: 54 }}>
                            <Circle />
                        </Group>
                    )}

                    {!isRead && (
                        <Group px={16} align="center" sx={{ height: 54 }}>
                            <Viewed bgColor={!isRead && "#E50C54"}>{length ? length : 1}</Viewed>
                        </Group>
                    )}

                    {isRead && length! >= 1 && (
                        <Group px={16} align="center" sx={{ height: 54 }}>
                            <Viewed>{length ? length : 1}</Viewed>
                        </Group>
                    )}
                </>
            )}
        </Root>
    );
};

export default ViewedStatus;

const Root = styled.div`
    display: flex;
    align-items: center;
`;

const Circle = styled.div`
    display: flex;
    width: 6px;
    height: 6px;
    border-radius: 50%;
    background-color: #4700e5;
    margin-right: 5px;
`;

const Viewed = styled.div<any>`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 33px;
    height: 20px;
    background-color: ${(props) => (props.bgColor ? props.bgColor : "#979CBC")};
    border-radius: 100px;
    color: #fff;
    font-size: 12px;
    font-weight: 400px;
`;
