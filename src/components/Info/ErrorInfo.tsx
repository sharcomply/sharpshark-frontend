import styled from "@emotion/styled";
import { Group, Stack } from "@mantine/core";
import Icon from "components/Icon";
import React, { ReactNode } from "react";

interface ErrorInfoProps {
    title: string;
    description?: ReactNode;
}

export default function ErrorInfo({ title, description }: ErrorInfoProps) {
    return (
        <React.Fragment>
            <Root>
                <Group position="apart" align={'flex-start'}>
                    <Group position="left" align={'flex-start'}>
                        <IconContainer>
                            <Icon icon={"material-symbols:error-outline"} size={17} color={"#E50C54"} />
                        </IconContainer>
                        <Stack spacing={0}>
                            <Title>{title}</Title>
                            <Description>{description}</Description>
                        </Stack>
                    </Group>
                </Group>
            </Root>
        </React.Fragment>
    );
}

const Root = styled.div`
    padding: 16px 0px 16px 16px;
    background: #fff2f8;
    border: 1px solid #e60c54;
    border-radius: 4px;
    width: 100%;
`;

const Title = styled.div`
    font-weight: 500;
    font-size: 16px;
    line-height: 20px;
`;

const Description = styled.div`
    margin-top: 4px;
    font-weight: 400;
    font-size: 14px;
    line-height: 15px;
    color: #666892;

    & a {
        cursor: pointer;
        color: #4700e5;
        text-decoration: underline;
    }
`;

const IconContainer = styled.div`
    padding: 8px 4px 8px 14px;
`;
