import styled from "@emotion/styled";

type ButtonProps = {
    color?: string;
    hoverColor?: string;
    width?: string;
};

export const Button = styled.button<ButtonProps>`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    color: #fff;
    border: 1px solid transparent;

    padding: 12px 16px;
    margin-top: 8px;
    margin-bottom: 8px;
    border-radius: 4px;

    font-weight: 500;
    font-size: 14px;
    line-height: 20px;

    user-select: none;
    transition: background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

    ${({ color }) =>
        color &&
        `
        background-color: ${color};
        border-color: ${color};
    `}

    ${({ width }) =>
        width &&
        `
        width: ${width};
    `}


    &:hover {
        ${({ hoverColor }) =>
            hoverColor &&
            `
            background-color: ${hoverColor};
            border-color: ${hoverColor};
        `}
    }

    &:disabled {
        opacity: 0.6;
    }
`;

export const OutlineButton = styled(Button)`
    color: #666892;
    background: #fcfdfe;
    border: 1px solid #dddeee;

    &:hover {
        color: ${(props) => props.hoverColor};
        background: #fcfdfe;
        border: 1px solid #4700e5;
    }
`;
