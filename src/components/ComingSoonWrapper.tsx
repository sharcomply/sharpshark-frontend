import styled from "@emotion/styled";
import { ReactNode } from "react";
import { useTranslation } from "react-i18next";

interface ComingSoonWrapperProps {
    children?: ReactNode;
    orientation: "vertical" | "horizontal";
}
const ComingSoonWrapper = ({ children, orientation }: ComingSoonWrapperProps) => {
    const { t } = useTranslation();

    return (
        <Wrapper orientation={orientation}>
            {children}
            <Span orientation={orientation}>{t("common.coming_soon")}</Span>
        </Wrapper>
    );
};

const Wrapper = styled.div<any>`
    display: flex;
    flex-direction: ${(props) => (props.orientation === "vertical" ? "column" : "row")};
    align-items: center;
    cursor: pointer;
`;

const Span = styled.span<any>`
    font-size: 12px;
    font-weight: 500;
    color: #cccccc;
    margin-top: ${(props) => (props.orientation === "vertical" ? "5px" : "0px")};
    margin-left: ${(props) => (props.orientation === "vertical" ? "0px" : "10px")};
`;

export default ComingSoonWrapper;
