import styled from "@emotion/styled";
import { Group, Stack, Text } from "@mantine/core";
import Icon from "components/Icon";
import { ReactNode, useMemo } from "react";

// Props
type ComponentProps = {
    variant?: 'error' | 'warning' | string,
    title?: string;
    text?: string | ReactNode;
}

// Component
export const AlertBox = (props: ComponentProps) => {
    const { text = '', title, variant } = props;
    
    const variables = useMemo(() => {
        let backgroundColor = 'transparent'
        let borderColor = 'transparent'
        let titleColor = '#0A001E'
        let iconColor = '#0A001E'
        
        if (variant === 'error') {
            backgroundColor = '#FFF2F8'
            borderColor = '#E60C54'
            titleColor = '#E60C54'
            iconColor = '#E60C54'
        }
        
        if (variant === 'warning') {
            backgroundColor = '#FDFAE4'
            borderColor = '#FFC933'
            titleColor = '#FFC933'
            iconColor = '#FFC933'
        }
        
        return { backgroundColor, borderColor, titleColor, iconColor }
    } ,[variant])
    
    return (
        <Root px={16} p={16} spacing={4} sx={{ backgroundColor: variables.backgroundColor, borderColor: variables.borderColor }}>
            <Group spacing={16} px={14}>
                <Icon icon="ic:outline-info" color={variables.iconColor} />
                {title
                    ? <Text size={'md'} color={variables.titleColor} weight={"500"}>{title}</Text>
                    : <StyledText size={'sm'} color={'#0A001E'}>{text}</StyledText>
                }
            </Group>
            {title && (
                <Group px={48} position={'apart'}>
                    <StyledText size={'sm'} color={'#0A001E'}>{text}</StyledText>
                </Group>
            )}
        </Root>
    )
}

// Styling
const Root = styled(Stack)`
    border-width: 1px;
    border-style: solid;
    border-radius: 4px;
`

const StyledText = styled(Text as any)`
    & a {
        color: #4700E5;
        text-decoration: underline;
    }
`
