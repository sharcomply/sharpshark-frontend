import styled from "@emotion/styled";

export const TabButton = styled.button<{ isActive: boolean; marginRight?: string }>`
    display: inline-block;
    font-family: SuisseIntl,sans-serif;
    padding: 4px 12px;
    min-height: unset;
    font-size: 12px;
    font-weight: 500;
    line-height: 16px;
    text-transform: uppercase;
    border: 1px solid #DDDEEE;
    border-radius: 4px;
    transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    margin-right: ${(props) => props.marginRight};

    ${({ isActive }) => `
        background-color: ${isActive ? "#666892" : "#fff"};
        color: ${isActive ? "#fff" : "#666892"};
        cursor: ${isActive ? "default" : "pointer"};
    `}

    &:hover {
        ${({ isActive }) => `
        color: ${!isActive && "#fff"};
        background-color: ${!isActive && "#253dd9"};
        border-color:${!isActive && "#666892"};
    `}
    }

    &:disabled {
        opacity: 0.6;
        &:hover {
            ${({ isActive }) => `
            background-color: ${isActive ? "#666892" : "#fff"};
            color: ${isActive ? "#fff" : "#666892"};
            opacity: ${!isActive ? "1" : "0.65"};
            cursor: ${isActive ? "default" : "pointer"};
        `}
        }
    }
`;

export const TabPane = styled.div<{ isActive: boolean }>`
    ${({ isActive }) => `
        display: ${!isActive ? "none" : "auto"}
    `}
`;
