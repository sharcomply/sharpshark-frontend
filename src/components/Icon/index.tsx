import styled from "@emotion/styled";
import { Icon as Iconify, IconProps as IconifyProps } from "@iconify/react";
import React, { PropsWithChildren, ReactNode } from "react";

// Props
export interface IconProps extends PropsWithChildren<Omit<IconifyProps, 'icon'>> {
    icon: string;
    color?: string;
    hoverColor?: string;
    size?: number;
    rotate?: number;
    container?: ReactNode;
}

// Component
export default function Icon(props: IconProps) {
    const { icon, color = "#ffffff", hoverColor, size = 18, container, rotate, ...iconProps } = props;

    const childrenWithProps = React.Children.map(container, (child) => {
        if (!React.isValidElement(child)) return child;

        return React.cloneElement(child, { children: <Iconify {...iconProps} icon={icon}  /> });
    });

    return (
        <StyledIcon color={color} hoverColor={hoverColor || color} size={size} style={rotate ? { transform: `rotate(${rotate}deg)` } : {}}>
            {childrenWithProps || <Iconify {...iconProps} icon={icon}  />}
        </StyledIcon>
    );
}

// Styling
interface IconWrapperProps {
    color?: string;
    hoverColor?: string;
    size?: number;
}

const StyledIcon = styled.div<IconWrapperProps>`
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    font-size: ${(props) => props.size}px;
    color: ${(props) => props.color};

    & > * {
        display: flex;
    }

    &:hover {
        color: ${(props) => props.hoverColor};
        transition: color 0.2s ease-in-out;
    }
`;
