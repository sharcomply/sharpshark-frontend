import styled from "@emotion/styled";

const TempContainer = styled.div`
    pointer-events: none;
`;

export default TempContainer;
