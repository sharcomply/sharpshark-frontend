import styled from "@emotion/styled";
import Icon from "components/Icon";
import React from "react";
import { useTranslation } from "react-i18next";

type InfoButtonProps = {
    isActive?: boolean;
    onClick?: any;
    enabled?: boolean; //временно
};

export default function InfoButton({ isActive, onClick, enabled }: InfoButtonProps) {
    const { t } = useTranslation();

    return (
        <React.Fragment>
            <Root onClick={onClick}>
                <Icon icon={"ic:round-help-outline"} color={isActive ? "#4700e5" : "#8b8fad"} hoverColor={isActive ? "#4700e5" : "#8b8fad"} />
                {!isActive ? (
                    <Icon icon={"ic:round-expand-more"} color={"#8b8fad"} hoverColor={"#8b8fad"} />
                ) : (
                    <Icon icon={"ic:round-expand-less"} color={"#4700e5"} hoverColor={"#4700e5"} />
                )}

                {!enabled && <Text>({t("common.coming_soon")})</Text>}
            </Root>
        </React.Fragment>
    );
}

//Styling

const Root = styled.div`
    display: flex;
    align-items: center;
`;

const Text = styled.div`
    font-size: 14px;
    color: #8b8fad;
`;
