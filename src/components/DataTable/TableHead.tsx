import styled from '@emotion/styled';
import { Text } from '@mantine/core';

type TitleTable = {
    title: string;
    width?: string;
}

type ComponentProps = {
    items?: TitleTable[]
}

export const TableHead = ({ items = [] }: ComponentProps) => {
    
    const renderItems = () => items.map((item, index) => (
        <th key={index} style={{ width: item.width || 'auto' }}>
            <Text
                sx={{ fontSize: 12, lineHeight: '16px' }}
                weight={'500'}
                transform={'uppercase'}
                color={'#0A001E'}
            >
                {item.title}
            </Text>
        </th>
    ));
    
    return (
        <Root>
            <tr>
                {renderItems()}
            </tr>
        </Root>
    );
};


// Styling
const Root = styled.thead`
    font-weight: bold;
    color: #0a001e;

    & th {
        vertical-align: bottom;
        border: none !important;
        text-align: -webkit-match-parent;
        padding: 16px;
    }
`;