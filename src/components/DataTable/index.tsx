import styled from "@emotion/styled";

// Styling
export const TableContainer = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border-radius: 0.25rem;
    justify-content: space-between;
    padding: 0;
    transition: all 0.4s ease;
`;

export const Table = styled.table`
    table-layout: fixed;
    width: 100%;
    margin-bottom: 1rem;
    color: #0d0222;
    border-spacing: 0;
    border-collapse: collapse;
`;

export const TableHead = styled.thead`
    font-weight: bold;
    color: #0a001e;

    & th {
        padding: 0.75rem;
        vertical-align: bottom;
        border-top: 1px solid #dee2e6;
        font-size: 12px;
        border-bottom: none !important;
        text-align: -webkit-match-parent;
    }
`;

export const TableRow = styled.tr`
    border-top: 1px solid #DDDEEE;
`;

export const TableData = styled.td`
    font-family: SuisseIntl, sans-serif;
    color: #666892;
    height: 54px;

    & p {
        font-weight: 400 !important;
    }
`;

export const Title = styled.span`
    color: #000;
    font-weight: bold;
    margin-right: 20px;
`;

export const Text = styled.p`
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;

export const DateRow = styled.tr`
    background: #f7f8fc;

    & td {
        font-size: 12px;
        padding-top: 3px;
        padding-bottom: 2px;
        font-family: SuisseIntl, sans-serif;
        color: #666892;
        letter-spacing: 0.2px;
        padding-left: 0.75rem;
    }
`;

export const MenuAction = styled.div`
    cursor: pointer;
    height: 46px;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    padding: 13px 16px;
    transition: background 0.2s ease-in-out, border 0.2s ease-in-out;
    border-bottom: 0.5px solid #dfe0eb;
    color: #666892;
    font-weight: 600;

    &:hover {
        background: #4700e5;
        color: white;
    }
`;

export const MenuActionDanger = styled(MenuAction)`
    color: #dc3545;

    &:hover {
        background: #e60c54;
        color: #ffffff;
    }
`;

export const ActionsContainer = styled.div`
    position: relative;
`;

export const ActionText = styled.span<{ color: string }>`
    cursor: pointer;
    color: ${(props) => props.color};
    margin-right: 1.5rem;
`;

export const ActionsButton = styled.img`
    cursor: pointer;
`;

export const Name = styled.span`
    color: #000;
    font-weight: bold;
`;
