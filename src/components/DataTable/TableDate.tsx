import styled from '@emotion/styled';
import { Text } from '@mantine/core';

// Props
type ComponentProps = {
    title: string;
    cols?: number;
    titlePos?: number;
}

// Component
export function TableDate({ title, cols = 2, titlePos = 3 }: ComponentProps) {
    return (
        <Root>
            {Array(cols).fill('col').map((_, key) => (
                <Content key={key}>
                    { key === (titlePos - 1) && (
                        <Text sx={{ fontSize: 12, lineHeight: '16px' }} color={'#666892'} weight={'500'}>{title}</Text>
                    )}
                </Content>
            ))}
        </Root>
    );
}

// Styling
const Root = styled.tr`
    background-color: #F7F8FF;
`;

const Content = styled.td`
    height: 20px;
    padding: 0 16px;
`;
