export enum RowType {
    Today,
    ThisWeek,
    EarlierThisMonth,
    LastMonth,
    Earlier,
}

export const formatDate = (d: string): string => {
    const date = new Date(d);
    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    const day = ("0" + date.getDate()).slice(-2);
    const month = months[date.getMonth()];
    const year = date.getFullYear();
    const hours = ("0" + date.getHours()).slice(-2);
    const minutes = ("0" + date.getMinutes()).slice(-2);
    return `${day} ${month} ${year}, ${hours}:${minutes}`;
};
