import styled from "@emotion/styled";

type StatusTextProps = {
    backgroundColor: string;
    color: string;
};

const StatusText = styled.span<StatusTextProps>`
    padding: 6.5px 12px 0px 12px;
    text-align: center;
    border-radius: 100px;
    font-weight: 500;
    font-size: 12px;
    line-height: 16px;
    letter-spacing: 0.2px;
    text-transform: uppercase;
    background-color: ${(props) => props.backgroundColor};
    color: ${(props) => props.color};
`;

export default StatusText;
