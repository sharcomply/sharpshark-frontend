import styled from "@emotion/styled";
import { Tooltip } from "@mantine/core";

interface ButtonWithTooltipProps {
    tooltipText: string;
    buttonText: string;
    closeIcon?: boolean;
    onClick?: () => void;
    backgroundcolor?: string;
    textcolor?: string;
    disabled?: boolean;
}

const ButtonWithTooltip = ({ backgroundcolor, tooltipText, buttonText, closeIcon, onClick, textcolor, disabled }: ButtonWithTooltipProps) => {
    return (
        <Root onClick={onClick} disabled={disabled}>
            <ButtonTooltip
                label={tooltipText}
                position="bottom"
                placement="center"
                transition="fade"
                transitionDuration={300}
                transitionTimingFunction="ease"
                withArrow={false}
                backgroundcolor={backgroundcolor}
                textcolor={textcolor}
                disabled={disabled}
                styles={{
                    body: {
                        backgroundColor: "#666892",
                    },
                }}
            >
                {closeIcon && (
                    <CloseContainer>
                        <LineLeft className="line" />
                        <LineRight className="line" />
                    </CloseContainer>
                )}
                <span>{buttonText}</span>
            </ButtonTooltip>
        </Root>
    );
};

export default ButtonWithTooltip;

const Root = styled.div<any>`
    pointer-events: ${(props) => (props.disabled ? "none" : "auto")};
`;

const CloseContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    min-width: 10px;
    height: 10px;
    margin-right: 11px;
`;
const LineLeft = styled.div`
    width: 100%;
    height: 2px;
    background-color: #000;
    transform: rotate(45deg) translateY(70%);
    border-radius: 2px;
`;
const LineRight = styled(LineLeft)`
    transform: rotate(-45deg) translateY(-70%);
`;

const ButtonTooltip = styled(Tooltip)<any>`
    display: flex;
    align-items: center;
    background-color: ${(props) => (props.backgroundcolor ? props.backgroundcolor : "#fcfdfe")};
    border: 1px solid #dddeee;
    padding: 16px 18px 16px 18px;
    font-weight: 500;
    font-size: 14px;
    cursor: pointer;
    transition: all 0.4s ease;
    border-radius: 4px;
    color: ${(props) => (props.textcolor ? props.textcolor : "#000")};
    &:hover {
        border-color: #1c00a6;
        span {
            color: ${(props) => (props.textcolor ? props.textcolor : "#1c00a6")};
        }
        .line {
            background-color: #1c00a6;
        }
    }
    opacity: ${(props) => (props.disabled ? "0.6" : "1")};
`;
