import { configureStore } from "@reduxjs/toolkit";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";

// Slices
import { authReducers, authActions } from 'features/Auth/store';
import { appLoaderReducers, appLoaderActions } from 'features/ApplicationLoader/store';
import { appModalReducers, appModalActions } from 'features/Modals/store';
import { sidebarReducers, sidebarActions } from '../features/Sidebar/store';
import { toolbarActions, toolbarReducers } from '../features/Toolbar/store';
import { draftActions, draftReducers } from '../features/DraftDetails/store';
import { draftListActions, draftListReducers } from 'features/DraftList/store';
import { protectedListActions, protectedListReducers } from 'features/ProtectedList/store';
import { protectedDraftActions, protectedDraftReducers } from '../features/ProtectedDetails/store';
import { newVersionActions, newVersionReducers } from 'features/NewDraftVersion/store';
import { alertListReducers } from 'features/AlertsList/store';
import { settingReducers } from "features/Settings/store";
import { wizzardFromReducers } from "features/DisputForms/store";
import {disputeReducers} from "../features/DisputesDetail/store";


export const store = configureStore({
  reducer: {
    appLoader: appLoaderReducers,
    appModal: appModalReducers,
    auth: authReducers,
    sidebar: sidebarReducers,
    toolbar: toolbarReducers,
    draft: draftReducers,
    dispute: disputeReducers,
    draftList: draftListReducers,
    protectedList: protectedListReducers,
    protectedDraft: protectedDraftReducers,
    versionForm: newVersionReducers,
    alertList: alertListReducers,
    setting: settingReducers,
    wizzardFrom: wizzardFromReducers,
  },
  devTools: {
    actionCreators: {
      ...authActions,
      ...appLoaderActions,
      ...appModalActions,
      ...sidebarActions,
      ...toolbarActions,
      ...draftActions,
      ...draftListActions,
      ...protectedListActions,
      ...protectedDraftActions,
      ...newVersionActions
    }
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: ['draft/setDraftAlert/type', 'draft/setOverlay/type'],
      ignoredActionPaths: ['payload.message', 'payload.message'],
      ignoredPaths: ['draft.alert.message', 'draft.overlay.message'],
    },
  }),
});

export type AppState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<AppState> = useSelector;
