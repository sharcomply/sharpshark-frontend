import { useEffect } from 'react';

export function useHasItem<T>(item: T, effect: (item: NonNullable<T>) => void) {
    useEffect(() => {
        if (!item) return
        
        effect(item as NonNullable<T>)
    }, [item])
}