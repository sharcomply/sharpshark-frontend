import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

export function useRedirectEffect<T>(watch: T, redirect = "/") {
    const navigate = useNavigate();

    useEffect(() => {
        if (!watch) return;

        navigate(redirect);
    }, [navigate, redirect, watch]);
}
