import { useContext, useEffect, useCallback } from "react";
import { UNSAFE_NavigationContext as NavigationContext } from "react-router-dom";
import type { History } from "history";

export function useBlocker(blocker: any, when: boolean = true) {
    const { navigator } = useContext(NavigationContext);
    const blockNavigator = navigator as unknown as History;

    useEffect(() => {
        if (!when) return;

        const unblock = blockNavigator.block((tx: any) => {
            const autoUnblockingTx = {
                ...tx,
                retry() {
                    unblock();
                    tx.retry();
                },
            };

            blocker(autoUnblockingTx);
        });

        return unblock;
    }, [navigator, blocker, when]);
}

export function usePrompt(message: string, when: boolean = true) {
    const blocker = useCallback(
        (tx: any) => {
            if (tx.location?.hash === "#scan-result") {
                return tx.retry();
            }
            // eslint-disable-next-line no-alert
            if (window.confirm(message)) tx.retry();
        },
        [message]
    );

    useBlocker(blocker, when);
}
