import {useQuery} from "react-query";
import {fetchLocales} from "../api/requests/locales";
import {useMemo} from "react";

export const useLocales = () => {
    const {data} = useQuery(['LOCALES'],  fetchLocales, {retry: 1})

    const localesData = useMemo(() => {
        const res:any = []
        data?.items.forEach((item) => {
            res.push({value: item.name, label: item.name})

        })
        return res
    }, [data])

    return {
        data,
        localesData
    }
}