import { useEffect } from 'react';

export function useWatchEffect<T>(watch: T, effect: () => void) {
    useEffect(() => {
        if (!watch) return
        
        effect()
    }, [watch])
}
