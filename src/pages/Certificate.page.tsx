import Certificate from "features/Certificate";
import { useProtectedDraftStore } from "features/ProtectedDetails/hooks/useProtectedDraftStore";
import { useEffect } from "react";
import usePublicDraft from "features/Certificate/hooks/usePublicDraft";
import CertificateNotFound from "features/Certificate/components/CertificateNotFound";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";

export default function CertificatePage() {
    const { draft, isError } = usePublicDraft();
    const {
        actions: { setTokenId },
    } = useProtectedDraftStore();

    const { setDraftId, setMintedNetworks } = useDraftStore();

    useEffect(() => {
        setTokenId(draft!?.tokenId);
        setDraftId(draft?._id);
        setMintedNetworks(draft?.mintedNetworks[0]?.name);
    }, [draft, setTokenId, setDraftId, setMintedNetworks]);

    if (isError) return <CertificateNotFound />;

    return <Certificate draft={draft!} />;
}
