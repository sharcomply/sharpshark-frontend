import { Group, Stack } from "@mantine/core";
import PageTitle from "features/PageTitle";
import InfoButton from "components/InfoButton";
import { TabLink } from "components/TabLink";
import { Outlet } from "react-router-dom";
import { useProtectedListStore } from "features/ProtectedList/hooks/useProtectedListStore";
import useProtectedList from "features/ProtectedList/hooks/useProtectedList";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";

//Component
export default function ProtectedListPage() {
    const { state, actions: { setCount, clearStore } } = useProtectedListStore()
    const { protects } = useProtectedList()
    const { t } = useTranslation();

    useEffect(() => {
        setCount('locked', protects?.count || 0)
    }, [setCount, protects])

    useEffect(() => {
        return () => clearStore()
    }, [clearStore])

    return (
        <Stack>
            <PageTitle title={t("protected_drafts.protected-document")} tooltipText={t("tooltips.protected")} />
            <Group position={"apart"} spacing={16}>
                <Group spacing={12}>
                    <TabLink variant="outlined" to="/protected/locked" label={`${t("protected_drafts.tabs.locked")} (${state.counts.locked})`} />
                    <TabLink variant="outlined" to="/protected/unlocked" disabled={true} label={`${t("protected_drafts.tabs.unlocked")} ${t("common.coming_soon")}`} />
                </Group>
                <InfoButton />
            </Group>

            <Outlet />
        </Stack>
    );
}
