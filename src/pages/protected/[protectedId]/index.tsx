import { Outlet, useNavigate } from "react-router-dom";
import useCurrentProtected from "features/ProtectedDetails/hooks/useCurrentProtected";
import { useProtectedDraftStore } from "features/ProtectedDetails/hooks/useProtectedDraftStore";
import { useBackButton } from "features/Toolbar/hooks/useBackButton";
import { useHasItem } from "hooks/useHasItem";
import { Stack, Text } from "@mantine/core";
import { SharkButton } from "components/SharkButton";
import { useTranslation } from "react-i18next";
import ProtectedSkeleton from "features/ProtectedDetails/components/ProtectedSkeleton";
import {useEffect, useState} from "react";

export interface ProtectedOutletProps {
    draft: DraftDetails;
}

export default function ProtectedPage() {
    const navigate = useNavigate()
    const {t} = useTranslation()
    const { protect, isLoading } = useCurrentProtected();
    const store = useProtectedDraftStore();
    const [isOverlay, setIsOverlay] = useState(false)
    useBackButton();
    useHasItem(protect, (item) => store.actions.setDraftId(item._id));
    // RENDER

    useEffect(() => {
      const timer = setTimeout(() => {
        if (isLoading || !protect) {
          setIsOverlay(true)
        }
        clearTimeout(timer)
      }, 5000)
    }, [protect, isLoading])

    if (isLoading || !protect) {
        return <ProtectedSkeleton isOverlay={isOverlay}  />;
    }

    if (!protect.metadata) {
      setIsOverlay(false)
        return (
            <Stack align={"center"}>
                <Text size="md" color={"red"} align="center">
                    {t("errors.messages.no_metadata")}
                </Text>
                <SharkButton onClick={() => navigate(0)} variant="outlined" buttonType="primary">
                    {t("buttons.reload_page_btn")}
                </SharkButton>
            </Stack>
        );
    }

    return <Outlet context={{ draft: protect }} />;
}
