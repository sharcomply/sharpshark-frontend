import { useEffect } from "react";
import Icon from "components/Icon";
import { useNavigate } from "react-router-dom";
import { Group, Loader, Stack, Text } from "@mantine/core";
import { useEmailVerify } from "features/Auth/hooks/useEmailVerify";
import { useTranslation } from "react-i18next";

//Component
export default function VerifyPage() {
    const navigate = useNavigate();
    const { isError, isSuccess, isLoading, token } = useEmailVerify()
    const {t} = useTranslation()
    useEffect(() => {
        if (token) return 
        navigate("/", { replace: true });
    }, [navigate, token]);

    return (
        <Stack p={16} align={"center"} justify={"center"} sx={{ minHeight: "100vh" }}>
            {!!isSuccess && (
                <Stack>
                    <Group position={"center"} align={"center"}>
                        <Icon icon="ic:round-check-circle-outline" color="#29CCB1" size={64} />
                    </Group>
                    <Text color={"#0A001E"}>
                        {t("responses.email_verification_success")}
                    </Text>
                </Stack>
            )}

            {!!isError && (
                <Stack>
                    <Group position={"center"} align={"center"}>
                        <Group sx={{ borderRadius: 100, border: "5px solid #E50C54" }}>
                            <Icon icon="ic:round-close" color="#E50C54" size={42} />
                        </Group>
                    </Group>
                    <Text color={"#0A001E"}>
                        {t("errors.email_verification_failed")}
                    </Text>
                </Stack>
            )}

            {!!isLoading && (
                <Stack>
                    <Group position={"center"} align={"center"}>
                        <Loader color={"#0A001E"} size={52} />
                    </Group>
                    <Text color={"#0A001E"}>{t("statuses.email_verification")}</Text>
                </Stack>
            )}
        </Stack>
    );
}
