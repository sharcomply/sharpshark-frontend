import { Stack } from "@mantine/core";
import PageTitle from "features/PageTitle";

const ComingSoonPage = () => {
    return (
        <Stack>
            <PageTitle title={"This is a stub page there will be real links soon"} tooltipText="" />
        </Stack>
    );
};

export default ComingSoonPage;
