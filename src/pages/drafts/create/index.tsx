import { useEffect } from "react";
import { useToolbarStore } from "features/Toolbar/hooks/useToolbarStore";
import { Outlet } from "react-router-dom";

export default function DraftCreatePage() {
    const { setBackButton } = useToolbarStore();

    // Enable Back Button For This Page
    useEffect(() => {
        setBackButton(true);
        return () => setBackButton(false);
    }, [setBackButton]);

    return <Outlet />
}
