import { Group, Stack } from "@mantine/core";
import InfoButton from "components/InfoButton";
import { TabLink } from "components/TabLink";
import useDeletedDraftList from "features/DeletedDraftList/hooks/useDeletedDraftList";
import useDraftList from "features/DraftList/hooks/useDraftList";
import { useDraftListStore } from "features/DraftList/hooks/useDraftListStore";
import PageTitle from "features/PageTitle";
import { useEffect, useState } from "react";
import { Outlet } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Alert } from "features/DraftList/components/Alert";
import { useAlertData } from "../../helpers/alertData";

//Component
export default function DraftListPage() {
    const {
        state,
        actions: { setCount, clearStore },
    } = useDraftListStore();
    const { drafts: workingDrafts } = useDraftList();
    const { drafts: deletedDrafts } = useDeletedDraftList();
    const { t } = useTranslation();
    const { title, text } = useAlertData("All_draft");

    useEffect(() => {
        setCount("working", workingDrafts?.count || 0);
    }, [setCount, workingDrafts]);

    useEffect(() => {
        setCount("deleted", deletedDrafts?.count || 0);
    }, [setCount, deletedDrafts]);

    useEffect(() => {
        return () => clearStore();
    }, [clearStore]);

    const [activeInfo, setActiveInfo] = useState<boolean>(false);

    return (
        <Stack>
            <PageTitle title={t("drafts.all-draft")} tooltipText={t("tooltips.drafts_arent_published")} />
            <Group position={"apart"} spacing={16}>
                <Group spacing={12}>
                    <TabLink variant="outlined" to="/drafts/working-on" label={`${t("drafts.tabs.working_on")} (${state.counts.working})`} />
                    <TabLink variant="outlined" to="/drafts/deleted" label={`${t("drafts.tabs.deleted")} (${state.counts.deleted})`} />
                </Group>
                <InfoButton isActive={activeInfo} onClick={() => setActiveInfo(!activeInfo)} enabled />
            </Group>
            {activeInfo && <Alert title={title} text={text} variant={"info"} />}
            <Outlet />
        </Stack>
    );
}
