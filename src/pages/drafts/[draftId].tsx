import { useParams } from "react-router-dom";
import { Stack } from "@mantine/core";
import { useEffect } from "react";

import useDraft from "features/DraftDetails/hooks/useDraft";
import { useToolbarStore } from "features/Toolbar/hooks/useToolbarStore";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import DraftDetails from "features/DraftDetails";
import DeletedDraftDetails from "features/DeletedDraftDetails";
import DraftSkeleton from "features/DraftDetails/components/DraftSkeleton";

export default function DraftPage() {
    const { draftId } = useParams();
    const { setBackButton } = useToolbarStore();
    const { clearStore } = useDraftStore();
    const { draft, isLoading } = useDraft(draftId);

    // Enable Back Button For This Page
    // clean store state
    useEffect(() => {
        setBackButton(true);
        // clearStore();
        return () => {
            setBackButton(false);
            clearStore();
        };
    }, [setBackButton, clearStore]);

    if(isLoading) {
        return <DraftSkeleton/>
    }

    if (!draft) {
        return null;
    }

    // Render
    return <Stack>{draft?.isDeleted ? <DeletedDraftDetails draft={draft} /> : <DraftDetails draft={draft} />}</Stack>;
}
