import styled from "@emotion/styled";
import React, { useEffect } from "react";
import PageTitle from "features/PageTitle";
import { useToolbarStore } from "../features/Toolbar/hooks/useToolbarStore";
import { useTranslation } from "react-i18next";
import InfoButton from "components/InfoButton";
import { Outlet } from "react-router-dom";
import { TabLink } from "components/TabLink";
import { Group } from "@mantine/core";
import { Alert } from "../features/DraftDetails/components/Alert";
import { useDraftAlert } from "../features/DraftDetails/hooks/useDraftAlert";

//Component
export default function SettingsPage() {
    const { setBackButton } = useToolbarStore();
    const { t } = useTranslation();
    const alert = useDraftAlert();

    useEffect(() => {
        setBackButton(true);
        return () => setBackButton(false);
    }, [setBackButton]);

    return (
        <React.Fragment>
            <PageTitle title={t("settings.title")} tooltipText={t("tooltips.drafts_arent_published")} actions={<></>} />
            {alert.state.show && <Alert title={alert.state.title} text={alert.state.message} variant={alert.state.type} />}
            <ToolsContainer>
                <Group spacing={12}>
                    <TabLink variant={"outlined"} to="/settings/wallets" label={t("settings.tabs.wallets")} />
                    <TabLink variant={"outlined"} to="/settings/info" label={t("settings.tabs.info")} />
                    <TabLink variant={"outlined"} to="/settings/pricing" label={t("settings.tabs.pricing")} />
                </Group>
                <InfoButton />
            </ToolsContainer>
            <Outlet />
        </React.Fragment>
    );
}

//Styling
const ToolsContainer = styled.div`
    display: flex;
    justify-content: space-between;
    margin-bottom: 16px;
    margin-top: 19px;
`;
