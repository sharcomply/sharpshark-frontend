import { Group, Stack } from "@mantine/core";
import PageTitle from "features/PageTitle";
import InfoButton from "components/InfoButton";
import { TabLink } from "components/TabLink";
import { Outlet } from "react-router-dom";
import { useEffect } from "react";
import { useAlertListStore } from "features/AlertsList/hooks/useAlertListStore";
import useAlertList from "features/AlertsList/hooks/useAlertList";
import useAlertNotActual from "features/AlertsNotActualList/hooks/useAlertList";
import { useTranslation } from "react-i18next";
//Component
export default function AlertsListPage() {
    const newAlert = useAlertList({ isSkipped: false });
    const notActualAlert = useAlertNotActual({ isSkipped: true }, "ALERTS_NOT_ACTUAL_LIST");
    const { t } = useTranslation();
    const {
        state,
        actions: { setCount },
    } = useAlertListStore();

    useEffect(() => {
        setCount("new", newAlert?.alerts?.count || 0);
    }, [setCount, newAlert]);

    useEffect(() => {
        setCount("notActual", notActualAlert!?.alerts!?.count || 0);
    }, [setCount, notActualAlert]);

    // useEffect(() => {
    //     return () => clearStore();
    // }, [clearStore]);
    return (
        <Stack>
            <PageTitle title={t("alerts.copyright_alerts")} tooltipText="" />
            <Group position={"apart"} spacing={16}>
                <Group spacing={12}>
                    <TabLink variant="outlined" to="/alerts/new" label={`${t("alerts.tabs.new")} (${state?.counts.new})`} />
                    <TabLink variant="outlined" to="/alerts/not-actual" label={`${t("alerts.tabs.not-actual")} (${state?.counts.notActual})`} />
                </Group>
                <InfoButton />
            </Group>

            <Outlet />
        </Stack>
    );
}
