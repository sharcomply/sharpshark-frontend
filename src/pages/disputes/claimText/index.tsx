import { Group, Stack, Text } from "@mantine/core";
import PageTitle from "features/PageTitle";
import { Outlet, useParams } from "react-router-dom";
import { useDisputDetail } from "../../../features/DisputesList/hooks/useDisputesList";
import { useEffect, useState } from "react";

const DisputClaimTextPage = () => {
    const { disputId } = useParams();
    const { disput } = useDisputDetail(disputId);
    const [claimText, setClaimText] = useState("");
    const status = disput?.status;

    useEffect(() => {
        if (disput) {
            if (
                status === "Draft: Website Owner" ||
                status === "In Progress: Website Owner" ||
                status === "Claim Sent: Website Owner" ||
                status === undefined
            ) {
                if (disput.ownerClaimText) {
                    setClaimText(disput.ownerClaimText);
                }
            }
            if (status === "Draft: Provider" || status === "In Progress: Provider" || status === "Claim Sent: Provider") {
                if (disput.providerClaimText) {
                    setClaimText(disput.providerClaimText);
                }
            }
            if (
                status === "Draft: Google" ||
                status === "In Progress: Google" ||
                status === "Claim Sent: Google" ||
                status === "Under Court" ||
                status === "Closed"
            ) {
                if (disput.ownerClaimText) {
                    setClaimText(disput.ownerClaimText);
                }
            }
        }
    }, [claimText, disput, status]);
    console.log("claimText", claimText);

    return (
        <Stack style={{ padding: "40px 0 48px 96px" }}>
            <PageTitle title={"Dispute claim text"} tooltipText="" />
            <Group position={"apart"} spacing={16}>
                <Text
                    size={"md"}
                    color={"#0A001E"}
                    weight={"500"}
                    dangerouslySetInnerHTML={{ __html: claimText ? claimText : "You have to select your demands, generate a claim text and save the dispute" }}
                />
            </Group>

            <Outlet />
        </Stack>
    );
};

export default DisputClaimTextPage;

