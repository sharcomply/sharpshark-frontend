import { Group, Stack } from "@mantine/core";
import InfoButton from "components/InfoButton";
import { TabLink } from "components/TabLink";
import { useDisputesCounter } from "features/DisputesList/hooks/useDisputesCounters";
import useDisputesList from "features/DisputesList/hooks/useDisputesList";
import PageTitle from "features/PageTitle";
import { useTranslation } from "react-i18next";
import { Outlet } from "react-router-dom";

const DisputListPage = () => {
    const { t } = useTranslation();
    const { data } = useDisputesCounter();
    const { count: NotrelevantCount } = useDisputesList("NOTRELEVANT", "NotRelevant");

    return (
        <Stack>
            <PageTitle title={"Dispute resolution"} tooltipText="" />
            <Stack spacing={16}>
                <Group spacing={12}>
                    Website owner level:
                    <TabLink
                        variant="outlined"
                        to="/disputes/draft-website-owner"
                        label={t("dispute.tabs.draft", { count: data?.["Draft: Website Owner"] || 0 })}
                    />
                    <TabLink
                        variant="outlined"
                        to="/disputes/in-progress-website-owner"
                        label={t("dispute.tabs.in_progress", { count: data?.["In Progress: Website Owner"] || 0 })}
                    />
                    <TabLink
                        variant="outlined"
                        to="/disputes/claim-sent-website-owner"
                        label={t("dispute.tabs.claim_sent", { count: data?.["Claim Sent: Website Owner"] || 0 })}
                    />
                    <TabLink
                        variant="outlined"
                        to="/disputes/letter-sent-website-owner"
                        label={t("dispute.tabs.letter_sent", { count: data?.["Letter Sent: Website Owner"] || 0 })}
                    />
                </Group>
                <Group spacing={12}>
                    Provider level:
                    <TabLink variant="outlined" to="/disputes/draft-provider" label={t("dispute.tabs.draft", { count: data?.["Draft: Provider"] || 0 })} />
                    <TabLink
                        variant="outlined"
                        to="/disputes/in-progress-provider"
                        label={t("dispute.tabs.in_progress", { count: data?.["In Progress: Provider"] || 0 })}
                    />
                    <TabLink
                        variant="outlined"
                        to="/disputes/claim-sent-provider"
                        label={t("dispute.tabs.claim_sent", { count: data?.["Claim Sent: Provider"] || 0 })}
                    />
                    <TabLink
                        variant="outlined"
                        to="/disputes/letter-sent-provider"
                        label={t("dispute.tabs.letter_sent", { count: data?.["Letter Sent: Provider"] || 0 })}
                    />
                </Group>
                <Group position={"apart"}>
                    <Group spacing={12}>
                        Google level:
                        <TabLink variant="outlined" to="/disputes/draft-google" label={t("dispute.tabs.draft", { count: data?.["Draft: Google"] || 0 })} />
                        <TabLink
                            variant="outlined"
                            to="/disputes/in-progress-google"
                            label={t("dispute.tabs.in_progress", { count: data?.["In Progress: Google"] || 0 })}
                        />
                        <TabLink
                            variant="outlined"
                            to="/disputes/claim-sent-google"
                            label={t("dispute.tabs.claim_sent", { count: data?.["Claim Sent: Google"] || 0 })}
                        />
                        <TabLink
                            variant="outlined"
                            to="/disputes/letter-sent-google"
                            label={t("dispute.tabs.letter_sent", { count: data?.["Letter Sent: Google"] || 0 })}
                        />
                        <TabLink variant="outlined" to="/disputes/ended" label={t("dispute.tabs.closed", { count: data?.Closed || 0 })} />
                        <TabLink variant="outlined" to="/disputes/not-relevant" label={t("dispute.tabs.not_relevant", { count: NotrelevantCount || 0 })} />
                    </Group>
                    <InfoButton />
                </Group>
            </Stack>

            <Outlet />
        </Stack>
    );
};

export default DisputListPage;

