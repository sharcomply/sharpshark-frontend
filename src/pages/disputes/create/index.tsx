import { Group, Stack } from "@mantine/core";
import InfoButton from "components/InfoButton";
import { SharkButton } from "components/SharkButton";
import { TabLink } from "components/TabLink";
import { useDisputeCreate } from "features/DisputesCollectingEvidence/hooks/useDisputeCreate";
import { useDisputDetail } from "features/DisputesList/hooks/useDisputesList";
import DisputForms from "features/DisputForms";
import { useWizzardFormStore } from "features/DisputForms/hooks/useWizzardFormStore";
import PageTitle from "features/DisputesDetail/component/PageTitle";
import { useCallback, useEffect, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { Alert } from "features/DraftList/components/Alert";
import { useAlertData } from "../../../helpers/alertData";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import useDisputesStore from "../../../features/DisputesDetail/hooks/useDisputesStore";
import { useUnmount } from "../../../hooks/useUnmount";
import useAuthStore from "features/Auth/hooks/useAuthStore";
import useAlert from "features/AlertsList/hooks/useAlert";
import { useAlertListStore } from "features/AlertsList/hooks/useAlertListStore";
import ClaimTextGenerator from "helpers/claimTextGenerator";

function usePrevious(value: any): { type: string; message: string } | undefined {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    }, [value]);
    return ref.current;
}

const DisputCreatePage = () => {
    const { t } = useTranslation();
    const { disputId, alertId } = useParams();
    const { disput, refetch } = useDisputDetail(disputId!);
    const { alert } = useAlert(alertId);
    const draftStore = useDraftStore();
    const { status: disputStatus } = useDisputesStore();
    const { user } = useAuthStore();
    const {
        state,
        actions: { setStep },
    } = useWizzardFormStore();
    const {
        actions: { setDisputPayload },
    } = useAlertListStore();

    const prevStatus = usePrevious(disputStatus);

    const { control, register, handleSubmit, getValues, setValue, reset, setError, formState } = useForm({
        defaultValues: { ...disput!, userContactEmail: user?.disputesPrefillEmail, documentName: "", discoveredTime: "" },
    });

    useEffect(() => {
        if (user) {
            setValue("userContactEmail", user.disputesPrefillEmail);
        }
    }, [user]);

    const demands = getValues("demands");
    const status = getValues("status");
    const { title, text } = useAlertData("Website_Owners_Level", state.step);
    const [disableTabs, setDisableTabs] = useState<Array<number>>([]);
    const { handleCreate, handleUpdate, loading } = useDisputeCreate();
    const [activeInfo, setActiveInfo] = useState<boolean>(false);
    const ownerClaimText = getValues("ownerClaimText");
    const providerClaimText = getValues("providerClaimText");
    const googleClaimText = getValues("googleClaimText");
    const publicationLinks = getValues("publicationLinks") as any;
    const publicationQuestionScreenshots = getValues("publicationQuestionScreenshots") as any;
    const publicationQuestionLinks = getValues("publicationQuestionLinks") as any;
    const discoveredDate = getValues("discoveredTime");
    const publicationQuestionWebarchiveLinks = getValues("publicationQuestionWebarchiveLinks") as any;
    const publicationWebarchiveLinks = getValues("publicationWebarchiveLinks") as any;
    const draftsScreenshots = getValues("draftsScreenshots") as any;
    const token_id = getValues("documentId.tokenId");
    const similarContent = getValues("alertId.scanId.similarContent" as any);
    const alertRef = useRef<HTMLDivElement | null>(null);
    const textGenerator = new ClaimTextGenerator({
        t,
        demand: demands!,
        details: {
            publicationLinks,
            publicationQuestionScreenshots,
            discoveredDate,
            publicationQuestionLinks,
            publicationWebarchiveLinks,
            publicationQuestionWebarchiveLinks,
            draftsScreenshots,
            token_id,
            similarContent,
        },
    });
    const actionItems = () => (
        <SharkButton type="submit" buttonType="secondary" variant="outlined" loading={loading} disabled={loading}>
            × {t("buttons.save_close_btn")}
        </SharkButton>
    );

    useEffect(() => {
        if (!draftStore.alert.show) return;
        alertRef.current?.scrollIntoView({ behavior: "smooth", block: "end" });
    }, [draftStore.alert]);

    const checkStatusTitle = useCallback((status: any) => {
        switch (status) {
            case undefined:
                return "Website owner’s level";
            case "In Progress: Website Owner":
            case "Draft: Website Owner":
            case "Claim Sent: Website Owner":
                return "Website owner’s level";
            case "Draft: Provider":
            case "In Progress: Provider":
            case "Claim Sent: Provider":
                return "Provider's level";
            case "Draft: Google":
            case "In Progress: Google":
            case "Claim Sent: Google":
                return "Search engine’s level";

            default:
                return "Website owner’s level";
        }
    }, []);

    const checkStatus = useCallback((status: any) => {
        switch (status) {
            case undefined:
                return "Draft: Website Owner";

            case "Draft: Website Owner":
                return "Draft: Website Owner";

            case "In Progress: Website Owner":
                return "In Progress: Website Owner";

            case "Claim Sent: Website Owner":
                return "Draft: Provider";

            case "Draft: Provider":
                return "Draft: Provider";

            case "Claim Sent: Provider":
                return "Draft: Google";

            case "Draft: Google":
                return "Draft: Google";

            default:
                return status;
        }
    }, []);

    const onSubmit = useCallback(
        (values: Disput) => {
            if (disputId) {
                handleUpdate({ ...values, status: checkStatus(values.status) });
            } else handleCreate({ ...values, status: checkStatus(values.status) });
        },
        [handleUpdate, handleCreate, disputId, checkStatus]
    );
    useUnmount(() => {
        setDisableTabs([]);
        setStep(0);
    });
    useEffect(() => {
        if (state.step === 0) {
            if (disableTabs.includes(1)) {
                return;
            } else {
                setDisableTabs([...disableTabs, 1]);
            }
        }
        if (state.step === 1) {
            if (!disableTabs.includes(1)) {
                setDisableTabs([...disableTabs, 1]);
            } else if (!disableTabs.includes(2)) {
                setDisableTabs([...disableTabs, 2]);
            } else {
                return;
            }
        }
        if (state.step === 2) {
            if (!disableTabs.includes(1)) {
                setDisableTabs([...disableTabs, 1]);
            } else if (!disableTabs.includes(2)) {
                setDisableTabs([...disableTabs, 2]);
            } else if (!disableTabs.includes(3)) {
                setDisableTabs([...disableTabs, 3]);
            } else {
                return;
            }
        }
    }, [disableTabs, state.step]);

    const getDateText = (date: string) => {
        const dateLocal = new Date(date);
        let utc = "";

        if (dateLocal.getTimezoneOffset() <= 0) {
            utc = `+${Math.abs(dateLocal.getTimezoneOffset() / 60)}`;
        }
        if (dateLocal.getTimezoneOffset() > 0) {
            utc = `-${Math.abs(dateLocal.getTimezoneOffset() / 60)}`;
        }

        return dateLocal.toLocaleTimeString().slice(0, -3) + " UTC " + utc;
    };

    useEffect(() => {
        if (disput) {
            if (disput.demands) {
                reset({
                    ...disput,
                    ownerClaimText: !disput.ownerClaimText ? getValues("ownerClaimText") : disput.ownerClaimText,
                    documentName: disput.documentId.title,
                    discoveredTime: getDateText((disput.alertId as unknown as Alert).date),
                });
            } else {
                reset({
                    ...disput,
                    demands: getValues("demands"),
                    ownerClaimText: !disput.ownerClaimText ? getValues("ownerClaimText") : disput.ownerClaimText,
                    providerClaimText: !disput.providerClaimText ? getValues("providerClaimText") : disput.providerClaimText,
                    googleClaimText: !disput.googleClaimText ? getValues("googleClaimText") : disput.googleClaimText,
                    userContactEmail: user?.disputesPrefillEmail,
                });
            }
        }

        if (alert) {
            setDisputPayload({ appearedOn: alert.scanId.similarContent[0].url, alertId: alert._id });
            setValue("documentName", alert.documentId.title);
            setValue("discoveredTime", getDateText(alert.date));
        }
    }, [alert, reset, disput, getValues]);

    useEffect(() => {
        if (!getValues("ownerClaimType")) {
            setValue("ownerClaimType", "By email");
        }
    });

    useEffect(() => {
        if (Number(getValues("payAmount") < 0)) {
            setValue("payAmount", 0);
        }
    });

    useEffect(() => {
        return () => {
            draftStore.showDraftAlert(false);
            draftStore.setDraftAlert({
                type: undefined,
                title: undefined,
                message: undefined,
            });
        };
    }, []);

    const getText = async (type: "providerClaimText" | "googleClaimText" | "ownerClaimText") => {
        let text = "";
        if (type === "providerClaimText") {
            text = await textGenerator.getProviderClaimText();
        }
        if (type === "googleClaimText") {
            text = await textGenerator.getGoogleClaimText();
        }
        if (type === "ownerClaimText") {
            text = await textGenerator.getOwnerClaimText();
        }
        setValue(type, text);
    };
    // useEffect(() => {
    //
    //     if (status === "Draft: Provider" || status === "In Progress: Provider" || status === "Claim Sent: Provider") {
    //         disput && getText('providerClaimText');
    //     } else if (
    //         status === "Draft: Google" ||
    //         status === "In Progress: Google" ||
    //         status === "Claim Sent: Google" ||
    //         status === "Under Court" ||
    //         status === "Closed"
    //     ) {
    //         disput && getText('googleClaimText');
    //     } else {
    //         disput && demands && getText('ownerClaimText');
    //     }
    // }, [demands, disput, status]);

    useEffect(() => {
        const updateClaimText = async () => {
            if (disput) {
                if (
                    status === "Draft: Website Owner" ||
                    status === "In Progress: Website Owner" ||
                    status === "Claim Sent: Website Owner" ||
                    status === undefined
                ) {
                    await getText("ownerClaimText");
                }
                if (status === "Draft: Provider" || status === "In Progress: Provider" || status === "Claim Sent: Provider") {
                    await getText("providerClaimText");
                }
                if (
                    status === "Draft: Google" ||
                    status === "In Progress: Google" ||
                    status === "Claim Sent: Google" ||
                    status === "Under Court" ||
                    status === "Closed"
                ) {
                    await getText("googleClaimText");
                }
            }
        };
        updateClaimText();
    }, [status, demands, disput]);

    useEffect(() => {
        switch (status) {
            case undefined:
            case "Draft: Website Owner":
            case "In Progress: Website Owner":
            case "Claim Sent: Website Owner":
                if (ownerClaimText && disput && ownerClaimText?.length !== disput?.ownerClaimText?.length) {
                    onSubmit({ ...getValues() });
                }
                break;
            case "Draft: Provider":
            case "In Progress: Provider":
            case "Claim Sent: Provider":
                if (providerClaimText && disput && providerClaimText?.length !== disput?.providerClaimText?.length) {
                    onSubmit({ ...getValues() });
                }
                break;
            case "Draft: Google":
            case "In Progress: Google":
            case "Claim Sent: Google":
                if (googleClaimText && disput && googleClaimText?.length !== disput?.googleClaimText?.length) {
                    onSubmit({ ...getValues() });
                }
                break;

            default:
                if (ownerClaimText && disput && ownerClaimText?.length !== disput?.ownerClaimText?.length) {
                    onSubmit({ ...getValues() });
                }
                break;
        }
    }, [googleClaimText, ownerClaimText, providerClaimText, status, getValues]);

    useEffect(() => {
        const update = async () => {
            if (prevStatus && prevStatus.message === t("statuses.uploading") && disputStatus.message === t("statuses.saved")) {
                await refetch();
            }
        };
        update();
    }, [prevStatus, disputStatus, t, getValues, refetch]);

    if (disputId && !disput) return null;

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Stack>
                <PageTitle title={`Dispute resolution: ${checkStatusTitle(disput?.status)}`} tooltipText="" actions={actionItems()} />
                <Group position={"apart"} spacing={16}>
                    <Group spacing={12}>
                        <div>
                            <TabLink disabled={!disableTabs.includes(1)} variant="outlined" to="collecting-evidence" label="Collect evidence" />
                        </div>
                        <div>
                            <TabLink disabled={!disableTabs.includes(2)} variant="outlined" to="generating-claim" label="Generate claim" />
                        </div>
                        <div>
                            <TabLink disabled={!disableTabs.includes(3)} variant="outlined" to="sending" label="Send" />
                        </div>
                    </Group>
                    <InfoButton isActive={activeInfo} onClick={() => setActiveInfo(!activeInfo)} enabled />
                </Group>
                {activeInfo && <Alert title={title} text={text} variant={"info"} />}
                {draftStore.alert.show && (
                    <div ref={alertRef}>
                        <Alert title={draftStore.alert.title} text={draftStore.alert.message} variant={draftStore.alert.type} />
                    </div>
                )}

                <DisputForms setError={setError} getValues={getValues} setValue={setValue} control={control} register={register} formState={formState} />
            </Stack>
        </form>
    );
};

export default DisputCreatePage;

