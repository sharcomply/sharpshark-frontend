import styled from "@emotion/styled";
import React, { useState } from "react";
import PageTitle from "features/PageTitle";
import { TabButton } from "components/Tabs";
import { Tooltip } from "@mantine/core";
import InfoButton from "components/InfoButton";
import { useTranslation } from "react-i18next";

export default function AlertsPage() {
    const [activeTab, setActiveTab] = useState(-1);
    const { t } = useTranslation();
    // const data: any = [
    //     {
    //         id: 8,
    //         title: "Title",
    //         text: "texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext",
    //         appearedDate: "2022-05-05T12:00:00Z",
    //         discoveredDate: "2022-05-05T12:00:00Z",
    //     },
    // ];
    return (
        <React.Fragment>
            <PageTitle title={t("alerts.copyright_alerts")} tooltipText={t("tooltips.posible_copyright_violation")} actions={<></>} />
            <ToolsContainer>
                <ButtonsContainer>
                    <Tooltip
                        label={t("common.coming_soon")}
                        position="bottom"
                        placement="center"
                        transition="fade"
                        transitionDuration={300}
                        transitionTimingFunction="ease"
                        withArrow={false}
                        styles={{
                            body: {
                                backgroundColor: "#666892",
                            },
                        }}
                    >
                        <TabButton onClick={() => setActiveTab(0)} isActive={activeTab === 0} marginRight="12px" disabled>
                            {t("alerts.tabs.new")}
                        </TabButton>
                    </Tooltip>
                    <Tooltip
                        label={t("common.coming_soon")}
                        position="bottom"
                        placement="center"
                        transition="fade"
                        transitionDuration={300}
                        transitionTimingFunction="ease"
                        withArrow={false}
                        styles={{
                            body: {
                                backgroundColor: "#66 6892",
                            },
                        }}
                    >
                        <TabButton onClick={() => setActiveTab(1)} isActive={activeTab === 1} disabled>
                            {t("alerts.tabs.not-actual")}
                        </TabButton>
                    </Tooltip>
                </ButtonsContainer>
                <InfoButton />
            </ToolsContainer>
            {/* <AlertsTable data={data} /> */}
        </React.Fragment>
    );
}

//Styling
const ToolsContainer = styled.div`
    display: flex;
    justify-content: space-between;
    margin-bottom: 16px;
`;

const ButtonsContainer = styled.div`
    display: flex;
    justify-content: space-between;
`;
