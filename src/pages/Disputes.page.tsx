import styled from "@emotion/styled";
import React, { useState } from "react";
import PageTitle from "features/PageTitle";
import { TabButton } from "components/Tabs";
import { Tooltip } from "@mantine/core";
import InfoButton from "components/InfoButton";
import { useTranslation } from "react-i18next";

export default function DisputesPage() {
    const [activeTab, setActiveTab] = useState(-1);
    const {t} = useTranslation()
    // const alertsInDisputesData: any = [
    //     {
    //         id: 9,
    //         title: "Title",
    //         text: "texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext",
    //         appearedDate: "2022-05-05T12:00:00Z",
    //         discoveredDate: "2022-05-05T12:00:00Z",
    //     },
    // ];

    // const websiteData: any = [
    //     {
    //         id: 10,
    //         title: "Title",
    //         text: "texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext",
    //         appearedDate: "2022-05-05T12:00:00Z",
    //         discoveredDate: "2022-05-05T12:00:00Z",
    //         warningSentDate: "2022-05-05T12:00:00Z",
    //     },
    // ];

    // const providerData: any = [
    //     {
    //         id: 11,
    //         title: "Title",
    //         text: "texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext",
    //         appearedDate: "2022-05-05T12:00:00Z",
    //         discoveredDate: "2022-05-05T12:00:00Z",
    //         claimSentDate: "2022-05-05T12:00:00Z",
    //     },
    // ];

    // const searchEngineData: any = [
    //     {
    //         id: 12,
    //         title: "Title",
    //         text: "texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext",
    //         appearedDate: "2022-05-05T12:00:00Z",
    //         discoveredDate: "2022-05-05T12:00:00Z",
    //         claimSentDate: "2022-05-05T12:00:00Z",
    //     },
    // ];

    // const endedData: any = [
    //     {
    //         id: 13,
    //         title: "Title",
    //         text: "texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext",
    //         appearedDate: "2022-05-05T12:00:00Z",
    //         discoveredDate: "2022-05-05T12:00:00Z",
    //         tableEndedDate: "2022-05-05T12:00:00Z",
    //     },
    // ];

    return (
        <React.Fragment>
            <PageTitle title={t("dispute.title")} tooltipText={t("tooltips.your_open_disputes")} actions={<></>} />
            <ToolsContainer>
                <ButtonsContainer>
                    <Tooltip
                        label={t("common.coming_soon")}
                        position="bottom"
                        placement="center"
                        transition="fade"
                        transitionDuration={300}
                        transitionTimingFunction="ease"
                        withArrow={false}
                        styles={{
                            body: {
                                backgroundColor: "#666892",
                            },
                        }}
                    >
                        <TabButton onClick={() => setActiveTab(0)} isActive={activeTab === 0} marginRight="12px" disabled>
                            {t("dispute.tabs.alerts_in_dispute")}
                        </TabButton>
                    </Tooltip>
                    <Tooltip
                        label={t("common.coming_soon")}
                        position="bottom"
                        placement="center"
                        transition="fade"
                        transitionDuration={300}
                        transitionTimingFunction="ease"
                        withArrow={false}
                        styles={{
                            body: {
                                backgroundColor: "#666892",
                            },
                        }}
                    >
                        <TabButton onClick={() => setActiveTab(1)} isActive={activeTab === 1} marginRight="12px" disabled>
                            Website
                        </TabButton>
                    </Tooltip>
                    <Tooltip
                        label={t("common.coming_soon")}
                        position="bottom"
                        placement="center"
                        transition="fade"
                        transitionDuration={300}
                        transitionTimingFunction="ease"
                        withArrow={false}
                        styles={{
                            body: {
                                backgroundColor: "#666892",
                            },
                        }}
                    >
                        <TabButton onClick={() => setActiveTab(2)} isActive={activeTab === 2} marginRight="12px" disabled>
                            Prodivder
                        </TabButton>
                    </Tooltip>
                    <Tooltip
                        label={t("common.coming_soon")}
                        position="bottom"
                        placement="center"
                        transition="fade"
                        transitionDuration={300}
                        transitionTimingFunction="ease"
                        withArrow={false}
                        styles={{
                            body: {
                                backgroundColor: "#666892",
                            },
                        }}
                    >
                        <TabButton onClick={() => setActiveTab(3)} isActive={activeTab === 3} marginRight="12px" disabled>
                            Search Engines
                        </TabButton>
                    </Tooltip>
                    <Tooltip
                        label={t("common.coming_soon")}
                        position="bottom"
                        placement="center"
                        transition="fade"
                        transitionDuration={300}
                        transitionTimingFunction="ease"
                        withArrow={false}
                        styles={{
                            body: {
                                backgroundColor: "#666892",
                            },
                        }}
                    >
                        <TabButton onClick={() => setActiveTab(4)} isActive={activeTab === 4} disabled>
                            {t("dispute.tabs.ended")}
                        </TabButton>
                    </Tooltip>
                </ButtonsContainer>
                <InfoButton />
            </ToolsContainer>
            {/* <Card>
                {" "}
                <TabPane isActive={activeTab === 0}>
                    {" "}
                    <AlertsInDisputesTable data={alertsInDisputesData} />
                </TabPane>
                <TabPane isActive={activeTab === 1}>
                    <WebsiteTable data={websiteData} />
                </TabPane>
                <TabPane isActive={activeTab === 2}>
                    <ProviderTable data={providerData} />
                </TabPane>
                <TabPane isActive={activeTab === 3}>
                    <SearchEnginesTable data={searchEngineData} />
                </TabPane>
                <TabPane isActive={activeTab === 4}>
                    <EndedTable data={endedData} />
                </TabPane>
            </Card> */}
        </React.Fragment>
    );
}

//Styling
const ToolsContainer = styled.div`
    display: flex;
    justify-content: space-between;
    margin-bottom: 16px;
`;

const ButtonsContainer = styled.div`
    display: flex;
    justify-content: space-between;
`;

// const Card = styled.div`
//     position: relative;
//     display: flex;
//     flex-direction: column;
//     min-width: 0;
//     word-wrap: break-word;
//     background-color: #fff;
//     background-clip: border-box;
//     border: 1px solid rgba(0, 0, 0, 0.13);
//     border-radius: 0.25rem;
//     height: 100%;
// `;
