import { Contract as EthersContract, Signer, utils } from "ethers";
import type { Provider } from "@ethersproject/providers";
import type { SharkContractInterface, SharkContract } from "../contracts/SharkContract";
import { abi } from '../../constants/abi';

export class SharkContractFactory {
  static readonly abi = abi;
  static createInterface(): SharkContractInterface {
    return new utils.Interface(abi) as SharkContractInterface;
  }
  static connect(address: string, signerOrProvider: Signer | Provider): SharkContract {
    return new EthersContract(address, abi, signerOrProvider) as SharkContract;
  }
}
