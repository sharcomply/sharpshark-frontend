import { ContractAddressBook } from 'constants/contracts';
import { useSigner } from './useSigner';
import { useMemo } from 'react';
import { SharkContractFactory } from '../factories/SharkContractFactory';
import { useWeb3React } from '@web3-react/core';
import { ChainId } from '../../constants/networks';
import { ProviderAddressBook } from 'constants/providers';
import { ethers } from 'ethers';
import useDraftStore from 'features/DraftDetails/hooks/useDraftStore';

export function useContractWithMintedNetwork(isFromCache?:boolean) {
    const { signer, isSignerReady } = useSigner();
    const web3React = useWeb3React()
    const { mintedNetworks } = useDraftStore();

    let chainId: ChainId = web3React.chainId || ChainId.GOERLI_TESTNET

    switch (mintedNetworks) {
        case "polygon":
            chainId = ChainId.POLYGON_MAINNET!;
            break;
        case "bsc":
            chainId = ChainId.BSC_TESTNET!;
            break;
        case "eth":
            chainId = ChainId.GOERLI_TESTNET!;
            break;
    }

    let address = ContractAddressBook[chainId] as string;
    let provider = ProviderAddressBook[chainId] as ethers.providers.Provider;

    return useMemo(() => {
        const isReady = isSignerReady(signer);

        if (isReady && !!signer && !isFromCache) {
            return SharkContractFactory.connect(address, signer);
        }

        return SharkContractFactory.connect(address, provider);
    }, [address, isSignerReady, provider, signer]);
}
