import { ContractAddressBook } from 'constants/contracts';
import { useSigner } from './useSigner';
import { useMemo } from 'react';
import { SharkContractFactory } from '../factories/SharkContractFactory';
import { useWeb3React } from '@web3-react/core';
import { ChainId } from '../../constants/networks';
import { ProviderAddressBook } from 'constants/providers';
import { ethers } from 'ethers';

export function useContract() {
    const { signer, isSignerReady } = useSigner();
    const web3React = useWeb3React()

    const chainId: ChainId = web3React.chainId || ChainId.GOERLI_TESTNET

    const address = ContractAddressBook[chainId] as string;
    const provider = ProviderAddressBook[chainId] as ethers.providers.Provider;

    return useMemo(() => {
        const isReady = isSignerReady(signer);

        if (isReady && !!signer) {
            return SharkContractFactory.connect(address, signer);
        }

        return SharkContractFactory.connect(address, provider);
    }, [address, isSignerReady, provider, signer]);
}