import { useWeb3React } from "@web3-react/core";
import { useCallback, useEffect } from "react";

import { connector } from "../connector";
import useAuthStore from "features/Auth/hooks/useAuthStore";
import { useWhoAmi } from "features/Auth/hooks/useWhoAmi";
import { removeToken } from "helpers/tokenStorage";
import { useNavigate } from "react-router-dom";
import { useQueryClient } from "react-query";

export const useAutoConnect = () => {
    const { connected, setConnect, setUser, isLoggedIn, setLoggedIn } = useAuthStore();
    const { refetch, ...whoami } = useWhoAmi();
    const { activate, active, account } = useWeb3React();
    const navigate = useNavigate();
    const queryClient = useQueryClient();

    useEffect(() => {
        const clearQueryCache = async () => {
            await queryClient.removeQueries();
        };
        if (!isLoggedIn || !connected) {
            clearQueryCache();
        }
    }, [isLoggedIn, connected]);

    const checkAuthorized = useCallback(async () => {
        const isAuthorized = await connector.isAuthorized();
        const response = whoami.data ? whoami : await refetch();

        if (!isAuthorized) {
            removeToken();
            await queryClient.removeQueries();
        }
        if (!isAuthorized || !isLoggedIn || response.isError) {
            // removeToken()
            setLoggedIn(false);
            setUser(null);
            return setConnect(false);
        }

        await activate(connector, undefined, true).catch(() => setConnect(false));

        // window.ethereum.on('accountsChanged', () => {
        //     navigate(0)
        // })

        if (account && whoami.data?.user) {
            const { user } = whoami.data;

            setUser({
                _id: user._id,
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                isEmailVerified: user.isEmailVerified,
                networkType: user.networkType,
                nonce: user.nonce,
                isVerification: user.isVerification,
                address: user.walletAddress,
                disputesPrefillEmail: user.disputesPrefillEmail,
                country: user.country
            });
        }

        setConnect(true);
    }, [whoami.data, refetch, isLoggedIn, activate, account, setConnect, setUser, navigate]);

    // intentionally only running on mount
    useEffect(() => {
        checkAuthorized();
    }, [checkAuthorized]);

    // if the connection worked, wait until we get confirmation
    useEffect(() => {
        if (connected || !active) return;

        setConnect(false);
    }, [connected, active, setConnect]);

    return connected;
};
