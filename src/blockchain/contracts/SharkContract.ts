import type {
    BaseContract,
    BigNumber,
    BigNumberish,
    BytesLike,
    CallOverrides,
    ContractTransaction,
    Overrides,
    PayableOverrides,
    PopulatedTransaction,
    Signer,
    utils,
} from "ethers";
import type {
    FunctionFragment,
    Result,
    EventFragment,
} from "@ethersproject/abi";
import type {Listener, Provider} from "@ethersproject/providers";
import type {
    TypedEventFilter,
    TypedEvent,
    TypedListener,
    OnEvent,
} from "../common";

export declare namespace SharkKeeper {
    export type SubTokenInfoStruct = {
        createdDate: BigNumberish;
        expiredDate: BigNumberish;
        parentTokenId: BigNumberish;
        licenseHash: string;
        minter: string;
        isExists: boolean;
    };

    export type SubTokenInfoStructOutput = [
        BigNumber,
        BigNumber,
        BigNumber,
        string,
        string,
        boolean
    ] & {
        createdDate: BigNumber;
        expiredDate: BigNumber;
        parentTokenId: BigNumber;
        licenseHash: string;
        minter: string;
        isExists: boolean;
    };

    export type TokenInfoStruct = {
        saleAmount: BigNumberish;
        saleMinterAmount: BigNumberish;
        saleMasterAmount: BigNumberish;
        parentTokenId: BigNumberish;
        version: BigNumberish;
        metadataHash: string;
        contentHash: string;
        licenseHash: string;
        minter: string;
        saleIsEnabled: boolean;
        createdDate: BigNumberish;
        isExists: boolean;
    };

    export type TokenInfoStructOutput = [
        BigNumber,
        BigNumber,
        BigNumber,
        BigNumber,
        BigNumber,
        string,
        string,
        string,
        string,
        boolean,
        BigNumber,
        boolean
    ] & {
        saleAmount: BigNumber;
        saleMinterAmount: BigNumber;
        saleMasterAmount: BigNumber;
        parentTokenId: BigNumber;
        version: BigNumber;
        metadataHash: string;
        contentHash: string;
        licenseHash: string;
        minter: string;
        saleIsEnabled: boolean;
        createdDate: BigNumber;
        isExists: boolean;
    };
}

export interface SharkContractInterface extends utils.Interface {
    functions: {
        "approve(address,uint256)": FunctionFragment;
        "balanceOf(address)": FunctionFragment;
        "buyCert(uint256)": FunctionFragment;
        "contentHash(uint256)": FunctionFragment;
        "createdDate(uint256)": FunctionFragment;
        "currentOwner(uint256)": FunctionFragment;
        "expiredDate(uint256)": FunctionFragment;
        "getApproved(uint256)": FunctionFragment;
        "getRequestPayed(uint256)": FunctionFragment;
        "getSubTokenInfo(uint256)": FunctionFragment;
        "getTokenInfo(uint256)": FunctionFragment;
        "getTransfersAllowed()": FunctionFragment;
        "isApprovedForAll(address,address)": FunctionFragment;
        "licenseHash(uint256)": FunctionFragment;
        "metaDataHash(uint256)": FunctionFragment;
        "mint(uint256,address,string,string,uint256,bytes,uint256,uint256)": FunctionFragment;
        "minter(uint256)": FunctionFragment;
        "name()": FunctionFragment;
        "owner()": FunctionFragment;
        "ownerOf(uint256)": FunctionFragment;
        "parentTokenId(uint256)": FunctionFragment;
        "payMonitoringRequest(uint256,uint256,uint256,bytes)": FunctionFragment;
        "payRequest(uint256,uint256,bytes)": FunctionFragment;
        "renounceOwnership()": FunctionFragment;
        "safeTransferFrom(address,address,uint256)": FunctionFragment;
        "safeTransferFrom(address,address,uint256,bytes)": FunctionFragment;
        "setApprovalForAll(address,bool)": FunctionFragment;
        "setLicenseHash(uint256,string)": FunctionFragment;
        "setPublicKey(address)": FunctionFragment;
        "setSale(uint256,bool,uint256,uint256,uint256,string,uint256,bytes)": FunctionFragment;
        "setTransfersAllowed(bool)": FunctionFragment;
        "supportsInterface(bytes4)": FunctionFragment;
        "symbol()": FunctionFragment;
        "tokenURI(uint256)": FunctionFragment;
        "transferFrom(address,address,uint256)": FunctionFragment;
        "transferOwnership(address)": FunctionFragment;
        "version(uint256)": FunctionFragment;
    };

    getFunction(
        nameOrSignatureOrTopic:
            | "approve"
            | "balanceOf"
            | "buyCert"
            | "contentHash"
            | "createdDate"
            | "currentOwner"
            | "expiredDate"
            | "getApproved"
            | "getRequestPayed"
            | "getSubTokenInfo"
            | "getTokenInfo"
            | "getTransfersAllowed"
            | "isApprovedForAll"
            | "licenseHash"
            | "metaDataHash"
            | "mint"
            | "minter"
            | "name"
            | "owner"
            | "ownerOf"
            | "parentTokenId"
            | "payMonitoringRequest"
            | "payRequest"
            | "renounceOwnership"
            | "safeTransferFrom(address,address,uint256)"
            | "safeTransferFrom(address,address,uint256,bytes)"
            | "setApprovalForAll"
            | "setLicenseHash"
            | "setPublicKey"
            | "setSale"
            | "setTransfersAllowed"
            | "supportsInterface"
            | "symbol"
            | "tokenURI"
            | "transferFrom"
            | "transferOwnership"
            | "version"
    ): FunctionFragment;

    encodeFunctionData(
        functionFragment: "approve",
        values: [string, BigNumberish]
    ): string;

    encodeFunctionData(functionFragment: "balanceOf", values: [string]): string;

    encodeFunctionData(
        functionFragment: "buyCert",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "contentHash",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "createdDate",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "currentOwner",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "expiredDate",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "getApproved",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "getRequestPayed",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "getSubTokenInfo",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "getTokenInfo",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "getTransfersAllowed",
        values?: undefined
    ): string;

    encodeFunctionData(
        functionFragment: "isApprovedForAll",
        values: [string, string]
    ): string;

    encodeFunctionData(
        functionFragment: "licenseHash",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "metaDataHash",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "mint",
        values: [
            BigNumberish,
            string,
            string,
            string,
            BigNumberish,
            BytesLike,
            BigNumberish,
            BigNumberish
        ]
    ): string;

    encodeFunctionData(
        functionFragment: "minter",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(functionFragment: "name", values?: undefined): string;

    encodeFunctionData(functionFragment: "owner", values?: undefined): string;

    encodeFunctionData(
        functionFragment: "ownerOf",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "parentTokenId",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "payMonitoringRequest",
        values: [BigNumberish, BigNumberish, BigNumberish, BytesLike]
    ): string;

    encodeFunctionData(
        functionFragment: "payRequest",
        values: [BigNumberish, BigNumberish, BytesLike]
    ): string;

    encodeFunctionData(
        functionFragment: "renounceOwnership",
        values?: undefined
    ): string;

    encodeFunctionData(
        functionFragment: "safeTransferFrom(address,address,uint256)",
        values: [string, string, BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "safeTransferFrom(address,address,uint256,bytes)",
        values: [string, string, BigNumberish, BytesLike]
    ): string;

    encodeFunctionData(
        functionFragment: "setApprovalForAll",
        values: [string, boolean]
    ): string;

    encodeFunctionData(
        functionFragment: "setLicenseHash",
        values: [BigNumberish, string]
    ): string;

    encodeFunctionData(
        functionFragment: "setPublicKey",
        values: [string]
    ): string;

    encodeFunctionData(
        functionFragment: "setSale",
        values: [
            BigNumberish,
            boolean,
            BigNumberish,
            BigNumberish,
            BigNumberish,
            string,
            BigNumberish,
            BytesLike
        ]
    ): string;

    encodeFunctionData(
        functionFragment: "setTransfersAllowed",
        values: [boolean]
    ): string;

    encodeFunctionData(
        functionFragment: "supportsInterface",
        values: [BytesLike]
    ): string;

    encodeFunctionData(functionFragment: "symbol", values?: undefined): string;

    encodeFunctionData(
        functionFragment: "tokenURI",
        values: [BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "transferFrom",
        values: [string, string, BigNumberish]
    ): string;

    encodeFunctionData(
        functionFragment: "transferOwnership",
        values: [string]
    ): string;

    encodeFunctionData(
        functionFragment: "version",
        values: [BigNumberish]
    ): string;

    decodeFunctionResult(functionFragment: "approve", data: BytesLike): Result;

    decodeFunctionResult(functionFragment: "balanceOf", data: BytesLike): Result;

    decodeFunctionResult(functionFragment: "buyCert", data: BytesLike): Result;

    decodeFunctionResult(
        functionFragment: "contentHash",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "createdDate",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "currentOwner",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "expiredDate",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "getApproved",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "getRequestPayed",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "getSubTokenInfo",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "getTokenInfo",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "getTransfersAllowed",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "isApprovedForAll",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "licenseHash",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "metaDataHash",
        data: BytesLike
    ): Result;

    decodeFunctionResult(functionFragment: "mint", data: BytesLike): Result;

    decodeFunctionResult(functionFragment: "minter", data: BytesLike): Result;

    decodeFunctionResult(functionFragment: "name", data: BytesLike): Result;

    decodeFunctionResult(functionFragment: "owner", data: BytesLike): Result;

    decodeFunctionResult(functionFragment: "ownerOf", data: BytesLike): Result;

    decodeFunctionResult(
        functionFragment: "parentTokenId",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "payMonitoringRequest",
        data: BytesLike
    ): Result;

    decodeFunctionResult(functionFragment: "payRequest", data: BytesLike): Result;

    decodeFunctionResult(
        functionFragment: "renounceOwnership",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "safeTransferFrom(address,address,uint256)",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "safeTransferFrom(address,address,uint256,bytes)",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "setApprovalForAll",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "setLicenseHash",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "setPublicKey",
        data: BytesLike
    ): Result;

    decodeFunctionResult(functionFragment: "setSale", data: BytesLike): Result;

    decodeFunctionResult(
        functionFragment: "setTransfersAllowed",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "supportsInterface",
        data: BytesLike
    ): Result;

    decodeFunctionResult(functionFragment: "symbol", data: BytesLike): Result;

    decodeFunctionResult(functionFragment: "tokenURI", data: BytesLike): Result;

    decodeFunctionResult(
        functionFragment: "transferFrom",
        data: BytesLike
    ): Result;

    decodeFunctionResult(
        functionFragment: "transferOwnership",
        data: BytesLike
    ): Result;

    decodeFunctionResult(functionFragment: "version", data: BytesLike): Result;

    events: {
        "Approval(address,address,uint256)": EventFragment;
        "ApprovalForAll(address,address,bool)": EventFragment;
        "Mint(address,uint256,bool)": EventFragment;
        "MonitoringRequestPayed(uint256,uint256)": EventFragment;
        "OwnershipTransferred(address,address)": EventFragment;
        "RequestPayed(uint256)": EventFragment;
        "Transfer(address,address,uint256)": EventFragment;
    };

    getEvent(nameOrSignatureOrTopic: "Approval"): EventFragment;

    getEvent(nameOrSignatureOrTopic: "ApprovalForAll"): EventFragment;

    getEvent(nameOrSignatureOrTopic: "Mint"): EventFragment;

    getEvent(nameOrSignatureOrTopic: "MonitoringRequestPayed"): EventFragment;

    getEvent(nameOrSignatureOrTopic: "OwnershipTransferred"): EventFragment;

    getEvent(nameOrSignatureOrTopic: "RequestPayed"): EventFragment;

    getEvent(nameOrSignatureOrTopic: "Transfer"): EventFragment;
}

export interface ApprovalEventObject {
    owner: string;
    approved: string;
    tokenId: BigNumber;
}

export type ApprovalEvent = TypedEvent<[string, string, BigNumber],
    ApprovalEventObject>;

export type ApprovalEventFilter = TypedEventFilter<ApprovalEvent>;

export interface ApprovalForAllEventObject {
    owner: string;
    operator: string;
    approved: boolean;
}

export type ApprovalForAllEvent = TypedEvent<[string, string, boolean],
    ApprovalForAllEventObject>;

export type ApprovalForAllEventFilter = TypedEventFilter<ApprovalForAllEvent>;

export interface MintEventObject {
    to: string;
    tokenId: BigNumber;
    isSubToken: boolean;
}

export type MintEvent = TypedEvent<[string, BigNumber, boolean],
    MintEventObject>;

export type MintEventFilter = TypedEventFilter<MintEvent>;

export interface MonitoringRequestPayedEventObject {
    requestId: BigNumber;
    scansTotal: BigNumber;
}

export type MonitoringRequestPayedEvent = TypedEvent<[BigNumber, BigNumber],
    MonitoringRequestPayedEventObject>;

export type MonitoringRequestPayedEventFilter =
    TypedEventFilter<MonitoringRequestPayedEvent>;

export interface OwnershipTransferredEventObject {
    previousOwner: string;
    newOwner: string;
}

export type OwnershipTransferredEvent = TypedEvent<[string, string],
    OwnershipTransferredEventObject>;

export type OwnershipTransferredEventFilter =
    TypedEventFilter<OwnershipTransferredEvent>;

export interface RequestPayedEventObject {
    requestId: BigNumber;
}

export type RequestPayedEvent = TypedEvent<[BigNumber],
    RequestPayedEventObject>;

export type RequestPayedEventFilter = TypedEventFilter<RequestPayedEvent>;

export interface TransferEventObject {
    from: string;
    to: string;
    tokenId: BigNumber;
}

export type TransferEvent = TypedEvent<[string, string, BigNumber],
    TransferEventObject>;

export type TransferEventFilter = TypedEventFilter<TransferEvent>;

export interface SharkContract extends BaseContract {
    connect(signerOrProvider: Signer | Provider | string): this;
    attach(addressOrName: string): this;
    deployed(): Promise<this>;

    interface: SharkContractInterface;

    queryFilter<TEvent extends TypedEvent>(
        event: TypedEventFilter<TEvent>,
        fromBlockOrBlockhash?: string | number | undefined,
        toBlock?: string | number | undefined
    ): Promise<Array<TEvent>>;

    listeners<TEvent extends TypedEvent>(
        eventFilter?: TypedEventFilter<TEvent>
    ): Array<TypedListener<TEvent>>;
    listeners(eventName?: string): Array<Listener>;
    removeAllListeners<TEvent extends TypedEvent>(
        eventFilter: TypedEventFilter<TEvent>
    ): this;
    removeAllListeners(eventName?: string): this;
    off: OnEvent<this>;
    on: OnEvent<this>;
    once: OnEvent<this>;
    removeListener: OnEvent<this>;

    functions: {
        approve(
            to: string,
            tokenId: BigNumberish,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        balanceOf(owner: string, overrides?: CallOverrides): Promise<[BigNumber]>;

        buyCert(
            tokenId: BigNumberish,
            overrides?: PayableOverrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        contentHash(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<[string]>;

        createdDate(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<[BigNumber]>;

        currentOwner(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<[string]>;

        expiredDate(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<[BigNumber]>;

        getApproved(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<[string]>;

        getRequestPayed(
            requestId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<[boolean]>;

        getSubTokenInfo(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<[SharkKeeper.SubTokenInfoStructOutput]>;

        getTokenInfo(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<[SharkKeeper.TokenInfoStructOutput]>;

        getTransfersAllowed(overrides?: CallOverrides): Promise<[boolean]>;

        isApprovedForAll(
            owner: string,
            operator: string,
            overrides?: CallOverrides
        ): Promise<[boolean]>;

        licenseHash(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<[string]>;

        metaDataHash(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<[string]>;

        mint(
            tokenId: BigNumberish,
            to: string,
            contentHash_: string,
            metaDataHash_: string,
            expire: BigNumberish,
            signature: BytesLike,
            parentTokenId: BigNumberish,
            version: BigNumberish,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        minter(tokenId: BigNumberish, overrides?: CallOverrides): Promise<[string]>;

        name(overrides?: CallOverrides): Promise<[string]>;

        owner(overrides?: CallOverrides): Promise<[string]>;

        ownerOf(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<[string]>;

        parentTokenId(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<[BigNumber]>;

        payMonitoringRequest(
            requestId: BigNumberish,
            scansTotal: BigNumberish,
            expire: BigNumberish,
            signature: BytesLike,
            overrides?: PayableOverrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        payRequest(
            requestId: BigNumberish,
            expire: BigNumberish,
            signature: BytesLike,
            overrides?: PayableOverrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        renounceOwnership(
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        "safeTransferFrom(address,address,uint256)"(
            from: string,
            to: string,
            tokenId: BigNumberish,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        "safeTransferFrom(address,address,uint256,bytes)"(
            from: string,
            to: string,
            tokenId: BigNumberish,
            _data: BytesLike,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        setApprovalForAll(
            operator: string,
            approved: boolean,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        setLicenseHash(
            tokenId: BigNumberish,
            licenseHash_: string,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        setPublicKey(
            publicKey_: string,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        setSale(
            tokenId: BigNumberish,
            isEnabled: boolean,
            amount: BigNumberish,
            minterAmount: BigNumberish,
            masterAmount: BigNumberish,
            licenseHash_: string,
            expire: BigNumberish,
            signature: BytesLike,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        setTransfersAllowed(
            canTransfer: boolean,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        supportsInterface(
            interfaceId: BytesLike,
            overrides?: CallOverrides
        ): Promise<[boolean]>;

        symbol(overrides?: CallOverrides): Promise<[string]>;

        tokenURI(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<[string]>;

        transferFrom(
            from: string,
            to: string,
            tokenId: BigNumberish,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        transferOwnership(
            newOwner: string,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<ContractTransaction>;

        version(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<[BigNumber]>;
    };

    approve(
        to: string,
        tokenId: BigNumberish,
        overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    balanceOf(owner: string, overrides?: CallOverrides): Promise<BigNumber>;

    buyCert(
        tokenId: BigNumberish,
        overrides?: PayableOverrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    contentHash(
        tokenId: BigNumberish,
        overrides?: CallOverrides
    ): Promise<string>;

    createdDate(
        tokenId: BigNumberish,
        overrides?: CallOverrides
    ): Promise<BigNumber>;

    currentOwner(
        tokenId: BigNumberish,
        overrides?: CallOverrides
    ): Promise<string>;

    expiredDate(
        tokenId: BigNumberish,
        overrides?: CallOverrides
    ): Promise<BigNumber>;

    getApproved(
        tokenId: BigNumberish,
        overrides?: CallOverrides
    ): Promise<string>;

    getRequestPayed(
        requestId: BigNumberish,
        overrides?: CallOverrides
    ): Promise<boolean>;

    getSubTokenInfo(
        tokenId: BigNumberish,
        overrides?: CallOverrides
    ): Promise<SharkKeeper.SubTokenInfoStructOutput>;

    getTokenInfo(
        tokenId: BigNumberish,
        overrides?: CallOverrides
    ): Promise<SharkKeeper.TokenInfoStructOutput>;

    getTransfersAllowed(overrides?: CallOverrides): Promise<boolean>;

    isApprovedForAll(
        owner: string,
        operator: string,
        overrides?: CallOverrides
    ): Promise<boolean>;

    licenseHash(
        tokenId: BigNumberish,
        overrides?: CallOverrides
    ): Promise<string>;

    metaDataHash(
        tokenId: BigNumberish,
        overrides?: CallOverrides
    ): Promise<string>;

    mint(
        tokenId: BigNumberish,
        to: string,
        contentHash_: string,
        metaDataHash_: string,
        expire: BigNumberish,
        signature: BytesLike,
        parentTokenId: BigNumberish,
        version: BigNumberish,
        overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    minter(tokenId: BigNumberish, overrides?: CallOverrides): Promise<string>;

    name(overrides?: CallOverrides): Promise<string>;

    owner(overrides?: CallOverrides): Promise<string>;

    ownerOf(tokenId: BigNumberish, overrides?: CallOverrides): Promise<string>;

    parentTokenId(
        tokenId: BigNumberish,
        overrides?: CallOverrides
    ): Promise<BigNumber>;

    payMonitoringRequest(
        requestId: BigNumberish,
        scansTotal: BigNumberish,
        expire: BigNumberish,
        signature: BytesLike,
        overrides?: PayableOverrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    payRequest(
        requestId: BigNumberish,
        expire: BigNumberish,
        signature: BytesLike,
        overrides?: PayableOverrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    renounceOwnership(
        overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    "safeTransferFrom(address,address,uint256)"(
        from: string,
        to: string,
        tokenId: BigNumberish,
        overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    "safeTransferFrom(address,address,uint256,bytes)"(
        from: string,
        to: string,
        tokenId: BigNumberish,
        _data: BytesLike,
        overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    setApprovalForAll(
        operator: string,
        approved: boolean,
        overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    setLicenseHash(
        tokenId: BigNumberish,
        licenseHash_: string,
        overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    setPublicKey(
        publicKey_: string,
        overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    setSale(
        tokenId: BigNumberish,
        isEnabled: boolean,
        amount: BigNumberish,
        minterAmount: BigNumberish,
        masterAmount: BigNumberish,
        licenseHash_: string,
        expire: BigNumberish,
        signature: BytesLike,
        overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    setTransfersAllowed(
        canTransfer: boolean,
        overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    supportsInterface(
        interfaceId: BytesLike,
        overrides?: CallOverrides
    ): Promise<boolean>;

    symbol(overrides?: CallOverrides): Promise<string>;

    tokenURI(tokenId: BigNumberish, overrides?: CallOverrides): Promise<string>;

    transferFrom(
        from: string,
        to: string,
        tokenId: BigNumberish,
        overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    transferOwnership(
        newOwner: string,
        overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    version(tokenId: BigNumberish, overrides?: CallOverrides): Promise<BigNumber>;

    callStatic: {
        approve(
            to: string,
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<void>;

        balanceOf(owner: string, overrides?: CallOverrides): Promise<BigNumber>;

        buyCert(tokenId: BigNumberish, overrides?: CallOverrides): Promise<void>;

        contentHash(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<string>;

        createdDate(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        currentOwner(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<string>;

        expiredDate(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        getApproved(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<string>;

        getRequestPayed(
            requestId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<boolean>;

        getSubTokenInfo(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<SharkKeeper.SubTokenInfoStructOutput>;

        getTokenInfo(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<SharkKeeper.TokenInfoStructOutput>;

        getTransfersAllowed(overrides?: CallOverrides): Promise<boolean>;

        isApprovedForAll(
            owner: string,
            operator: string,
            overrides?: CallOverrides
        ): Promise<boolean>;

        licenseHash(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<string>;

        metaDataHash(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<string>;

        mint(
            tokenId: BigNumberish,
            to: string,
            contentHash_: string,
            metaDataHash_: string,
            expire: BigNumberish,
            signature: BytesLike,
            parentTokenId: BigNumberish,
            version: BigNumberish,
            overrides?: CallOverrides
        ): Promise<void>;

        minter(tokenId: BigNumberish, overrides?: CallOverrides): Promise<string>;

        name(overrides?: CallOverrides): Promise<string>;

        owner(overrides?: CallOverrides): Promise<string>;

        ownerOf(tokenId: BigNumberish, overrides?: CallOverrides): Promise<string>;

        parentTokenId(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        payMonitoringRequest(
            requestId: BigNumberish,
            scansTotal: BigNumberish,
            expire: BigNumberish,
            signature: BytesLike,
            overrides?: CallOverrides
        ): Promise<void>;

        payRequest(
            requestId: BigNumberish,
            expire: BigNumberish,
            signature: BytesLike,
            overrides?: CallOverrides
        ): Promise<void>;

        renounceOwnership(overrides?: CallOverrides): Promise<void>;

        "safeTransferFrom(address,address,uint256)"(
            from: string,
            to: string,
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<void>;

        "safeTransferFrom(address,address,uint256,bytes)"(
            from: string,
            to: string,
            tokenId: BigNumberish,
            _data: BytesLike,
            overrides?: CallOverrides
        ): Promise<void>;

        setApprovalForAll(
            operator: string,
            approved: boolean,
            overrides?: CallOverrides
        ): Promise<void>;

        setLicenseHash(
            tokenId: BigNumberish,
            licenseHash_: string,
            overrides?: CallOverrides
        ): Promise<void>;

        setPublicKey(publicKey_: string, overrides?: CallOverrides): Promise<void>;

        setSale(
            tokenId: BigNumberish,
            isEnabled: boolean,
            amount: BigNumberish,
            minterAmount: BigNumberish,
            masterAmount: BigNumberish,
            licenseHash_: string,
            expire: BigNumberish,
            signature: BytesLike,
            overrides?: CallOverrides
        ): Promise<void>;

        setTransfersAllowed(
            canTransfer: boolean,
            overrides?: CallOverrides
        ): Promise<void>;

        supportsInterface(
            interfaceId: BytesLike,
            overrides?: CallOverrides
        ): Promise<boolean>;

        symbol(overrides?: CallOverrides): Promise<string>;

        tokenURI(tokenId: BigNumberish, overrides?: CallOverrides): Promise<string>;

        transferFrom(
            from: string,
            to: string,
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<void>;

        transferOwnership(
            newOwner: string,
            overrides?: CallOverrides
        ): Promise<void>;

        version(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;
    };

    filters: {
        "Approval(address,address,uint256)"(
            owner?: string | null,
            approved?: string | null,
            tokenId?: BigNumberish | null
        ): ApprovalEventFilter;
        Approval(
            owner?: string | null,
            approved?: string | null,
            tokenId?: BigNumberish | null
        ): ApprovalEventFilter;

        "ApprovalForAll(address,address,bool)"(
            owner?: string | null,
            operator?: string | null,
            approved?: null
        ): ApprovalForAllEventFilter;
        ApprovalForAll(
            owner?: string | null,
            operator?: string | null,
            approved?: null
        ): ApprovalForAllEventFilter;

        "Mint(address,uint256,bool)"(
            to?: null,
            tokenId?: null,
            isSubToken?: null
        ): MintEventFilter;
        Mint(to?: null, tokenId?: null, isSubToken?: null): MintEventFilter;

        "MonitoringRequestPayed(uint256,uint256)"(
            requestId?: null,
            scansTotal?: null
        ): MonitoringRequestPayedEventFilter;
        MonitoringRequestPayed(
            requestId?: null,
            scansTotal?: null
        ): MonitoringRequestPayedEventFilter;

        "OwnershipTransferred(address,address)"(
            previousOwner?: string | null,
            newOwner?: string | null
        ): OwnershipTransferredEventFilter;
        OwnershipTransferred(
            previousOwner?: string | null,
            newOwner?: string | null
        ): OwnershipTransferredEventFilter;

        "RequestPayed(uint256)"(requestId?: null): RequestPayedEventFilter;
        RequestPayed(requestId?: null): RequestPayedEventFilter;

        "Transfer(address,address,uint256)"(
            from?: string | null,
            to?: string | null,
            tokenId?: BigNumberish | null
        ): TransferEventFilter;
        Transfer(
            from?: string | null,
            to?: string | null,
            tokenId?: BigNumberish | null
        ): TransferEventFilter;
    };

    estimateGas: {
        approve(
            to: string,
            tokenId: BigNumberish,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        balanceOf(owner: string, overrides?: CallOverrides): Promise<BigNumber>;

        buyCert(
            tokenId: BigNumberish,
            overrides?: PayableOverrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        contentHash(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        createdDate(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        currentOwner(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        expiredDate(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        getApproved(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        getRequestPayed(
            requestId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        getSubTokenInfo(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        getTokenInfo(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        getTransfersAllowed(overrides?: CallOverrides): Promise<BigNumber>;

        isApprovedForAll(
            owner: string,
            operator: string,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        licenseHash(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        metaDataHash(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        mint(
            tokenId: BigNumberish,
            to: string,
            contentHash_: string,
            metaDataHash_: string,
            expire: BigNumberish,
            signature: BytesLike,
            parentTokenId: BigNumberish,
            version: BigNumberish,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        minter(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        name(overrides?: CallOverrides): Promise<BigNumber>;

        owner(overrides?: CallOverrides): Promise<BigNumber>;

        ownerOf(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        parentTokenId(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        payMonitoringRequest(
            requestId: BigNumberish,
            scansTotal: BigNumberish,
            expire: BigNumberish,
            signature: BytesLike,
            overrides?: PayableOverrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        payRequest(
            requestId: BigNumberish,
            expire: BigNumberish,
            signature: BytesLike,
            overrides?: PayableOverrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        renounceOwnership(
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        "safeTransferFrom(address,address,uint256)"(
            from: string,
            to: string,
            tokenId: BigNumberish,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        "safeTransferFrom(address,address,uint256,bytes)"(
            from: string,
            to: string,
            tokenId: BigNumberish,
            _data: BytesLike,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        setApprovalForAll(
            operator: string,
            approved: boolean,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        setLicenseHash(
            tokenId: BigNumberish,
            licenseHash_: string,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        setPublicKey(
            publicKey_: string,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        setSale(
            tokenId: BigNumberish,
            isEnabled: boolean,
            amount: BigNumberish,
            minterAmount: BigNumberish,
            masterAmount: BigNumberish,
            licenseHash_: string,
            expire: BigNumberish,
            signature: BytesLike,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        setTransfersAllowed(
            canTransfer: boolean,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        supportsInterface(
            interfaceId: BytesLike,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        symbol(overrides?: CallOverrides): Promise<BigNumber>;

        tokenURI(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;

        transferFrom(
            from: string,
            to: string,
            tokenId: BigNumberish,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        transferOwnership(
            newOwner: string,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<BigNumber>;

        version(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<BigNumber>;
    };

    populateTransaction: {
        approve(
            to: string,
            tokenId: BigNumberish,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        balanceOf(
            owner: string,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        buyCert(
            tokenId: BigNumberish,
            overrides?: PayableOverrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        contentHash(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        createdDate(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        currentOwner(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        expiredDate(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        getApproved(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        getRequestPayed(
            requestId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        getSubTokenInfo(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        getTokenInfo(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        getTransfersAllowed(
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        isApprovedForAll(
            owner: string,
            operator: string,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        licenseHash(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        metaDataHash(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        mint(
            tokenId: BigNumberish,
            to: string,
            contentHash_: string,
            metaDataHash_: string,
            expire: BigNumberish,
            signature: BytesLike,
            parentTokenId: BigNumberish,
            version: BigNumberish,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        minter(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        name(overrides?: CallOverrides): Promise<PopulatedTransaction>;

        owner(overrides?: CallOverrides): Promise<PopulatedTransaction>;

        ownerOf(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        parentTokenId(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        payMonitoringRequest(
            requestId: BigNumberish,
            scansTotal: BigNumberish,
            expire: BigNumberish,
            signature: BytesLike,
            overrides?: PayableOverrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        payRequest(
            requestId: BigNumberish,
            expire: BigNumberish,
            signature: BytesLike,
            overrides?: PayableOverrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        renounceOwnership(
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        "safeTransferFrom(address,address,uint256)"(
            from: string,
            to: string,
            tokenId: BigNumberish,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        "safeTransferFrom(address,address,uint256,bytes)"(
            from: string,
            to: string,
            tokenId: BigNumberish,
            _data: BytesLike,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        setApprovalForAll(
            operator: string,
            approved: boolean,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        setLicenseHash(
            tokenId: BigNumberish,
            licenseHash_: string,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        setPublicKey(
            publicKey_: string,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        setSale(
            tokenId: BigNumberish,
            isEnabled: boolean,
            amount: BigNumberish,
            minterAmount: BigNumberish,
            masterAmount: BigNumberish,
            licenseHash_: string,
            expire: BigNumberish,
            signature: BytesLike,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        setTransfersAllowed(
            canTransfer: boolean,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        supportsInterface(
            interfaceId: BytesLike,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        symbol(overrides?: CallOverrides): Promise<PopulatedTransaction>;

        tokenURI(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;

        transferFrom(
            from: string,
            to: string,
            tokenId: BigNumberish,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        transferOwnership(
            newOwner: string,
            overrides?: Overrides & { from?: string | Promise<string> }
        ): Promise<PopulatedTransaction>;

        version(
            tokenId: BigNumberish,
            overrides?: CallOverrides
        ): Promise<PopulatedTransaction>;
    };
}
  