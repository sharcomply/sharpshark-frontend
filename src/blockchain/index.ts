export type { SharkContract } from "./contracts/SharkContract";
export * as factories from "./contracts/SharkContract";
export { SharkContractFactory } from "./factories/SharkContractFactory";
