import { InjectedConnector } from '@web3-react/injected-connector'
import { ChainId } from '../constants/networks';

export const connector = new InjectedConnector({
    supportedChainIds: [ChainId.GOERLI_TESTNET, ChainId.BSC_TESTNET, ChainId.POLYGON_MAINNET]
})
