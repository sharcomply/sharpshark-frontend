interface WhoAmiResponse {
    websocket: string;
    data: {
        _id: string;
        createdAt: string;
        firstName: string;
        isVerification: boolean;
        lastName: string;
        networkType: string;
        nonce: string;
        updatedAt: string;
        walletAddress: string;
        walletAddressNear: string;
        email: string;
        isEmailVerified: boolean;
        disputesPrefillEmail?: string;
        country?: string;
    };
    counts: {
        drafts: number;
        protects: number;
        alerts: number;
        unreadAlerts: number;
        disputes: number;
    };
}

interface GetCountriesResponse {
    name: string;
    code: string;
}

