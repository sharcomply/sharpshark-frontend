interface Disput {
    _id: string;
    alertId: string;
    demands?: string;
    appearedOn: string;
    payAmount: number;
    publicationQuestionLinks: string[];
    publicationQuestionWebarchiveLinks: string[];
    isPublicationQuestionWebarchiveFailed: boolean;
    documentId: Draft;
    publicationLinks?: string[];
    publicationWebarchiveLinks?: string[];
    isPublicationWebarchiveFailed?: boolean;
    createdAt: string;
    ownerClaimText?: string;
    providerClaimEmail?: string;
    providerClaimText?: string;

    ownerClaimEmails?: string[];
    ownerClaimType?: string;

    googleClaimText?: string;

    providerClaimEmails?: string;
    isNotRelevant?: boolean;
    status?: string;
}

type DisputesListItem = Omit<Disput, "alertId"> & { alertId: Alert };

interface DisputeAppearedOns {
    appearedOn: string;
    disputes: DisputesListItem[];
}
interface DisputesGroupingResponse {
    appearedOns: DisputeAppearedOns[];
    documentId: Draft;
}

interface SaveDispute {
    publicationQuestionScreenshots?: File[];
    draftsScreenshots?: File[];
    ownerClaimText?: string;
    providerClaimText?: string;
    ownerClaimEmails?: string[];
    ownerClaimType?: string;

    providerClaimEmails?: string;
    googleClaimText?: string;
    status?: string;
    publicationLinks?: string[];
    publicationWebarchiveLinks?: string[];
    isPublicationWebarchiveFailed?: boolean;
    alertId?: string;
    demands?: string;
    appearedOn?: string;
    payAmount?: number;
    publicationQuestionLinks?: string[];
    publicationQuestionWebarchiveLinks?: string[];
    isPublicationQuestionWebarchiveFailed?: boolean;
}

interface EditDispute {
    _id: string;
    publicationQuestionScreenshots?: string[];
    draftsScreenshots?: string[];
    ownerClaimText?: string;
    providerClaimText?: string;
    googleClaimText?: string;
    ownerClaimEmails?: string[];
    ownerClaimType?: string;

    providerClaimEmails?: string;

    status?: string;
    publicationLinks?: string[];
    publicationWebarchiveLinks?: string[];
    isPublicationWebarchiveFailed?: boolean;
    alertId?: string;
    demands?: string;
    appearedOn?: string;
    payAmount?: number;
    publicationQuestionLinks?: string[];
    publicationQuestionWebarchiveLinks?: string[];
    isPublicationQuestionWebarchiveFailed?: boolean;
    isNotRelevant?: boolean;
}

interface ItemDispute extends Disput {
    publicationQuestionScreenshots?: string[];
    draftsScreenshots?: string[];
}

interface GetDisputeParams {
    status?: "Draft: Website Owner" | "Draft: Provider" | "Draft: Google";
    documentId?: string;
}

interface FindEmails {
    emails: string[];
}

interface DisputesCountersList {
    Closed: number;
    "Draft: Provider": number;
    "Claim Sent: Provider": number;
    "Draft: Google": number;
    "Draft: Website Owner": number;
    "Letter Sent: Website Owner": number;
    "In Progress: Website Owner": number;
    "In Progress: Google": number;
    "In Progress: Provider": number;
    "Claim Sent: Website Owner": number;
    "Claim Sent: Google": number;
    "Letter Sent: Provider": number;
    "Letter Sent: Google": number;
}

