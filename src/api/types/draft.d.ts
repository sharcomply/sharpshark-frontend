interface Draft {
    _id: string;
    title: string;
    type?: "image" | "text";
    isMinted: boolean;
    tokenId: string;
    linkUrl: string;
    ownerId: string;
    version: number;
    commonId?: string;
    authors: string[];
    additionalImages: string[];
    coAuthors: string[];
    participants: string[];
    createdAt: string;
    updatedAt: string;
    metadataHash: string;
    metadataUrl: string;
    parentId?: string;
    parentTokenId: string;
    sourceHash: string;
    blockchainTokenUrlLink: string;
    blockchainTokenUrlQrCodeLink: string;
    linkUrlQrCodeLink: string;
    metadataUrlQrCodeLink: string;
    mintTransactionHash: string;
    mintTransactionLink: string;
    mintTransactionQrCodeLink: string;
    text?: string;
    imageFile?: Blob;
    fullTitle: string;
    mintedNetworks: Array<any>;
}

interface DeletedDraft extends Draft {
    deleteDate: string;
    isDeleted: boolean;
}

interface DraftDetails extends Draft {
    activeMonitor: any;
    deleteDate?: string;
    isDeleted?: boolean;
    metadata?: any;
    cachedMetadata?: any;
    cachedSource?: any;
    versionsHistory?: Record<number, string>;
    versionsHistoryObjects?: Record<number, Draft>;
}
