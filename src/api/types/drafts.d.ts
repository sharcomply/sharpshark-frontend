interface SaveDraftParams {
    title: string;
    fullTitle: string;
    linkUrl?: string;
    type?: "image" | "binaryFile" | "text";
    authors?: string[];
    coAuthors?: string[];
    participants?: string[];
    parentId?: string;
    additionalImages?: string[]
}

interface EditDraftParams {
    _id: string;
    title: string;
    linkUrl?: string;
    fullTitle?: string;
    linkUrl?: string;
    type?: "image" | "binaryFile" | "text";
    authors?: string[];
    coAuthors?: string[];
    participants?: string[];
    additionalImages?: string[]
}

interface GetDraftParams {
    isMinted?: boolean;
    commonId?: string;
}

interface GetUploadLink {
    downloadLink: string;
    uploadLink: {
        url: string;
        fields: any;
    };
}

interface RecoverDraftParams {
    draftId?: string;
}

interface SetupMonitoringParams {
    draftId: string;
    scansTotal: number;
    network?: string
}

interface SetupMonitoringResult {
    requestId: string;
    toBlockchain: {
        requestId: string,
        scansTotal: number,
        scans_total?: number,
        expire: number,
        signature: string,
        priceInWei: string
        v?: number
    }

}
interface SaleSignature {
    amount: string
    expire: number
    isEnabled: boolean
    licenseHash: string
    masterAmount: string
    minterAmount: string
    signature: number[] | string
    tokenId: string
    network: string
    token_id: string,
    is_enabled: boolean,
    minter_amount: string,
    master_amount: string,
    license_hash: string,
    v: number,
}

interface CertificatePaymentLinkParams {
    id: string
    network: string;
    successUrl: string;
    cancelUrl: string;
}

interface CertificatePaymentLinkResult {
    url: string;
}

interface ConvertFromWeiParams {
    amount: string
}

interface ConvertFromWeiResult {
    bsc: string;
    eth: string;
    polygon: string;
    near: string;
}

