interface LoginCodeParams {
    network?: string;
    address: string;
}

interface LoginCodeResponse {
    codeToSign: string;
}

interface ConfirmLoginCodeParams {
    code: string;
    signature: string;
    address: string;
    network: string;
}

interface ConfirmLoginCodeResponse {
    authToken: string;
}

interface ConfirmEmailParams {
    token: string;
}

interface ConfirmEmailResponse {
    isEmailVerified: boolean;
    email: string;
}

interface SendEmailVerifyParams {
    email: string;
}

interface ConfirmKYCVerification {
    verification_url: string;
}

interface SendKYCVerification {
    email?: string;
    country?: string;
    language?: string;
}

interface SendEmailVerifyResponse {
    isEmailVerified: boolean;
    email: string;
}
