interface SimilarContent {
  url: string;
  title: string;
  introduction: string;
  similarity: number;
  _id: string;
}

interface ScanId {
  _id: string;
  ownerId: string;
  documentId: string;
  purpose: string;
  aggregatedScore: number;
  isRequestPayed: boolean;
  similarContent: SimilarContent[];
  __v: number;
}

interface Alert {
  _id: string;
  type: string;
  scanId: ScanId;
  ownerId: string;
  documentId: Draft;
  date: string;
  isRead: boolean;
  report: string;
  updatedAt: string;
  isReacted: boolean;
  isSkipped: boolean;
  isRead: boolean;
  __v: number;
}

interface GetAlertParams {
  isSkipped?: boolean;
  documentId?: string | null;
}