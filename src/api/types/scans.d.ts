interface StartScanParams {
    documentId: string;
    network?: string
}

interface EstimateScanParams {
    documentId: string;
}

interface EstimateDocumentResponse {
    success: boolean,
    statusCode: string
}

interface StartScanResponse {
    requestId: string;
    scanId?: string,
}

interface StartScanBlockchainResponse {
    requestId: string;
    scanId?: string,
    toBlockchain: {
        priceInWei: string,
        expire: number | string,
        requestId: string,
        signature: string | number[],
        v?: number
    },


}


interface GetScanPaymentLink {
    url: string;
}

interface ScanPaymentLinkParams {
    requestId: string;
    successUrl: string;
    cancelUrl: string;
    network: string
}

interface MonitoringPaymentLinkParams {
    requestId: string;
    successUrl: string;
    cancelUrl: string;
}

interface GetScanParams {
    scanId: string;
}

interface GetScanResponse {
    _id: string;
    requestId: string;
    ownerId: string;
    documentId: string;
    aggregatedScore: number;
    isRequestPayed: boolean;
    similarContent: ScanSimilarContent[];
}

interface ScanSimilarContent {
    _id: string;
    url: string;
    title: string;
    similarity: number;
    introduction: string;
}

interface UploadFileParams {
    file: File;
}

interface UploadFileResponse {
    url: string;
}

interface LicenseParametersResponse {
    id: string;
    amountWei: string;
    licenseHash: string;
    network: string
}