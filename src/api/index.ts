import ky, { BeforeErrorHook, BeforeRequestHook } from "ky";
import { getToken } from "helpers/tokenStorage";
import * as auth from "./requests/auth";
import * as scans from "./requests/scans";
import * as drafts from "./requests/drafts";
import * as constants from "./requests/constants";
import * as alerts from "./requests/alerts";
import * as disputes from "./requests/disputes"
import { handleError } from "features/ErrorHandler";

export const createApi = () => {
    const authRequest: BeforeRequestHook = (request) => {
        const token = getToken();
        if (token) {
            request.headers.append("AUTHORIZATION", `${token}`);
        }

        return request;
    };

    const errorRequest: BeforeErrorHook = (error) => {
        const { response } = error;
    
        if (response.status < 500) return error;
        response.json().then((data: any) => {
            handleError({
                id: `${response.status}:${response.url}`,
                title: "Unexpected Error",
                message: "Something went wrong",
                details: data?.data?.message,
            })
        })
        return error;
    }

    return ky.create({
        retry: 0,
        timeout: false,
        hooks: {
            beforeRequest: [authRequest],
            beforeError: [errorRequest],
        },
        
    });
};

export const backend = {
    auth,
    scans,
    drafts,
    alerts,
    constants,
    disputes
};
