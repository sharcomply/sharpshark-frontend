import {api} from "index";
import {LocaleResponseType} from "../types/locales";

export const fetchLocales = async () => {

    const response = await api
        .get(`/api/v1/public/get-locales`)
        .json<ApiResponseList<LocaleResponseType>>();

    return response.data

};
