import { api } from '../../index';
// Errors Codes
// const ErrorCodes = {
//
// }

export const startScan = async (params: StartScanParams): Promise<StartScanBlockchainResponse> => {

    const query = new URLSearchParams({
        documentId: params.documentId,
    });

    const searchParams = new URLSearchParams(window.location.search);

    if (searchParams.has("secret")) {
        query.append("secret", searchParams.get("secret")!)
    }

    const response = await api
        .get(`/api/v1/user/start-scan?${query}&network=${params.network}`)
        .json<ApiResponse<StartScanBlockchainResponse>>();


    return {
        requestId: response.data.requestId,
        scanId: response.data.scanId,
        toBlockchain: response.data.toBlockchain
    };

};

export const startEstimateScan = async (params: StartScanResponse) => {

    const query = new URLSearchParams({
        requestId: params.requestId
    });

    await api
        .get(`/api/v1/user/estimate-scan-pages?${query}`)
        .json<ApiResponse<any>>();
};

export const getScanPaymentLink = async (json: ScanPaymentLinkParams) => {
    const response = await api
        .post(`/api/v1/user/get-scan-payment-link`, { json })
        .json<ApiResponse<GetScanPaymentLink>>();

    return {
        url: response.data.url
    };
};

export const continueFromTransactionHash = async (transactionHash: string, accountId: string) => {
    const response = await api.get(`/api/v1/public/continue-from-transaction-hash/${transactionHash}?accountId=${accountId}`)

    return response
}

export const startEstimateDocument = async (params: EstimateScanParams) => {
    const response = await api.get(`/api/v1/user/estimate-document-pages?documentId=${params.documentId}`)
        .json<ApiResponse<EstimateDocumentResponse>>();

    return {
        success: response.success
    }
}



export const getScan = async (id?: string): Promise<GetScanResponse> => {
    const response = await api.get(`/api/v1/user/get-scans/${id}`).json<ApiResponse<GetScanResponse>>();

    if (!response.data) throw Error('Scan Not Found')

    return {
        _id: response.data._id,
        requestId: response.data.requestId,
        ownerId: response.data.ownerId,
        documentId: response.data.documentId,
        aggregatedScore: response.data.aggregatedScore,
        isRequestPayed: response.data.isRequestPayed,
        similarContent: response.data.similarContent,
    };
};

export const uploadFile = async (params: UploadFileParams) => {

    const body = new FormData();
    body.append('file', params.file);

    const response = await api
        .post(`/api/v1/user/upload-file`, { body })
        .json<ApiResponse<UploadFileResponse>>()

    return {
        url: response.data.url
    };

};

export const getMonitoringPaymentLink = async (json: MonitoringPaymentLinkParams) => {

    const response = await api
        .post(`/api/v1/user/get-monitoring-payment-link`, { json })
        .json<ApiResponse<GetScanPaymentLink>>()

    return {
        url: response.data.url
    };

};


