import { api } from "../../index";
import ky from "ky";

export const saveDraft = async (params: SaveDraftParams) => {
    const json = {
        title: params.title,
        fullTitle: params.fullTitle,
        linkUrl: params.linkUrl,
        type: params.type,
        authors: params.authors,
        coAuthors: params.coAuthors,
        participants: params.participants,
        parentId: params.parentId,
        additionalImages: params.additionalImages

    };

    const response = await api.post(`/api/v1/user/save-draft`, { json }).json<ApiResponse<Draft>>();

    return response.data
};

export const editDraft = async (params: EditDraftParams) => {
    const json = {
        title: params.title,
        linkUrl: params.linkUrl,
        fullTitle: params.fullTitle,
        type: params.type,
        authors: params.authors,
        coAuthors: params.coAuthors,
        participants: params.participants,
        additionalImages: params.additionalImages
    };

    const response = await api.patch(`/api/v1/user/edit-draft/${params._id}`, { json }).json<ApiResponse<Draft>>();

    return response.data;
};

export const getDraft = async (id?: string) => {
    const response = await api.get(`/api/v1/user/get-drafts/${id}`).json<ApiResponse<DraftDetails>>();

    if (response.data.linkUrl && response.data.type === "image") {
        const file = await fetchFile(response.data.linkUrl);
        response.data.imageFile = await file?.blob()
    } else {
        const file = await fetchFile(response.data.linkUrl);
        response.data.text = await file?.text()
    }

    if (response.data.metadataUrl) {
        try {
            const file = await fetchFile(response.data.metadataUrl);
            response.data.metadata = await file?.json();
        } catch (e) {
            response.data.metadata = null
        }

    }

    if (!response.data.metadataUrl) {
        const metadata = {
            Title: response.data.fullTitle,
            authors: response.data.authors,
            coAuthors: response.data.coAuthors,
            participants: response.data.participants,
        }


        response.data.metadata = metadata
    }

    return response.data;
};

export const deleteDraft = async (id?: string): Promise<boolean> => {
    try {
        await api.delete(`/api/v1/user/delete-draft/${id}`).json<ApiResponse<{}>>();

        return true;
    } catch (e) {
        return false;
    }
};

export const recoverDraft = async (id?: string): Promise<boolean> => {
    try {
        await api.post(`/api/v1/user/recover-draft/${id}`).json<ApiResponse<{}>>();

        return true;
    } catch (e) {
        return false;
    }
};

export const getDraftList = async (params?: GetDraftParams) => {
    const query = new URLSearchParams();

    if (params?.commonId) {
        query.append('commonId', params.commonId)
    }

    if (params && Object.hasOwn(params, "isMinted")) {
        query.append('isMinted', String(!!params?.isMinted))
    }

    const response = await api.get(`/api/v1/user/get-drafts?${query}`).json<ApiResponseList<Draft>>();

    return {
        items: response.data.items,
        count: response.data.count,
    };
};

export const getDeletedDraftList = async () => {
    const response = await api.get(`/api/v1/user/get-deleted-drafts`).json<ApiResponseList<DeletedDraft>>();

    return {
        items: response.data.items,
        count: response.data.count,
    };
}

export const getFileUploadLink = async (id: string, fileExt = 'txt') => {
    const response = await api.get(`/api/v1/user/get-file-upload-link/${id}?fileExt=${fileExt}`).json<ApiResponse<GetUploadLink>>();

    return {
        uploadLink: response.data.uploadLink,
        downloadLink: response.data.downloadLink,
    };
};

export const uploadFile = async (url: string, file: File, fields: any) => {
    if (!file) return;

    const body = new FormData();
    Object.keys(fields).forEach((key) => {
        body.append(key, fields[key]);
    });
    body.append("file", file);

    return await ky.post(url, { body, timeout: false }).json();
};


export const fetchFile = async (url?: string) => {
    if (!url) return;

    return await ky.get(url, { timeout: false });
};

export const setupMonitoring = async (params?: SetupMonitoringParams) => {
    const json = {
        scansTotal: params?.scansTotal,
        network: params?.network
    }

    const response = await api.post(`/api/v1/user/setup-monitoring/${params?.draftId}`, { json }).json<ApiResponse<SetupMonitoringResult>>();

    return response.data
}

export const setLinenseParameters = async (params: LicenseParametersResponse) => {

    const query = new URLSearchParams({
        licenseHash: params.licenseHash,
        amountWei: params.amountWei,
        network: params.network
    });

    const response = await api
        .get(`/api/v1/user/get-sale-signature/${params.id}?${query}`)
        .json<ApiResponse<SaleSignature>>();
    return response.data
};

export const getCertificatePaymentLink = async (params?: CertificatePaymentLinkParams) => {
    const json = {
        cancelUrl: params?.cancelUrl,
        successUrl: params?.successUrl,
        network: params?.network
    }

    const response = await api.post(`/api/v1/public/get-graft-cert-payment-link/${params?.id}`, { json }).json<ApiResponse<CertificatePaymentLinkResult>>();

    return response.data
}


export const getConvertFromWei = async (params: ConvertFromWeiParams) => {

    const query = new URLSearchParams({
        amount: params.amount
    });

    const response = await api
        .get(`/api/v1/public/convert-from-wei?${query}`)
        .json<ApiResponse<ConvertFromWeiResult>>();
    return response.data

}

export const getConvertFromUsd = async (params: ConvertFromWeiParams) => {

    const query = new URLSearchParams({
        amount: params.amount
    });

    const response = await api
        .get(`/api/v1/public/convert-from-usd?${query}`)
        .json<ApiResponse<ConvertFromWeiResult>>();
    return response.data

}

export const getPublicDraft = async (id?: string) => {
    const response = await api.get(`/api/v1/public/get-drafts/${id}`).json<ApiResponse<DraftDetails>>();

    if (response.data.linkUrl && response.data.type === "image") {
        const file = await fetchFile(response.data.linkUrl);
        response.data.imageFile = await file?.blob()
    } else {
        const file = await fetchFile(response.data.linkUrl);
        response.data.text = await file?.text()
    }

    if (response.data.metadataUrl) {
        const file = await fetchFile(response.data.metadataUrl);
        response.data.metadata = await file?.json();
    }

    if (!response.data.metadataUrl) {
        const metadata = {
            Title: response.data.fullTitle,
            authors: response.data.authors,
            coAuthors: response.data.coAuthors,
            participants: response.data.participants,
        }


        response.data.metadata = metadata
    }

    return response.data;
};
