import { api } from "index";

export const getAlertsList = async (params?: GetAlertParams) => {
    const query = new URLSearchParams({
        isSkipped: params?.isSkipped ? "true" : "false",
    });

    if (params?.documentId) {
        query.append("documentId", params?.documentId);
    }

    const response = await api.get(`/api/v1/user/get-alerts?${query}`).json<ApiResponseList<Alert>>();

    return {
        items: response.data.items,
        count: response.data.count,
    };
};

export const closeAlert = async (params: string) => {
    const json = {
        isSkipped: true,
        isReacted: true,
    };

    const response = await api.post(`/api/v1/user/close-alerts?ids=${params}`, { json }).json<ApiResponse<Alert>>();

    return response.data;
};

export const restoreAlert = async (params: string) => {
    const json = {
        isSkipped: false,
        isReacted: false,
    };

    const response = await api.post(`/api/v1/user/close-alerts?ids=${params}`, { json }).json<ApiResponse<Alert>>();

    return response.data;
};

export const getAlertDetails = async (id?: string) => {
    const response = await api.get(`/api/v1/user/get-alert/${id}`).json<ApiResponse<Alert>>();

    return response.data;
};
