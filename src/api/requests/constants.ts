import { api } from "index";

export const getSidebarCounts = async () => {
    const response = await api.get(`/api/v1/user/whoami`).json<ApiResponse<WhoAmiResponse>>();

    return {
        counts: response.data.counts,
    };
};

export const getCountriesList = async () => {
    const response = await api.get(`/api/v1/public/get-countries`).json<ApiResponse<GetCountriesResponse[]>>();
    return response.data;
};
