import {api} from "../../index";
import {ChangeNearAddressTypes} from "../../features/Modals/interfaces/formTypes";
// Errors Codes
// const ErrorCodes = {
//
// }

export const getLoginCode = async (params: LoginCodeParams) => {
    const query = new URLSearchParams({
        network: params.network || "ethereum",
        address: params.address,
    });

    try {
        const response = await api.get(`/api/v1/public/get-login-code?${query}`).json<ApiResponse<LoginCodeResponse>>();
        return {
            code: response.data.codeToSign,
            isSuccess: true
        };
    } catch (e) {
        console.log(e)
        return {code: undefined, isSuccess: false}
    }


};

export const confirmLoginCode = async (params: ConfirmLoginCodeParams) => {
    const json: ConfirmLoginCodeParams = {
        code: params.code,
        signature: params.signature,
        address: params.address,
        network: params.network,
    };

    const response = await api.post(`/api/v1/public/confirm-login-code`, { json }).json<ApiResponse<ConfirmLoginCodeResponse>>();

    return {
        token: response.data.authToken,
    };
};

export const getWhoAmi = async () => {
    const response = await api.get(`/api/v1/user/whoami`).json<ApiResponse<WhoAmiResponse>>();

    return {
        user: response.data.data,
        websocket: response.data.websocket,
    };
};

export const confirmEmail = async (params: ConfirmEmailParams) => {
    if (!params.token) return;

    const query = new URLSearchParams({
        token: params.token,
    });

    const response = await api.get(`/api/v1/public/email?${query}`).json<ApiResponse<ConfirmEmailResponse>>();

    return {
        isEmailVerified: response.data.isEmailVerified,
        email: response.data.email,
    };
};

export const sendEmailVerification = async (params: SendEmailVerifyParams) => {
    const json: SendEmailVerifyParams = {
        email: params.email,
    };

    const response = await api.post(`/api/v1/user/email`, { json }).json<ApiResponse<ConfirmEmailResponse>>();

    return {
        isEmailVerified: response.data.isEmailVerified,
        email: response.data.email,
    };
};

export const changeNearAddressRequest = async (payload: ChangeNearAddressTypes) => {
    const response = await api.post("/api/v1/user/near-address", { json: payload }).json<ApiResponse<ChangeNearAddressTypes>>();

    return response;
};

export const sendKYCVerification = async (params: SendKYCVerification) => {
    const json: SendKYCVerification = {
        email: params.email,
        country: params.country,
        language: params.language,
    };

    const response = await api.post(`/api/v1/user/kyc-verification`, { json }).json<ApiResponse<ConfirmKYCVerification>>();

    return {
        data: response.data,
    };
};
