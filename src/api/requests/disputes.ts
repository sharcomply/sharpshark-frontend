import { api } from "index";

export const saveDispute = async (params: SaveDispute) => {
    const json = {
        ...params,
    };
    const response = await api.post(`/api/v1/user/save-dispute`, { json }).json<ApiResponse<Disput>>();

    return response.data;
};

export const editDisput = async (params: EditDispute) => {
    const json = {
        ...params,
    };

    const response = await api.post(`/api/v1/user/edit-dispute/${params._id}`, { json }).json<ApiResponse<Disput>>();

    return response.data;
};

export const getDisputesList = async (status?: string) => {
    let query = `status=${status}`;
    if (status === "NotRelevant") query = "isNotRelevant=true";
    const response = await api.get(`/api/v1/user/get-disputes?${query}`).json<ApiResponseList<Disput>>();

    return {
        items: response.data.items,
        count: response.data.count,
    };
};

export const getDisputesGroupingList = async (status?: string) => {
    let query = `status=${status}`;
    if (status === "NotRelevant") query = "isNotRelevant=true";
    query += "&groupByAppearedOn=true";
    const response = await api.get(`/api/v1/user/get-disputes-grouped?${query}`).json<ApiResponseList<DisputesGroupingResponse>>();

    return {
        items: response.data.items,
        count: response.data.count,
    };
};

export const getDisputeIdGroupingList = async (status: string, id: string) => {
    let query = `status=${status}&groupByAppearedOn=true&documentId=${id}`;
    const response = await api.get(`/api/v1/user/get-disputes-grouped?${query}`).json<ApiResponseList<DisputesGroupingResponse>>();

    return {
        items: response.data.items,
        count: response.data.count,
    };
};

export const getDisputesCounters = async () => {
    const res = await api.get("/api/v1/user/get-disputes-counters").json<ApiResponse<DisputesCountersList>>();

    return res.data;
};

export const getDisputesListId = async (status?: string, id?: string) => {
    const response = await api.get(`/api/v1/user/get-disputes?status=${status}&documentId=${id}`).json<ApiResponseList<Disput>>();

    return {
        items: response.data.items,
        count: response.data.count,
    };
};

export const getDisput = async (id?: string) => {
    const response = await api.get(`/api/v1/user/get-dispute/${id}`).json<ApiResponse<ItemDispute>>();

    return response.data;
};

export const getImageUploadLink = async (id: string) => {
    const response = await api.get(`/api/v1/user/get-image-upload-link/${id}?fileExt=png`).json<ApiResponse<GetUploadLink>>();

    return {
        uploadLink: response.data.uploadLink,
        downloadLink: response.data.downloadLink,
    };
};

export const sendEmailDispute = async (id: string) => {
    const response = await api.post(`/api/v1/user/send-emails-dispute/${id}`).json<ApiResponse<Disput>>();

    return response.data;
};

export const findEmails = async (id: string) => {
    const response = await api.post(`/api/v1/user/find-emails-dispute/${id}`).json<ApiResponse<FindEmails>>();

    return response.data;
};

export const deleteDisput = async (id: string) => {
    try {
        await api.delete(`/api/v1/user/delete-dispute/${id}`).json<ApiResponse<{}>>();

        return true;
    } catch (error) {
        return false;
    }
};

