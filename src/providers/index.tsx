import { PropsWithChildren, useEffect } from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { Provider as ReduxProvider } from "react-redux";
import { MantineProvider } from "@mantine/core";
import { Web3ReactProvider } from "@web3-react/core";
import { Web3Provider } from "@ethersproject/providers";

import { store } from "store";
import AuthProvider from "../features/Auth";
import { ScanClientProvider } from "../features/Scan/ScanProvider";
import VerificationProvider from "features/Verification/VerificationProvider";
import PlatformWarning from "features/PlatformWarning";
import { useLocation } from "react-router-dom";

import * as buffer from "buffer";
import { NearProvider } from "./NearProvider";
import { useMediaQuery } from "@mantine/hooks";
import { NotificationsProvider } from "@mantine/notifications";

(window as any).Buffer = buffer.Buffer;

const queryClient = new QueryClient();
const isDev = process.env.NODE_ENV === "development";

function getLibrary(provider: any): Web3Provider {
    return new Web3Provider(provider);
}

export const Providers = ({ children }: PropsWithChildren<{}>) => {
    const { pathname } = useLocation();

    const mobile = useMediaQuery("(max-width: 600px)");

    useEffect(() => {
        if (!pathname.includes("certificate")) {
            const html = document.querySelector("html") as any;
            html.style.zoom = "initial";
        }
    }, [pathname]);

    return (
        <MantineProvider
            theme={{
                fontFamily: "SuisseInt`l",
                colors: {
                    default: ["#5E00FF", "#4800FF", "#3600FF", "#4700E5", "#2C00FF", "#2800E8", "#2400D1", "#2000BC", "#1D00A9", "#1A0098"],
                    black: ["#181422", "#161020", "#130D1F", "#110A1E", "#0E071E", "#0C031E", "#0A001E", "#0A0318", "#0A0414", "#090511"],
                    white: ["#FFFFFF", "#FFFFFF", "#FFFFFF", "#FFFFFF", "#FFFFFF", "#FFFFFF", "#FFFFFF", "#FFFFFF", "#FFFFFF", "#FFFFFF"],
                },
                primaryColor: "default",
            }}
        >
            <NotificationsProvider>
                <QueryClientProvider client={queryClient}>
                    <ReduxProvider store={store}>
                        <Web3ReactProvider getLibrary={getLibrary}>
                            <NearProvider>
                                <AuthProvider>
                                    <ScanClientProvider>
                                        <VerificationProvider>
                                            {!pathname.includes("certificate") && <PlatformWarning />}
                                            {!mobile && children}
                                            {mobile && !!pathname.includes("certificate") && children}
                                        </VerificationProvider>
                                    </ScanClientProvider>
                                </AuthProvider>
                            </NearProvider>
                        </Web3ReactProvider>
                    </ReduxProvider>
                    {isDev && <ReactQueryDevtools initialIsOpen={false} position={"bottom-right"} />}
                </QueryClientProvider>
            </NotificationsProvider>
        </MantineProvider>
    );
};
