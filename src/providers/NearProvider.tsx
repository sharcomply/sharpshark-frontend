import React, { PropsWithChildren, useCallback, useContext, useEffect, useMemo, useState } from "react";
import { connect, ConnectedWalletAccount, Contract, keyStores, Near, WalletConnection } from "near-api-external-link-fork";
import { changeMethods, viewMethods } from "../constants/NearContract";
// import { useModal } from "../features/Modals/hooks/useModal";
// import { ModalContent, ModalType } from "../features/Modals/interfaces";
import { useWhoAmi } from "../features/Auth/hooks/useWhoAmi";
import useAuthStore from "../features/Auth/hooks/useAuthStore";
import { useCrossTabState } from "hooks/useCrossTbState";
import { NEAR_CONTRACT } from "../constants/providers";

const NearContext = React.createContext<{
    Contract: any;
    Connection: Near;
    WalletConnection: WalletConnection;
    Account: ConnectedWalletAccount;
} | null>(null);

export const keyStore = new keyStores.BrowserLocalStorageKeyStore();

const connectionConfig = {
    networkId: "mainnet",
    keyStore: keyStore, // first create a key store
    nodeUrl: "https://rpc.mainnet.near.org",
    walletUrl: "https://wallet.mainnet.near.org",
    helperUrl: "https://helper.mainnet.near.org",
    explorerUrl: "https://explorer.mainnet.near.org",
};

export const NearProvider = (props: PropsWithChildren<{}>) => {
    const [nearConnection, setNearConnection] = useState<Near | undefined>();
    const [walletConnection, setWalletConnection] = useState<WalletConnection | undefined>();
    // const { handleOpen } = useModal();
    const { data, isLoading } = useWhoAmi();
    const { network } = useAuthStore();

    const [nearConnect] = useCrossTabState("nearConnectToPay", "");

    const setupConnection = useCallback(async () => {
        const connection = await connect(connectionConfig);
        const wallet = new WalletConnection(connection, null);
        setNearConnection(connection);
        setWalletConnection(wallet);
    }, [setNearConnection, setWalletConnection]);

    useEffect(() => {
        setupConnection();
    }, [setupConnection, nearConnect]);

    const value = useMemo(() => {
        if (!walletConnection || !nearConnection) return null;
        const NearContract = new Contract(walletConnection.account(), NEAR_CONTRACT!, {
            viewMethods: viewMethods,
            changeMethods: changeMethods,
        });

        return {
            Contract: NearContract,
            Connection: nearConnection,
            WalletConnection: walletConnection,
            Account: walletConnection.account(),
        };
    }, [walletConnection, nearConnection]);

    useEffect(() => {
        if (isLoading) return;
        if (network !== "near") return;
        if (!data?.user || !walletConnection?.account().accountId) return;
        if (data?.user.walletAddressNear) return;
        // handleOpen({ type: ModalType.SETTINGS, content: ModalContent.ChangeNearAddress });
    }, [data, isLoading, walletConnection, network]);

    return <NearContext.Provider value={value} {...props} />;
};

export const useNearContext = () => {
    const context = useContext(NearContext);
    return context || null;
};
