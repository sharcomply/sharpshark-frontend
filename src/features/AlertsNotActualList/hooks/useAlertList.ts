import { useQuery } from "react-query";
import { backend } from "api";

export default function useAlertList(params?: GetAlertParams, key?: string) {
  const { data: alerts, ...options } = useQuery([key], () => backend.alerts.getAlertsList(params), {
    keepPreviousData: true,
  });

  return {
    alerts,
    ...options,
  };
}