import { Stack, Text } from "@mantine/core";
import styled from "@emotion/styled";
import { TableHead } from "../../components/DataTable/TableHead";
import { Fragment } from "react";
import { getListByDate } from "helpers/getListByDate";
import { TableDate } from "../../components/DataTable/TableDate";
import { AlertItem } from "./components/AlertItem";
import useAlertList from "./hooks/useAlertList";
import { useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";

// Component
export default function AlertsNotActualList() {
    const { protectId } = useParams();
    const { t } = useTranslation();
    const { alerts } = useAlertList(
        {
            isSkipped: true,
            documentId: protectId,
        },
        protectId ? "PROTECTED_ALERTS_NOT_ACTUAL_LIST" : "ALERTS_NOT_ACTUAL_LIST"
    );

    const itemsByDate = getListByDate(alerts?.items || []);

    return (
        <Root>
            <Table>
                <TableHead
                    items={[
                        { title: t("table.titles.your_title"), width: "48%" },
                        { title: t("table.titles.appeared_on"), width: "20%" },
                        { title: t("table.titles.discovered"), width: "12%" },
                        { title: "", width: "15%" },
                        { title: "", width: "5%" },
                    ]}
                />

                <tbody>
                    {!!alerts?.items.length && (
                        <Fragment>
                            {renderListByDate(t("table.row-by-date.today"), itemsByDate.today)}
                            {renderListByDate(t("table.row-by-date.this-week"), itemsByDate.week)}
                            {renderListByDate(t("table.row-by-date.last-month"), itemsByDate.lastMonth)}
                            {renderListByDate(t("table.row-by-date.earlier-this-month"), itemsByDate.month)}
                            {renderListByDate(t("table.row-by-date.earlier"), itemsByDate.earlier)}
                        </Fragment>
                    )}
                    {!alerts?.items.length && (
                        <tr style={{ borderTop: "1px solid #DDDEEE" }}>
                            <td style={{ padding: 16 }}>
                                <Text size={"sm"} color={"#666892"} weight={"400"}>
                                    {t("table.empty-table")}
                                </Text>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    )}
                </tbody>
            </Table>
        </Root>
    );
}

// List By Date
function renderListByDate(title: string, items: Alert[]) {
    if (!items.length) return null;

    return (
        <Fragment>
            <TableDate title={title} cols={5} titlePos={3} />
            {items.map((item) => {
                return <AlertItem key={item._id} item={item} />;
            })}
        </Fragment>
    );
}

// Styling
const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
`;

export const Table = styled.table`
    width: 100%;
    table-layout: fixed;
    border-collapse: collapse;
    overflow-wrap: anywhere;
`;
