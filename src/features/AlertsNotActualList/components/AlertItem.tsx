import { Group, Text } from "@mantine/core";
import { humanizeDate } from "helpers/humanizeDate";
import styled from "@emotion/styled";
import { ActionMenu } from "./ActionMenu";
import ComingSoonWrapper from "components/ComingSoonWrapper";

// Props
type ComponentProps = {
    item: Alert;
};

// Component
export const AlertItem = ({ item }: ComponentProps) => {
    return (
        <>
            {!item.scanId.similarContent.length && (
                <Root>
                    <td>
                        <Group spacing={0} px={16} align="center" sx={{ height: 54 }}>
                            <Text
                                size={"sm"}
                                color={"#0A001E"}
                                weight={"500"}
                                style={{
                                    overflow: "hidden",
                                    textOverflow: "ellipsis",
                                    whiteSpace: "nowrap",
                                }}
                            >
                                {item.documentId.title}
                            </Text>
                        </Group>
                    </td>

                    <td>
                        <Text
                            size={"sm"}
                            color={"#666892"}
                            weight={"500"}
                            style={{
                                overflow: "hidden",
                                textOverflow: "ellipsis",
                                whiteSpace: "nowrap",
                            }}
                        >
                            Similar content not found
                        </Text>
                    </td>

                    <td>
                        <Group px={16} align="center" sx={{ height: 54 }}>
                            <Text size={"sm"} color={"#666892"} weight={"500"}>
                                {humanizeDate(item.date).shortDate}
                            </Text>
                        </Group>
                    </td>

                    <td>
                        <Group px={16} position="right" align="center" sx={{ height: 54 }}>
                            <ComingSoonWrapper orientation="vertical">
                                <Text size={"sm"} color={"#4700E5"} weight={"500"}>
                                    Report mistake
                                </Text>
                            </ComingSoonWrapper>
                        </Group>
                    </td>

                    <td>
                        <Group px={16} position="right" align="center" sx={{ height: 54, pointerEvents: "none" }}>
                            <ActionMenu protectId={item._id} />
                        </Group>
                    </td>
                </Root>
            )}
            {!!item.scanId.similarContent.length &&
                item.scanId.similarContent.map((a) => (
                    <Root>
                        <td>
                            <Group direction={"row"} spacing={0} style={{ overflow: "hidden" }} noWrap={true}>
                                <Group px={16} align="center" style={{ overflow: "hidden", minWidth: "20%", maxWidth: "50%" }} sx={{ height: 54 }}>
                                    <Text
                                        size={"sm"}
                                        color={"#0A001E"}
                                        weight={"500"}
                                        style={{
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        {a.title}
                                    </Text>
                                </Group>
                                <Group px={16} align="center" style={{ overflow: "hidden" }} sx={{ height: 54 }}>
                                    <Text
                                        size={"sm"}
                                        color={"#666892"}
                                        weight={"500"}
                                        style={{
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        {a.introduction}
                                    </Text>
                                </Group>
                            </Group>
                        </td>

                        <td>
                            <a href={a.url}>
                                <Group px={16} align="center" sx={{ height: 54 }}>
                                    <Text
                                        size={"sm"}
                                        color={"#666892"}
                                        weight={"500"}
                                        style={{
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        {a.url}
                                    </Text>
                                </Group>
                            </a>
                        </td>

                        <td>
                            <Group px={16} align="center" sx={{ height: 54 }}>
                                <Text size={"sm"} color={"#666892"} weight={"500"}>
                                    {humanizeDate(item.date).shortDate}
                                </Text>
                            </Group>
                        </td>

                        <td>
                            <Group px={16} position="right" align="center" sx={{ height: 54 }}>
                                <ComingSoonWrapper orientation="vertical">
                                    <Text size={"sm"} color={"#4700E5"} weight={"500"}>
                                        Report mistake
                                    </Text>
                                </ComingSoonWrapper>
                            </Group>
                        </td>

                        <td>
                            <Group px={16} position="right" align="center" sx={{ height: 54, pointerEvents: "none" }}>
                                <ActionMenu protectId={item._id} />
                            </Group>
                        </td>
                    </Root>
                ))}
        </>
    );
};

// Styling
const Root = styled.tr`
    transition: background-color 200ms linear;
    border-top: 1px solid #dddeee;

    &:hover {
        background-color: #edeeff;
    }
`;
