// Props
import { Group, Popover, Text } from "@mantine/core";
import Icon from "../../../components/Icon";
import { useCallback, useState } from "react";
import { restoreAlert } from "api/requests/alerts";
import styled from "@emotion/styled";
import { useQueryClient } from "react-query";
import { useTranslation } from "react-i18next";

// Variables
const rootStyles = {
    background: "#FFFFFF",
    border: "1px solid #DDDEEE",
    boxShadow: " 0px 2px 12px rgba(0, 0, 0, 0.1)",
    radius: 4,
    overflow: "hidden",
};

type ComponentProps = {
    protectId: string;
};

// Component
export function ActionMenu({ protectId }: ComponentProps) {
    const { t } = useTranslation();
    const [opened, setOpened] = useState(false);
    const queryClient = useQueryClient();
    const onRestore = useCallback(
        async (id: string) => {
            if (!id) return;
            await restoreAlert(id);
            await Promise.all([queryClient.refetchQueries(["ALERTS_LIST"]), queryClient.refetchQueries(["ALERTS_NOT_ACTUAL_LIST"])]);
        },
        [queryClient]
    );

    const control = (
        <Group align={"center"} position={"center"} px={4} onClick={() => setOpened((value) => !value)} sx={{ pointerEvents: "all" }}>
            <Icon icon={"ic:sharp-more-vert"} size={24} color={"#b1b2c9"} hoverColor={"#b1b2c9"} />
        </Group>
    );

    return (
        <Popover
            opened={opened}
            target={control}
            onClose={() => setOpened(false)}
            width={160}
            radius={4}
            position="bottom"
            placement="end"
            transition={"pop-top-right"}
            styles={{ body: rootStyles, inner: { padding: 0 } }}
        >
            <MenuItem px={16} py={12} onClick={() => onRestore(protectId)}>
                <Text size={"sm"} weight={"500"}>
                    {t("buttons.restore_btn")}
                </Text>
            </MenuItem>
            {/* View And Edit */}
            {/* <Link to={`/protected/${protectId}`}>
                <MenuItem px={16} py={12}>
                    <Text size={'sm'} weight={'500'}>View Document</Text>
                </MenuItem>
            </Link> */}

            {/* Protect */}
            {/* <MenuItem px={16} py={12} className="disabled">
                <Text size={'sm'} weight={'500'}>
                    Add a new version
                    <br/>
                    (Coming Soon)
                </Text>
            </MenuItem> */}

            {/* Protect */}
            {/* <MenuItem px={16} py={12} className="disabled">
                <Text size={'sm'} weight={'500'}>
                    View certificate
                    <br/>
                    (Coming Soon)
                </Text>
            </MenuItem> */}

            {/* Delete */}
            {/* <MenuItem px={16} py={12} className="disabled">
                <Text size={'sm'} weight={'500'}>
                    × Stop monitoring
                    <br/>
                    (Coming Soon)
                </Text>
            </MenuItem> */}
        </Popover>
    );
}

// Styling
const MenuItem = styled(Group)`
    cursor: pointer;
    color: #666892;

    &:hover {
        background-color: #4700e5;
        color: #ffffff;
    }

    &.disabled {
        color: #bbbed8;
    }

    &.disabled:hover {
        background-color: #f7f7fa;
        color: #8b8fad;
    }
`;

