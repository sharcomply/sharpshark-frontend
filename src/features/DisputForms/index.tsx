import DisputesCollectingEvidence from "features/DisputesCollectingEvidence";
import DisputesGeneratingClaim from "features/DisputesGeneratingClaim";
import DisputesSending from "features/DisputesSending";
import { Control, UseFormRegister, UseFormSetError, UseFormStateReturn, UseFormSetValue } from "react-hook-form";
import { useEffect } from "react";
import { useWizzardFormStore } from "./hooks/useWizzardFormStore";

interface Props {
    control: Control<any>;
    register: UseFormRegister<any>;
    getValues: (props?: string) => string;
    setValue: UseFormSetValue<any>;
    setError: UseFormSetError<any>;
    formState: UseFormStateReturn<any>;
}

const DisputForms = ({ control, register, getValues, setValue, setError, formState }: Props) => {
    const {
        actions: { setStep },
    } = useWizzardFormStore();

    useEffect(() => {
        if (window.location.pathname.includes("/collecting-evidence")) {
            setStep(0);
        }
        if (window.location.pathname.includes("/generating-claim")) {
            setStep(1);
        }
        if (window.location.pathname.includes("/sending")) {
            setStep(2);
        }
    });

    return (
        <>
            {window.location.pathname.includes("/collecting-evidence") && (
                <DisputesCollectingEvidence getValues={getValues} setValue={setValue} control={control} register={register} />
            )}
            {window.location.pathname.includes("/generating-claim") && (
                <DisputesGeneratingClaim getValues={getValues} register={register} control={control} formState={formState} />
            )}
            {window.location.pathname.includes("/sending") && (
                <DisputesSending setError={setError} getValues={getValues} control={control} register={register} />
            )}
        </>
    );
};

export default DisputForms;
