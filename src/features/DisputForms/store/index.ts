import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Types
export interface DisputFormsState {
  step: number
}

// Initial State
const initialState: DisputFormsState = {
  step: 0
};

// Slice
const wizzardFromSlice = createSlice({
  name: "wizzardForm",
  initialState,
  reducers: {
    clear: () => initialState,


    setStep: (state, action: PayloadAction<DisputFormsState["step"]>) => {
      state.step = action.payload;
    },
  },
});

export const wizzardFromReducers = wizzardFromSlice.reducer;
export const wizzardFromActions = wizzardFromSlice.actions;