import CustomEvent from './CustomEvent';
import { WSAction, WSActionHandler, WSEvent, WSSEventHandler, WSStatus, WSStatusHandler } from './interfaces/ws.interfaces';
import { ScanCurrentStatus, ScanEvent } from '../Scan/ScanClient';
import { VerificationStatus } from 'features/Verification/VerificationProvider';


export class WsClient {
    socket?: WebSocket;

    protected actionHandlers: Record<string, WSActionHandler<any>[]>;
    protected statusHandlers: Record<string, WSStatusHandler<any>[]>;
    protected eventHandlers: Record<string, WSSEventHandler<any>[]>;

    openEvent = new CustomEvent<void>();
    closeEvent = new CustomEvent<void>();
    refetchTime: any;
    url?: string;

    constructor() {
        this.actionHandlers = {};
        this.statusHandlers = {};
        this.eventHandlers = {}

        this.initEvents();
        this.onMessage = this.onMessage.bind(this);
        this.onClose = this.onClose.bind(this);
        this.onOpen = this.onOpen.bind(this);
        this.refetchTime = null;
    }

    protected connect(url: string) {
        if (this.refetchTime) {
            clearTimeout(this.refetchTime);
            this.refetchTime = null;
        }

        this.url = url;
        this.socket = new WebSocket(url);
        this.initEvents();
    }

    protected initEvents() {
        if (!this.socket) return;
        this.socket.addEventListener('message', this.onMessage);
        this.socket.addEventListener('close', this.onClose);
        this.socket.addEventListener('open', this.onOpen);
    }

    addActionHandler<T extends WSAction>(action: ScanEvent, handler: WSActionHandler<T>) {
        const actionHandlers = this.actionHandlers[action] || [];
        actionHandlers.push(handler);
        this.actionHandlers[action] = actionHandlers;
    }

    removeActionHandler<T extends WSAction>(
        action: string,
        handler: WSActionHandler<T>
    ) {
        const handlers = this.actionHandlers[action];
        if (handlers && handlers.length) {
            this.actionHandlers[action] = handlers.filter((i) => i !== handler);
        }
    }

    addStatusHandler<T extends WSStatus>(action: ScanCurrentStatus, handler: WSStatusHandler<T>) {
        const statusHandlers = this.statusHandlers[action] || [];
        statusHandlers.push(handler);
        this.statusHandlers[action] = statusHandlers;
    }

    removeStatusHandler<T extends WSStatus>(
        action: string,
        handler: WSStatusHandler<T>
    ) {
        const handlers = this.statusHandlers[action];
        if (handlers && handlers.length) {
            this.statusHandlers[action] = handlers.filter((i) => i !== handler);
        }
    }

    addEventHandler<T extends WSEvent>(action: VerificationStatus, handler: WSSEventHandler<T>) {
        const eventHandlers = this.eventHandlers[action] || [];
        eventHandlers.push(handler);
        this.eventHandlers[action] = eventHandlers;
    }

    removeEventHandler<T extends WSEvent>(
        action: string,
        handler: WSSEventHandler<T>
    ) {
        const handlers = this.eventHandlers[action];
        if (handlers && handlers.length) {
            this.eventHandlers[action] = handlers.filter((i) => i !== handler);
        }
    }

    protected onMessage(ev: MessageEvent) {
        try {
            const json = JSON.parse(ev.data) as WSEvent;

            if (!json.action) return
            const actionHandlers = this.actionHandlers[json.action] || [];
            actionHandlers.forEach((handler) => handler(json));

            // if (!json.status) return;
            const statusHandlers = this.statusHandlers[json.status] || [];
            statusHandlers.forEach((handler) => handler(json));

            // if (!json.event) return;
            const eventHandlers = this.eventHandlers[json.event] || [];
            eventHandlers.forEach((handler) => handler(json));


        }
        catch (e) {
            console.log('WS Parse message error');
            console.log(e);
        }
    }

    protected onOpen() {
        console.log('ws open');
        this.openEvent.dispatchEvent();
    }

    protected onClose() {
        console.log('ws close');
        this.closeEvent.dispatchEvent();

        this.refetchTime = setTimeout(() => {
            if (!this.url) return;
            this.connect(this.url);
        }, 60 * 1000);
    }

    close() {
        if (this.socket) {
            this.socket.removeEventListener('message', this.onMessage);
            this.socket.removeEventListener('close', this.onClose);
            this.socket.removeEventListener('open', this.onOpen);
            this.socket.close();
            this.closeEvent.dispatchEvent();
        }
    }
}
