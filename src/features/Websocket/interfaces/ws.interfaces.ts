export interface WSEvent {
  action: string;
  event: string;
  status: string;
  data: any;
}

export interface WSAction {
  action: string;
  status?: string;
  data: any;
}

export interface WSStatus {
  action: string;
  status: string;
  data: any;
}

export type WSHandler<T extends WSAction> = (action: T) => void;
export type WSActionHandler<T extends WSAction> = (action: T) => void;
export type WSStatusHandler<T extends WSStatus> = (action: T) => void;
export type WSSEventHandler<T extends WSEvent> = (action: T) => void;

