import { useContract } from "blockchain/hooks/useContract";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { OverlayMessage } from "../constants/StatusMessages";
import { OverlaySteps } from "../store";
import useDraftStore from "./useDraftStore";
import useAuthStore from "../../Auth/hooks/useAuthStore";

export function useDraftOverlay() {
    const { overlay: state, setOverlay, showOverlay, mintPayload, licence } = useDraftStore();
    const { t } = useTranslation();
    const contract = useContract();
    const { network } = useAuthStore();

    // При нажатии на кнопку Protect

    const setPendingPaymentFiat = useCallback(
        (url: string) => {
            setOverlay({
                type: "default",
                message: OverlayMessage.PENDING_PAYMENT_FIAT(url, t),
                step: OverlaySteps.PENDING_PAYMENT_FIAT,
            });
        },
        [setOverlay]
    );

    const setPendingPayment = useCallback(
        (type: string = "metamask", closeNear?: () => void) => {
            setOverlay({
                type: "default",
                message: type === "metamask" ? OverlayMessage.PENDING_PAYMENT(t) : OverlayMessage.PENDING_PAYMENT_NEAR(t, closeNear),
                step: OverlaySteps.PENDING_PAYMENT,
            });
        },
        [setOverlay]
    );

    const setPaymentReceived = useCallback(() => {
        setOverlay({
            type: "default",
            message: OverlayMessage.PAYMENT_RECEIVED(t),
            step: OverlaySteps.PAYMENT_RECEIVED,
        });
    }, [setOverlay]);

    const setWorkChecked = useCallback(() => {
        setOverlay({
            type: "default",
            message: t("overlay.messages.work_checked"),
            step: OverlaySteps.SCAN_SUCCESS,
        });
    }, [setOverlay]);

    const setPreparingWork = useCallback(() => {
        setOverlay({
            type: "default",
            message: t("overlay.messages.preparing_work"),
            step: OverlaySteps.PREPARING_WORK,
        });
    }, [setOverlay]);

    const setMetadataUploaded = useCallback(() => {
        setOverlay({
            type: "default",
            message: t("overlay.messages.metadata_uploaded"),
            step: OverlaySteps.METADATA_UPLOADED,
        });
    }, [setOverlay]);

    const setWorkReady = useCallback(
        async (mintNetwork?: string, closeNear?: () => void) => {
            let currentNetwork = "";
            if (network === "5" || network === "1") currentNetwork = "goerli";
            if (network === "137" || network === "80001") currentNetwork = "polygon";
            if (network === "56" || network === "97") currentNetwork = "binance";
            if (network === "near") currentNetwork = "near";

            setOverlay({
                type: "default",
                message: OverlayMessage.WORK_READY(t, mintNetwork ? mintNetwork : currentNetwork, closeNear),
                step: OverlaySteps.SCAN_SUCCESS,
            });
        },
        [setOverlay, network]
    );

    const setWaitingMint = useCallback(async () => {
        const networkVersion = await window.ethereum.request({ method: "net_version" });
        let currentNetwork = "";
        if (networkVersion === "5" || networkVersion === "1") currentNetwork = "goerli";
        if (networkVersion === "137" || networkVersion === "80001") currentNetwork = "polygon";
        if (networkVersion === "56" || networkVersion === "97") currentNetwork = "binance";

        setOverlay({
            type: "default",
            message: t("overlay.messages.waiting_mint", { network: currentNetwork }),
            step: OverlaySteps.SCAN_SUCCESS,
        });
    }, [setOverlay]);

    const setMinted = useCallback(() => {
        setOverlay({
            type: "success",
            message: OverlayMessage.MINTED(mintPayload!.tokenId, contract.address, t),
            step: OverlaySteps.MINTED,
        });
    }, [mintPayload, setOverlay]);

    const setlLicence = useCallback(() => {
        setOverlay({
            type: "default",
            message: OverlayMessage.SET_LICENCE(() => setMinted()),
            step: OverlaySteps.METADATA_UPLOADED,
        });
    }, [setOverlay, setMinted]);


    const setMintedWithLicence = useCallback(() => {
        setOverlay({
            type: "success",
            message: OverlayMessage.MINTED_WITH_LICENCE(mintPayload?.tokenId!, contract.address, t, licence),
            step: OverlaySteps.MINTED,
        });
    }, [mintPayload, setOverlay, licence]);

    const setMintedWithoutLicense = useCallback(() => {
         setOverlay({
             type: "licenseError",
             message: OverlayMessage.MINTED_WITHOUT_LICENSE(mintPayload!.tokenId, contract.address, t),
             step: OverlaySteps.MINTED,
         });
    }, [mintPayload, setOverlay])

    const open = useCallback(() => {
        showOverlay(true);
    }, [showOverlay]);

    const close = useCallback(() => {
        showOverlay(false);
    }, [showOverlay]);

    const clear = useCallback(() => {
        setOverlay({ type: undefined, message: undefined, step: undefined });
    }, [setOverlay]);

    return {
        state,
        actions: {
            setMetadataUploaded,
            setPaymentReceived,
            setPendingPayment,
            setPendingPaymentFiat,
            setPreparingWork,
            setWaitingMint,
            setWorkChecked,
            setWorkReady,
            setlLicence,
            setMinted,
            setMintedWithLicence,
            setMintedWithoutLicense,
            close,
            clear,
            open,
        },
    };
}


