import useDraftStore from './useDraftStore';
import { useCallback } from 'react';
import { DraftState } from '../store';


export const useDraftStatus = () => {
    const { status: state, setDraftStatus } = useDraftStore()
    
    const update = useCallback((status: DraftState["status"]) => {
        setDraftStatus(status)
    }, [setDraftStatus])
    
    
    return {
        state,
        actions: {
            update
        }
    }
}