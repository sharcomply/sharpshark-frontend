import { useCallback } from "react";
import useDraftStore from "./useDraftStore";

export function useDraftAlert() {
    const { alert: state, setDraftAlert, showDraftAlert } = useDraftStore();

    const error = useCallback((title: string, message: string) => {
        setDraftAlert({ type: "error", title, message });
    }, [setDraftAlert])

    const warn = useCallback((title: string, message: string) => {
        setDraftAlert({ type: "warn", title, message });
    }, [setDraftAlert])

    const info = useCallback((title: string, message: string) => {
        setDraftAlert({ type: "info", title, message });
    }, [setDraftAlert])


    const open = useCallback(() => {
        showDraftAlert(true)
    }, [showDraftAlert])

    const close = useCallback(() => {
        showDraftAlert(false)
    }, [showDraftAlert])

    const clear = useCallback(() => {
        setDraftAlert({ type: undefined, message: undefined, title: undefined });
    }, [setDraftAlert])

    return {
        state,
        actions: {
            error,
            warn,
            info,
            close,
            clear,
            open,
        }
    }
}
