import { backend } from "api";
import { useContract } from "blockchain/hooks/useContract";
import { useModal } from "features/Modals/hooks/useModal";
import { ScanCurrentStatus } from "features/Scan/ScanClient";
import { useScanClient } from "features/Scan/ScanProvider";
import { WSAction, WSStatus } from "features/Websocket/interfaces/ws.interfaces";
import { useCallback, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { AlertMessage } from "../constants/StatusMessages";
import { useDraftAlert } from "./useDraftAlert";
import { useDraftOverlay } from "./useDraftOverlay";
import useDraftStore from "./useDraftStore";
import { getNetworkById } from "../../../constants/networks";
import { getConvertFromUsd, getConvertFromWei, setLinenseParameters } from "api/requests/drafts";
import { utils } from "near-api-external-link-fork";
import { useNearContext } from "../../../providers/NearProvider";
import useAuthStore from "../../Auth/hooks/useAuthStore";
import { useLocation, useParams } from "react-router-dom";
import { continueFromTransactionHash } from "../../../api/requests/scans";
import { useWhoAmi } from "../../Auth/hooks/useWhoAmi";
import { DraftState } from "../store";
import { useCrossTabState } from "../../../hooks/useCrossTbState";

export function useDraftListener() {
    const { t } = useTranslation();
    const { client, connected } = useScanClient();
    const contract = useContract();
    const draftStore = useDraftStore();
    const overlay = useDraftOverlay();
    const { handleClose } = useModal();
    const alert = useDraftAlert();
    const Near = useNearContext();
    const { network } = useAuthStore();
    const { search } = useLocation();
    const { data } = useWhoAmi();
    const { token_id } = useParams();
    const [nearLicence, setNearLicence] = useCrossTabState("setNearLicence", "");
    const [nearErrorCode, setNearErrorCode] = useCrossTabState("errorCode", "");

    // const {
    //     actions: {open: openMessage, close: closeMessage},
    // } = useDraftMessages();

    const onSetLicence = useCallback(async () => {
        try {
            console.log("SET LICENCE", draftStore.licence);
            draftStore.showDraftAlert(false);
            if (
                draftStore.licence.type === "© all rights reserved, no distribution" ||
                draftStore.licence.type === "Public, non-commercial distribution" ||
                draftStore.licence.type === "Private, no distribution" ||
                draftStore.licence.type ===
                    "Time Machine © 2022 by H. G. Wells is licensed under CC0 1.0: https://creativecommons.org/publicdomain/zero/1.0/" ||
                draftStore.licence.type === "Time Machine © 2022 by H. G. Wells is licensed under CC BY 4.0: https://creativecommons.org/licenses/by/4.0/" ||
                draftStore.licence.type ===
                    "Time Machine © 2022 by H. G. Wells is licensed under CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/" ||
                draftStore.licence.type ===
                    "Time Machine © 2022 by H. G. Wells is licensed under CC BY-ND 4.0: https://creativecommons.org/licenses/by-nd/4.0/" ||
                draftStore.licence.type ===
                    "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC 4.0: https://creativecommons.org/licenses/by-nc/4.0/" ||
                draftStore.licence.type ===
                    "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/" ||
                draftStore.licence.type ===
                    "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-ND 4.0: https://creativecommons.org/licenses/by-nc-nd/4.0/" ||
                draftStore.licence.type === "Free-to-use"
            ) {
                const json = {
                    licenseHash: draftStore.licence.type,
                    amountWei: "0",
                    id: draftStore.draftId!,
                    network: network,
                };

                const response = await setLinenseParameters(json);

                console.log(`NETWORK ISD`, network);

                if (network === "near") {
                    const amountInYocto = utils.format.parseNearAmount(`${0.05}`);
                    console.log(response);
                    draftStore.setLicenceParameters({ type: response.license_hash, price: json.amountWei, isSell: true });
                    Near?.Contract.set_sale({
                        amount: amountInYocto,
                        args: {
                            amount: response.amount,
                            token_id: response.token_id,
                            is_enabled: response.is_enabled,
                            minter_amount: response.minter_amount,
                            master_amount: response.master_amount,
                            license_hash: response.license_hash,
                            expire: response.expire,
                            signature: response.signature,
                            v: response.v!,
                        },
                        callbackUrl: `${window.location.href}?nearAddLicenceSuccess=true`,
                    });
                } else {
                    const { wait } = await contract.setSale(
                        response.tokenId,
                        response.isEnabled,
                        response.amount,
                        response.minterAmount,
                        response.masterAmount,
                        response.licenseHash,
                        response.expire,
                        response.signature
                    );

                    await wait();
                }
                draftStore.setLicenceParameters({ type: draftStore.licence.type });
                handleClose();
            }

            if (draftStore.licence.type === "© all rights reserved, commercial distribution") {
                if (!draftStore.licence.price) {
                    draftStore.showDraftAlert(true);
                    draftStore.setDraftAlert({
                        type: "error",
                        title: t("errors.titles.smth_wrong"),
                        message: t("The price value cannot be zero."),
                    });
                }
                const convertPrice = await getConvertFromUsd({ amount: draftStore.licence.price! });
                let json = {
                    licenseHash: "",
                    amountWei: "0",
                    id: draftStore.draftId!,
                    network: network,
                };
                switch (network) {
                    case "near":
                        json = {
                            licenseHash: draftStore.licence.type,
                            amountWei: convertPrice.near,
                            id: draftStore.draftId!,
                            network: network,
                        };
                        break;
                    case "80001":
                        json = {
                            licenseHash: draftStore.licence.type,
                            amountWei: convertPrice.polygon,
                            id: draftStore.draftId!,
                            network: network,
                        };
                        break;
                    case "5":
                        json = {
                            licenseHash: draftStore.licence.type,
                            amountWei: convertPrice.eth,
                            id: draftStore.draftId!,
                            network: network,
                        };
                        break;
                    case "97":
                        json = {
                            licenseHash: draftStore.licence.type,
                            amountWei: convertPrice.bsc,
                            id: draftStore.draftId!,
                            network: network,
                        };
                        break;
                }

                const response = await setLinenseParameters(json);
                if (network === "near") {
                    const amountInYocto = utils.format.parseNearAmount(`${0.05}`);
                    const price = await getConvertFromWei({ amount: response.amount?.toString()! });
                    draftStore.setLicenceParameters({ type: response.license_hash, price: price.near, isSell: true });
                    Near?.Contract.set_sale({
                        amount: amountInYocto,
                        args: {
                            amount: response.amount,
                            token_id: response.token_id,
                            is_enabled: response.is_enabled,
                            minter_amount: response.minter_amount,
                            master_amount: response.master_amount,
                            license_hash: response.license_hash,
                            expire: response.expire,
                            signature: response.signature,
                            v: response.v!,
                        },
                        callbackUrl: `${window.location.href}?nearAddLicenceSuccess=true`,
                    });
                } else {
                    const { wait } = await contract.setSale(
                        response.tokenId,
                        response.isEnabled,
                        response.amount,
                        response.minterAmount,
                        response.masterAmount,
                        response.licenseHash,
                        response.expire,
                        response.signature
                    );
                    await wait();
                    draftStore.setLicenceParameters({ type: response.licenseHash, price: draftStore.licence.price, isSell: true });
                }
            }
            return true;
        } catch (e: any) {
            draftStore.showDraftAlert(true);
            console.log(`error`, e);
            const wait = async () => draftStore.setLicenceParameters({ type: undefined, price: undefined, isSell: undefined });
            await wait();
            if (e.code === 4001 || e.code === "ACTION_REJECTED") {
                draftStore.setDraftAlert({
                    type: "error",
                    title: t("errors.titles.choose_licence_canceled"),
                    message: t("errors.messages.choose_licence_canceled"),
                });
            } else if (e.code === -32603) {
                draftStore.setDraftAlert({
                    type: "error",
                    title: t("errors.titles.smth_wrong"),
                    message: t("errors.messages.reconnect_to_the_network"),
                });
            } else if (e.code === "UNPREDICTABLE_GAS_LIMIT") {
                draftStore.setDraftAlert({
                    type: "error",
                    title: t("errors.titles.execution"),
                    message: t("errors.messages.execution"),
                });
            }
            return false;
        }
    }, [draftStore, network]);

    const onPaymentReceived = useCallback(
        ({ action }: WSStatus) => {
            if (action === "monitoringStatus") {
                overlay.actions.close();
                overlay.actions.clear();
            }

            if (action === "scanCurrentStatus") {
                overlay.actions.setPaymentReceived();
            }
        },
        [overlay.actions]
    );

    const closeNear = useCallback(() => {
        overlay.actions.close();
        overlay.actions.clear();
        draftStore.setDraftStatus({
            type: "error",
            message: t("statuses.protect_failed"),
        });
        draftStore.showDraftAlert(true);
        draftStore.setDraftAlert({
            type: "error",
            title: t("errors.titles.protection_canceled"),
            message: t("errors.messages.protection_canceled"),
        });
    }, [overlay, draftStore]);

    const onScanSuccess = useCallback(
        async ({ data }: WSAction) => {
            console.log(data);
            console.log(window.location.pathname);
            if (data.receiver_id) {
                try {
                    if (data.token_id) {
                        const correctData: DraftState["mintPayload"] = {
                            tokenId: data.token_id,
                            to: data.to,
                            contentHash: data.content_hash,
                            metadataHash: data.metadata_hash,
                            licenseHash: data.license_hash,
                            metadata: data.metadata,
                            parentTokenId: data.parent_token_id,
                            version: data.version,
                            expire: data.expire,
                            signature: data.signature,
                            v: data.v,
                            receiver_id: data.receiver_id,
                        };

                        draftStore.setMintPayload(correctData);
                    } else {
                        draftStore.setMintPayload(data);
                    }
                    draftStore.showMint(true);
                    console.log(data);
                    alert.actions.open();
                    alert.actions.warn(t("warn.titles.do_not_exit_protection_near"), t("warn.messages.do_not_exit_protection_near"));

                    // Сменяем статус драфта что проверка на антиплагиант прошла успешно
                    overlay.actions.setWorkReady("near", closeNear);

                    const amountInYocto = utils.format.parseNearAmount("0.05");

                    const res = await Near?.Contract.nft_mint({
                        amount: amountInYocto,
                        args: {
                            token_id: data.token_id,
                            metadata: data.metadata,
                            receiver_id: data.receiver_id,
                            content_hash: data.content_hash,
                            metadata_hash: data.metadata_hash,
                            expire: data.expire,
                            signature: data.signature,
                            v: data.v,
                            parent_token_id: data.parent_token_id,
                            version: data.version,
                        },
                        callbackUrl: `${window.location.origin}/protected/${token_id}`,
                    });
                    console.log(`res`, res);
                } catch (e) {
                    console.log(e);
                }

                return;
            }
            try {
                // Пишем в стор метаданные для минта
                draftStore.setMintPayload(data);
                draftStore.showMint(true);
                alert.actions.open();
                alert.actions.warn(t("warn.titles.do_not_exit_protection"), t("warn.messages.do_not_exit_protection"));

                // Сменяем статус драфта что проверка на антиплагиант прошла успешно
                overlay.actions.setWorkReady();

                // Запускаем минт
                console.log(`success`, data);

                await contract.mint(data.tokenId, data.to, data.contentHash, data.metadataHash, data.expire, data.signature, data.parentTokenId, data.version);

                overlay.actions.setWaitingMint();
            } catch (e: any) {
                // В зависимости от того какая у нас ошибка - отображаем нужный нам статус
                draftStore.showDraftAlert(true);
                draftStore.setProtectLoading({ metaMask: false, fiat: false });
                // alert.actions.close();
                if (e.code === 4001 || e.code === "ACTION_REJECTED") {
                    draftStore.setDraftAlert({
                        type: "error",
                        title: t("errors.titles.mint_canceled"),
                        message: t("errors.messages.mint_canceled"),
                    });
                } else if (e.code === 32603) {
                    draftStore.setDraftAlert({
                        type: "error",
                        title: t("errors.titles.smth_wrong"),
                        message: t("errors.messages.smth_wrong"),
                    });
                } else if (e.code === "UNPREDICTABLE_GAS_LIMIT") {
                    draftStore.setDraftAlert({
                        type: "error",
                        title: t("errors.titles.execution"),
                        message: t("errors.messages.execution"),
                    });
                } else {
                    alert.actions.close();
                }

                console.log("[ERROR_CODE]", e);

                draftStore.setDraftStatus({ type: "error", message: t("statuses.protect_failed") });
                overlay.actions.close();
                overlay.actions.clear();
            }
        },
        [draftStore, alert.actions, t, overlay.actions, Near?.Contract, token_id, contract]
    );

    const onPreparingWork = useCallback(() => {
        overlay.actions.setPreparingWork();
        overlay.actions.open();
    }, [overlay.actions]);

    const onMetadataUploaded = useCallback(() => {
        overlay.actions.setMetadataUploaded();
    }, [overlay.actions]);

    const onScanFailed = useCallback(
        ({ scanId }: any) => {
            alert.actions.open();

            draftStore.setDraftStatus({
                type: "error",
                message: t("statuses.protect_failed"),
            });
            draftStore.showDraftAlert(true);
            draftStore.setDraftAlert({
                type: "error",
                title: t("errors.titles.scan_failed"),
                message: AlertMessage.SCAN_FAILED(t),
            });

            draftStore.setScanId(scanId);

            overlay.actions.clear();
            overlay.actions.close();

            draftStore.showOverlay(false);
            draftStore.setOverlay({
                type: undefined,
                message: undefined,
                step: undefined,
            });
            draftStore.setProtectLoading({ metaMask: false, fiat: false });
        },
        [alert.actions, draftStore, overlay.actions, t]
    );

    const onSendToCheckError = useCallback(() => {
        draftStore.showOverlay(false);
        draftStore.setOverlay({
            type: undefined,
            message: undefined,
            step: undefined,
        });
        draftStore.setDraftStatus({
            type: "error",
            message: t("statuses.protect_failed"),
        });
        draftStore.showDraftAlert(true);
        draftStore.setDraftAlert({
            type: "error",
            title: t("errors.titles.smth_wrong"),
            message: t("errors.messages.we_failed_to_check_file"),
        });
        draftStore.setProtectLoading({ metaMask: false, fiat: false });
        alert.actions.close();
    }, [alert.actions, draftStore, t]);

    useEffect(() => {
        if (nearLicence === "true") {
            overlay.actions.open();
            overlay.actions.setMintedWithLicence();
            setNearLicence("false");
            draftStore.setLicenceParameters({ type: undefined, price: undefined, isSell: undefined });

            draftStore.setDraftStatus({
                type: "success",
                message: t("statuses.protected"),
            });

            draftStore.showMint(false);
            draftStore.setMintPayload(undefined);

            alert.actions.close();

            draftStore.setProtectLoading({ metaMask: false, fiat: false, near: false, mintedSucces: true });
        }
        if (nearErrorCode === "userRejected") {
              if (draftStore.overlay.step === 8 && draftStore.overlay.type === "success") {
                  setNearErrorCode("false");
                   overlay.actions.setMintedWithoutLicense();
                  return;
              }
              if (draftStore.overlay.step === 6) {
                  setNearErrorCode("false");
                  overlay.actions.setMintedWithoutLicense()
                  return;
              }
            overlay.actions.close();
            // overlay.actions.open();
            // overlay.actions.setMinted();
            setNearErrorCode("false");
            // draftStore.setLicenceParameters({ type: undefined, price: undefined, isSell: undefined });

            draftStore.setDraftStatus({
                type: "error",
                message: t("statuses.protect_failed"),
            });
            alert.actions.open();
            alert.actions.error(t("errors.titles.protection_canceled"), t("errors.messages.protection_canceled"));
            // draftStore.setDraftStatus({
            //     type: "success",
            //     message: t("statuses.protected"),
            // });

            // draftStore.showMint(false);
            // draftStore.setMintPayload(undefined);

            // alert.actions.close();

            draftStore.setProtectLoading({ metaMask: false, fiat: false, near: false, mintedSucces: true });
        }
    }, [nearLicence, setNearLicence, nearErrorCode, setNearErrorCode]);

    const onMinted = useCallback(async () => {
        overlay.actions.setlLicence();
        console.log(`MINTED`);
        const status = await onSetLicence();

        // Сообщаем об успешной операции минта
        if (draftStore.paymentMethod !== "Near") {
            if (status) {
                overlay.actions.setMintedWithLicence();
            } else overlay.actions.setMintedWithoutLicense();
            draftStore.setLicenceParameters({ type: undefined, price: undefined, isSell: undefined });
            draftStore.setDraftStatus({
                type: "success",
                message: t("statuses.protected"),
            });

            draftStore.showMint(false);
            draftStore.setMintPayload(undefined);

            alert.actions.close();

            draftStore.setProtectLoading({ metaMask: false, fiat: false, mintedSucces: true });
        }
    }, [alert.actions, draftStore, onSetLicence, overlay.actions, t]);

    const onSendToEstimateError = useCallback(() => {
        draftStore.setProtectLoading({ metaMask: false, fiat: false });
        console.log("SEND_TO_ESTIMATE_ERROR");
    }, [draftStore]);

    const onSendToEstimateSucces = useCallback(() => {
        console.log("SEND_TO_ESTIMATE_SUCCESS");
    }, []);

    const onEstimateSuccess = useCallback(
        async (data: any) => {
            console.log(`onEstimateSucces`, data);
            /**
             * {data}
             * {
             *     "action": "scanEstimateCurrentStatus",
             *     "status": "estimatedSuccess",
             *     "scanId": "62e45f57d6faffc774538c5c",
             *     "draftId": "62e43f0a47697aa8d2e25cc9"
             * }
             */
            const { draftId } = data;

            draftStore.setDraftId(draftId);

            if (draftStore.paymentMethod === "Near") {
                try {
                    const { requestId, toBlockchain } = await backend.scans.startScan({ documentId: draftId, network: "near" });
                    await Near?.Contract?.pay_request({
                        amount: toBlockchain.priceInWei,
                        args: {
                            amount: toBlockchain.priceInWei,
                            request_id: requestId,
                            expire: toBlockchain.expire as string,
                            signature: toBlockchain.signature as number[],
                            v: toBlockchain.v!,
                        },
                    });
                } catch (e: any) {
                    // Close overlay and clean state
                    console.log(e);

                    console.log(e.Error);

                    overlay.actions.close();
                    overlay.actions.clear();
                    draftStore.showDraftAlert(false);
                    draftStore.setDraftAlert({
                        type: undefined,
                        title: undefined,
                        message: undefined,
                    });
                    // closeMessage();
                    draftStore.setDraftStatus({
                        type: "error",
                        message: t("statuses.protect_failed"),
                    });
                    draftStore.setProtectLoading({ near: false });

                    // code: 4001, message: 'MetaMask Tx Signature: User denied transaction signature.'
                    if (e.code === 4001 || e.code === "ACTION_REJECTED") {
                        draftStore.showDraftAlert(true);
                        draftStore.setDraftAlert({
                            type: "error",
                            title: t("errors.titles.protection_canceled"),
                            message: t("errors.messages.protection_canceled"),
                        });
                    } else if (e.code === -32603 || e.code === -32000) {
                        draftStore.showDraftAlert(true);
                        draftStore.setDraftAlert({
                            type: "error",
                            title: t("errors.titles.smth_wrong"),
                            message: t("You don't have enough funds."),
                        });
                    } else if (e.type === "REQUEST_VALIDATION_ERROR") {
                        draftStore.showDraftAlert(true);
                        draftStore.setDraftAlert({
                            type: "error",
                            title: t("errors.titles.near_connect_account"),
                            message: t("errors.messages.near_connect_account"),
                        });
                    }
                    console.log("[PROTECT] ", e);
                }
            }

            if (draftStore.paymentMethod === "Stripe") {
                try {
                    const { requestId } = await backend.scans.startScan({ documentId: draftId, network: getNetworkById(network)?.symbol });

                    const { url } = await backend.scans.getScanPaymentLink({
                        requestId,
                        successUrl: `${window.location.href}?stripeSucces=true`,
                        cancelUrl: `${window.location.href}?stripeSucces=true`,
                        network: getNetworkById(network)?.symbol || "",
                    });

                    window.open(url);

                    handleClose();
                    draftStore.showDraftAlert(false);
                    draftStore.setDraftStatus({
                        type: "warn",
                        message: t("statuses.protecting"),
                    });

                    overlay.actions.open();
                    overlay.actions.setPendingPaymentFiat(url);
                } catch (e: any) {
                    const error = await e.response.json();
                    overlay.actions.close();
                    overlay.actions.clear();
                    alert.actions.close();
                    draftStore.setDraftStatus({
                        type: "error",
                        message: t("statuses.protect_failed"),
                    });

                    draftStore.setProtectLoading({ fiat: false });
                    draftStore.showDraftAlert(true);
                    draftStore.setDraftAlert({
                        type: "error",
                        title: t("errors.titles.smth_wrong"),
                        message: error.data.message,
                    });
                    handleClose();
                }
            }

            if (draftStore.paymentMethod === "Metamask") {
                try {
                    const { toBlockchain } = await backend.scans.startScan({ documentId: draftId, network: getNetworkById(network)?.symbol });
                    const tx = await contract.payRequest(toBlockchain.requestId, toBlockchain.expire, toBlockchain.signature, {
                        value: toBlockchain.priceInWei,
                    });
                    const res = await tx.wait();
                    console.log("[TAX_WAIT]", res);
                } catch (e: any) {
                    // Close overlay and clean state
                    console.log(e);
                    overlay.actions.close();
                    overlay.actions.clear();
                    alert.actions.close();
                    draftStore.setDraftStatus({
                        type: "error",
                        message: t("statuses.protect_failed"),
                    });
                    draftStore.setProtectLoading({ metaMask: false });

                    // code: 4001, message: 'MetaMask Tx Signature: User denied transaction signature.'
                    if (e.code === 4001 || e.code === "ACTION_REJECTED") {
                        draftStore.showDraftAlert(true);
                        draftStore.setDraftAlert({
                            type: "error",
                            title: t("errors.titles.protection_canceled"),
                            message: t("errors.messages.protection_canceled"),
                        });
                    } else if (e.code === -32603 || e.code === "INSUFFICIENT_FUNDS") {
                        draftStore.showDraftAlert(true);
                        draftStore.setDraftAlert({
                            type: "error",
                            title: t("errors.titles.smth_wrong"),
                            message: t("You don't have enough funds."),
                        });
                    } else if (e.code === "UNSUPPORTED_OPERATION") {
                        draftStore.showDraftAlert(true);
                        draftStore.setDraftAlert({
                            type: "error",
                            title: t("errors.titles.smth_wrong"),
                            message: t(
                                "You are trying to protect a document on a network that we do not support. You should change the network to which we support"
                            ),
                        });
                    }
                    console.log(e.code);
                    console.log("[PROTECT] ", e);
                }
            }
        },
        [draftStore, handleClose, overlay.actions, t, contract]
    );

    const continueTransaction = useCallback(async () => {
        const hash = search.split("transactionHashes=")[1];
        console.log(hash);
        if (!data?.user.walletAddressNear) return;
        continueFromTransactionHash(hash, data?.user.walletAddressNear!);
    }, [data]);

    useEffect(() => {
        if (!data?.user) return;
        if (search.includes("transactionHashes")) {
            console.log(`search`);
            continueTransaction();
            overlay.actions.open();
            overlay.actions.setPaymentReceived();
        }
    }, [search, data, continueFromTransactionHash]);

    // Listen WS by Status
    useEffect(() => {
        if (!connected) return;

        client.addStatusHandler(ScanCurrentStatus.SCAN_FAILED, onScanFailed);
        client.addStatusHandler(ScanCurrentStatus.SCAN_SUCCESS, onScanSuccess);
        client.addStatusHandler(ScanCurrentStatus.PAYMENT_RECEIVED, onPaymentReceived);
        client.addStatusHandler(ScanCurrentStatus.SEND_TO_CHECK_ERROR, onSendToCheckError);
        client.addStatusHandler(ScanCurrentStatus.PREPARING_WORK, onPreparingWork);
        client.addStatusHandler(ScanCurrentStatus.METADATA_UPLOADED, onMetadataUploaded);
        client.addStatusHandler(ScanCurrentStatus.MINTED, onMinted);
        client.addStatusHandler(ScanCurrentStatus.SEND_TO_ESTIMATE_ERROR, onSendToEstimateError);
        client.addStatusHandler(ScanCurrentStatus.SEND_TO_ESTIMATE_SUCCESS, onSendToEstimateSucces);
        client.addStatusHandler(ScanCurrentStatus.ESTIMATE_SUCCESS, onEstimateSuccess);

        return () => {
            client.removeStatusHandler(ScanCurrentStatus.SCAN_FAILED, onScanFailed);
            client.removeStatusHandler(ScanCurrentStatus.SCAN_SUCCESS, onScanSuccess);
            client.removeStatusHandler(ScanCurrentStatus.PAYMENT_RECEIVED, onPaymentReceived);
            client.removeStatusHandler(ScanCurrentStatus.SEND_TO_CHECK_ERROR, onSendToCheckError);
            client.removeStatusHandler(ScanCurrentStatus.PREPARING_WORK, onPreparingWork);
            client.removeStatusHandler(ScanCurrentStatus.METADATA_UPLOADED, onMetadataUploaded);
            client.removeStatusHandler(ScanCurrentStatus.MINTED, onMinted);
            client.removeStatusHandler(ScanCurrentStatus.SEND_TO_ESTIMATE_ERROR, onSendToEstimateError);
            client.removeStatusHandler(ScanCurrentStatus.SEND_TO_ESTIMATE_SUCCESS, onSendToEstimateSucces);
            client.removeStatusHandler(ScanCurrentStatus.ESTIMATE_SUCCESS, onEstimateSuccess);
        };
    }, [
        client.removeStatusHandler,
        client.addStatusHandler,
        connected,
        client,
        onScanFailed,
        onScanSuccess,
        onPaymentReceived,
        onSendToCheckError,
        onPreparingWork,
        onMetadataUploaded,
        onMinted,
        onEstimateSuccess,
        onSendToEstimateSucces,
        onSendToEstimateError,
    ]);
}


