import { useQuery } from 'react-query';
import { backend } from 'api';


export default function useDraft(id?: string) {
    const { data: draft, ...options } = useQuery([ 'DRAFT', id], () => backend.drafts.getDraft(id), {
        enabled: !!id,
        retry: false,
        refetchOnWindowFocus: false,
        keepPreviousData: true
    });

    return {
        draft,
        ...options
    };
}
