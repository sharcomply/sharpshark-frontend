import { useQuery } from 'react-query';
import { backend } from 'api';


export default function useDraftVersions(commonId?: string) {
    const request = () => {
        return backend.drafts.getDraftList({ commonId })
    }

    return useQuery([ 'DRAFT_VERSIONS' ], request, {
        enabled: !!commonId,
        retry: false,
        refetchOnWindowFocus: false,
        keepPreviousData: true
    })
}
