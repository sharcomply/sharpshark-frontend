import { useMutation, useQueryClient } from "react-query";
import { backend } from "api";
import { useCallback, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useSidebarCounts } from "features/Sidebar/hooks/useSidebarCounts";
import { useDraftAlert } from "./useDraftAlert";
import { DraftEditFormState } from "./useDraftEditForm";
import { fetchFile } from "api/requests/drafts";
import { useTranslation } from "react-i18next";
import useDraftStore from "./useDraftStore";

export interface UpdateDraftForm {
    title: string;
    text: string;
    file?: File;
}

export const useDraftUpdate = () => {
    const { t } = useTranslation()
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const { refetch } = useSidebarCounts();
    const alert = useDraftAlert();
    const [isLoading, setLoading] = useState(false)
    const { draftId, protectId } = useParams();
    const draftStore = useDraftStore();

    const { mutateAsync: update, isLoading: editLoading } = useMutation(backend.drafts.editDraft, {
        async onSuccess() {
            await Promise.all([
                queryClient.invalidateQueries([`DRAFT_LIST`]),
                queryClient.invalidateQueries(["DRAFT"])
            ]);
        },
    });

    const handleUpdate = useCallback(
        async (form: DraftEditFormState) => {
            if (!draftId && !protectId) return;
            draftStore.setDraftStatus({
                type: "success",
                message: t("statuses.uploading")
            });
            // Clear all errors before sending request
            alert.actions.close();
            alert.actions.clear();

            const payload = {
                uploadFile: form.file || form.image,
                fileExt: "txt",
            }

            setLoading(true)

            if (form.type === 'text' && !form.file) {
                const blob = new Blob([form.text], { type: "text/plain" });
                payload.uploadFile = new File([blob], form.title);
            }

            if (form.type === 'image' && form.image) {
                payload.fileExt = form.image.type.split("/")[1]
            }

            try {

                // Get Links for Upload And Download
                const { uploadLink, downloadLink } = await backend.drafts.getFileUploadLink(draftId! || protectId!, payload.fileExt);

                if (form.file?.type === 'text/plain') {
                    const uploadedFile = await fetchFile(downloadLink)
                    const uploadedFileTxt = await uploadedFile?.text()
                    if (uploadedFileTxt && uploadedFileTxt?.length < 50) {
                        alert.actions.open()
                        alert.actions.error(t("errors.titles.validation"), t("errors.messages.small_length"))
                        setLoading(false)
                        return
                    }
                }
                // Upload Link
                await backend.drafts.uploadFile(uploadLink.url, payload.uploadFile!, uploadLink.fields);

                const uploadFile = async () => {
                    let additionalImages: string[] = [];
                    for (const item of form.additionalImages!) {
                        if (typeof item !== "string") {
                            const { uploadLink, downloadLink } = await backend.disputes.getImageUploadLink(draftId! || protectId!);
                            additionalImages.push(downloadLink);
                            await backend.drafts.uploadFile(uploadLink.url, item, uploadLink.fields);
                        } else additionalImages.push(item);
                    }
                    return { additionalImages };
                };

                const { additionalImages } = await uploadFile();

                console.log(additionalImages)


                // update draft using link to file
                await update({
                    _id: draftId || protectId!,
                    title: form.title,
                    type: form.type,
                    linkUrl: downloadLink,
                    fullTitle: form.fullTitle,
                    authors: form.authors.filter(i => i.length),
                    coAuthors: form.coAuthors.filter(i => i.length),
                    participants: form.participants.filter(i => i.length),
                    additionalImages
                });

                draftStore.setDraftStatus({
                    type: "success",
                    message: t("statuses.saved")
                });

                // Redirect to created draft
                await refetch();
                // navigate(`/drafts/${draft._id}`, { replace: true });

                alert.actions.close()
                alert.actions.clear()

                setLoading(false)

            } catch (e) {
                console.log("[DRAFT_SAVE]", e);
                setLoading(false)
            }
        },
        [draftId, update, refetch, navigate, alert.actions, draftStore]
    );

    return {
        handleUpdate,
        isLoading: editLoading || isLoading,
    };
};
