import { useEffect } from "react";
import { useDraftMessages } from "./useDraftMessages";

const useDraftOnBeforeUnload = () => {
  const { draftMessage } = useDraftMessages();

  useEffect(() => {
    const eventListener = (event: any) => (event.returnValue = '');
    if (draftMessage.show) {
      window.addEventListener('beforeunload', eventListener);

    }
    if (!draftMessage.show) {
      window.removeEventListener('beforeunload', eventListener);
    }

    return () => {
      window.removeEventListener('beforeunload', eventListener);
    };
  }, [draftMessage.show]);
}

export default useDraftOnBeforeUnload