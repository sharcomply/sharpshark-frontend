import { backend } from "api";
import { useModal } from "features/Modals/hooks/useModal";
import { ModalContent, ModalType } from "features/Modals/interfaces";
import { ScanCurrentStatus } from "features/Scan/ScanClient";
import { useScanClient } from "features/Scan/ScanProvider";
import { WSStatus } from "features/Websocket/interfaces/ws.interfaces";
import { useCrossTabState } from "hooks/useCrossTbState";
import { useCallback, useEffect } from "react";
import { useTranslation } from "react-i18next";
import useDraftStore from "./useDraftStore";

export function useMonitoringListener() {
    const { client, connected } = useScanClient();
    const draftStore = useDraftStore();
    const { handleClose } = useModal();
    const { t } = useTranslation();
    const { handleOpen, setModalContent, setModalStatus } = useModal();
    const [nearStartMonitoringSuccess, setNearStartMonitoringSuccess] = useCrossTabState("nearMonitoringSuccess", "");
    const [nearErrorCode, setNearErrorCode] = useCrossTabState("errorCode", "");

    const onPaymentReceivedByNear = useCallback(() => {
        draftStore.setDraftStatus({
            type: "warn",
            message: t("statuses.tracking"),
        });
    }, [draftStore])



    const onPaymentReceived = useCallback(({ status }: WSStatus) => {
        if (status === "paymentReceived") {
            console.log("Payment for monitoring was successful");
        }
    }, []);

    const onSendToEstimateError = useCallback(() => {
        draftStore.setProtectLoading({ metaMask: false, fiat: false });
        console.log("SEND_TO_ESTIMATE_ERROR");
    }, [draftStore]);

    const onSendToEstimateSucces = useCallback(() => {
        console.log("SEND_TO_ESTIMATE_SUCCESS");
    }, []);

    const onEstimateSucces = useCallback(() => {
        try {
            backend.scans
                .getMonitoringPaymentLink({
                    requestId: draftStore.paymentPayload.requestId!,
                    successUrl: `${window.location.href}?stripeSucces=true`,
                    cancelUrl: `${window.location.href}?stripeSucces=true`,
                })
                .then((data) => {
                    window.open(data.url);

                    handleClose();
                });
            console.log("ESTIMATE_SUCCESS");
        } catch (error) { }
    }, [draftStore, handleClose]);

    useEffect(() => {
        if (nearStartMonitoringSuccess === "true") {
            onPaymentReceivedByNear()
            setNearStartMonitoringSuccess("false")
        }
    }, [nearStartMonitoringSuccess, setNearStartMonitoringSuccess, onPaymentReceivedByNear])

    useEffect(() => {
        if (nearErrorCode === "userRejected" && localStorage.getItem(`${window.location.href}`) === 'true') {
            handleOpen({ type: ModalType.SHARED, content: ModalContent.CustomModal });
            setModalStatus("Cancel");
            setModalContent("Monitoring payment was canceled");
            setNearErrorCode("false")
            localStorage.removeItem(`${window.location.href}`)
            draftStore.setProtectLoading({ near: false });

        }
    }, [nearErrorCode, setNearErrorCode])

    // Listen WS by Status
    useEffect(() => {
        if (!connected) return;
        client.addStatusHandler(ScanCurrentStatus.SEND_TO_ESTIMATE_ERROR, onSendToEstimateError);
        client.addStatusHandler(ScanCurrentStatus.SEND_TO_ESTIMATE_SUCCESS, onSendToEstimateSucces);
        client.addStatusHandler(ScanCurrentStatus.ESTIMATE_SUCCESS, onEstimateSucces);
        client.addStatusHandler(ScanCurrentStatus.PAYMENT_RECEIVED, onPaymentReceived);

        return () => {
            client.removeStatusHandler(ScanCurrentStatus.SEND_TO_ESTIMATE_ERROR, onSendToEstimateError);
            client.removeStatusHandler(ScanCurrentStatus.SEND_TO_ESTIMATE_SUCCESS, onSendToEstimateSucces);
            client.removeStatusHandler(ScanCurrentStatus.ESTIMATE_SUCCESS, onEstimateSucces);
            client.removeStatusHandler(ScanCurrentStatus.PAYMENT_RECEIVED, onPaymentReceived);
        };
    }, [
        client.removeStatusHandler,
        client.addStatusHandler,
        connected,
        client,
        onPaymentReceived,
        onEstimateSucces,
        onSendToEstimateSucces,
        onSendToEstimateError,
    ]);
}
