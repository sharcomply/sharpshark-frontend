import { useCallback } from "react";
import useDraftStore from "./useDraftStore";

export function useDraftMessages() {
  const { draftMessage, setDraftMessange, showDraftMessages } = useDraftStore();

  const open = useCallback(() => {
    showDraftMessages(true)
  }, [showDraftMessages])

  const close = useCallback(() => {
    showDraftMessages(false)
  }, [showDraftMessages])

  const clear = useCallback(() => {
    setDraftMessange({ show: false, message: undefined });
  }, [setDraftMessange])

  return {
    draftMessage,
    actions: {
      close,
      clear,
      open,
    }
  }
}
