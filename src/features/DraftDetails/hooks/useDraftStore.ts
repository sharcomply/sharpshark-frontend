import { useAppDispatch, useAppSelector } from "store";
import { useCallback } from "react";
import { draftActions, DraftState } from "../../DraftDetails/store";

export default function useDraftStore() {
    const state = useAppSelector((state) => state.draft);
    const dispatch = useAppDispatch();

    const setDraftId = useCallback((draftId: DraftState["draftId"]) => {
        dispatch(draftActions.setDraftId(draftId));
    }, [dispatch]);

    const setTokenId = useCallback((tokenId: DraftState["tokenId"]) => {
        dispatch(draftActions.setTokenId(tokenId));
    }, [dispatch]);

    const setDraftType = useCallback((draftType: DraftState["draftType"]) => {
        dispatch(draftActions.setDraftType(draftType));
    }, [dispatch]);

    const setScanId = useCallback((draftId: DraftState["scanId"]) => {
        dispatch(draftActions.setScanId(draftId));
    }, [dispatch]);

    const setDraftStatus = useCallback((status: DraftState["status"]) => {
        dispatch(draftActions.setDraftStatus(status));
    }, [dispatch]);

    const setDraftMessange = useCallback((status: DraftState["draftMessage"]) => {
        dispatch(draftActions.setDraftMessange(status));
    }, [dispatch]);

    const showDraftAlert = useCallback((status: boolean) => {
        dispatch(draftActions.showDraftAlert(status));
    }, [dispatch]);

    const showDraftMessages = useCallback((status: boolean) => {
        dispatch(draftActions.showDraftMessages(status));
    }, [dispatch]);

    const setDraftAlert = useCallback((error: Omit<DraftState["alert"], "show">) => {
        dispatch(draftActions.setDraftAlert(error));
    }, [dispatch]);

    const showOverlay = useCallback((status: boolean) => {
        dispatch(draftActions.showOverlay(status));
    }, [dispatch]);

    const setOverlay = useCallback((error: Omit<DraftState["overlay"], "show">) => {
        dispatch(draftActions.setOverlay(error));
    }, [dispatch]);

    const setMintPayload = useCallback((data: DraftState["mintPayload"]) => {
        dispatch(draftActions.setMintPayload(data));
    }, [dispatch]);

    const setScanPaymentPayload = useCallback((data: DraftState["paymentPayload"]) => {
        dispatch(draftActions.setScanPaymentPayload(data));
    }, [dispatch])

    const setProtectLoading = useCallback((data: DraftState["protectLoading"]) => {
        dispatch(draftActions.setProtectLoading(data));
    }, [dispatch])

    const setLicenceParameters = useCallback((data: DraftState["licence"]) => {
        console.log(data)
        dispatch(draftActions.setLicenceParameters(data));
    }, [dispatch])

    const setMintedNetworks = useCallback((data: DraftState["mintedNetworks"]) => {
        dispatch(draftActions.setMintedNetworks(data));
    }, [dispatch])

    const showMint = useCallback((value: boolean) => {
        dispatch(draftActions.showMint(value));
    }, [dispatch])

    const setPaymentMethod = useCallback((value: "Stripe" | "Metamask" | "Near") => {
        dispatch(draftActions.setPaymentMethod(value))
    }, [dispatch])

    const setDraftStatusBeforeProtect = useCallback((data: DraftState["draftStatusBeforeProtect"]) => {
        dispatch(draftActions.setDraftStatusBeforeProtect(data))
    }, [dispatch])

    const setImageInTextEditor = useCallback((imageTextEditor: DraftState["imageTextEditor"]) => {
        dispatch(draftActions.setImageInTextEditor(imageTextEditor))
    }, [dispatch])



    const clearStore = useCallback(() => {
        dispatch(draftActions.clearStore());
    }, [dispatch])

    return {
        ...state,
        setDraftId,
        setDraftType,
        setScanId,
        setDraftStatus,
        showDraftAlert,
        setDraftAlert,
        showOverlay,
        showDraftMessages,
        setOverlay,
        setLicenceParameters,
        setDraftMessange,
        setProtectLoading,
        setMintedNetworks,
        setMintPayload,
        setScanPaymentPayload,
        setDraftStatusBeforeProtect,
        showMint,
        setTokenId,
        setImageInTextEditor,
        clearStore,
        setPaymentMethod
    };
}
