import styled from "@emotion/styled";
import { Group, Stack } from "@mantine/core";
import { useCallback, useEffect, useState } from "react";
import { useContract } from "blockchain/hooks/useContract";
import { useModal } from "features/Modals/hooks/useModal";
import { ModalContent, ModalType } from "features/Modals/interfaces";
import { useDraftListener } from "features/DraftDetails/hooks/useDraftListener";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import { PageTitle } from "features/DraftDetails/components/PageTitle";
import { Alert } from "features/DraftDetails/components/Alert";
import { Overlay } from "features/DraftDetails/components/Overlay";
import { SharkButton } from "components/SharkButton";
import { useDraftUpdate } from "features/DraftDetails/hooks/useDraftUpdate";
import { useDraftOverlay } from "features/DraftDetails/hooks/useDraftOverlay";
import { DraftEditFormState, useDraftEditForm } from "./hooks/useDraftEditForm";
import { DraftEditForm } from "./components/DraftEditForm";
import { useDraftAlert } from "./hooks/useDraftAlert";
import { useTranslation } from "react-i18next";
import useDraftOnBeforeUnload from "./hooks/useDraftOnBeforeUnload";
import { useDraftMessages } from "./hooks/useDraftMessages";
import { utils } from "near-api-external-link-fork";
import { useNearContext } from "../../providers/NearProvider";

// Props
type ComponentProps = {
    draft: DraftDetails;
};

// Component
export default function DraftDetails({ draft }: ComponentProps) {
    const [isCollapseOpened, setIsCollapseOpened] = useState<boolean | undefined>();
    const { form } = useDraftEditForm({
        title: draft.title,
        type: draft.type || "text",
        text: draft.text || "",
        file: undefined,
        image: draft.imageFile,
        authors: draft.authors,
        coAuthors: draft.coAuthors,
        fullTitle: (draft.isMinted ? draft.metadata?.Title : draft?.fullTitle) || "",
        participants: draft.participants,
        additionalImages: draft.additionalImages,
    });

    const { t } = useTranslation();

    const alert = useDraftAlert();
    const overlay = useDraftOverlay();
    const contract = useContract();
    const { handleOpen } = useModal();
    const { canMint, mintPayload, status, setDraftStatus, setDraftId, setDraftType, setDraftStatusBeforeProtect } = useDraftStore();
    const { handleUpdate, isLoading } = useDraftUpdate();
    const [canProtect, setCanProtect] = useState(false);
    const [openProtect, setOpenProtect] = useState(false);
    const draftStore = useDraftStore();
    const Near = useNearContext();
    const {
        actions: { open: openMessage, close: closeMessage },
    } = useDraftMessages();

    // useEffect(() => {
    //     if (overlay.state.type === "success") {
    //         handleOpen({ type: ModalType.PROTECTED, content: ModalContent.LicenseAndMode });
    //     }
    // }, [handleOpen, overlay.state.type]);

    useEffect(() => {
        setDraftStatusBeforeProtect("new");
        if (status.message === "Saved" && openProtect) {
            handleOpen({ type: ModalType.PROTECTED, content: ModalContent.LicenseAndMode });
        }
        return () => {
            setOpenProtect(false);
        };
    }, [handleOpen, openProtect, status.message]);

    useEffect(() => {
        if (!draft) return;

        if (!draft.text && !draft.imageFile) {
            alert.actions.open();
            alert.actions.error(t("errors.titles.draft_empty"), t("errors.messages.draft_empty"));
            setCanProtect(true);
        }

        setDraftId(draft._id);
        setDraftType(draft.type);
        return () => {
            setDraftId(undefined);
            setDraftType(undefined);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [draft]);

    useEffect(() => {
        if (canMint && !overlay.state.show) {
            openMessage();
        } else {
            closeMessage();
        }
    }, [canMint, closeMessage, openMessage, overlay.state.show, t]);

    useDraftListener();
    useDraftOnBeforeUnload();

    const onMint = useCallback(async () => {
        if (!mintPayload || !canMint) return;
        setDraftStatus({ type: undefined, message: undefined });
        if (draftStore.paymentMethod === "Near") {
            overlay.actions.open();
            overlay.actions.setWorkReady();
            alert.actions.open();
            alert.actions.warn(t("warn.titles.do_not_exit_protection_near"), t("warn.messages.do_not_exit_protection_near"));
            const amountInYocto = utils.format.parseNearAmount("0.05");

            const res = await Near?.Contract.nft_mint({
                amount: amountInYocto,
                args: {
                    token_id: mintPayload.tokenId,
                    metadata: mintPayload.metadata,
                    receiver_id: mintPayload.receiver_id,
                    content_hash: mintPayload.contentHash,
                    metadata_hash: mintPayload.metadataHash,
                    expire: mintPayload.expire,
                    signature: mintPayload.signature,
                    v: mintPayload.v,
                    parent_token_id: mintPayload.parentTokenId,
                    version: mintPayload.version,
                },
                callbackUrl: `${window.location.origin}/protected/${mintPayload.tokenId}`,
            });

            console.log("res", res);
            overlay.actions.setWaitingMint();
        } else {
            try {
                // Сменяем статус драфта что проверка на антиплагиант прошла успешно
                overlay.actions.open();
                overlay.actions.setWorkReady();
                alert.actions.open();
                alert.actions.warn(t("warn.titles.do_not_exit_protection"), t("warn.messages.do_not_exit_protection"));

                // Запускаем минт
                await contract.mint(
                    mintPayload.tokenId,
                    mintPayload.to,
                    mintPayload.contentHash,
                    mintPayload.metadataHash,
                    mintPayload.expire,
                    mintPayload.signature,
                    mintPayload.parentTokenId,
                    mintPayload.version
                );

                overlay.actions.setWaitingMint();
            } catch (e: any) {
                // В зависимости от того какая у нас ошибка - отображаем нужный нам статус
                alert.actions.open();
                if (e.code === 4001 || e.code === "ACTION_REJECTED") {
                    alert.actions.error(t("errors.titles.mint_canceled"), t("errors.messages.mint_canceled"));
                } else if (e.code === 32603) {
                    alert.actions.error(t("errors.titles.smth_wrong"), t("errors.messages.smth_wrong"));
                } else if (e.code === "UNPREDICTABLE_GAS_LIMIT") {
                    alert.actions.error(t("errors.titles.execution"), t("errors.messages.execution"));
                } else if (e.code === "INVALID_ARGUMENT") {
                    alert.actions.error(t("errors.titles.smth_wrong"), t("Invalid Argument"));
                }
                console.log(e);

                setDraftStatus({ type: "error", message: t("statuses.protect_failed") });
                overlay.actions.close();
                overlay.actions.clear();
            }
        }
    }, [alert.actions, canMint, contract, mintPayload, overlay.actions, setDraftStatus, t]);

    const onProtect = useCallback(async () => {
        await handleUpdate(form.control._formValues as DraftEditFormState);
        setOpenProtect(true);
        handleCloseCollapses();
    }, [form.control._formValues, handleUpdate]);

    const handleCloseCollapses = useCallback(() => {
        setIsCollapseOpened(true);
        const timout = setTimeout(() => {
            setIsCollapseOpened(undefined);
            clearInterval(timout);
        }, 1);
    }, [setIsCollapseOpened]);

    // Render
    const actionItems = () => (
        <Group>
            <SharkButton
                type="submit"
                buttonType={"secondary"}
                variant={"outlined"}
                loading={isLoading}
                disabled={isLoading || status.message === t("statuses.protecting") || overlay.state.show}
            >
                × {t("buttons.save_close_btn")}
            </SharkButton>
            {canMint && (
                <Group onClick={handleCloseCollapses}>
                    <SharkButton
                        type="button"
                        onClick={onMint}
                        buttonType={"secondary"}
                        variant={"filled"}
                        disabled={status.message === t("statuses.protecting") || overlay.state.show}
                    >
                        {t("buttons.mint_btn")}
                    </SharkButton>
                </Group>
            )}
            {!canMint && (
                <Group onClick={handleCloseCollapses}>
                    <SharkButton
                        type="button"
                        onClick={form.handleSubmit(onProtect)}
                        buttonType={"secondary"}
                        variant={"filled"}
                        disabled={isLoading || status.message === t("statuses.protecting") || overlay.state.show || canProtect}
                    >
                        {t("buttons.protect_btn")}
                    </SharkButton>
                </Group>
            )}
        </Group>
    );

    return (
        <form onSubmit={form.handleSubmit(handleUpdate)}>
            <Stack>
                <PageTitle title={draft?.title || t("drafts.edit-draft")} tooltipText={t("tooltips.drafts_arent_protected")} actions={actionItems()} />

                {alert.state.show && <Alert title={alert.state.title} text={alert.state.message} variant={alert.state.type} />}

                <FormRoot spacing={0} sx={{ minHeight: "380px", overflowY: "scroll", maxHeight: overlay.state.show ? "100vh" : "auto" }}>
                    {overlay.state.show && <Overlay text={overlay.state.message} variant={overlay.state.type} />}

                    <DraftEditForm isLoading={isLoading} isCollapseClosed={isCollapseOpened} form={form} />
                </FormRoot>
            </Stack>
        </form>
    );
}

const FormRoot = styled(Stack)`
    position: relative;
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    overflow: hidden;
    justify-content: space-between;

    img {
        max-width: 480px;
        height: auto !important;
    }
`;
