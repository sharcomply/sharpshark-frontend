import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ReactNode } from "react";

// Draft Statuses
export enum OverlaySteps {
    PENDING_PAYMENT,
    PENDING_PAYMENT_FIAT,
    PAYMENT_RECEIVED,
    SEND_TO_CHECK_SUCCESS,
    SEND_TO_CHECK_ERROR,
    PREPARING_WORK,
    METADATA_UPLOADED,
    SCAN_SUCCESS,
    MINTED,
}

export enum DraftError {
    SCAN_FAILED,
    SCAN_PAYMENT_FAILED,
    MINT_PAYMENT_FAILED,
}

// Types
export interface DraftState {
    draftId?: string;
    draftType?: string;
    tokenId?: string;
    scanId?: string;
    status: {
        type?: "error" | "warn" | "success";
        message?: string;
    };
    canMint?: boolean;
    alert: {
        show: boolean;
        type?: "error" | "warn" | "info";
        title?: string;
        message?: string | ReactNode;
    };
    overlay: {
        show: boolean;
        type?: "success" | "default" | "licenseError";
        step?: OverlaySteps;
        message?: string | ReactNode;
    };
    mintPayload?: {
        tokenId: string;
        to: string;
        contentHash: string;
        metadataHash: string;
        licenseHash: string;
        parentTokenId: number;
        version: number;
        expire: string;
        signature: string | number[];
        metadata?: any;
        receiver_id?: string;
        v?: number;
    };
    paymentPayload: {
        requestId?: string;
    };
    protectLoading: {
        metaMask?: boolean;
        near?: boolean;
        fiat?: boolean;
        mintedSucces?: boolean;
    };
    licence: {
        type?: string;
        price?: string;
        isSell?: boolean;
    };
    draftMessage: {
        show: boolean;
        message?: string | ReactNode;
    };
    paymentMethod?: "Stripe" | "Metamask" | "Near";
    mintedNetworks?: string;
    draftStatusBeforeProtect?: "new" | "upgrade";
    imageTextEditor?: string;
}

// Initial State
const initialState: DraftState = {
    draftId: undefined,
    draftType: undefined,
    tokenId: undefined,
    status: {
        type: undefined,
        message: undefined,
    },
    canMint: false,
    alert: {
        show: false,
        type: undefined,
        message: undefined,
    },
    overlay: {
        show: false,
        type: undefined,
        step: undefined,
        message: undefined,
    },
    mintPayload: undefined,
    paymentPayload: {
        requestId: undefined,
    },
    protectLoading: {
        metaMask: false,
        fiat: false,
        mintedSucces: false,
        near: false
    },
    licence: {
        type: undefined,
        price: undefined,
        isSell: undefined,

    },
    draftMessage: {
        show: false,
        message: undefined
    },
    paymentMethod: undefined,
    mintedNetworks: undefined,
    draftStatusBeforeProtect: undefined,
    imageTextEditor: undefined,
};

// Slice
const draftSlice = createSlice({
    name: "draft",
    initialState,
    reducers: {
        setDraftId: (state, action: PayloadAction<DraftState["draftId"]>) => {
            state.draftId = action.payload;
        },
        setTokenId: (state, action: PayloadAction<DraftState["tokenId"]>) => {
            state.tokenId = action.payload;
        },
        setScanId: (state, action: PayloadAction<DraftState["scanId"]>) => {
            state.scanId = action.payload;
        },
        setDraftType: (state, action: PayloadAction<DraftState["draftType"]>) => {
            state.draftType = action.payload;
        },

        setDraftStatus: (state, action: PayloadAction<DraftState["status"]>) => {
            state.status.type = action.payload.type;
            state.status.message = action.payload.message;
        },
        showDraftAlert: (state, action: PayloadAction<boolean>) => {
            state.alert.show = action.payload;
        },
        showDraftMessages: (state, action: PayloadAction<boolean>) => {
            state.draftMessage.show = action.payload;
        },
        setDraftAlert: (state, action: PayloadAction<Omit<DraftState["alert"], "show">>) => {
            state.alert.type = action.payload.type;
            state.alert.title = action.payload.title;
            state.alert.message = action.payload.message;
        },
        setDraftMessange: (state, action: PayloadAction<Omit<DraftState["draftMessage"], "show">>) => {
            state.alert.message = action.payload.message;
        },
        showOverlay: (state, action: PayloadAction<boolean>) => {
            state.overlay.show = action.payload;
        },
        setOverlay: (state, action: PayloadAction<Omit<DraftState["overlay"], "show">>) => {
            state.overlay.type = action.payload.type;
            state.overlay.step = action.payload.step;
            state.overlay.message = action.payload.message;
        },
        setMintPayload: (state, action: PayloadAction<DraftState["mintPayload"]>) => {
            state.mintPayload = action.payload;
        },
        setScanPaymentPayload: (state, action: PayloadAction<DraftState["paymentPayload"]>) => {
            state.paymentPayload.requestId = action.payload.requestId
        },
        setProtectLoading: (state, action: PayloadAction<DraftState["protectLoading"]>) => {
            state.protectLoading.metaMask = action.payload.metaMask
            state.protectLoading.near = action.payload.near
            state.protectLoading.fiat = action.payload.fiat
            state.protectLoading.mintedSucces = action.payload.mintedSucces
        },
        setLicenceParameters: (state, action: PayloadAction<DraftState["licence"]>) => {
            state.licence.price = action.payload.price
            state.licence.type = action.payload.type
            state.licence.isSell = action.payload.isSell
        },

        setMintedNetworks: (state, action: PayloadAction<DraftState["mintedNetworks"]>) => {
            state.mintedNetworks = action.payload
        },

        showMint: (state, action: PayloadAction<boolean>) => {
            state.canMint = action.payload;
        },

        setPaymentMethod: (state, action: PayloadAction<"Stripe" | "Metamask" | "Near">) => {
            state.paymentMethod = action.payload
        },

        setDraftStatusBeforeProtect: (state, action: PayloadAction<DraftState["draftStatusBeforeProtect"]>) => {
            state.draftStatusBeforeProtect = action.payload
        },

        setImageInTextEditor: (state, action: PayloadAction<DraftState["imageTextEditor"]>) => {
            state.imageTextEditor = action.payload
        },

        clearStore: (state) => ({
            ...initialState,
            scanId: state.scanId,
        }),
    },
});

export const draftReducers = draftSlice.reducer;
export const draftActions = draftSlice.actions;

