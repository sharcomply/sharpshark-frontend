import styled from "@emotion/styled";
import { CloseButton, Group, Image, Stack, Text, Textarea } from "@mantine/core";
import Icon from "components/Icon";
import { Fragment, useCallback, useEffect, useRef, useState } from "react";
import { Dropzone } from "@mantine/dropzone";
import { formatBytes } from "helpers/formatBytes";
import ComingSoonWrapper from "components/ComingSoonWrapper";
import { Controller, useFieldArray, UseFormReturn, useWatch } from "react-hook-form";
import { DraftEditFormState } from "../hooks/useDraftEditForm";
import { SharkButton } from "components/SharkButton";
import { Collapse } from "./Collapse";
import { ListInput } from "./ListInput";
import { useTranslation } from "react-i18next";
import { DraftSkeletonForm } from "./DraftSkeletonForm";
import { usePrompt } from "hooks/usePrompt";
import DraftJs from "features/DraftJs/DraftJs";
import { Alert } from "./Alert";

interface FormProps {
    form: UseFormReturn<DraftEditFormState>;
    isCollapseClosed: boolean | undefined;
    isLoading?: boolean;
}

export const DraftEditForm = ({ form, isCollapseClosed, isLoading }: FormProps) => {
    const fileRef = useRef<any>(null);
    const imageRef = useRef<any>(null);
    const { control, setValue } = form as any;
    const { t } = useTranslation();
    const file = useWatch({ control, name: "file" });
    const image = useWatch({ control, name: "image" });
    const text = useWatch({ control, name: "text" });
    const type = useWatch({ control, name: "type" });
    const fullTitle = useWatch({ control, name: "fullTitle" });
    const [initialValues, setInitialValues] = useState<DraftEditFormState | undefined>();

    useEffect(() => {
        setInitialValues(form.getValues());
    }, []);

    const isFormDirty = (): boolean => {
        if (!initialValues) return false;
        return (
            file !== initialValues.file ||
            image !== initialValues.image ||
            text !== initialValues.text ||
            type !== initialValues.type ||
            fullTitle !== initialValues.fullTitle ||
            !!form.formState.dirtyFields.title ||
            !!form.formState.dirtyFields.authors ||
            !!form.formState.dirtyFields.coAuthors ||
            !!form.formState.dirtyFields.participants ||
            !!form.formState.dirtyFields.additionalImages
        );
    };

    usePrompt("Are you sure you want to leave this page? You have unsaved changes.", isFormDirty());

    const { fields: authors, append: appendAuthors, remove: removeAuthors } = useFieldArray({ control, name: "authors" });
    const { fields: coAuthors, append: appendCoAuthors, remove: removeCoAuthors } = useFieldArray({ control, name: "coAuthors" });
    const { fields: participants, append: appendParticipants, remove: removeParticipants } = useFieldArray({ control, name: "participants" });
    const { append: appendAdditionalImages, remove: removeAdditionalImages } = useFieldArray({ control, name: "additionalImages" });
    const additionalImages = useWatch({ control, name: "additionalImages" });

    const imageRefAdditionalImages = useRef<any>(null);

    const selectAdditionalImages = useCallback(() => {
        if (!imageRefAdditionalImages.current) return;
        imageRefAdditionalImages.current();
    }, []);

    const selectFile = useCallback(() => {
        if (!fileRef.current) return;
        fileRef.current();
    }, []);

    const selectImage = useCallback(() => {
        if (!imageRef.current) return;
        imageRef.current();
    }, []);

    // Form Handlers
    const handleText = useCallback(
        (text: string) => {
            if (file) setValue("file", undefined);
            if (image) setValue("image", undefined);
            if (type !== "text") setValue("type", "text");
            setValue("text", text);
        },
        [file, image, setValue, type]
    );

    const handleFile = useCallback(
        (file: File) => {
            if (!!text.length) setValue("text", "");
            if (image) setValue("image", undefined);
            if (type !== "text") setValue("type", "text");
            setValue("file", file);
        },
        [image, setValue, text.length, type]
    );

    const handleImage = useCallback(
        (image: File) => {
            if (!!text.length) setValue("text", "");
            if (file) setValue("file", undefined);
            if (type !== "image") setValue("type", "image");
            setValue("image", image);
        },
        [file, setValue, text.length, type]
    );

    const clearFile = useCallback(() => {
        if (type !== "text") setValue("type", "text");
        setValue("file", undefined);
    }, [setValue, type]);

    const clearImage = useCallback(() => {
        if (type !== "text") setValue("type", "text");
        setValue("image", undefined);
    }, [setValue, type]);

    return (
        <Fragment>
            <Stack spacing={0}>
                {isLoading ? (
                    <DraftSkeletonForm />
                ) : (
                    <Stack spacing={0}>
                        <Stack style={{ maxHeight: "460px", minHeight: "140px", overflow: "scroll" }} pt={16} px={16} spacing={0} sx={{ flex: 1 }}>
                            <Stack>
                                <Controller
                                    name="title"
                                    control={control}
                                    render={({ field }) => (
                                        <Input
                                            {...field}
                                            id="title"
                                            placeholder={t("drafts.title_placeholder")}
                                            variant={"unstyled"}
                                            autosize
                                            px={16}
                                            py={8}
                                            value={String(field.value)}
                                            onChange={(e) => field.onChange(String(e.target.value))}
                                        />
                                    )}
                                />
                            </Stack>
                            <Stack sx={{ flex: 1, position: "relative" }}>
                                {!(file || image) && (
                                    <Controller
                                        name="text"
                                        control={control}
                                        render={({ field: { value } }) => (
                                            // <TextArea
                                            //     py={8}
                                            //     px={16}
                                            //     id="text"
                                            //     {...field}
                                            //     value={String(field.value)}
                                            //     onChange={(e) => handleText(String(e.target.value))}
                                            //     placeholder={t("drafts.text_palaceholder")}
                                            //     autosize
                                            //     variant={"unstyled"}
                                            //     styles={{
                                            //         root: { flex: 1 },
                                            //         wrapper: { height: "100%" },
                                            //         input: { minHeight: "100%!important", overflow: "hidden" },
                                            //     }}
                                            // />

                                            <DraftJs
                                                placeholder={t("drafts.text_palaceholder")}
                                                newEditorState={value}
                                                onChangeState={(e) => handleText(String(e))}
                                            />
                                        )}
                                    />
                                )}

                                {!!image && (
                                    <Controller
                                        name="image"
                                        control={control}
                                        render={({ field }) =>
                                            field.value ? (
                                                <Stack px={16} py={8} spacing={6}>
                                                    <Group>
                                                        <Image width={480} height={360} src={URL.createObjectURL(field.value)} withPlaceholder radius={"sm"} />
                                                    </Group>
                                                    <Group>
                                                        <SharkButton buttonType="secondary" variant="outlined" size="small" onClick={clearImage}>
                                                            {t("buttons.remove_btn")}
                                                        </SharkButton>
                                                    </Group>
                                                </Stack>
                                            ) : (
                                                <div />
                                            )
                                        }
                                    />
                                )}

                                <Controller
                                    name="file"
                                    control={control}
                                    render={({ field }) =>
                                        field.value ? (
                                            <Stack px={16} py={8} spacing={6}>
                                                <Group>
                                                    <Text color={"#666892"} size={"md"}>
                                                        Selected file:
                                                    </Text>
                                                    <Text color={"#5A3DD1"} size={"md"} weight={"500"}>
                                                        {field.value.name}
                                                    </Text>
                                                    <Text color={"#666892"} size={"md"}>
                                                        ({formatBytes(field.value.size)})
                                                    </Text>
                                                </Group>
                                                <Group>
                                                    <SharkButton buttonType="secondary" variant="outlined" size="small" onClick={clearFile}>
                                                        {t("buttons.remove_btn")}
                                                    </SharkButton>
                                                </Group>
                                            </Stack>
                                        ) : (
                                            <div />
                                        )
                                    }
                                />

                                {!(text.replace(/<\/?\w+[^>]*\/?>/g, "") || !!text.match(/(?=(<img>*))/gm) || file || image) && (
                                    <Group px={16} py={0} spacing={6} sx={{ position: "absolute", left: 15, top: 80 }}>
                                        <Text color={"#666892"} size={"md"}>
                                            Or
                                        </Text>
                                        <Text onClick={selectFile} color={"#5A3DD1"} size={"md"} underline={true} weight={"500"} style={{ cursor: "pointer" }}>
                                            {t("drafts.file-upload-text.text_2")}
                                        </Text>
                                        <Text color={"#666892"} size={"md"}>
                                            {t("drafts.file-upload-text.text_3")}
                                        </Text>
                                    </Group>
                                )}
                            </Stack>

                            <Dropzone
                                style={{ display: "none" }}
                                openRef={fileRef}
                                accept={["text/plain"]}
                                onDrop={(files) => handleFile(files[0])}
                                onReject={(files) => console.log("rejected file", files[0])}
                                maxSize={104857600} // 100mb limit
                            >
                                {() => null}
                            </Dropzone>

                            <Dropzone
                                style={{ display: "none" }}
                                openRef={imageRef}
                                accept={["image/jpeg", "image/png"]}
                                onDrop={(files) => handleImage(files[0])}
                                onReject={(files) => console.log("rejected image", files[0])}
                                maxSize={104857600}
                            >
                                {() => null}
                            </Dropzone>
                        </Stack>

                        <Collapse isClosed={isCollapseClosed} label={t("textfields.title")} opened={!!fullTitle.length}>
                            <ListInput control={control} name={`fullTitle`} placeholder={t("textfields.metadataTitle")} flex={1} />
                        </Collapse>

                        <Collapse isClosed={isCollapseClosed} label={t("textfields.authors")} opened={!!authors.length}>
                            {authors.map((item, index) => (
                                <ListInput
                                    key={item.id}
                                    control={control}
                                    maxLength={40}
                                    name={`authors.${index}`}
                                    placeholder={`${t("common.author")} #${index + 1}`}
                                    onDelete={() => removeAuthors(index)}
                                />
                            ))}
                            <Group>
                                <SharkButton onClick={() => appendAuthors("")} buttonType="secondary" size="small" variant="outlined">
                                    {t("buttons.add_btn")}
                                </SharkButton>
                            </Group>
                        </Collapse>

                        <Collapse isClosed={isCollapseClosed} label={t("textfields.coAuthors")} opened={!!coAuthors.length}>
                            {coAuthors.map((item, index) => (
                                <ListInput
                                    key={item.id}
                                    control={control}
                                    name={`coAuthors.${index}`}
                                    maxLength={40}
                                    placeholder={`${t("textfields.coAuthor")} #${index + 1}`}
                                    onDelete={() => removeCoAuthors(index)}
                                />
                            ))}
                            <Group>
                                <SharkButton onClick={() => appendCoAuthors("")} buttonType="secondary" size="small" variant="outlined">
                                    {t("buttons.add_btn")}
                                </SharkButton>
                            </Group>
                        </Collapse>

                        <Collapse isClosed={isCollapseClosed} label={t("textfields.participants")} opened={!!participants.length}>
                            {participants.map((item, index) => (
                                <ListInput
                                    key={item.id}
                                    control={control}
                                    name={`participants.${index}`}
                                    maxLength={40}
                                    placeholder={`${t("textfields.participant")} #${index + 1}`}
                                    onDelete={() => removeParticipants(index)}
                                />
                            ))}
                            <Group>
                                <SharkButton onClick={() => appendParticipants("")} buttonType="secondary" size="small" variant="outlined">
                                    {t("buttons.add_btn")}
                                </SharkButton>
                            </Group>
                        </Collapse>
                        <Collapse label={"additionalImages"}>
                            {!!additionalImages.filter((item: any) => typeof item !== "string").length && (
                                <Alert title={"New image"} text={"To get a link to the picture, first save a draft"} variant="warn" />
                            )}

                            <div style={{ display: "grid", gap: "20px", gridTemplateColumns: "repeat(3, 1fr)", width: "50%" }}>
                                {additionalImages?.length > 0 &&
                                    additionalImages.map((field: any, index: number) => (
                                        <div>
                                            <div
                                                key={index}
                                                style={{
                                                    display: "flex",
                                                    alignItems: "flex-start",
                                                    marginBottom: "8px",
                                                }}
                                            >
                                                <img
                                                    alt=""
                                                    src={typeof field !== "string" ? URL.createObjectURL(field) : field}
                                                    style={{ maxWidth: "200px", height: "150px", maxHeight: "150px" }}
                                                />
                                                <CloseButton onClick={() => removeAdditionalImages(index)} />
                                            </div>
                                            {typeof field === "string" && (
                                                <SharkButton
                                                    type="button"
                                                    buttonType="secondary"
                                                    size="small"
                                                    variant="outlined"
                                                    mt="32px"
                                                    onClick={() => navigator.clipboard.writeText(field)}
                                                >
                                                    Copy url image
                                                </SharkButton>
                                            )}
                                        </div>
                                    ))}
                            </div>

                            <Dropzone
                                style={{ display: "none" }}
                                openRef={imageRefAdditionalImages}
                                accept={["image/jpeg", "image/png"]}
                                onDrop={(files) => appendAdditionalImages(files[0])}
                                onReject={(files) => console.log("rejected image", files)}
                                maxSize={104857600} // 100mb limit
                            >
                                {() => null}
                            </Dropzone>
                            <Group>
                                <SharkButton
                                    type="button"
                                    onClick={() => selectAdditionalImages()}
                                    buttonType="secondary"
                                    size="small"
                                    variant="outlined"
                                    style={{ marginTop: "20px" }}
                                >
                                    {t("buttons.add_btn")}
                                </SharkButton>
                            </Group>
                        </Collapse>
                    </Stack>
                )}

                <Group px={16} style={{ borderTop: "1px solid #DDDEEE", height: 56 }} spacing={0}>
                    <Group spacing={16}>
                        <Icon icon={"ic:round-insert-photo"} size={20} color={"#666892"} onClick={selectImage} />

                        <ComingSoonWrapper orientation="vertical">
                            <Icon icon={"ic:baseline-insert-link"} size={20} color={"#DDDEEE"} rotate={-30} />
                        </ComingSoonWrapper>

                        <Divider />

                        <ComingSoonWrapper orientation="vertical">
                            <Icon icon={"ic:round-visibility-off"} size={20} color={"#DDDEEE"} />
                        </ComingSoonWrapper>
                    </Group>
                </Group>
            </Stack>
        </Fragment>
    );
};

const Divider = styled.div`
    height: 24px;
    background-color: #dddeee;
    width: 1px;
`;

const Input = styled(Textarea)`
    #title {
        font-weight: 700;
        font-size: 24px;
        line-height: 30px;
        color: #0a001e;
    }

    #title::placeholder {
        font-weight: 700;
        font-size: 24px;
        line-height: 30px;
        color: #666892;
    }
`;

// const TextArea = styled(Textarea)`
//     #text {
//         font-weight: 400;
//         font-size: 16px;
//         line-height: 20px;
//         color: #0a001e;
//         padding: 0;
//     }

//     #text::placeholder {
//         font-weight: 400;
//         font-size: 16px;
//         line-height: 20px;
//         color: #666892;
//     }
// `;
