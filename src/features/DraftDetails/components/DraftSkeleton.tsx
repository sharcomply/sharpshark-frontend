import styled from "@emotion/styled";
import { Group, Stack } from "@mantine/core";
import { PageTitle } from "features/DraftDetails/components/PageTitle";
import { DraftSkeletonForm } from "./DraftSkeletonForm";
import { CustomSkeleton } from "./Skeleton";

// Component
export default function DraftSkeleton() {
    // Render
    const actionItems = () => (
        <Group>
            <Group>
                <CustomSkeleton height={42} width={130} radius={4} fill />
                <CustomSkeleton height={42} width={80} radius={4} fill />
            </Group>
        </Group>
    );

    const TitlesGroup = () => (
        <Group style={{ gap: 8 }}>
            <CustomSkeleton height={16} width={16} radius={16} fill />
            <CustomSkeleton height={30} width={200} radius={4} fill />
            <CustomSkeleton height={24} width={62} radius={100} fill />
        </Group>
    );

    return (
        <form>
            <Stack>
                <PageTitle isLoading={true} title={TitlesGroup()} actions={actionItems()} />

                <Group style={{ gap: 12 }}>
                    <CustomSkeleton height={26} width={80} radius={4} fill />
                    <CustomSkeleton height={26} width={168} radius={4} fill />
                    <CustomSkeleton height={26} width={144} radius={4} fill />
                    <CustomSkeleton height={26} width={180} radius={4} fill />
                </Group>

                <FormRoot sx={{ minHeight: "380px", overflowY: "scroll" }}>
                    <DraftSkeletonForm />
                </FormRoot>
            </Stack>
        </form>
    );
}

const FormRoot = styled(Stack)`
    position: relative;
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    overflow: hidden;
    justify-content: space-between;

    img {
        max-width: 480px;
        height: auto !important;
    }
`;
