import styled from "@emotion/styled";
import { Text } from "@mantine/core";
import { useTranslation } from "react-i18next";
// import useDraftStore from "../hooks/useDraftStore";

const Message = () => {
    // const { draftMessage } = useDraftStore();
    const { t } = useTranslation();
    return (
        <Root>
            <Text
                size="md"
                weight={"500"}
                color={"red"}
                align={"center"}
                dangerouslySetInnerHTML={{ __html: t("Do not change the network of the protected document in the MetaMask.") }}
            ></Text>
        </Root>
    );
};

export default Message;

const Root = styled.div`
    padding: 20px;
`;
