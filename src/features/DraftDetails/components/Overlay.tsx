import { Box, Center, Overlay as MantineOverlay, Stack, Text } from "@mantine/core";
import { Fragment, ReactNode, useCallback, useMemo } from "react";
import protectedLoader from "assets/images/protected-loader.svg";
import doneLoader from "assets/images/protected-loader-done.svg";
import useDraftStore from "../hooks/useDraftStore";
import { OverlaySteps } from "../store";
import { SharkButton } from "components/SharkButton";
import { useNavigate } from "react-router-dom";
import { useQueryClient } from "react-query";
import ActionDone from "features/Icons/ActionDone";
import styled from "@emotion/styled";
import { Icon } from "@iconify/react";

// Props
type ComponentProps = {
    variant?: "success" | "default" | "licenseError" | string;
    text?: string | ReactNode;
};

// Component
export function Overlay({ variant, text }: ComponentProps) {
    const { overlay, showOverlay, draftId } = useDraftStore();
    const queryClient = useQueryClient();
    const navigate = useNavigate();

    const borderColor = useMemo(() => {
        if (variant === "success") return "#29CCB1";
        if (variant === "licenseError") return "#000000";
        return "#4700E5";
    }, [variant]);

    const backgroundColor = useMemo(() => {
        if (variant === "success") return "#EAFAF6";
        if (variant === "licenseError") return "#e7f797";
        return "#EDEEFF";
    }, [variant]);

    const loader = useMemo(() => {
        if (variant === "success")
            return (
                <Box sx={{ position: "relative", width: "100px", height: "100px" }}>
                    <img height={100} width={100} src={doneLoader} alt="" />
                    <LoaderDoneContainer>
                        <ActionDone />
                    </LoaderDoneContainer>
                </Box>
            );

        if (variant === "licenseError") {
            return <Icon icon={"mdi:warning-outline"} width={75} height={75}  />;
        }
        return <img height={100} width={100} src={protectedLoader} alt="" />;
    }, [variant]);

    const LoaderIcon = useMemo(() => {
        if (variant === "licenseError") {
            return loader;
        }
        if (overlay.step === OverlaySteps.MINTED) {
            return (
                <Success>
                    <ActionDone height={64} />
                </Success>
            );
        }
        return loader;
    }, [overlay.step, variant, loader]);

    const onSubmit = useCallback(async () => {
        showOverlay(false);

        await Promise.all([
            queryClient.invalidateQueries([`SIDEBAR_COUNTS`]),
            // queryClient.invalidateQueries(["PROTECTED_LIST"]),
            // queryClient.invalidateQueries(["DRAFT_LIST"]),
            // queryClient.invalidateQueries(["DRAFT"]),
        ]);

        navigate(`/protected/${draftId}`, { replace: true });
    }, [draftId, navigate, queryClient, showOverlay]);

    return (
        <Fragment>
            <MantineOverlay
                opacity={0.9}
                zIndex={5}
                sx={{
                    borderWidth: 1,
                    borderStyle: "solid",
                    borderColor,
                    backgroundColor,
                    borderRadius: 8,
                }}
            />
            <Center sx={{ position: "absolute", inset: 0, zIndex: 10 }}>
                <Stack spacing={16} align="center" sx={{ maxWidth: "75%" }}>
                    {LoaderIcon}
                    <Text size="md" weight={"500"} color={"#0A001E"} align={"center"} sx={{whiteSpace: 'pre-line'}}>
                        {text}
                    </Text>
                    {overlay.step === OverlaySteps.MINTED && (
                        <SharkButton onClick={onSubmit} buttonType={"secondary"} variant={"outlined"}>
                            Ok
                        </SharkButton>
                    )}
                </Stack>
            </Center>
        </Fragment>
    );
}

// Styling
const LoaderDoneContainer = styled.div`
    width: 30px;
    height: 15px;
    position: absolute;
    top: 50%;
    transform: translate(35px, -50%);
`;

const Success = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 80px;
    height: 80px;
    border-radius: 50%;
    border: 4px solid #29ccb1;
`;

