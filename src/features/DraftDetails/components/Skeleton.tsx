import styled from "@emotion/styled";

type ComponentProps = {
    height?: number;
    width?: number;
    radius?: number;
    marginTop?: number;
    marginBottom?: number;
    marginRight?: number;
    marginLeft?: number;
    fill?: boolean;
};
export const CustomSkeleton = (props: ComponentProps) => {
    const { height, width, radius, marginTop, marginBottom, marginRight, marginLeft, fill } = props;
    return (
        <Skeleton
            style={{
                height: `${height}px`,
                width: `${width}px`,
                borderRadius: `${radius}px`,
                marginTop: `${marginTop}px`,
                marginBottom: `${marginBottom}px`,
                marginRight: `${marginRight}px`,
                marginLeft: `${marginLeft}px`,
                background: fill
                    ? "rgba(75, 80, 109, 0.2)"
                    : "linear-gradient(45deg, rgba(255, 255, 255, 0.0001) 0%, rgba(75, 80, 109, 0.2) 0%, rgba(75, 80, 109, 0) 100%)",
            }}
        />
    );
};
const Skeleton = styled.div`
    -webkit-animation: blink 1.5s linear infinite;
    animation: blink 1.5s linear infinite;
    @-webkit-keyframes blink {
        50% {
            opacity: 0.2;
        }
    }
    @keyframes blink {
        50% {
            opacity: 0.2;
        }
    }
`;
