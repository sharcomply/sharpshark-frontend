import { Stack } from "@mantine/core";
import { Fragment } from "react";
import { CustomSkeleton } from "./Skeleton";

export const DraftSkeletonForm = () => {
    return (
        <Fragment>
            <Stack style={{ maxHeight: "460px", minHeight: "380px", overflow: "scroll", justifyContent: "space-between" }} pt={16} spacing={0} sx={{ flex: 1 }}>
                <Stack px={16} style={{ gap: 0 }}>
                    <CustomSkeleton height={40} width={300} radius={4} />

                    <CustomSkeleton height={14} width={1000} radius={4} marginTop={22} marginBottom={2} />
                    <CustomSkeleton height={14} width={600} radius={4} marginTop={4} marginBottom={4} />
                    <CustomSkeleton height={14} width={600} radius={4} marginTop={4} marginBottom={24} />
                </Stack>
            </Stack>
        </Fragment>
    );
};
