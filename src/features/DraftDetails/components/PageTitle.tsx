import { Group, Text, Tooltip } from "@mantine/core";
import Icon from "components/Icon";
import { ReactNode } from "react";
import useDraftStore from "../hooks/useDraftStore";

// Props
type ComponentProps = {
    title?: string | ReactNode;
    icon?: string;
    iconColor?: string;
    tooltipText?: ReactNode;
    actions?: ReactNode;
    isLoading?: boolean;
};

export const PageTitle = (props: ComponentProps) => {
    const { title, icon = "ep:question-filled", iconColor = "#9FA2B4", actions, tooltipText, isLoading } = props;
    const { status } = useDraftStore();

    const badgeColor = status.type === "error" ? "#E50C54" : status.type === "success" ? "#29CCB1" : status.type === "warn" ? "#FFC933" : "#979CBC";
    const badgeTextColor = status.type === "error" ? "#FFFFFF" : !status.type ? "#FFFFFF" : "#0A001E";

    return (
        <Group position={"apart"} sx={{ height: 42, maxWidth: "calc(100vw - 356px)" }} spacing={0} noWrap={true}>
            {/* Title */}
            <Group spacing={8} position={"left"} sx={{ maxWidth: "40%" }} noWrap={true}>
                {tooltipText && (
                    <Tooltip
                        label={tooltipText}
                        position="bottom"
                        placement="center"
                        transition="fade"
                        transitionDuration={300}
                        transitionTimingFunction="ease"
                        withArrow={false}
                        styles={{ body: { backgroundColor: "#666892" } }}
                    >
                        <Icon icon={icon} color={iconColor} hoverColor={iconColor} />
                    </Tooltip>
                )}
                <Text
                    color={"#0a001e"}
                    weight="700"
                    sx={{
                        fontSize: 24,
                        lineHeight: "30px",
                        overflow: "hidden",
                        whiteSpace: "nowrap",
                        textOverflow: "ellipsis",
                    }}
                >
                    {title}
                </Text>
                {!isLoading && (
                    <Group px={12} sx={{ borderRadius: 100, backgroundColor: badgeColor, height: 24 }}>
                        <Text color={badgeTextColor} size={"xs"} transform={"uppercase"} sx={{ whiteSpace: "nowrap" }}>
                            {status.message || "Saved"}
                        </Text>
                    </Group>
                )}
            </Group>

            {/* Actions */}
            <Group spacing={20} position={"right"}>
                {actions}
            </Group>
        </Group>
    );
};
