import {SharkButton} from "components/SharkButton";
import {Fragment} from "react";
import {TFunction} from "react-i18next";
import {Link} from "react-router-dom";
import {DraftState} from "../store";
import {Stack} from "@mantine/core";

export enum DraftStatusMessage {
    SAVED = "Saved",
    UPLOADING = "Uploading...",
    UPLOADED = "Uploaded",
    UPLOAD_FAILED = "Failed to upload",
    PROTECTING = "On it",
    PROTECTED = "All set!",
    PROTECT_FAILED = "Oops!",
}

export const OverlayMessage = {
    PENDING_PAYMENT_FIAT: (url: string, t: TFunction<"translation", undefined>) => (
        <Fragment>
            <span style={{display: "flex", alignItems: "center"}}>
                {t("overlay.messages.check_stripe")} &nbsp;
                <SharkButton width={"100px"} as="a" target={"__blank"} href={url} buttonType={"secondary"} variant={"filled"}>
                    {t("buttons.continue_btn")}
                </SharkButton>
            </span>
            <br/>
            {t("overlay.messages.waiting_payment")}
        </Fragment>
    ),
    PENDING_PAYMENT: (t: TFunction<"translation", undefined>) => (
        <Fragment>
            {t("overlay.messages.check_metamask")}
            <br/>
            {t("overlay.messages.waiting_payment")}
        </Fragment>
    ),
    PENDING_PAYMENT_NEAR: (t: TFunction<"translation", undefined>, handleClick?: () => void) => (
        <Fragment>
            Now you will be redirected to your near wallet. Please Confirm payment request.
            <br/>
            {t("overlay.messages.waiting_payment")}
            <Stack mt={16} align={"center"}>
                <SharkButton
                    align={"center"}
                    onClick={handleClick}
                    buttonType={"secondary"}
                    variant={"filled"}
                >Cancel</SharkButton>
            </Stack>
        </Fragment>
    ),
    // PAYMENT_SEND_BUT_NOT_RECEIVED: "We have discovered that you have sent payment through metamask. Waiting for the confirmation from blockchain to our server that the payment was computed",
    PAYMENT_RECEIVED: (t: TFunction<"translation", undefined>) => (
        <Fragment>
            {t("overlay.messages.payment_received")}
            <br/>
            {t("overlay.messages.wait_result")}
        </Fragment>
    ),
    WORK_READY: (t: TFunction<"translation", undefined>, network: string, handleClick?: () => void) => (
        network !== 'near' ?
            <Fragment>
                {t("overlay.messages.work_ready_for_upload", {network})}
                <br/>
                {t("overlay.messages.we_prepared_commit", {network})}
            </Fragment>
            :
            <Fragment>
                The work is ready for being uploaded into decentralized network near network.
                <br/>
                Check your near wallet, we have prepared a transaction commit, the fee for blockchain calculations to anchor the data will be taken, and than our
                contract will save your data into decentralized journal of near network
                <Stack mt={16} align={"center"}>
                    <SharkButton
                        align={"center"}
                        onClick={handleClick}
                        buttonType={"secondary"}
                        variant={"filled"}
                    >Cancel</SharkButton>
                </Stack>

            </Fragment>
    ),
    MINTED: (tokenId: string, to: string, t: TFunction<"translation", undefined>) => (
        <Fragment>
            {t("overlay.messages.data_anchored")}
            <br/>
            {t("overlay.messages.work_token_id", {tokenId: tokenId, to: to})}
            <br/>
        </Fragment>
    ),
    SET_LICENCE: (handleClick: () => void) => (
            <Fragment>
                Waiting for the blockchain to send a license installation confirmation event

                <Stack mt={16} align={"center"}>
                    <SharkButton
                        align={"center"}
                        onClick={handleClick}
                        buttonType={"secondary"}
                        variant={"filled"}
                    >Cancel</SharkButton>
                </Stack>
            </Fragment>
    ),
    MINTED_WITH_LICENCE: (tokenId: string, to: string, t: TFunction<"translation", undefined>, licence: DraftState["licence"]) => {

        return (<Fragment>
            {t("overlay.messages.data_anchored")}
            <br/>
            {t("overlay.messages.work_token_id", {tokenId: tokenId, to: to})}
            <br/>
            {!!licence.type && `Licence Type: ${licence.type}`} {!!licence.price && `, Licence Price ${licence.price}$`}
        </Fragment>)
    },
    MINTED_WITHOUT_LICENSE: (tokenId: string, to: string, t: TFunction<"translation", undefined>) => {
        return (
            <Fragment>
                {t("overlay.messages.license_cancel")}
                <br />
                {t("overlay.messages.work_token_id", { tokenId: tokenId, to: to })}
                <br />
            </Fragment>
        );
    },
};

export const AlertTitle = {
    SCAN_FAILED: "The content is already on the web",
};

// export const AlertMessage = (link?: string = "#") => {
//     const { t } = useTranslation();
//     return (
//         <div
//             dangerouslySetInnerHTML={{ __html: t("errors.messages.scan_failed", { link: <Link to={link}>{t("overlay.messages.see_where_link")}</Link> }) }}
//         ></div>
//     );
// };

// export const AlertMessage = {
//     SCAN_FAILED: (t: TFunction<"translation", undefined>) => (
//         <div
//             dangerouslySetInnerHTML={{ __html: t("errors.messages.scan_failed", { link: <Link to={"#"}>{t("overlay.messages.see_where_link")}</Link> }) }}
//         ></div>
//     ),
// };

export const AlertMessage = {
    SCAN_FAILED: (t: TFunction<"translation", undefined>) => (
        <div>
            {t("overlay.messages.already_published")} <Link to={"#scan-result"}>{t("overlay.messages.see_where_link")}</Link>.
            <br/>
            {t("overlay.messages.third_party_quotes")}
        </div>
    ),
};
