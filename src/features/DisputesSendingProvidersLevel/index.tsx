import styled from "@emotion/styled";
import { Group, Input, Stack, Text } from "@mantine/core";
import { SharkButton } from "components/SharkButton";
import AddButton from "features/DisputesCollectingEvidence/components/AddButton";
import RemoveButton from "features/DisputesCollectingEvidence/components/RemoveButton";
import { Control, FieldValues, useFieldArray, UseFormRegister } from "react-hook-form";

interface Props {
    control: Control;
    register: UseFormRegister<FieldValues>;
}

const DisputesSendingProvidersLevel = ({ control, register }: Props) => {
    const {
        fields: fieldsOwnerClaimEmails,
        append: appendOwnerClaimEmails,
        remove: removeOwnerClaimEmails,
    } = useFieldArray({
        control,
        name: "ownerClaimEmails",
    });

    return (
        <Root>
            <Wrapper>
                <Title>Step 3. Sending the claim to the provider</Title>
                <Title>By email</Title>
                <div style={{ display: "flex", flexDirection: "column" }}>
                    <SubTitle>contact email</SubTitle>
                    <div style={{ width: "378px", marginBottom: "8px" }}>
                        <Description>
                            Their provider is [Provider’s name]. This is the email we found. You may add more contacts if you know any others.
                        </Description>
                    </div>
                    {fieldsOwnerClaimEmails.map((field, index) => (
                        <div key={field.id} style={{ display: "flex", alignItems: "center", marginBottom: "8px" }}>
                            <Input
                                {...register(`ownerClaimEmails.${index}`)}
                                variant="unstyled"
                                style={{ marginRight: "10px", width: "350px", border: "1px solid #29CCB1", borderRadius: "4px", paddingLeft: "12px" }}
                            />
                            <RemoveButton onClick={() => removeOwnerClaimEmails(index)} />
                        </div>
                    ))}
                    <AddButton onClick={() => appendOwnerClaimEmails("")}>Add more</AddButton>
                </div>
            </Wrapper>

            <Footer>
                <Group>
                    <Text size={"sm"} weight={"400"} color={"#666892"}>
                        Document “Document long title, this is it’s long, long, very long title”
                    </Text>
                </Group>
                <Group spacing={12}>
                    <Group spacing={8}></Group>
                    <SharkButton
                        // onClick={handleUpdate}
                        variant="filled"
                        buttonType="secondary"
                        size="small"
                        px={8}
                        py={4}
                    >
                        Send the claim!
                    </SharkButton>
                </Group>
            </Footer>
        </Root>
    );
};

export default DisputesSendingProvidersLevel;

const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
`;

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    margin: 32px 32px 72px 48px;
`;

const Footer = styled(Group)`
    position: relative;
    background-color: #ffffff;
    border-top: 1px solid #dddeee;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
    padding: 14px 16px;
    height: 56px;
    overflow: hidden;
    justify-content: space-between;
`;

const Title = styled.span`
    font-weight: 700;
    font-size: 20px;
    line-height: 24px;
    margin-bottom: 16px;
`;

const SubTitle = styled.span`
    font-weight: 700;
    font-size: 12px;
    line-height: 16px;
    text-transform: uppercase;
    margin-bottom: 6px;
`;

const Description = styled.span`
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;
    color: #666892;
    margin-bottom: 8px;
`;
