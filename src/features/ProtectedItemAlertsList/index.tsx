import { Stack, Text } from "@mantine/core";
import styled from "@emotion/styled";
import { TableHead } from "../../components/DataTable/TableHead";
import { Fragment, useEffect } from "react";
import { getListByDate } from "helpers/getListByDate";
import { TableDate } from "../../components/DataTable/TableDate";
import useAlertList from "./hooks/useAlertList";
import { AlertItem } from "./components/AlertItem";
import { useParams } from "react-router-dom";
import useChangeUnreadAlerts from "features/AlertsList/hooks/useChangeUnreadAlerts";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { AppState } from "../../store";

// Component
export default function ProtectedItemAlertsList() {
    const { protectId } = useParams();
    const newAlertsCount = useSelector((state: AppState) => state.sidebar.alerts.unread);
    const { alerts, refetch } = useAlertList({ isSkipped: false, documentId: protectId ? protectId : null }, protectId);

    useEffect(() => {
        if (newAlertsCount) {
            if (newAlertsCount > 0) {
                refetch();
            }
        }
    }, [newAlertsCount, refetch]);

    const itemsByDate = getListByDate(alerts?.items || []);
    const { t } = useTranslation();
    useChangeUnreadAlerts({
        alerts,
        refetch,
    });

    return (
        <Root>
            <Table>
                <TableHead
                    items={[
                        { title: t("table.titles.your_title"), width: "48%" },
                        { title: t("table.titles.appeared_on"), width: "20%" },
                        { title: t("table.titles.discovered"), width: "12%" },
                        { title: "", width: "12%" },
                        { title: "", width: "6%" },
                        { title: "", width: "2%" },
                    ]}
                />

                <tbody>
                    {!!alerts?.items.length && (
                        <>
                            <tr style={{ backgroundColor: "#000" }}>
                                <td colSpan={6} align="center" width={"100%"} style={{ padding: "5px", fontSize: "12px", fontWeight: 500, color: "#fff" }}>
                                    {t("table.are_these_cases_violations")}
                                </td>
                            </tr>
                            <Fragment>
                                {renderListByDate(t("table.row-by-date.today"), itemsByDate.today)}
                                {renderListByDate(t("table.row-by-date.this-week"), itemsByDate.week)}
                                {renderListByDate(t("table.row-by-date.last-month"), itemsByDate.lastMonth)}
                                {renderListByDate(t("table.row-by-date.earlier-this-month"), itemsByDate.month)}
                                {renderListByDate(t("table.row-by-date.earlier"), itemsByDate.earlier)}
                            </Fragment>
                        </>
                    )}

                    {!alerts?.items.length && (
                        <tr style={{ borderTop: "1px solid #DDDEEE" }}>
                            <td style={{ padding: 16 }}>
                                <Text size={"sm"} color={"#666892"} weight={"400"}>
                                    {t("table.empty-table")}
                                </Text>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    )}
                </tbody>
            </Table>
        </Root>
    );
}

// List By Date
function renderListByDate(title: string, items: any) {
    if (!items.length) return null;

    return (
        <Fragment>
            <TableDate title={title} cols={6} titlePos={3} />
            {items.map((item: Alert) => {
                return <AlertItem key={item._id} item={item} />;
            })}
        </Fragment>
    );
}

// Styling
const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
`;

export const Table = styled.table`
    width: 100%;
    table-layout: fixed;
    border-collapse: collapse;
    overflow-wrap: anywhere;
`;
