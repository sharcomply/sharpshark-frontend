import { useQuery } from "react-query";
import { backend } from "api";

export default function useAlertList(params?: GetAlertParams, id?: string, config?: any) {
  const { data: alerts, ...options } = useQuery(["PROTECTED_ALERTS_LIST", id], () => backend.alerts.getAlertsList(params), {
    enabled: !!id,
    refetchOnWindowFocus: false,
      ...config
  });

  return {
    alerts,
    ...options,
  };
}

export function useUpdateAlertList(params?: GetAlertParams, id?: string) {
  const { data: alerts, ...options } = useQuery(["PROTECTED_ALERTS_LIST", id], () => backend.alerts.getAlertsList(params), {
    enabled: false
  });

  return {
    alerts,
    ...options,
  };
}