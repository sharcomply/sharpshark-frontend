import { Group, Text } from "@mantine/core";
import { humanizeDate } from "helpers/humanizeDate";
import styled from "@emotion/styled";
import { SharkButton } from "components/SharkButton";
import { useCallback } from "react";
import { closeAlert } from "api/requests/alerts";
import { ActionMenu } from "features/AlertsList/components/ActionMenu";
import ViewedStatus from "components/ViewedStatus";
import { useUpdateAlertList } from "../hooks/useAlertList";
import { useTranslation } from "react-i18next";
import { useAlertListStore } from "../../AlertsList/hooks/useAlertListStore";
import { Link } from "react-router-dom";

// Props
type ComponentProps = {
    item: Alert;
};

// Component
export const AlertItem = ({ item }: ComponentProps) => {
    const { t } = useTranslation();
    const { refetch } = useUpdateAlertList({ documentId: item.documentId._id }, item.documentId._id);
    const {
        actions: { setDisputPayload },
    } = useAlertListStore();
    const onDelete = useCallback(
        async (id: string) => {
            if (!id) return;
            await closeAlert(id);
            refetch();
        },
        [refetch]
    );

    const onSaveAlertParam = useCallback(
        (appearedOn: string) => {
            setDisputPayload({ appearedOn, alertId: item._id });
        },
        [setDisputPayload, item]
    );

    if (!item.scanId) return null;

    return (
        <>
            {!item.scanId.similarContent.length && (
                <Root>
                    <td>
                        <Group direction={"row"} spacing={0} style={{ overflow: "hidden" }} noWrap={true}>
                            <Group>
                                <ViewedStatus isRead={item.isRead} />
                            </Group>
                            <Group px={16} align="center" style={{ overflow: "hidden" }} sx={{ height: 54 }}>
                                <Text
                                    size={"sm"}
                                    color={"#0A001E"}
                                    weight={"500"}
                                    style={{
                                        overflow: "hidden",
                                        textOverflow: "ellipsis",
                                        whiteSpace: "nowrap",
                                        // width: "99%",
                                    }}
                                >
                                    {item.documentId.title}
                                </Text>
                            </Group>
                        </Group>
                    </td>

                    <td>
                        <Group px={16} align="center" sx={{ height: 54 }}>
                            <Text
                                size={"sm"}
                                color={"#666892"}
                                weight={"500"}
                                style={{
                                    overflow: "hidden",
                                    textOverflow: "ellipsis",
                                    whiteSpace: "nowrap",
                                }}
                            >
                                Similar content not found
                            </Text>
                        </Group>
                    </td>

                    <td>
                        <Group px={16} align="center" sx={{ height: 54 }}>
                            <Text size={"sm"} color={"#666892"} weight={"500"}>
                                {humanizeDate(item.date).shortDate}
                            </Text>
                        </Group>
                    </td>
                    <td></td>
                    <td>
                        <Group px={16} position="center" align="center" sx={{ height: 54 }} onClick={() => onDelete(item._id)}>
                            <Text size={"sm"} color={"#4700E5"} weight={"500"} style={{ cursor: "pointer" }}>
                                {t("buttons.skip_btn")}
                            </Text>
                        </Group>
                    </td>

                    <td>
                        <Group px={16} position="right" align="center" sx={{ height: 54 }}>
                            <ActionMenu alertId={item._id} refetch={refetch} url={item.scanId.similarContent[0]?.url} />
                        </Group>
                    </td>
                </Root>
            )}
            {!!item.scanId.similarContent.length && (
                <>
                    <Root>
                        <td>
                            <Group direction={"row"} spacing={0} style={{ overflow: "hidden" }} noWrap={true}>
                                <Group>
                                    <ViewedStatus isRead={item.isRead} />
                                </Group>
                                <Group px={16} align="center" style={{ overflow: "hidden", minWidth: "20%", maxWidth: "50%" }} sx={{ height: 54 }}>
                                    <Text
                                        size={"sm"}
                                        color={"#0A001E"}
                                        weight={"500"}
                                        style={{
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",
                                            whiteSpace: "nowrap",
                                            // width: "99%",
                                        }}
                                    >
                                        {item.documentId.title}
                                    </Text>
                                </Group>
                                <Group px={16} align="center" style={{ overflow: "hidden" }} sx={{ height: 54 }}>
                                    <Text
                                        size={"sm"}
                                        color={"#666892"}
                                        weight={"500"}
                                        style={{
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",
                                            whiteSpace: "nowrap",
                                            // width: "45%",
                                        }}
                                    >
                                        {item.scanId.similarContent[0].title}
                                    </Text>
                                </Group>
                            </Group>
                        </td>

                        <td>
                            <a href={item.scanId.similarContent[0] ? item.scanId.similarContent[0].url : "#"}>
                                <Group px={16} align="center" sx={{ height: 54 }}>
                                    <Text
                                        size={"sm"}
                                        color={"#666892"}
                                        weight={"500"}
                                        style={{
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        {item.scanId.similarContent[0].url}
                                    </Text>
                                </Group>
                            </a>
                        </td>

                        <td>
                            <Group px={16} align="center" sx={{ height: 54 }}>
                                <Text size={"sm"} color={"#666892"} weight={"500"}>
                                    {humanizeDate(item.date).shortDate}
                                </Text>
                            </Group>
                        </td>

                        <td>
                            <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                <Link to={`/disputes/create/${item._id}/collecting-evidence`}>
                                    <SharkButton
                                        variant={"filled"}
                                        buttonType="primary"
                                        onClick={() => onSaveAlertParam(item.scanId.similarContent[0].url)}
                                        width={"100%"}
                                        style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                    >
                                        React...
                                    </SharkButton>
                                </Link>
                            </Group>
                        </td>

                        <td>
                            <Group px={16} position="center" align="center" sx={{ height: 54 }} onClick={() => onDelete(item._id)}>
                                <Text size={"sm"} color={"#4700E5"} weight={"500"} style={{ cursor: "pointer" }}>
                                    Skip
                                </Text>
                            </Group>
                        </td>

                        <td>
                            <Group px={16} position="right" align="center" sx={{ height: 54 }}>
                                <ActionMenu alertId={item._id} refetch={refetch} url={item.scanId.similarContent[0].url} />
                            </Group>
                        </td>
                    </Root>
                    {item.scanId.similarContent.slice(1).map((similarContent) => (
                        <Root className="children" style={{ border: "none" }}>
                            <td></td>
                            <td>
                                <Group direction={"row"} spacing={0} noWrap={true}>
                                    <a href={similarContent.url} style={{ overflow: "hidden" }}>
                                        <Group px={16} align="center" sx={{ height: 54 }}>
                                            <Text
                                                size={"sm"}
                                                color={"#666892"}
                                                weight={"500"}
                                                style={{
                                                    overflow: "hidden",
                                                    textOverflow: "ellipsis",
                                                    whiteSpace: "nowrap",
                                                }}
                                            >
                                                {similarContent.url}
                                            </Text>
                                        </Group>
                                    </a>
                                </Group>
                            </td>
                            <td colSpan={4}></td>
                        </Root>
                    ))}
                </>
            )}
        </>
    );
};

// Styling
const Root = styled.tr`
    transition: background-color 200ms linear;
    border-top: 1px solid #dddeee;

    &:hover {
        background-color: #edeeff;
    }
`;
