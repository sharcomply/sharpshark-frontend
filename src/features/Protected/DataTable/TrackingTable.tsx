import { Grid, Menu } from "@mantine/core";
import {
    Table,
    TableContainer,
    TableHead,
    TableRow,
    TableData,
    Text,
    Title,
    DateRow,
    ActionsContainer,
    ActionsButton,
    MenuAction,
    MenuActionDanger,
} from "components/DataTable";
import { formatDate, RowType } from "components/DataTable/shared";
import { getListByDate, sortedListTypes } from "helpers/getListByDate";
import { ReactNode, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import actionIcon from "../../../assets/images/action-icon.svg";

// Props
interface DataTableProps {
    data: Array<any>;
}

// Component
export default function TrackingTable({ data }: DataTableProps) {
    const [sortedList, setSortedList] = useState<sortedListTypes | undefined>(undefined)
    const {t} = useTranslation()
    useEffect(() => {
        if(!data) return
        setSortedList(getListByDate(data, 'protectedDate'))
    }, [data, setSortedList]);


    const renderActions = (index: number): ReactNode => {
        return (
            <ActionsContainer>
                <Menu
                    closeOnItemClick
                    control={
                        <button type="button">
                            <ActionsButton src={actionIcon} alt="" />
                        </button>
                    }
                >
                    <Menu.Item component={MenuAction}>
                        <Link to={`/drafts/${data[index].id}`}>{t("buttons.view_document_btn")}</Link>
                    </Menu.Item>
                    <Menu.Item component={MenuAction}>
                        <Link to={`/drafts/${data[index].id}`}>{t("buttons.add_new_version_btn")}</Link>
                    </Menu.Item>
                    <Menu.Item component={MenuAction}>{t("buttons.view_certificate_btn")}</Menu.Item>
                    <Menu.Item component={MenuActionDanger}>{t('buttons.stop_monitoring_btn')}</Menu.Item>
                </Menu>
            </ActionsContainer>
        );
    };

    const renderRows = (type: RowType): ReactNode => {
        let rowList = [];
        let header = "";

        if(!sortedList) return
        switch (type) {
            case RowType.Today:
                rowList = sortedList.today;
                header = t('table.row-by-date.today');
                break;
            case RowType.ThisWeek:
                rowList = sortedList.week;
                header = t("table.row-by-date.this-week");
                break;
            case RowType.LastMonth:
                rowList = sortedList.lastMonth;
                header = t("table.row-by-date.last-month");
                break;
            case RowType.EarlierThisMonth:
                rowList = sortedList.month;
                header = t("table.row-by-date.earlier-this-month");
                break;
            case RowType.Earlier:
                rowList = sortedList.earlier;
                header = t("table.row-by-date.earlier");
                break;
        }

        if (rowList.length === 0) {
            return;
        }

        return (
            <>
                <DateRow>
                    <td></td>
                    <td>{header}</td>
                    <td></td>
                </DateRow>
                {rowList.map((row, index) => {
                    return (
                        <TableRow key={index}>
                            <TableData>
                                <Text>
                                    <Title>{row.title}</Title>
                                    {row.text}
                                </Text>
                            </TableData>
                            <TableData>{formatDate(row.protectedDate)}</TableData>
                            <TableData>
                                <Grid justify="space-between" align="center">
                                    {formatDate(row.updatedDate)}
                                    {renderActions(index)}
                                </Grid>
                            </TableData>
                        </TableRow>
                    );
                })}{" "}
            </>
        );
    };

    return (
        <TableContainer>
            <Table>
                <TableHead>
                    <tr>
                        <th>{t('table.header-title')}</th>
                        <th>{t("table.header-protected")}</th>
                        <th>{t("table.header-updated")}</th>
                    </tr>
                </TableHead>
                {data.length > 0 ? (
                    <tbody>
                        {renderRows(RowType.Today)}
                        {renderRows(RowType.ThisWeek)}
                        {renderRows(RowType.EarlierThisMonth)}
                        {renderRows(RowType.LastMonth)}
                        {renderRows(RowType.Earlier)}
                    </tbody>
                ) : (
                    <tbody>
                        <TableRow>
                            <TableData>
                                <Text>{t("table.empty-table")}</Text>
                            </TableData>
                            <td></td>
                            <td></td>
                        </TableRow>
                    </tbody>
                )}
            </Table>
        </TableContainer>
    );
}
