import styled from "@emotion/styled";
import { LoadingOverlay } from "@mantine/core";
import useApplicationLoader from "./hooks/useApplicationLoader";

// Component
export default function ApplicationLoader() {
    const loader = useApplicationLoader();

    return (
        <LoadingOverlay
            visible={loader.state.loading}
            loaderProps={{ size: "lg", color: "white", variant: "oval" }}
            overlayOpacity={0.7}
            overlayColor="#000000"
            loader={
                <LoaderRoot>
                    <LoaderIndicator />
                    <LoaderText>{loader.state.text}</LoaderText>
                </LoaderRoot>
            }
        />
    );
}

// Styling
const LoaderRoot = styled.div`
    display: flex;
    flex-direction: column;
`;

const LoaderText = styled.div`
    font-size: 1.2em;
    text-align: center;
    color: #fff;
`;

const LoaderIndicator = styled.div`
    margin: 7px auto;
    font-size: 5px;
    position: relative;
    text-indent: -9999em;
    border-top: 1.1em solid rgba(255, 255, 255, 0.2);
    border-right: 1.1em solid rgba(255, 255, 255, 0.2);
    border-bottom: 1.1em solid rgba(255, 255, 255, 0.2);
    border-left: 1.1em solid #ffffff;
    -webkit-transform: translateZ(0);
    -ms-transform: translateZ(0);
    transform: translateZ(0);
    -webkit-animation: rotate 1.1s infinite linear;
    animation: rotate 1.1s infinite linear;

    &,
    &::after {
        border-radius: 50%;
        width: 10em;
        height: 10em;
    }

    @keyframes rotate {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
`;
