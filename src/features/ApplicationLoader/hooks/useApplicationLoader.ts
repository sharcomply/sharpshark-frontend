import { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "store";
import { appLoaderActions } from "../store"

export default function useApplicationLoader() {
  const state = useSelector((state: AppState) => state.appLoader);
  const dispatch = useDispatch();

  const start = useCallback((text?: string) => {
    dispatch(appLoaderActions.start(text));
  }, [dispatch]);

  const stop = useCallback(() => {
    dispatch(appLoaderActions.stop());
  }, [dispatch]);

  return {
    state,
    start,
    stop,
  };
}
