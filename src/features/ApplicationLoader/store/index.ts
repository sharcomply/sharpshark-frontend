import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Types
export interface LoaderState {
  loading: boolean;
  text: string;
}

// Initial State
const initialState: LoaderState = {
  loading: false,
  text: "Processing",
};

// Slice
const appLoaderSlice = createSlice({
  name: "app-loader",
  initialState,
  reducers: {
    start: (state, action: PayloadAction<string | undefined>) => ({
      loading: true,
      text: action.payload || state.text,
    }),
    stop: () => ({
      loading: false,
      text: "Processing",
    }),
  },
});

export const appLoaderReducers = appLoaderSlice.reducer
export const appLoaderActions = appLoaderSlice.actions
