import { Group, Stack, Text } from "@mantine/core";
import { SharkButton } from "components/SharkButton";
import { useDraftListener } from "features/DraftDetails/hooks/useDraftListener";
import { UseFormReturn } from "react-hook-form";
import { PageTitle } from "./components/PageTitle";
import { VersionForm } from "./components/VersionForm";
import { Container } from "./containers";
import { NewDraftVersionState, useNewDraftVersionForm } from "./hooks/useNewDraftVersion";
import { useDraftVersionStore } from "./hooks/useDraftVersionStore";
import { useRedirectEffect } from "hooks/useRedirectEffect";
import { useModal } from "features/Modals/hooks/useModal";
import { ModalContent, ModalType } from "features/Modals/interfaces";
import { useCallback, useEffect, useState } from "react";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import useDraftOnBeforeUnload from "features/DraftDetails/hooks/useDraftOnBeforeUnload";
import { useUnmount } from "hooks/useUnmount";
// import { useDraftMessages } from "features/DraftDetails/hooks/useDraftMessages";
import { useDraftOverlay } from "features/DraftDetails/hooks/useDraftOverlay";
import { useContract } from "blockchain/hooks/useContract";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";
import { useNearContext } from "../../providers/NearProvider";
import { utils } from "near-api-external-link-fork";
import { useDraftMessages } from "../DraftDetails/hooks/useDraftMessages";
import { useDraftVersionCreate } from "./hooks/useDraftVersionCreate";

export interface NotMintedOutletProps {
    draft: DraftDetails;
    form: UseFormReturn<NewDraftVersionState, any>;
}

export function NewDraftVersion() {
    const overlay = useDraftOverlay();
    const contract = useContract();
    const alert = useDraftAlert();
    const navigate = useNavigate();
    const { handleOpen } = useModal();
    const { t } = useTranslation();

    const { initialForm, parent, setNewForm } = useDraftVersionStore();
    const { form } = useNewDraftVersionForm(initialForm);
    const { create, loading } = useDraftVersionCreate();

    const { protectLoading, setProtectLoading, canMint, setDraftStatus, mintPayload, status, setDraftStatusBeforeProtect } = useDraftStore();
    const [isCollapseOpened, setIsCollapseOpened] = useState<boolean | undefined>();
    const draftStore = useDraftStore();
    const Near = useNearContext();

    const {
        actions: { open: openMessage, close: closeMessage },
    } = useDraftMessages();
    // const {
    //     actions: { open: openMessage, close: closeMessage },
    // } = useDraftMessages();

    // useEffect(() => {
    //     if (overlay.state.type === "success") {
    //         handleOpen({ type: ModalType.PROTECTED, content: ModalContent.LicenseAndMode });
    //     }
    // }, [handleOpen, overlay.state.type]);

    const onSubmit = useCallback(
        (data: NewDraftVersionState) => {
            create(data);
        },
        [create]
    );

    const onOpenModal = useCallback(
        async (data: NewDraftVersionState) => {
            await create(data);
            setNewForm(form.getValues());
            setDraftStatusBeforeProtect("upgrade");

            handleOpen({ type: ModalType.PROTECTED, content: ModalContent.LicenseAndMode });
        },
        [handleOpen, form, setNewForm]
    );

    useRedirectEffect(!parent, "/protected");
    useDraftListener();
    useDraftOnBeforeUnload();

    useUnmount(() => {
        setProtectLoading({ mintedSucces: false });
    });
    useEffect(() => {
        if (canMint && !overlay.state.show) {
            openMessage();
        } else {
            closeMessage();
        }
    }, [canMint, closeMessage, openMessage, overlay.state.show, t]);
    useUnmount(() => {
        closeMessage();
    });

    const onMint = useCallback(async () => {
        if (!mintPayload || !canMint) return;
        setDraftStatus({ type: undefined, message: undefined });
        alert.actions.close();
        // openMessage();
        alert.actions.open();
        alert.actions.warn(t("warn.titles.do_not_exit_protection"), t("warn.messages.do_not_exit_protection"));
        if (draftStore.paymentMethod === "Near") {
            overlay.actions.open();
            overlay.actions.setWorkReady();
            const amountInYocto = utils.format.parseNearAmount("0.05");

            const res = await Near?.Contract.nft_mint({
                amount: amountInYocto,
                args: {
                    token_id: mintPayload.tokenId,
                    metadata: mintPayload.metadata,
                    receiver_id: mintPayload.receiver_id,
                    content_hash: mintPayload.contentHash,
                    metadata_hash: mintPayload.metadataHash,
                    expire: mintPayload.expire,
                    signature: mintPayload.signature,
                    v: mintPayload.v,
                    parent_token_id: mintPayload.parentTokenId,
                    version: mintPayload.version,
                },
                callbackUrl: `${window.location.origin}/protected/${mintPayload.tokenId}`,
            });

            console.log("res", res);
            overlay.actions.setWaitingMint();
        } else {
            try {
                // Сменяем статус драфта что проверка на антиплагиант прошла успешно
                overlay.actions.open();
                overlay.actions.setWorkReady();

                // Запускаем минт
                await contract.mint(
                    mintPayload.tokenId,
                    mintPayload.to,
                    mintPayload.contentHash,
                    mintPayload.metadataHash,
                    mintPayload.expire,
                    mintPayload.signature,
                    mintPayload.parentTokenId,
                    mintPayload.version
                );

                overlay.actions.setWaitingMint();
            } catch (e: any) {
                // В зависимости от того какая у нас ошибка - отображаем нужный нам статус
                alert.actions.open();
                if (e.code === 4001 || e.code === "ACTION_REJECTED") {
                    alert.actions.error(t("errors.titles.mint_canceled"), t("errors.messages.mint_canceled"));
                } else if (e.code === 32603) {
                    alert.actions.error(t("errors.titles.smth_wrong"), t("errors.messages.smth_wrong"));
                } else if (e.code === "UNPREDICTABLE_GAS_LIMIT") {
                    alert.actions.error(t("errors.titles.execution"), t("errors.messages.execution"));
                } else {
                    alert.actions.close();
                }

                setDraftStatus({ type: "error", message: t("statuses.protect_failed") });
                overlay.actions.close();
                overlay.actions.clear();
            }
        }
    }, [mintPayload, canMint, setDraftStatus, alert.actions, t, overlay.actions, contract]);

    const handleCloseCollapses = useCallback(() => {
        setIsCollapseOpened(true);
        const timout = setTimeout(() => {
            setIsCollapseOpened(undefined);
            clearInterval(timout);
        }, 1);
    }, [setIsCollapseOpened]);

    if (!parent?.isMinted) {
        return (
            <Stack align={"center"}>
                <Text size="md" color={"red"} align="center">
                    {t("errors.titles.didnt_protect_prev_version")}
                </Text>
                <SharkButton onClick={() => navigate(-1)} variant="outlined" buttonType="primary">
                    {t("certificate.history.go_back")}
                </SharkButton>
            </Stack>
        );
    }

    // RENDER
    return (
        <form>
            <Stack>
                <PageTitle
                    title={parent?.metadata.Title || t("drafts.new_draft_version")}
                    versionHistory={parent?.versionsHistory}
                    version={parent && parent.versionsHistory ? Object.keys(parent.versionsHistory).length + 1 : 1}
                    actions={
                        <Group onClick={handleCloseCollapses} noWrap={true}>
                            <SharkButton
                                type="button"
                                buttonType="secondary"
                                variant="outlined"
                                loading={loading}
                                disabled={protectLoading.fiat || protectLoading.metaMask || protectLoading.mintedSucces || protectLoading.near || loading}
                                onClick={form.handleSubmit(onSubmit)}
                            >
                                × {t("buttons.save_close_btn")}
                            </SharkButton>
                            {canMint && (
                                <Group onClick={handleCloseCollapses}>
                                    <SharkButton
                                        type="button"
                                        onClick={onMint}
                                        buttonType={"secondary"}
                                        variant={"filled"}
                                        disabled={status.message === t("statuses.protecting") || overlay.state.show}
                                    >
                                        {t("buttons.mint_btn")}
                                    </SharkButton>
                                </Group>
                            )}
                            {!canMint && (
                                <SharkButton
                                    onClick={form.handleSubmit(onOpenModal)}
                                    disabled={protectLoading.fiat || protectLoading.metaMask || protectLoading.mintedSucces || protectLoading.near || loading}
                                    variant={"filled"}
                                    buttonType={"primary"}
                                    loading={protectLoading.fiat || protectLoading.metaMask || loading}
                                >
                                    {t("buttons.upgrade_btn")}
                                </SharkButton>
                            )}
                        </Group>
                    }
                />

                <Container>
                    <VersionForm isCollapseClosed={isCollapseOpened} form={form} />
                </Container>
            </Stack>
        </form>
    );
}
