import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { NewDraftVersionState } from "../hooks/useNewDraftVersion";

// Types
export interface NewVersionState {
    parent?: DraftDetails;
    draftId?: string;
    initialForm: NewDraftVersionState;
    newForm: NewDraftVersionState
}

// Initial State
const initialState: NewVersionState = {
    parent: undefined,
    draftId: undefined,
    initialForm: {
        title: "",
        fullTitle: "",
        type: "text",
        text: "",
        file: undefined,
        image: undefined,
        authors: [],
        coAuthors: [],
        participants: [],
        additionalImages: []

    },
    newForm: {
        title: "",
        fullTitle: "",
        type: "text",
        text: "",
        file: undefined,
        image: undefined,
        authors: [],
        coAuthors: [],
        participants: [],
        additionalImages: []

    },

};

// Slice
const slice = createSlice({
    name: "VersionForm",
    initialState,
    reducers: {
        setParent: (state, action: PayloadAction<NewVersionState["parent"]>) => {
            state.parent = action.payload;
        },
        setDraft: (state, action: PayloadAction<NewVersionState["draftId"]>) => {
            state.draftId = action.payload;
        },
        setInitialForm: (state, action: PayloadAction<NewVersionState['initialForm'] | undefined>) => {
            state.initialForm = action.payload || initialState.initialForm;
        },
        setNewForm: (state, action: PayloadAction<NewVersionState['initialForm'] | undefined>) => {
            state.newForm = action.payload || initialState.initialForm;
        },
        clear: () => initialState
    },
});

export const newVersionReducers = slice.reducer;
export const newVersionActions = slice.actions;
