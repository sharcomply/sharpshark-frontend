import { ReactNode, useEffect, useState } from "react";
import { Collapse as CollapseRoot, Group, Stack, Text } from "@mantine/core"
import Icon from "components/Icon";
import styled from "@emotion/styled";

// Props
type CollapseProps = {
    opened?: boolean;
    label?: string;
    children?: ReactNode;
    isClosed?: boolean | undefined
}

export const Collapse = ({ opened = false, label, children, isClosed }: CollapseProps) => {
    const [isOpen, setOpen] = useState(opened);

    useEffect(() => {
        if(isClosed === undefined) return
        setOpen(!isClosed)
    }, [isClosed, setOpen])

    return (
        <Stack spacing={0} sx={{ borderTop: '1px solid #DDDEEE'}}>
            <Button onClick={() => setOpen((v) => !v)} px={16} py={8}>
                <Icon icon={`ic:round-keyboard-arrow-down`} color="#4b506d" />
                <Text size="md" weight={'500'} color="#4b506d">{label}</Text>
            </Button>
            <CollapseRoot in={isOpen}>
                <Stack spacing={8} p={16} pt={8}>
                    {children}
                </Stack>
            </CollapseRoot>
        </Stack>
    )
}

// Styling
const Button = styled(Group)`
    cursor: pointer;

    & {
        transition-duration: 150ms;
        transition-property: background-color;
        transition-timing-function: linear;
    }

    &:hover {
        background-color: rgb(248, 249, 250);
    }

    &.opened {
        background-color: rgb(248, 249, 250);
    }
`
