// Props
import { Group, Popover, Text } from "@mantine/core";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";
import { ReactNode, useState } from "react";
import clsx from "clsx";
import { useTranslation } from "react-i18next";

// Variables
const rootStyles = {
    background: "#FFFFFF",
    border: "1px solid #DDDEEE",
    boxShadow: " 0px 2px 12px rgba(0, 0, 0, 0.1)",
    radius: 4,
    overflow: "hidden",
};

type ComponentProps = {
    currentVersion?: number;
    children: ReactNode;
    versionHistory: Record<number, string>
};

// Component
export function VersionSelector({ children, currentVersion, versionHistory }: ComponentProps) {
    const [opened, setOpened] = useState(false);
    const {t} = useTranslation()
    const control = (
        <Group align={"center"} position={"center"} px={4} onClick={() => setOpened((value) => !value)}>
            {children}
        </Group>
    );

    return (
        <Popover
            opened={opened}
            target={control}
            onClose={() => setOpened(false)}
            radius={4}
            position="bottom"
            placement="start"
            transition={"pop-top-left"}
            styles={{ body: rootStyles, inner: { padding: 0 } }}
        >
            {/* Versions */}
            {Object.keys(versionHistory).map((key) => (
                <Link key={key} to={`/protected/${versionHistory[Number(key)]}/content`}>
                    <MenuItem px={10} py={10} className={clsx({ active: Number(key) === currentVersion, disabled: Number(key) === currentVersion })}>
                        <Text weight="500" sx={{ fontSize: 14, lineHeight: "16px" }}>
                            v.{key}
                        </Text>
                    </MenuItem>
                </Link>
            ))}

            {/* New Version */}
            <Link to={`/protected/new`}>
                <MenuItem px={10} py={10}>
                    <Text size={"sm"} weight={"500"} sx={{ fontSize: 14, lineHeight: "16px" }}>
                        {t("buttons.new_btn")}
                    </Text>
                </MenuItem>
            </Link>
        </Popover>
    );
}

// Styling
const MenuItem = styled(Group)`
    cursor: pointer;
    color: #666892;

    &:hover {
        background-color: #4700e5;
        color: #ffffff;
    }

    &.disabled {
        color: #bbbed8;
    }

    &.disabled:hover {
        background-color: #f7f7fa;
        color: #8b8fad;
    }

    &.active {
        color: #4700e5;
        background-color: transparent;
    }

    &.active.disabled {
        color: #4700e5;
        background-color: transparent;
    }
`;
