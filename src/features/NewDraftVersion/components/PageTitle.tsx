import { Group, Text } from "@mantine/core";
import Icon from "components/Icon";
import { ReactNode } from "react";
import NoSecurity from "features/Icons/NoSecurity";
import { VersionSelector } from "./VersionSelector";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";

// Props
type ComponentProps = {
    title?: string;
    actions?: ReactNode;
    version?: number;
    versionHistory?: Record<number, string>;
};

export const PageTitle = (props: ComponentProps) => {
    const { title, actions, version, versionHistory } = props;

    const { status } = useDraftStore();

    const badgeColor = status.type === "error" ? "#E50C54" : status.type === "success" ? "#29CCB1" : status.type === "warn" ? "#FFC933" : "#979CBC";
    const badgeTextColor = status.type === "error" ? "#FFFFFF" : !status.type ? "#FFFFFF" : "#0A001E";

    return (
        <Group sx={{ height: 42, maxWidth: "calc(100vw - 356px)" }} spacing={8} noWrap={true}>
            <Group spacing={4} noWrap={true} sx={{ cursor: "pointer" }}>
                <Icon container={<NoSecurity />} icon={""} />

                {!versionHistory && !!version && (
                    <Group px={4}>
                        <Text color={"#666892"} weight="700" sx={{ fontSize: 14, lineHeight: "16px" }}>
                            V.{version}
                        </Text>
                    </Group>
                )}

                {!!version && !!versionHistory && (
                    <VersionSelector versionHistory={versionHistory} currentVersion={version}>
                        <Text color={"#666892"} weight="700" sx={{ fontSize: 14, lineHeight: "16px" }}>
                            V.{version}
                        </Text>
                    </VersionSelector>
                )}
            </Group>

            <Group sx={{ maxWidth: "40%" }} noWrap={true}>
                <Text
                    color={"#0a001e"}
                    weight="700"
                    sx={{
                        fontSize: 24,
                        lineHeight: "30px",
                        overflow: "hidden",
                        whiteSpace: "nowrap",
                        textOverflow: "ellipsis",
                    }}
                >
                    {title}
                </Text>
            </Group>
            {status.message && (
                <Group px={12} sx={{ borderRadius: 100, backgroundColor: badgeColor, height: 24 }}>
                    <Text color={badgeTextColor} size={"xs"} transform={"uppercase"} sx={{ whiteSpace: "nowrap" }}>
                        {status.message}
                    </Text>
                </Group>
            )}

            <Group sx={{ flex: 1 }} />

            <Group spacing={20} noWrap={true}>
                {actions}
            </Group>
        </Group>
    );
};
