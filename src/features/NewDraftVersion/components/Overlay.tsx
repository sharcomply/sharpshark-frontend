import { Center, Overlay as MantineOverlay, Stack, Text } from "@mantine/core";
import { Fragment, ReactNode, useCallback, useMemo } from "react";
import protectedLoader from "assets/images/protected-loader.svg";
import doneLoader from "assets/images/protected-loader-done.svg";
import { SharkButton } from "components/SharkButton";
import { useNavigate, useParams } from "react-router-dom";
import { useQueryClient } from "react-query";
import { useDraftOverlay } from "features/DraftDetails/hooks/useDraftOverlay";
import { OverlaySteps } from "features/DraftDetails/store";
import { useDraftVersionStore } from "../hooks/useDraftVersionStore";
import styled from "@emotion/styled";
import ActionDone from "features/Icons/ActionDone";

// Props
type ComponentProps = {
    variant?: "success" | "default" | string;
    text?: string | ReactNode;
};

// Component
export function Overlay({ variant, text }: ComponentProps) {
    const { draftId } = useDraftVersionStore();
    const overlay = useDraftOverlay();
    const queryClient = useQueryClient();
    const params = useParams();
    const navigate = useNavigate();

    const borderColor = useMemo(() => {
        if (variant === "success") return "#29CCB1";
        return "#4700E5";
    }, [variant]);

    const backgroundColor = useMemo(() => {
        if (variant === "success") return "#EAFAF6";
        return "#EDEEFF";
    }, [variant]);

    const loader = useMemo(() => {
        if (variant === "success") return <img height={100} width={100} src={doneLoader} alt="" />;

        return <img height={100} width={100} src={protectedLoader} alt="" />;
    }, [variant]);

    const onSubmit = useCallback(async () => {
        overlay.actions.clear();
        overlay.actions.close();

        await Promise.all([
            queryClient.invalidateQueries([`SIDEBAR_COUNTS`]),
            queryClient.invalidateQueries(["PROTECTED_LIST"]),
            queryClient.invalidateQueries(["DRAFT_LIST"]),
            queryClient.invalidateQueries(["DRAFT"]),
            queryClient.invalidateQueries(["PROTECTED_ITEM", draftId || params.protectId])
            
        ]);

        if (!draftId && !params.protectId) return navigate(0);
        else return navigate(`/protected/${draftId || params.protectId}`, { replace: true });
    }, [draftId, navigate, overlay.actions, queryClient]);

    return (
        <Fragment>
            <MantineOverlay
                opacity={0.9}
                zIndex={5}
                sx={{
                    borderWidth: 1,
                    borderStyle: "solid",
                    borderColor,
                    backgroundColor,
                    borderRadius: 8,
                }}
            />
            <Center sx={{ position: "absolute", inset: 0, zIndex: 10 }}>
                <Stack spacing={16} align="center" sx={{ maxWidth: "75%" }}>
                {overlay.state.step === OverlaySteps.MINTED ? <Success><ActionDone height={64}/></Success> : loader}
                    <Text size="md" weight={"500"} color={"#0A001E"} align={"center"}>
                        {text}
                    </Text>
                    {overlay.state.step === OverlaySteps.MINTED && (
                        <SharkButton onClick={onSubmit} buttonType={"secondary"} variant={"outlined"} type="button">
                            Ok
                        </SharkButton>
                    )}
                </Stack>
            </Center>
        </Fragment>
    );
}

// Styling

const Success = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 80px;
    height: 80px;
    border-radius: 50%;
    border: 4px solid #29CCB1;
`