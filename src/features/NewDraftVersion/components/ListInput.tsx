import { Group, Input } from "@mantine/core";
import Icon from "components/Icon";
import { Control, Controller } from "react-hook-form";

// Props
type InputProps = {
    name: string;
    control: Control;
    placeholder: string;
    onDelete?: () => void;
    flex?: number,
    maxLength?: number
}

// Component
export const ListInput = ({ name, control, placeholder, onDelete, flex = 1/3, maxLength }: InputProps) => {
    return (
        <Group sx={{ flex: 1 }}>
            <Controller name={name} control={control} render={({ field }) => (
                <Input
                    sx={{ flex }}
                    placeholder={placeholder} 
                    rightSectionWidth={onDelete && 36}
                    maxLength={maxLength}
                    rightSection={
                        onDelete && <Icon color="#E50C54" icon="ic:round-delete" onClick={onDelete}/>
                    }
                    {...field}
                    styles={{
                        input: { 
                            height: 42,
                            minWidth: 402,
                            border: '1px solid #F0F1F7',
                            color: "#0A001E",
                            '&::placeholder': {
                                color: "#979CBC",
                                fontSize: 14,
                                lineHeight: '20px',
                            }
                        }
                    }}
                />
            )}/>
        </Group>
    )
}

// Styling
