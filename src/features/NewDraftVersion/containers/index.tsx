import styled from "@emotion/styled";
import { Stack } from "@mantine/core";
import { Alert } from "features/DraftDetails/components/Alert";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";
import { useDraftOverlay } from "features/DraftDetails/hooks/useDraftOverlay";
import { useUnmount } from "hooks/useUnmount";
import { Fragment, PropsWithChildren } from "react";
import { Overlay } from "../components/Overlay";

export function Container({ children }: PropsWithChildren<{}>) {
    const alert = useDraftAlert();
    const overlay = useDraftOverlay();

    useUnmount(() => {
        overlay.actions.clear()
        overlay.actions.close()
        alert.actions.clear()
        alert.actions.close()
    })

    return (
        <Fragment>
            {alert.state.show && <Alert title={alert.state.title} text={alert.state.message} variant={alert.state.type} />}
            <FormRoot sx={{ minHeight: "380px" }}>
                {overlay.state.show && <Overlay text={overlay.state.message} variant={overlay.state.type} />}
                { children }
            </FormRoot>
        </Fragment>
    );
}

const FormRoot = styled(Stack)`
    position: relative;
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    overflow: hidden;
`;
