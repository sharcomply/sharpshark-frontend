import { useCallback } from "react";
import { useAppDispatch, useAppSelector } from "store";
import { newVersionActions } from "../store";
import { NewDraftVersionState } from "./useNewDraftVersion";

export function useDraftVersionStore() {
    const { parent, initialForm, draftId, newForm } = useAppSelector(state => state.versionForm)
    const dispatch = useAppDispatch()

    const setParent = useCallback((payload: DraftDetails) => {
        const action = newVersionActions.setParent(payload)
        dispatch(action)
    }, [dispatch])

    const setDraftId = useCallback((payload: string) => {
        const action = newVersionActions.setDraft(payload)
        dispatch(action)
    }, [dispatch])

    const setInitialForm = useCallback((data?: NewDraftVersionState) => {
        const action = newVersionActions.setInitialForm(data)
        dispatch(action)
    }, [dispatch])

    const setNewForm = useCallback((data?: NewDraftVersionState) => {
        const action = newVersionActions.setNewForm(data)
        dispatch(action)
    }, [dispatch])

    const clear = useCallback(() => {
        const action = newVersionActions.clear()
        dispatch(action)
    }, [dispatch])

    return {
        parent,
        initialForm,
        setParent,
        setDraftId,
        draftId,
        setInitialForm,
        newForm,
        setNewForm,
        clear
    }
}
