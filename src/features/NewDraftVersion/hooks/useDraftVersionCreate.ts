import { useMutation, useQueryClient } from "react-query";
import { backend } from "api";
import { useCallback, useState } from "react";
import { useDraftVersionStore } from "./useDraftVersionStore";
import { NewDraftVersionState } from "./useNewDraftVersion";
import { useNavigate } from "react-router-dom";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import { useModal } from "../../Modals/hooks/useModal";
import { useDraftAlert } from "../../DraftDetails/hooks/useDraftAlert";
import { t } from "i18next";

export const useDraftVersionCreate = () => {
    const queryClient = useQueryClient();
    const { parent, setDraftId } = useDraftVersionStore();
    const { draftStatusBeforeProtect } = useDraftStore()
    const [isLoading, setLoading] = useState(false)
    const navigate = useNavigate()
    const onSuccess = () => Promise.all([
        queryClient.invalidateQueries([`PROTECTED_LIST`]),
        queryClient.invalidateQueries(["PROTECTED_ITEM"])
    ]);
    const alert = useDraftAlert();

    const { mutateAsync: saveDraft, isLoading: createLoading } = useMutation(backend.drafts.saveDraft, { onSuccess });
    const { mutateAsync: updateDraft, isLoading: updateLoading } = useMutation(backend.drafts.editDraft, { onSuccess });

    const { handleClose } = useModal();
    const transformForm = useCallback((form: NewDraftVersionState) => {
        const payload = {
            uploadFile: form.file || form.image,
            fileExt: "txt",
        };

        if (form.type === "text" && !form.file) {
            const blob = new Blob([form.text], { type: "text/plain" });
            payload.uploadFile = new File([blob], form.title);
        }

        if (form.type === "image" && form.image) {
            payload.fileExt = form.image.type.split("/")[1];
        }

        return payload;
    }, []);

    const handleSave = useCallback(async (form: NewDraftVersionState) => {
        return await saveDraft({
            title: form.title,
            type: form.type,
            fullTitle: form.fullTitle,
            authors: form.authors.filter(i => i.length),
            coAuthors: form.coAuthors.filter(i => i.length),
            participants: form.participants.filter(i => i.length),
            parentId: parent?._id,
        });
    }, [parent?._id, saveDraft])

    const create = useCallback(
        async (form: NewDraftVersionState) => {
            if (!parent) return;

            setLoading(true);

            const payload = transformForm(form);

            try {
                // Save Draft
                const { _id } = await handleSave(form);
                setDraftId(_id);
                // Get Links for Upload And Download
                const { uploadLink, downloadLink } = await backend.drafts.getFileUploadLink(_id, payload.fileExt);

                // Upload Link
                await backend.drafts.uploadFile(uploadLink.url, payload.uploadFile!, uploadLink.fields);

            const uploadFile = async () => {
                let additionalImages: string[] = [];
                for (const item of form.additionalImages!) {
                    if (typeof item !== "string") {
                        const { uploadLink, downloadLink } = await backend.disputes.getImageUploadLink(_id);
                        additionalImages.push(downloadLink);
                        await backend.drafts.uploadFile(uploadLink.url, item, uploadLink.fields);
                    } else additionalImages.push(item);
                }
                return { additionalImages };
            };

            const { additionalImages } = await uploadFile();

            // update draft using link to file
            await updateDraft({
                _id,
                title: form.title,
                linkUrl: downloadLink,
                additionalImages
            });


            setLoading(false);
            console.log(draftStatusBeforeProtect)
            if (!draftStatusBeforeProtect) {
                navigate(`/protected/${_id}`, { replace: true });

            }


                return await backend.drafts.getDraft(_id);
            } catch (e: any) {
                setLoading(false);
                const error = await e.response.json();
                if (error.data.message === "Parent not found") {
                    handleClose();
                    alert.actions.open();
                    alert.actions.error(t("The parent is not minted"), t("You have to mint the previous version"));
                }

                console.log("[DRAFT_SAVE]", e);
                throw e;
            }
        },
        [parent, transformForm, handleSave, setDraftId, updateDraft]
    );

    return {
        create,
        loading: createLoading || updateLoading || isLoading,
    };
};
