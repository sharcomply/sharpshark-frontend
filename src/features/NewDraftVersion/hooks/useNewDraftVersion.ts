import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { validationSchema } from "../validationSchema";
import { useEffect } from "react";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";
import { t } from "i18next";

export interface NewDraftVersionState {
    title: string;
    text: string;
    file?: File;
    image?: any;
    type: "text" | "image";
    fullTitle: string;
    authors: string[];
    coAuthors: string[];
    participants: string[];
    additionalImages: string[]
}

export const useNewDraftVersionForm = (initialValues: NewDraftVersionState) => {
    const {
        actions: { open, close, error },
    } = useDraftAlert();

    const form = useForm<NewDraftVersionState>({
        resolver: yupResolver(validationSchema),
        defaultValues: initialValues,
    });

    useEffect(() => {
        const errors = Object.values(form.formState.errors)[0];
        if (!errors) return;

        open();
        if (Array.isArray(errors) && errors.length) {
            errors.map((item) => {
                if (item?.message) {
                    error(t("errors.titles.validation"), item.message);
                }
            });
        } else {
            error(t("errors.titles.validation"), errors.message || "Validation error");
        }

        return () => {
            close();
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [error, close, open, error, form.formState.errors]);

    return {
        form,
    };
};
