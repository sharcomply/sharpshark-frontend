import { backend } from "api";
import { useContract } from "blockchain/hooks/useContract";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";
// import { useDraftMessages } from "features/DraftDetails/hooks/useDraftMessages";
import { useDraftOverlay } from "features/DraftDetails/hooks/useDraftOverlay";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import { useModal } from "features/Modals/hooks/useModal";
import { useDraftCreate } from "features/ProtectedDetails/hooks/useDraftCreate";
import { useProtectedDraftStore } from "features/ProtectedDetails/hooks/useProtectedDraftStore";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useDraftVersionCreate } from "./useDraftVersionCreate";
import { NewDraftVersionState } from "./useNewDraftVersion";
import { getNetworkById } from "../../../constants/networks";
import { useNearContext } from "../../../providers/NearProvider";
import { useWhoAmi } from "../../Auth/hooks/useWhoAmi";

export const useDraftVersionUpgrade = () => {
    const networkName = getNetworkById(window.ethereum.networkVersion);
    const { t } = useTranslation();
    const contract = useContract();
    const alert = useDraftAlert();
    const overlay = useDraftOverlay();
    const draftHandler = useDraftCreate();
    const draftHandlerCreate = useDraftVersionCreate();
    const draftStore = useDraftStore();
    const { handleClose } = useModal();
    const Near = useNearContext();
    const { data } = useWhoAmi();
    // const {
    //     actions: { open: openMessage, close: closeMessage },
    // } = useDraftMessages();

    const {
        state: { mode },
    } = useProtectedDraftStore();

    const protectWithMetaMask = useCallback(
        async (draft: Draft) => {
            if (!draft._id) return;
            handleClose();
            draftStore.setPaymentMethod("Metamask");

            alert.actions.clear();
            alert.actions.close();

            draftStore.setDraftStatus({
                type: "warn",
                message: t("statuses.protecting"),
            });

            // openMessage();
            alert.actions.open();
            alert.actions.warn(t("warn.titles.do_not_exit_protection"), t("warn.messages.do_not_exit_protection"));
            overlay.actions.open();
            overlay.actions.setPendingPayment();

            try {
                if (draft.type === "image") {
                    const { toBlockchain } = await backend.scans.startScan({ documentId: draft._id, network: networkName?.symbol });
                    const tx = await contract.payRequest(toBlockchain.requestId, toBlockchain.expire, toBlockchain.signature, {
                        value: toBlockchain.priceInWei,
                    });
                    await tx.wait();
                } else {
                    const { success } = await backend.scans.startEstimateDocument({ documentId: draft._id });
                    console.log(`estimate`, success);
                }
            } catch (e: any) {
                draftStore.setProtectLoading({ metaMask: false });
                // closeMessage();
                draftStore.setDraftStatus({ type: "error", message: t("statuses.protect_failed") });
                overlay.actions.close();
                overlay.actions.clear();

                // code: 4001, message: 'MetaMask Tx Signature: User denied transaction signature.'
                if (e.code === 4001 || e.code === "ACTION_REJECTED") {
                    alert.actions.open();
                    draftStore.setProtectLoading({ metaMask: false });

                    alert.actions.error(t("errors.titles.protection_canceled"), t("errors.messages.protection_canceled"));
                }

                console.log("[PROTECT] ", e);
            }
        },
        [alert.actions, contract, overlay.actions, handleClose, draftStore, networkName, t]
    );

    const closeNear = useCallback(() => {
        overlay.actions.close();
        overlay.actions.clear();
        draftStore.setDraftStatus({
            type: "error",
            message: t("statuses.protect_failed"),
        });
        draftStore.showDraftAlert(true);
        draftStore.setDraftAlert({
            type: "error",
            title: t("errors.titles.protection_canceled"),
            message: t("errors.messages.protection_canceled"),
        });

        draftStore.setProtectLoading({ metaMask: false });
    }, [overlay, draftStore]);

    const protectWithNear = useCallback(
        async (draft: Draft) => {
            if (!draft._id) return;
            handleClose();
            draftStore.setPaymentMethod("Near");
            if (!Near?.WalletConnection.account().accountId) {
                alert.actions.open();
                alert.actions.error(t("Connect Near address!"), t("You should connect the near address"));
                handleClose();
                return;
            } else if (data?.user.walletAddressNear !== Near?.WalletConnection.account().accountId) {
                alert.actions.open();
                draftStore.setDraftStatus({ type: "error", message: t("statuses.protect_failed") });
                alert.actions.error(t("Wrong near address"), t("Your connected wallet don't match to your address that linked to account"));
                return;
            }

            alert.actions.clear();
            alert.actions.close();

            draftStore.setDraftStatus({
                type: "warn",
                message: t("statuses.protecting"),
            });
            alert.actions.open();
            alert.actions.warn(t("warn.titles.do_not_exit_protection_near"), t("warn.messages.do_not_exit_protection_near"));
            // openMessage();
            overlay.actions.open();
            overlay.actions.setPendingPayment("near", closeNear);

            try {
                if (draft.type === "image") {
                    const { requestId, toBlockchain } = await backend.scans.startScan({ documentId: draft._id, network: "near" });
                    await Near?.Contract?.pay_request({
                        amount: toBlockchain.priceInWei,
                        args: {
                            amount: toBlockchain.priceInWei,
                            request_id: requestId,
                            expire: toBlockchain.expire as string,
                            signature: toBlockchain.signature as number[],
                            v: toBlockchain.v!,
                        },
                    });
                } else {
                    const { success } = await backend.scans.startEstimateDocument({ documentId: draft._id });
                    console.log(`estimate`, success);
                }
            } catch (e: any) {
                draftStore.setProtectLoading({ near: false });
                // closeMessage();
                alert.actions.close();
                draftStore.setDraftStatus({ type: "error", message: t("statuses.protect_failed") });
                overlay.actions.close();
                overlay.actions.clear();

                // code: 4001, message: 'MetaMask Tx Signature: User denied transaction signature.'
                if (e.code === 4001 || e.code === "ACTION_REJECTED") {
                    alert.actions.open();
                    draftStore.setProtectLoading({ metaMask: false });

                    alert.actions.error(t("errors.titles.protection_canceled"), t("errors.messages.protection_canceled"));
                }

                console.log("[PROTECT] ", e);
            }
        },
        [handleClose, draftStore, alert.actions, t, overlay.actions, Near?.Contract]
    );

    const protectWitFiat = useCallback(
        async (draft: Draft) => {
            draftStore.showDraftAlert(false);
            draftStore.setPaymentMethod("Stripe");
            alert.actions.open();
            alert.actions.warn(t("warn.titles.do_not_exit_protection"), t("warn.messages.do_not_exit_protection"));
            // openMessage();
            try {
                if (draft.type === "image") {
                    const { requestId } = await backend.scans.startScan({ documentId: draft._id, network: networkName?.symbol });

                    const { url } = await backend.scans.getScanPaymentLink({
                        requestId,
                        successUrl: `${window.location.href}?stripeSucces=true`,
                        cancelUrl: `${window.location.href}?stripeSucces=true`,
                        network: "near",
                    });

                    window.open(url);

                    handleClose();
                    draftStore.showDraftAlert(false);
                    draftStore.setDraftStatus({
                        type: "warn",
                        message: t("statuses.protecting"),
                    });

                    overlay.actions.open();
                    overlay.actions.setPendingPaymentFiat(url);
                } else {
                    await backend.scans.startEstimateDocument({ documentId: draft._id });
                }
            } catch (e: any) {
                // Close overlay and clean state
                overlay.actions.close();
                overlay.actions.clear();
                // closeMessage();
                alert.actions.close();

                draftStore.setDraftStatus({
                    type: "error",
                    message: t("statuses.protect_failed"),
                });

                draftStore.setProtectLoading({ fiat: false });
                // closeMessage();
                alert.actions.close();

                console.log("[PROTECT] ", e);
            }
        },
        [draftStore, alert.actions, t, networkName?.symbol, handleClose, overlay.actions]
    );

    const upgradeWithMetaMask = useCallback(
        async (form: NewDraftVersionState) => {
            try {
                if (mode === "view") {
                    const draft = await draftHandlerCreate.create(form);
                    if (!draft) throw new Error("Draft Not Created");
                    return await protectWithMetaMask(draft);
                }
                if (mode === "edit") {
                    const draft = await draftHandler.create(form);
                    if (!draft) throw new Error("Draft Not Created");
                    return await protectWithMetaMask(draft);
                }
            } catch (e) {
                console.log("[DRAFT_CHILD_CREATE]", e);
                draftStore.setProtectLoading({ metaMask: false });
            } finally {
                console.log(`protected`);
            }
        },
        [mode, draftHandler, draftHandlerCreate, protectWithMetaMask, draftStore]
    );

    const upgradeWithNear = useCallback(
        async (form: NewDraftVersionState) => {
            try {
                if (!Near?.WalletConnection.account().accountId) {
                    alert.actions.open();
                    alert.actions.error(t("Connect Near address!"), t("You should connect the near address"));
                    handleClose();
                    return;
                } else if (data?.user.walletAddressNear && data?.user.walletAddressNear !== Near?.WalletConnection.account().accountId) {
                    alert.actions.open();
                    alert.actions.error(t("Wrong near address"), t("Your connected wallet don't match to your address that linked to account"));
                    handleClose();
                    return;
                }
                if (mode === "view") {
                    const draft = await draftHandlerCreate.create(form);
                    if (!draft) throw new Error("Draft Not Created");
                    return await protectWithNear(draft);
                }
                if (mode === "edit") {
                    const draft = await draftHandler.create(form);
                    if (!draft) throw new Error("Draft Not Created");
                    return await protectWithNear(draft);
                }
            } catch (e) {
                console.log("[DRAFT_CHILD_CREATE]", e);
                draftStore.setProtectLoading({ near: false });
            } finally {
                console.log(`protected`);
            }
        },
        [mode, draftHandler, draftHandlerCreate, protectWithNear, draftStore, data]
    );

    const upgradeWithFiat = useCallback(
        async (form: NewDraftVersionState) => {
            try {
                if (mode === "view") {
                    const draft = await draftHandlerCreate.create(form);
                    if (!draft) throw new Error("Draft Not Created");
                    return await protectWitFiat(draft);
                }
                if (mode === "edit") {
                    const draft = await draftHandler.create(form);
                    if (!draft) throw new Error("Draft Not Created");
                    return await protectWitFiat(draft);
                }
            } catch (e) {
                console.log("[DRAFT_CHILD_CREATE]", e);
                draftStore.setProtectLoading({ fiat: false });
            }
        },
        [mode, draftHandler, draftHandlerCreate, protectWitFiat, draftStore]
    );

    return {
        upgradeWithMetaMask,
        upgradeWithFiat,
        upgradeWithNear,
    };
};
