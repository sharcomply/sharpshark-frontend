import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Types
export interface ProtectedDraftState {
    tokenId?: string;
    draftId?: string;
    mode: "view" | "edit" | "new";
}

// Initial State
const initialState: ProtectedDraftState = {
    draftId: undefined,
    mode: "view",
    tokenId: undefined
};

// Slice
const protectedSlice = createSlice({
    name: "protectedDraft",
    initialState,
    reducers: {
        setDraftId: (state, action: PayloadAction<ProtectedDraftState["draftId"]>) => {
            state.draftId = action.payload;
        },
        setMode: (state, action: PayloadAction<ProtectedDraftState['mode']>) => {
            state.mode = action.payload;
        },

        setTokenId: (state, action: PayloadAction<ProtectedDraftState['tokenId']>) => {
            state.tokenId = action.payload;
        },
        clear: () => initialState
    },
});

export const protectedDraftReducers = protectedSlice.reducer;
export const protectedDraftActions = protectedSlice.actions;
