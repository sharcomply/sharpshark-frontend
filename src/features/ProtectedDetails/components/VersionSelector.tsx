// Props
import { Group, Popover, Text } from "@mantine/core";
import styled from "@emotion/styled";
import { Link, useOutletContext } from "react-router-dom";
import { ReactNode, useCallback, useMemo, useState } from "react";
import clsx from "clsx";
import { useDraftVersionStore } from "features/NewDraftVersion/hooks/useDraftVersionStore";
import { useTranslation } from "react-i18next";
import { ProtectedOutletProps } from "pages/protected/[protectedId]";

// Variables
const rootStyles = {
    background: "#FFFFFF",
    border: "1px solid #DDDEEE",
    boxShadow: " 0px 2px 12px rgba(0, 0, 0, 0.1)",
    radius: 4,
    overflow: "hidden",
};

type ComponentProps = {
    currentVersion?: number;
    children: ReactNode;
    versionHistory: Record<number, string>;
    parent: DraftDetails;
};

// Component
export function VersionSelector({ children, currentVersion, versionHistory, parent }: ComponentProps) {
    const [opened, setOpened] = useState(false);
    const { clear, setParent, setInitialForm } = useDraftVersionStore();
    const { draft } = useOutletContext<ProtectedOutletProps>();

    const { t } = useTranslation();

    const lastParent = useMemo(() => {
        const versionSize = Object.keys(versionHistory).length;
        const lastParentId = versionHistory[versionSize];

        return { ...parent, _id: lastParentId };
    }, [parent, versionHistory]);

    const handleNew = useCallback(() => {
        clear();

        setParent(lastParent);
        setInitialForm({
            title: draft.title,
            fullTitle: draft.metadata?.Title || "",
            type: draft.type || "text",
            text: draft.text || "",
            image: draft.imageFile,
            authors: draft.authors,
            coAuthors: draft.coAuthors,
            participants: draft.participants,
            additionalImages: draft.additionalImages,
        });
    }, [
        draft.authors,
        draft.coAuthors,
        draft.imageFile,
        draft.metadata?.Title,
        draft.participants,
        draft.text,
        draft.title,
        draft.type,
        lastParent,
        setInitialForm,
        setParent,
        clear,
    ]);

    // RENDER
    const control = (
        <Group align={"center"} position={"center"} px={4} onClick={() => setOpened((value) => !value)}>
            {children}
        </Group>
    );

    return (
        <Popover
            opened={opened}
            target={control}
            onClose={() => setOpened(false)}
            radius={4}
            position="bottom"
            placement="start"
            transition={"pop-top-left"}
            styles={{ body: rootStyles, inner: { padding: 0 } }}
        >
            {/* Versions */}
            {Object.keys(versionHistory).map((key) => (
                <Link key={key} to={`/protected/${versionHistory[Number(key)]}/content`} onClick={clear}>
                    <MenuItem px={10} py={10} className={clsx({ active: Number(key) === currentVersion, disabled: Number(key) === currentVersion })}>
                        <Text weight="500" sx={{ fontSize: 14, lineHeight: "16px" }}>
                            v.{key}
                        </Text>
                    </MenuItem>
                </Link>
            ))}

            {/* New Version */}
            <Link to={`/protected/new`} onClick={handleNew}>
                <MenuItem px={10} py={10}>
                    <Text size={"sm"} weight={"500"} sx={{ fontSize: 14, lineHeight: "16px" }}>
                        {t("buttons.new_btn")}
                    </Text>
                </MenuItem>
            </Link>
        </Popover>
    );
}

// Styling
const MenuItem = styled(Group)`
    cursor: pointer;
    color: #666892;

    &:hover {
        background-color: #4700e5;
        color: #ffffff;
    }

    &.disabled {
        color: #bbbed8;
    }

    &.disabled:hover {
        background-color: #f7f7fa;
        color: #8b8fad;
    }

    &.active {
        color: #4700e5;
        background-color: transparent;
    }

    &.active.disabled {
        color: #4700e5;
        background-color: transparent;
    }
`;
