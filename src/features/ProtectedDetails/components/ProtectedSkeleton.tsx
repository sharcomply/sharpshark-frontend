import styled from "@emotion/styled";
import {Group, Loader, Overlay, Stack, Text} from "@mantine/core";
import {PageTitle} from "features/DraftDetails/components/PageTitle";
import {ProtectedSkeletonForm} from "./ProtectedSkeletonForm";
import {CustomSkeleton} from "../../DraftDetails/components/Skeleton";
import {useTranslation} from "react-i18next";

// Component
interface ComponentProps {
  isOverlay?: boolean
}

export default function ProtectedSkeleton({isOverlay}: ComponentProps) {
  const {t} = useTranslation()
  // Render
  const actionItems = () => (
    <Group>
      <Group>
        <CustomSkeleton height={42} width={130} radius={4} fill/>
        <CustomSkeleton height={42} width={130} radius={4} fill/>
      </Group>
    </Group>
  );

  const TitlesGroup = () => (
    <Group style={{gap: 8}}>
      <CustomSkeleton height={16} width={16} radius={16} fill/>
      <CustomSkeleton height={16} width={20} fill/>
      <CustomSkeleton height={30} width={200} radius={4} fill/>
    </Group>
  );

  return (
    <form>
      <Stack>
        <PageTitle isLoading={true} title={TitlesGroup()} actions={actionItems()}/>

        <Group style={{gap: 12}}>
          <CustomSkeleton height={26} width={80} radius={4} fill/>
          <CustomSkeleton height={26} width={168} radius={4} fill/>
          <CustomSkeleton height={26} width={144} radius={4} fill/>
          <CustomSkeleton height={26} width={180} radius={4} fill/>
        </Group>

        <FormRoot sx={{minHeight: "380px", overflowY: "scroll"}}>
          <ProtectedSkeletonForm/>
        </FormRoot>
        {isOverlay &&
            <Overlay>
                <Stack style={{display: "flex", justifyContent: 'center', alignItems: 'center', height: '100%'}}>
                    <Text color={'black'} align={'center'}
                          dangerouslySetInnerHTML={{__html: t('statuses.loading_ipfs')}}>
                    </Text>
                    <Loader/>
                </Stack>
            </Overlay>
        }
      </Stack>
    </form>
  );
}

const FormRoot = styled(Stack)`
  position: relative;
  background-color: #ffffff;
  border: 1px solid #dddeee;
  border-radius: 8px;
  overflow: hidden;
  justify-content: space-between;

  img {
    max-width: 480px;
    height: auto !important;
  }
`;

// const
