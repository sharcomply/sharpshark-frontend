import styled from "@emotion/styled";
import { Center, Overlay as MantineOverlay, Stack, Text } from "@mantine/core";
import { Fragment, ReactNode, useCallback, useMemo } from "react";
import protectedLoader from "assets/images/protected-loader.svg";
import doneLoader from "assets/images/protected-loader-done.svg";
import { SharkButton } from "components/SharkButton";
import { useNavigate } from "react-router-dom";
import { useQueryClient } from "react-query";
import { useProtectedDraftStore } from "../hooks/useProtectedDraftStore";
import { useDraftOverlay } from "features/DraftDetails/hooks/useDraftOverlay";
import { OverlaySteps } from "features/DraftDetails/store";
import ActionDone from "../../Icons/ActionDone";
import { Icon } from "@iconify/react";

// Props
type ComponentProps = {
    variant?: "success" | "default" | "licenseError" | string;
    text?: string | ReactNode;
};

// Component
export function Overlay({ variant, text }: ComponentProps) {
    const {
        state: { draftId },
    } = useProtectedDraftStore();
    const overlay = useDraftOverlay();
    const queryClient = useQueryClient();
    const navigate = useNavigate();

    const borderColor = useMemo(() => {
        if (variant === "success") return "#29CCB1";
        if (variant === "licenseError") return "#000000";
        return "#4700E5";
    }, [variant]);

    const backgroundColor = useMemo(() => {
        if (variant === "success") return "#EAFAF6";
        if (variant === "licenseError") return "#e7f797";
        return "#EDEEFF";
    }, [variant]);

    const loader = useMemo(() => {
        if (variant === "success") return <img height={100} width={100} src={doneLoader} alt="" />;
        if (variant === "licenseError") {
            return <Icon icon={"mdi:warning-outline"} width={75} height={75} />;
        }

        return <img height={100} width={100} src={protectedLoader} alt="" />;
    }, [variant]);

    const LoaderIcon = useMemo(() => {
        if (variant === "licenseError") {
            return loader;
        }
        if (overlay.state.step === OverlaySteps.MINTED) {
            return (
                <Success>
                    <ActionDone height={64} />
                </Success>
            );
        }
        return loader;
    }, [overlay.state.step, variant, loader]);

    const onSubmit = useCallback(async () => {
        overlay.actions.clear();
        overlay.actions.close();

        await Promise.all([
            queryClient.invalidateQueries([`SIDEBAR_COUNTS`]),
            queryClient.invalidateQueries(["PROTECTED_LIST"]),
            queryClient.invalidateQueries(["PROTECTED_ITEM"]),
            queryClient.invalidateQueries(["DRAFT_LIST"]),
            queryClient.invalidateQueries(["DRAFT"]),
        ]);

        navigate(`/protected/${draftId}`, { replace: true });
    }, [draftId, navigate, overlay.actions, queryClient]);

    return (
        <Fragment>
            <MantineOverlay
                opacity={0.9}
                zIndex={5}
                sx={{
                    borderWidth: 1,
                    borderStyle: "solid",
                    borderColor,
                    backgroundColor,
                    borderRadius: 8,
                }}
            />
            <Center sx={{ position: "absolute", inset: 0, zIndex: 10 }}>
                <Stack spacing={16} align="center" sx={{ maxWidth: "75%" }}>
                    {LoaderIcon}
                    <Text size="md" weight={"500"} color={"#0A001E"} align={"center"} sx={{whiteSpace: 'pre-line'}}>
                        {text}
                    </Text>
                    {overlay.state.step === OverlaySteps.MINTED && (
                        <SharkButton onClick={onSubmit} buttonType={"secondary"} variant={"outlined"} type="button">
                            Ok
                        </SharkButton>
                    )}
                </Stack>
            </Center>
        </Fragment>
    );
}

// Styling
const Success = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 80px;
    height: 80px;
    border-radius: 50%;
    border: 4px solid #29ccb1;
`;
