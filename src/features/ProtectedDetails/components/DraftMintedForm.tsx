import { Group, Image, Stack, Text } from "@mantine/core";

interface FormProps {
    title: string;
    text?: string;
    image?: any;
    minted?: boolean;
}

export const DraftMintedForm = ({ title, text, image }: FormProps) => {
    return (
        <Stack style={{ maxHeight: "670px", overflow: "scroll" }} pt={16} px={16} pb={16} spacing={0} sx={{ flex: 1 }}>
            <Stack px={16} py={10}>
                <Text py={8} color={"#0A001E"} weight={"700"} sx={{ fontSize: 24, lineHeight: "30px", overflowWrap: "anywhere" }}>
                    {String(title)}
                </Text>
            </Stack>

            <Stack px={16} py={8}>
                {!image && <div style={{ wordBreak: "break-all" }} dangerouslySetInnerHTML={{ __html: text! }} />}

                {!!image && (
                    <Group>
                        <Image width={480} height={360} src={URL.createObjectURL(image)} withPlaceholder radius={"sm"} />
                    </Group>
                )}
            </Stack>
        </Stack>
    );
};
