import { Group, Stack } from "@mantine/core";
import { Fragment } from "react";
import { CustomSkeleton } from "../../DraftDetails/components/Skeleton";

export const ProtectedSkeletonForm = () => {
    return (
        <Fragment>
            <Stack style={{ maxHeight: "460px", minHeight: "140px", overflow: "scroll", justifyContent: "space-between" }} pt={16} spacing={0} sx={{ flex: 1 }}>
                <Stack px={16} style={{ gap: 0 }}>
                    <CustomSkeleton height={40} width={300} radius={4} />

                    <CustomSkeleton height={14} width={1000} radius={4} marginTop={12} marginBottom={2} />
                    <CustomSkeleton height={14} width={600} radius={4} marginTop={4} marginBottom={4} />
                    <CustomSkeleton height={14} width={600} radius={4} marginTop={4} marginBottom={24} />
                </Stack>
                <Group px={16} style={{ justifyContent: "space-between", borderTop: "1px solid rgb(221, 222, 238)" }}>
                    <CustomSkeleton height={20} width={220} marginTop={10} marginBottom={10} radius={4} fill />
                    <Group style={{ alignItems: "center", gap: 8 }}>
                        <CustomSkeleton height={20} width={20} radius={20} fill />
                        <CustomSkeleton height={20} width={20} marginTop={10} marginBottom={10} radius={4} fill />
                        <CustomSkeleton height={30} width={140} marginTop={10} marginBottom={10} radius={4} fill />
                    </Group>
                </Group>
            </Stack>
        </Fragment>
    );
};
