import styled from "@emotion/styled";
import { Group, Image, Stack, Text, Textarea } from "@mantine/core";
import Icon from "components/Icon";
import { Fragment, useCallback, useRef } from "react";
import { Dropzone } from "@mantine/dropzone";
import { formatBytes } from "helpers/formatBytes";
import { Controller, useFieldArray, UseFormReturn, useWatch } from "react-hook-form";
import { DraftUpdateFormState } from "../hooks/useDraftUpdateForm";
import { SharkButton } from "components/SharkButton";
import { Collapse } from "./Collapse";
import { ListInput } from "./ListInput";
import { useTranslation } from "react-i18next";

interface FormProps {
    form: UseFormReturn<DraftUpdateFormState>;
}

export const DraftUpdateForm = ({ form }: FormProps) => {
    const fileRef = useRef<any>(null);
    const imageRef = useRef<any>(null);
    const { control, setValue } = form as any;
    const { t } = useTranslation();

    const file = useWatch({ control, name: "file" });
    const image = useWatch({ control, name: "image" });
    const text = useWatch({ control, name: "text" });
    const type = useWatch({ control, name: "type" });
    const fullTitle = useWatch({ control, name: "fullTitle" });

    const { fields: authors, append: appendAuthors, remove: removeAuthors } = useFieldArray({ control, name: "authors" });
    const { fields: coAuthors, append: appendCoAuthors, remove: removeCoAuthors } = useFieldArray({ control, name: "coAuthors" });
    const { fields: participants, append: appendParticipants, remove: removeParticipants } = useFieldArray({ control, name: "participants" });

    const selectFile = useCallback(() => {
        if (!fileRef.current) return;
        fileRef.current();
    }, []);

    const selectImage = useCallback(() => {
        if (!imageRef.current) return;
        imageRef.current();
    }, []);

    // Form Handlers
    const handleText = useCallback(
        (text: string) => {
            if (file) setValue("file", undefined);
            if (image) setValue("image", undefined);
            if (type !== "text") setValue("type", "text");
            setValue("text", text);
        },
        [file, image, setValue, type]
    );

    const handleFile = useCallback(
        (file: File) => {
            if (!!text.length) setValue("text", "");
            if (image) setValue("image", undefined);
            if (type !== "text") setValue("type", "text");
            setValue("file", file);
        },
        [image, setValue, text.length, type]
    );

    const handleImage = useCallback(
        (image: File) => {
            if (!!text.length) setValue("text", "");
            if (file) setValue("file", undefined);
            if (type !== "image") setValue("type", "image");
            setValue("image", image);
        },
        [file, setValue, text.length, type]
    );

    const clearFile = useCallback(() => {
        if (type !== "text") setValue("type", "text");
        setValue("file", undefined);
    }, [setValue, type]);

    const clearImage = useCallback(() => {
        if (type !== "text") setValue("type", "text");
        setValue("image", undefined);
    }, [setValue, type]);

    return (
        <Fragment>
            <Stack style={{maxHeight: '460px', minHeight: '140px', overflow: 'scroll'}}  pt={16} px={16} spacing={0} sx={{ flex: 1 }}>
                <Stack>
                    <Controller
                        name="title"
                        control={control}
                        render={({ field }) => (
                            <Input
                                {...field}
                                id="title"
                                placeholder={t("drafts.title_placeholder")}
                                variant={"unstyled"}
                                autosize
                                px={16}
                                py={8}
                                value={String(field.value)}
                                onChange={(e) => field.onChange(String(e.target.value))}
                            />
                        )}
                    />
                </Stack>

                <Stack sx={{ flex: 1, position: "relative" }}>
                    {!(file || image) && (
                        <Controller
                            name="text"
                            control={control}
                            render={({ field }) => (
                                <TextArea
                                    py={8}
                                    px={16}
                                    id="text"
                                    {...field}
                                    value={String(field.value)}
                                    onChange={(e) => handleText(String(e.target.value))}
                                    placeholder={t("drafts.text_palaceholder")}
                                    autosize
                                    variant={"unstyled"}
                                    styles={{
                                        root: { flex: 1 },
                                        wrapper: { height: "100%" },
                                        input: { minHeight: "100%!important" },
                                    }}
                                />
                            )}
                        />
                    )}

                    {!!image && (
                        <Stack px={16} py={8} spacing={6}>
                            <Group>
                                <Image width={480} height={360} src={URL.createObjectURL(image)} withPlaceholder radius={"sm"} />
                            </Group>
                            <Group>
                                <SharkButton buttonType="secondary" variant="outlined" size="small" onClick={clearImage}>
                                    {t("buttons.remove_btn")}
                                </SharkButton>
                            </Group>
                        </Stack>
                    )}

                    <Controller
                        name="file"
                        control={control}
                        render={({ field }) =>
                            field.value ? (
                                <Stack px={16} py={8} spacing={6}>
                                    <Group>
                                        <Text color={"#666892"} size={"md"}>
                                            {t("drafts.file-select-text.text_1")}
                                        </Text>
                                        <Text color={"#5A3DD1"} size={"md"} weight={"500"}>
                                            {field.value.name}
                                        </Text>
                                        <Text color={"#666892"} size={"md"}>
                                            ({formatBytes(field.value.size)})
                                        </Text>
                                    </Group>
                                    <Group>
                                        <SharkButton buttonType="secondary" variant="outlined" size="small" onClick={clearFile}>
                                            {t("buttons.remove_btn")}
                                        </SharkButton>
                                    </Group>
                                </Stack>
                            ) : (
                                <div />
                            )
                        }
                    />

                    {!(text || file || image) && (
                        <Group px={16} py={0} spacing={6} sx={{ position: "absolute", left: 0, top: 30 }}>
                            <Text color={"#666892"} size={"md"}>
                                {t("drafts.file-upload-text.text_1")}
                            </Text>
                            <Text onClick={selectFile} color={"#5A3DD1"} size={"md"} underline={true} weight={"500"} style={{ cursor: "pointer" }}>
                                {t("drafts.file-upload-text.text_2")}
                            </Text>
                            <Text color={"#666892"} size={"md"}>
                                {t("drafts.file-upload-text.text_3")}
                            </Text>
                        </Group>
                    )}
                </Stack>

                <Dropzone
                    style={{ display: "none" }}
                    openRef={fileRef}
                    accept={["text/plain"]}
                    onDrop={(files) => handleFile(files[0])}
                    onReject={(files) => console.log("rejected file", files[0])}
                    maxSize={104857600} // 100mb limit
                >
                    {() => null}
                </Dropzone>

                <Dropzone
                    style={{ display: "none" }}
                    openRef={imageRef}
                    accept={["image/jpeg", "image/png"]}
                    onDrop={(files) => handleImage(files[0])}
                    onReject={(files) => console.log("rejected image", files[0])}
                    maxSize={104857600} // 100mb limit
                >
                    {() => null}
                </Dropzone>
            </Stack>

            <Stack spacing={0}>
                <Collapse label="Title" opened={!!fullTitle.length}>
                    <ListInput control={control} name={`fullTitle`} placeholder={t("textfields.metadataTitle")} flex={1} />
                </Collapse>

                <Collapse label={t("textfields.authors")} opened={!!authors.length}>
                    {authors.map((item, index) => (
                        <ListInput
                            key={item.id}
                            control={control}
                            name={`authors.${index}`}
                            placeholder={`${t("common.author")} #${index + 1}`}
                            onDelete={() => removeAuthors(index)}
                        />
                    ))}
                    <Group>
                        <SharkButton onClick={() => appendAuthors("")} buttonType="secondary" size="small" variant="outlined">
                            ADD
                        </SharkButton>
                    </Group>
                </Collapse>

                <Collapse label={t("textfields.coAuthors")} opened={!!coAuthors.length}>
                    {coAuthors.map((item, index) => (
                        <ListInput
                            key={item.id}
                            control={control}
                            name={`coAuthors.${index}`}
                            placeholder={`${t("textfields.coAuthor")} #${index + 1}`}
                            onDelete={() => removeCoAuthors(index)}
                        />
                    ))}
                    <Group>
                        <SharkButton onClick={() => appendCoAuthors("")} buttonType="secondary" size="small" variant="outlined">
                            {t("buttons.add_btn")}
                        </SharkButton>
                    </Group>
                </Collapse>

                <Collapse label="Participants" opened={!!participants.length}>
                    {participants.map((item, index) => (
                        <ListInput
                            key={item.id}
                            control={control}
                            name={`participants.${index}`}
                            placeholder={`Participant #${index + 1}`}
                            onDelete={() => removeParticipants(index)}
                        />
                    ))}
                    <Group>
                        <SharkButton onClick={() => appendParticipants("")} buttonType="secondary" size="small" variant="outlined">
                            {t("buttons.add_btn")}
                        </SharkButton>
                    </Group>
                </Collapse>

                {type === "image" && (
                    <Group px={16} style={{ borderTop: "1px solid #DDDEEE", height: 56 }} spacing={0}>
                        <Group spacing={16}>
                            <Icon icon={"ic:round-insert-photo"} size={20} color={"#666892"} onClick={selectImage} />
                        </Group>
                    </Group>
                )}
            </Stack>
        </Fragment>
    );
};

const Input = styled(Textarea)`
    #title {
        font-weight: 700;
        font-size: 24px;
        line-height: 30px;
        color: #0a001e;
    }

    #title::placeholder {
        font-weight: 700;
        font-size: 24px;
        line-height: 30px;
        color: #666892;
    }
`;

const TextArea = styled(Textarea)`
    #text {
        font-weight: 400;
        font-size: 16px;
        line-height: 20px;
        color: #0a001e;
        padding: 0;
    }

    #text::placeholder {
        font-weight: 400;
        font-size: 16px;
        line-height: 20px;
        color: #666892;
    }
`;
