import styled from "@emotion/styled";
import { Stack } from "@mantine/core";
import { Alert } from "features/DraftDetails/components/Alert";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";
import { useDraftOverlay } from "features/DraftDetails/hooks/useDraftOverlay";
import { Fragment } from "react";
import { useOutletContext } from "react-router-dom";
import { DraftUpdateForm } from "../components/DraftUpdateForm";
import { Overlay } from "../components/Overlay";
import { NotMintedOutletProps } from "../containers/NotMintedContainer";

export function UpdateContent() {
    const { form } = useOutletContext<NotMintedOutletProps>();
    const alert = useDraftAlert();
    const overlay = useDraftOverlay();

    return (
        <Fragment>
            {alert.state.show && <Alert title={alert.state.title} text={alert.state.message} variant={alert.state.type} />}
            <FormRoot sx={{ minHeight: "380px" }}>
                {overlay.state.show && <Overlay text={overlay.state.message} variant={overlay.state.type} />}

                <DraftUpdateForm form={form} />
            </FormRoot>
        </Fragment>
    );
}

const FormRoot = styled(Stack)`
    position: relative;
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    overflow: hidden;
    justify-content: space-between;
`;
