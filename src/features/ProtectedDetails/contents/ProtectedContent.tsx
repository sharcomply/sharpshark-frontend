import styled from "@emotion/styled";
import { Group, Stack, Text, Tooltip } from "@mantine/core";
import Icon from "components/Icon";
import { SharkButton } from "components/SharkButton";
// import { Alert } from "features/DraftDetails/components/Alert";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";
import { useDraftOverlay } from "features/DraftDetails/hooks/useDraftOverlay";
import { useDraftVersionStore } from "features/NewDraftVersion/hooks/useDraftVersionStore";
import { DraftMintedForm } from "features/ProtectedDetails/components/DraftMintedForm";
import { humanizeDate } from "helpers/humanizeDate";
import { useUnmount } from "hooks/useUnmount";
import { ProtectedOutletProps } from "pages/protected/[protectedId]";
import { NotMintedOutletProps } from "../containers/NotMintedContainer";
import { Fragment, useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { Link, useOutletContext } from "react-router-dom";
import { Overlay } from "../components/Overlay";
import { VersionForm } from "../../NewDraftVersion/components/VersionForm";

export const ProtectedContent = () => {
    const overlay = useDraftOverlay();
    const alert = useDraftAlert();
    const { draft } = useOutletContext<ProtectedOutletProps>();
    const { form, isCollapseOpened } = useOutletContext<NotMintedOutletProps>();
    const { setParent, setInitialForm } = useDraftVersionStore();
    const { t } = useTranslation();

    const lastParent = useMemo(() => {
        if (!draft.versionsHistory) return draft;

        const versionSize = Object.keys(draft.versionsHistory).length;
        const lastParentId = draft.versionsHistory[versionSize];

        return { ...draft, _id: lastParentId };
    }, [draft]);

    useUnmount(() => {
        overlay.actions.clear();
        overlay.actions.close();
        alert.actions.clear();
        alert.actions.close();
    });

    const handleUpdate = useCallback(() => {
        setParent(lastParent);
        setInitialForm({
            title: draft.title,
            fullTitle: draft.metadata?.Title || "",
            type: draft.type || "text",
            text: draft.text || "",
            image: draft.imageFile,
            authors: draft.authors,
            coAuthors: draft.coAuthors,
            participants: draft.participants,
            additionalImages: draft.additionalImages,
        });
    }, [draft, lastParent, setInitialForm, setParent]);

    // RENDER
    return (
        <Fragment>
            {/*{alert.state.show && <Alert title={alert.state.title} text={alert.state.message} variant={alert.state.type} />}*/}

            <FormRoot sx={{ minHeight: "380px" }} spacing={0}>
                {overlay.state.show && <Overlay text={overlay.state.message} variant={overlay.state.type} />}

                {draft.isMinted ? (
                    <>
                        <DraftMintedForm title={draft.title} text={draft.text} image={draft.imageFile} minted={draft.isMinted} />
                        <Footer px={16}>
                            <Group>
                                <Text size={"sm"} weight={"400"} color={"#666892"}>
                                    {t("protected_drafts.protected_on", { date: humanizeDate(draft.updatedAt, "at").fullDate })}
                                </Text>
                            </Group>
                            <Group spacing={12}>
                                <Group spacing={8}>
                                    <Tooltip
                                        label={t("textfields.lables.current_work_version")}
                                        position="bottom"
                                        placement="center"
                                        transition="fade"
                                        transitionDuration={300}
                                        transitionTimingFunction="ease"
                                        withArrow={false}
                                        styles={{ body: { backgroundColor: "#666892" } }}
                                    >
                                        <Icon icon={"ep:question-filled"} color={"#666892"} />
                                    </Tooltip>
                                    <Text size={"sm"} weight={"400"} color={"#666892"}>
                                        v.{draft.version}
                                    </Text>
                                </Group>
                                <SharkButton
                                    as={Link}
                                    to={`/protected/new`}
                                    onClick={handleUpdate}
                                    variant="outlined"
                                    buttonType="secondary"
                                    size="small"
                                    px={8}
                                    py={4}
                                >
                                    {t("buttons.update_this_version")}
                                </SharkButton>
                            </Group>
                        </Footer>
                    </>
                ) : (
                    <VersionForm isCollapseClosed={isCollapseOpened} form={form} />
                )}
            </FormRoot>
        </Fragment>
    );
};

// Styling
const FormRoot = styled(Stack)`
    position: relative;
    background-color: #fbfcff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    overflow: hidden;
    justify-content: space-between;
`;

const Footer = styled(Group)`
    position: relative;
    background-color: #ffffff;
    border-top: 1px solid #dddeee;
    height: 56px;
    overflow: hidden;
    justify-content: space-between;
`;
