import { ProtectedOutletProps } from "pages/protected/[protectedId]";
import { useOutletContext } from "react-router-dom";
import { MintedContainer } from "./containers/MintedContainer";
import { NotMintedContainer } from "./containers/NotMintedContainer";

export const ProtectedDetails = () => {
    const { draft } = useOutletContext<ProtectedOutletProps>();

    if (!draft.isMinted) return <NotMintedContainer draft={draft} />;
    return <MintedContainer draft={draft} />;
};
