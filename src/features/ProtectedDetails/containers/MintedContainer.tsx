import styled from "@emotion/styled";
import { Group, Stack, Text } from "@mantine/core";
import { SharkButton } from "components/SharkButton";
import { TabLink } from "components/TabLink";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";
import { useDraftStatus } from "features/DraftDetails/hooks/useDraftStatus";
import { useMonitoringListener } from "features/DraftDetails/hooks/useMonitoringListener";
import LogoOutline from "features/Icons/LogoOutline";
import { useModal } from "features/Modals/hooks/useModal";
import { ModalContent, ModalType } from "features/Modals/interfaces";
import { useDetailedDraft } from "features/ProtectedRights/hooks/useDetailedDraft";
import { useMount } from "hooks/useMount";
import { useUnmount } from "hooks/useUnmount";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Link, Outlet, useNavigate } from "react-router-dom";
import { PageTitle } from "../components/PageTitle";
import { useProtectedDraftStore } from "../hooks/useProtectedDraftStore";
import { Alert } from "../../DeletedDraftDetails/components/Alert";
import useDraftStore from "../../DraftDetails/hooks/useDraftStore";
import { useContractWithMintedNetwork } from "blockchain/hooks/useContractWithMintedNetwork";

interface ComponentProps {
    draft: DraftDetails;
}

export const MintedContainer = ({ draft }: ComponentProps) => {
    const {
        actions: { setMode },
    } = useProtectedDraftStore();
    const alert = useDraftAlert();
    const status = useDraftStatus();
    const { handleOpen, handleClose } = useModal();
    const navigate = useNavigate();
    const { t } = useTranslation();
    const draftStore = useDraftStore();

    useMonitoringListener();

    useDetailedDraft();
    
    useEffect(() => {
        return () => {
            handleClose();
        }
    },[])
    
    useMount(() => setMode("view"));
    useEffect(() => {
        if (draft && !draft.isMinted) {
            return navigate(`/protected/${draft._id}/update`);
        }
    }, [draft, navigate]);

    useEffect(() => {
        draftStore.setMintedNetworks(draft.mintedNetworks[0]?.name);
    }, [draftStore.setMintedNetworks, draft]);

    useUnmount(() => {
        alert.actions.clear();
        alert.actions.close();
        status.actions.update({ type: undefined });
    });
    // RENDER
    const actions = () => (
        <Group noWrap={true}>
            <SharkButton
                buttonType={"secondary"}
                variant={"outlined"}
                onClick={() => handleOpen({ type: ModalType.PROTECTED, content: ModalContent.StartMonitoring })}
                disabled={
                    draftStore.protectLoading.fiat || draftStore.protectLoading.metaMask || draftStore.protectLoading.near || draft.activeMonitor?.isActive
                }
            >
                {draft.activeMonitor?.isActive ? t("buttons.tracking_btn") : t("buttons.track_now_btn")}
            </SharkButton>

            <Group spacing={0}>
                <Link to={`/certificate/${draft.tokenId}`}>
                    <SharkButton variant={"filled"} buttonType={"primary"} width={150} style={{ borderRadius: "4px 0 0 4px", padding: "0 10px 0 16px" }}>
                        {t("buttons.view_certificate_btn")}
                    </SharkButton>
                </Link>

                <SharkButton
                    onClick={() => handleOpen({ type: ModalType.PROTECTED, content: ModalContent.UsingYourContent })}
                    variant={"filled"}
                    buttonType={"primary"}
                    style={{ borderRadius: "0 4px 4px 0", borderLeft: "1px solid #1C00A6", padding: "0 16px 0 10px" }}
                >
                    <Group sx={{ height: "100%" }}>
                        <LogoOutline color={"#FFFFFF"} height={20} width={20} />
                    </Group>
                </SharkButton>
            </Group>
        </Group>
    );

    const contract = useContractWithMintedNetwork();

    return (
        <RootForm>
            <PageTitle
                title={draft.metadata.Title}
                actions={actions()}
                version={draft.version}
                versionHistory={draft.versionsHistory}
                isMinted={draft.isMinted}
                parent={draft}
            />
            {draftStore.alert.show && <Alert title={draftStore.alert.title} text={draftStore.alert.message} variant={draftStore.alert.type} />}

            <Group position={"apart"} spacing={16}>
                <Group spacing={12}>
                    <TabLink variant="outlined" to={`/protected/${draft._id}/content`} label={t("drafts.tabs.content")} />
                    <TabLink variant="outlined" to={`/protected/${draft._id}/about`} label={t("drafts.tabs.about_usage_rights")} />
                    <TabLink variant="outlined" to={`/protected/${draft._id}/alerts`} label={t("drafts.tabs.copyright_alerts")} />
                    <TabLink variant="outlined" to={`/protected/${draft._id}/disputes`} label={t("drafts.tabs.disputes")} />
                </Group>
                <Stack spacing={0}>
                    <Text size={"sm"}>Token ID: {draft.tokenId}</Text>
                    <Text size={"sm"}>Contract address: {contract.address}</Text>
                </Stack>
            </Group>

            <Outlet context={{ draft }} />
        </RootForm>
    );
};

const RootForm = styled(Stack)`
    img {
        max-width: 480px;
        height: auto !important;
    }
`;
