import styled from "@emotion/styled";
import { Group, Stack } from "@mantine/core";
import { useContract } from "blockchain/hooks/useContract";
import { SharkButton } from "components/SharkButton";
import { TabLink } from "components/TabLink";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";
import { useDraftListener } from "features/DraftDetails/hooks/useDraftListener";
import { useDraftMessages } from "features/DraftDetails/hooks/useDraftMessages";
import useDraftOnBeforeUnload from "features/DraftDetails/hooks/useDraftOnBeforeUnload";
import { useDraftOverlay } from "features/DraftDetails/hooks/useDraftOverlay";
import { useDraftStatus } from "features/DraftDetails/hooks/useDraftStatus";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import { useModal } from "features/Modals/hooks/useModal";
import { ModalContent, ModalType } from "features/Modals/interfaces";
import { useDraftVersionStore } from "features/NewDraftVersion/hooks/useDraftVersionStore";
import { useMount } from "hooks/useMount";
import { useUnmount } from "hooks/useUnmount";
import { useCallback, useEffect, useState } from "react";
import { UseFormReturn } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { Outlet } from "react-router-dom";
import { PageTitle } from "../components/PageTitle";
import { DraftUpdateFormState } from "../hooks/useDraftUpdateForm";
import { useProtectedDraftStore } from "../hooks/useProtectedDraftStore";
import { Alert } from "../../DraftDetails/components/Alert";
import { utils } from "near-api-external-link-fork";
import { useNearContext } from "../../../providers/NearProvider";
import { useDraftUpdate } from "features/DraftDetails/hooks/useDraftUpdate";
import { useDraftEditForm } from "features/DraftDetails/hooks/useDraftEditForm";

export interface NotMintedOutletProps {
    draft: DraftDetails;
    form: UseFormReturn<DraftUpdateFormState, any>;
    isCollapseOpened: boolean | undefined;
    handleCloseCollapses: () => void;
}

interface ComponentProps {
    draft: DraftDetails;
}

export function NotMintedContainer({ draft }: ComponentProps) {
    const overlay = useDraftOverlay();
    const contract = useContract();
    const alert = useDraftAlert();
    const status = useDraftStatus();
    const { t } = useTranslation();
    const { handleOpen } = useModal();
    const { setNewForm } = useDraftVersionStore();
    const { protectLoading, setProtectLoading, setDraftStatus, canMint, mintPayload, setDraftStatusBeforeProtect } = useDraftStore();
    const {
        actions: { open: openMessage, close: closeMessage },
    } = useDraftMessages();
    const draftStore = useDraftStore();
    const Near = useNearContext();
    const [isCollapseOpened, setIsCollapseOpened] = useState<boolean | undefined>();
    const { handleUpdate, isLoading } = useDraftUpdate();

    const handleCloseCollapses = useCallback(() => {
        setIsCollapseOpened(true);
        const timout = setTimeout(() => {
            setIsCollapseOpened(undefined);
            clearInterval(timout);
        }, 1);
    }, [setIsCollapseOpened]);

    // const {
    //     actions: { open: openMessage, close: closeMessage },
    // } = useDraftMessages();

    const {
        state: { mode },
        actions: { setMode, setDraftId },
    } = useProtectedDraftStore();

    useMount(() => {
        if (mode === "view") setMode("edit");
        setDraftId(draft._id);
    });

    useUnmount(() => {
        setDraftId(undefined);
        alert.actions.clear();
        alert.actions.close();
        status.actions.update({ type: undefined });
        closeMessage();
        setProtectLoading({ mintedSucces: false });
    });
    useDraftListener();
    useDraftOnBeforeUnload();

    const { form } = useDraftEditForm({
        title: draft.title,
        type: draft.type || "text",
        text: draft.text || "",
        file: undefined,
        image: draft.imageFile,
        authors: draft.authors,
        coAuthors: draft.coAuthors,
        fullTitle: draft.metadata.Title,
        participants: draft.participants,
        additionalImages: draft.additionalImages,
    });

    const onOpenModal = useCallback(() => {
        setNewForm(form.getValues());
        setDraftStatusBeforeProtect("upgrade");
        handleOpen({ type: ModalType.PROTECTED, content: ModalContent.LicenseAndMode });
    }, [handleOpen, form, setNewForm]);

    useEffect(() => {
        if (canMint && !overlay.state.show) {
            openMessage();
        } else {
            closeMessage();
        }
    }, [canMint, openMessage, overlay.state.show, t]);

    const onMint = useCallback(async () => {
        if (!mintPayload || !canMint) return;
        setDraftStatus({ type: undefined, message: undefined });
        alert.actions.close();
        // openMessage();
        if (draftStore.paymentMethod === "Near") {
            alert.actions.open();
            alert.actions.warn(t("warn.titles.do_not_exit_protection_near"), t("warn.messages.do_not_exit_protection_near"));
            overlay.actions.open();
            overlay.actions.setWorkReady();
            const amountInYocto = utils.format.parseNearAmount("0.05");

            const res = await Near?.Contract.nft_mint({
                amount: amountInYocto,
                args: {
                    token_id: mintPayload.tokenId,
                    metadata: mintPayload.metadata,
                    receiver_id: mintPayload.receiver_id,
                    content_hash: mintPayload.contentHash,
                    metadata_hash: mintPayload.metadataHash,
                    expire: mintPayload.expire,
                    signature: mintPayload.signature,
                    v: mintPayload.v,
                    parent_token_id: mintPayload.parentTokenId,
                    version: mintPayload.version,
                },
                callbackUrl: `${window.location.origin}/protected/${mintPayload.tokenId}`,
            });

            console.log("res", res);
            overlay.actions.setWaitingMint();
        } else {
            alert.actions.open();
            alert.actions.warn(t("warn.titles.do_not_exit_protection"), t("warn.messages.do_not_exit_protection"));
            try {
                // Сменяем статус драфта что проверка на антиплагиант прошла успешно
                overlay.actions.open();
                overlay.actions.setWorkReady();

                // Запускаем минт
                await contract.mint(
                    mintPayload.tokenId,
                    mintPayload.to,
                    mintPayload.contentHash,
                    mintPayload.metadataHash,
                    mintPayload.expire,
                    mintPayload.signature,
                    mintPayload.parentTokenId,
                    mintPayload.version
                );

                overlay.actions.setWaitingMint();
            } catch (e: any) {
                // В зависимости от того какая у нас ошибка - отображаем нужный нам статус
                alert.actions.open();
                // closeMessage();
                alert.actions.close();
                draftStore.showDraftAlert(true);

                if (e.code === 4001 || e.code === "ACTION_REJECTED") {
                    alert.actions.error(t("errors.titles.mint_canceled"), t("errors.messages.mint_canceled"));
                    draftStore.setDraftAlert({
                        type: "error",
                        title: t("errors.titles.mint_canceled"),
                        message: t("errors.messages.mint_canceled"),
                    });
                } else if (e.code === 32603) {
                    alert.actions.error(t("errors.titles.smth_wrong"), t("errors.messages.smth_wrong"));
                    draftStore.setDraftAlert({
                        type: "error",
                        title: t("errors.titles.smth_wrong"),
                        message: t("errors.messages.smth_wrong"),
                    });
                } else if (e.code === "UNPREDICTABLE_GAS_LIMIT") {
                    alert.actions.error(t("errors.titles.execution"), t("errors.messages.execution"));
                    draftStore.setDraftAlert({
                        type: "error",
                        title: t("errors.titles.execution"),
                        message: t("errors.messages.execution"),
                    });
                }

                setDraftStatus({ type: "error", message: t("statuses.protect_failed") });
                overlay.actions.close();
                overlay.actions.clear();
            }
        }
    }, [mintPayload, canMint, setDraftStatus, alert.actions, t, overlay.actions, contract]);

    // RENDER
    const defaultActions = () => (
        <>
            <SharkButton
                type="button"
                buttonType={"secondary"}
                variant={"outlined"}
                loading={isLoading}
                disabled={isLoading || status.state.message === t("statuses.protecting") || overlay.state.show}
                onClick={form.handleSubmit(handleUpdate)}
            >
                × {t("buttons.save_close_btn")}
            </SharkButton>
            {canMint && (
                <Group onClick={handleCloseCollapses}>
                    <SharkButton
                        type="button"
                        onClick={onMint}
                        buttonType={"secondary"}
                        variant={"filled"}
                        disabled={status.state.message === t("statuses.protecting") || overlay.state.show}
                    >
                        {t("buttons.mint_btn")}
                    </SharkButton>
                </Group>
            )}
            {!canMint && (
                <Group onClick={handleCloseCollapses} noWrap={true}>
                    <SharkButton
                        disabled={protectLoading.fiat || protectLoading.metaMask || protectLoading.near || protectLoading.mintedSucces}
                        onClick={onOpenModal}
                        variant={"filled"}
                        buttonType={"primary"}
                    >
                        {t("buttons.upgrade_btn")}
                    </SharkButton>
                </Group>
            )}
        </>
    );

    return (
        <form>
            <RootForm>
                <PageTitle
                    title={draft.metadata.Title}
                    versionHistory={draft.versionsHistory}
                    version={mode === "new" ? draft.version + 1 : draft.version}
                    isMinted={false}
                    actions={defaultActions()}
                    parent={draft}
                />
                {alert.state.show && <Alert title={alert.state.title} text={alert.state.message} variant={alert.state.type} />}

                <Group position={"apart"} spacing={16}>
                    <Group spacing={12}>
                        <TabLink variant="outlined" to={`/protected/${draft._id}/content`} label={t("drafts.tabs.content")} />
                        <TabLink variant="outlined" to={`/protected/${draft._id}/about`} label={t("drafts.tabs.about_usage_rights")} disabled={true} />
                        <TabLink variant="outlined" to={`/protected/${draft._id}/alerts`} label={t("drafts.tabs.copyright_alerts")} disabled={true} />
                        <TabLink
                            variant="outlined"
                            to={`/protected/${draft._id}/disputes`}
                            disabled={true}
                            label={`${t("drafts.tabs.disputes")} ${t("common.coming_soon")}`}
                        />
                    </Group>
                </Group>

                <Outlet context={{ draft, form, isCollapseOpened, handleCloseCollapses }} />
            </RootForm>
        </form>
    );
}

const RootForm = styled(Stack)`
    img {
        max-width: 480px;
        height: auto !important;
    }
`;
