import { backend } from "api";
import { useContract } from "blockchain/hooks/useContract";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";
import { useDraftOverlay } from "features/DraftDetails/hooks/useDraftOverlay";
import { useDraftStatus } from "features/DraftDetails/hooks/useDraftStatus";
import { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDraftCreate } from "./useDraftCreate";
import { DraftUpdateFormState } from "./useDraftUpdateForm";

export const useDraftUpgrade = () => {
    const {t} = useTranslation()
    const contract = useContract();
    const alert = useDraftAlert();
    const overlay = useDraftOverlay();
    const status = useDraftStatus();
    const [loading, setLoading] = useState(false);
    const draftHandler = useDraftCreate();

    const protect = useCallback(
        async (draft: Draft) => {
            alert.actions.clear();
            alert.actions.close();
            status.actions.update({
                type: "warn",
                message: t("statuses.protecting"),
            });

            // Open Overlay and Show Pernding Payment message
            overlay.actions.open();
            overlay.actions.setPendingPayment();

            try {
                const {success} = await backend.scans.startEstimateDocument({documentId: draft._id})
                if(!success) return
                const {toBlockchain} = await backend.scans.startScan({documentId: draft._id});

                const tx = await contract.payRequest(toBlockchain.requestId, toBlockchain.expire, toBlockchain.signature, {value: toBlockchain.priceInWei});
                console.log("[PAY_REQUEST]", tx);

                console.log("[PAY_REQUEST]", tx);

                const res = await tx.wait();
                console.log("[TAX_WAIT]", res);
            } catch (e: any) {
                // Close overlay and clean state
                overlay.actions.close();
                overlay.actions.clear();

                status.actions.update({
                    type: "error",
                    message: t("statuses.protect_failed"),
                });

                // code: 4001, message: 'MetaMask Tx Signature: User denied transaction signature.'
                if (e.code === 4001 || e.code === "ACTION_REJECTED") {
                    alert.actions.open();
                    alert.actions.error(t("errors.titles.protection_canceled"), t("errors.messages.protection_canceled"));
                }

                console.log("[PROTECT] ", e);
            }
        },
        [alert.actions, contract, overlay.actions, status.actions]
    );

    const upgrade = useCallback(
        async (form: DraftUpdateFormState) => {
            try {
                setLoading(true);
                const draft = await draftHandler.create(form);

                if (!draft) throw new Error("Draft Not Created");

                await protect(draft);
            } catch (e) {
                console.log("[DRAFT_CHILD_CREATE]", e);
                setLoading(false);
            }
        },
        [draftHandler, protect]
    );

    return {
        upgrade,
        loading: loading || draftHandler.loading,
    };
};
