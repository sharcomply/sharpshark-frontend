import { useCallback, useState } from "react";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";
import { backend } from "api";
import { useModal } from "../../Modals/hooks/useModal";
import { useQueryClient } from "react-query";
import { useDraftStatus } from "features/DraftDetails/hooks/useDraftStatus";
import { useProtectedDraftStore } from "./useProtectedDraftStore";
import { getMonitoringPaymentLink, startEstimateScan, startScan } from "api/requests/scans";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import { useTranslation } from "react-i18next";
import { getNetworkById } from "../../../constants/networks";
import { useNearContext } from "providers/NearProvider";
import { useContract } from "blockchain/hooks/useContract";

export const useStartTracking = () => {
    const { t } = useTranslation();
    const status = useDraftStatus();
    const protectedStore = useProtectedDraftStore();
    const contract = useContract();
    const [isLoadingMetaMask, setLoadingMetaMask] = useState(false);
    const [isLoadingFiat, setLoadingFiat] = useState(false);
    const [isNearLoading, setNearLoading] = useState(false);

    const alert = useDraftAlert();
    const { handleClose } = useModal();
    const queryClient = useQueryClient();
    const draftStore = useDraftStore();
    const Near = useNearContext();

    const { setProtectLoading } = useDraftStore();

    const startTrakingWithFiat = useCallback(
        async (scansTotal: number) => {
            if (!protectedStore.state.draftId) return;
            const networkName = getNetworkById(window.ethereum.networkVersion);
            draftStore.setPaymentMethod("Stripe");
            setProtectLoading({ fiat: true });
            setLoadingFiat(true);
            alert.actions.close();
            draftStore.setDraftStatus({
                type: "warn",
                message: t("statuses.tracking"),
            });

            try {
                const { requestId } = await backend.drafts.setupMonitoring({
                    draftId: protectedStore.state.draftId,
                    scansTotal,
                    network: networkName?.symbol,
                });
                draftStore.setScanPaymentPayload({ requestId });

                console.log("scanTotal", scansTotal);

                console.log("requestId", requestId);

                await getMonitoringPaymentLink({
                    requestId,
                    successUrl: `${window.location.href}?stripeSucces=true`,
                    cancelUrl: `${window.location.href}?stripeSucces=false`,
                }).then((data) => {
                    window.open(data.url);
                });

                handleClose();
            } catch (e: any) {
                const { requestId } = await startScan({ documentId: protectedStore.state.draftId, network: networkName?.symbol });
                await startEstimateScan({ requestId });
                const error = await e.response.json();
                if (error.statusCode === 500) {
                    alert.actions.open();
                    alert.actions.error(t("errors.titles.monitoring_failed"), t(`${error.data.message}. Increase the number of tracking days. `));
                }

                handleClose();
            } finally {
                setLoadingFiat(false);
                setProtectLoading({ fiat: false });
                draftStore.setDraftStatus({
                    message: "",
                });
            }
        },
        [protectedStore.state.draftId, draftStore, setProtectLoading, t, handleClose]
    );

    const startTrakingWithMetaMask = useCallback(
        async (scansTotal: number) => {
            if (!protectedStore.state.draftId) return;
            const networkName = getNetworkById(window.ethereum.networkVersion);
            draftStore.setPaymentMethod("Metamask");
            setProtectLoading({ metaMask: true });
            setLoadingMetaMask(true);
            alert.actions.close();
            draftStore.setDraftStatus({
                type: "warn",
                message: t("statuses.tracking"),
            });

            try {
                const { toBlockchain } = await backend.drafts.setupMonitoring({
                    draftId: protectedStore.state.draftId,
                    scansTotal,
                    network: networkName?.symbol,
                });

                const tx = await contract.payMonitoringRequest(toBlockchain.requestId, toBlockchain.scansTotal, toBlockchain.expire, toBlockchain.signature, {
                    value: toBlockchain.priceInWei,
                });

                console.log("[PAY_PAYLOAD]", tx);

                const res = await tx.wait();
                console.log("[TAX_RESULT]", res);

                await Promise.all([queryClient.invalidateQueries(["PROTECT_ITEM", protectedStore.state.draftId])]);

                handleClose();
            } catch (e: any) {
                // Close overlay and clean state
                status.actions.update({
                    type: "error",
                    message: t("statuses.failed"),
                });

                // code: 4001, message: 'MetaMask Tx Signature: User denied transaction signature.'
                if (e.code === 4001 || e.code === "ACTION_REJECTED") {
                    alert.actions.open();
                    alert.actions.error(t("errors.titles.monitoring_failed"), t("errors.messages.monitoring_failed"));
                }

                console.log("[MONITORING_ERROR] ", e);
            } finally {
                setLoadingMetaMask(false);
                setProtectLoading({ metaMask: false });
                draftStore.setDraftStatus({
                    message: "",
                });
            }
        },
        [protectedStore.state.draftId, draftStore, setProtectLoading, t, contract, queryClient, handleClose, status.actions, alert.actions]
    );

    const startTrakingWithNear = useCallback(
        async (scansTotal: number) => {
            if (!protectedStore.state.draftId) return;

            draftStore.setPaymentMethod("Near");
            setNearLoading(true);
            setProtectLoading({ near: true });

            try {
                const { toBlockchain, requestId } = await backend.drafts.setupMonitoring({
                    draftId: protectedStore.state.draftId,
                    scansTotal,
                    network: "near",
                });

                console.log(toBlockchain);
                localStorage.setItem(`${window.location.href}`, 'true')

                const res = await Near?.Contract.pay_monitoring_request({
                    amount: toBlockchain.priceInWei,
                    args: {
                        amount: toBlockchain.priceInWei,
                        request_id: requestId,
                        scans_total: toBlockchain.scans_total!,
                        expire: toBlockchain.expire,
                        signature: toBlockchain.signature,
                        v: toBlockchain.v,
                    },
                    callbackUrl: `${window.location.origin}?nearMonitoringSuccess=true`,
                });
                console.log(`res`, res);

                await Promise.all([queryClient.invalidateQueries(["PROTECT_ITEM", protectedStore.state.draftId])]);

                handleClose();
            } catch (e: any) {
                // Close overlay and clean state
                status.actions.update({
                    type: "error",
                    message: t("statuses.failed"),
                });

                // code: 4001, message: 'MetaMask Tx Signature: User denied transaction signature.'
                if (e.code === 4001) {
                    alert.actions.open();
                    alert.actions.error(t("errors.titles.monitoring_failed"), t("errors.messages.monitoring_failed"));
                } else if (e.type === "REQUEST_VALIDATION_ERROR") {
                    draftStore.showDraftAlert(true);
                    draftStore.setDraftAlert({
                        type: "error",
                        title: t("errors.titles.near_connect_account"),
                        message: t("errors.messages.near_connect_account"),
                    });
                }

                console.log("[MONITORING_ERROR] ", e);
            } finally {
                setProtectLoading({ near: false });
                setNearLoading(false);
                draftStore.setDraftStatus({
                    message: "",
                });
            }
        },
        [alert.actions, contract, handleClose, protectedStore.state.draftId, queryClient, status.actions, t, draftStore]
    );

    return {
        isLoadingFiat,
        isLoadingMetaMask,
        isNearLoading,
        startTrakingWithMetaMask,
        startTrakingWithFiat,
        startTrakingWithNear,
    };
};
