import { useAppDispatch, useAppSelector } from '../../../store';
import { useCallback } from 'react';
import { protectedDraftActions, ProtectedDraftState } from '../store';


export const useProtectedDraftStore = () => {
    const state = useAppSelector(state => state.protectedDraft)
    const dispatch = useAppDispatch()

    const setMode = useCallback((mode: ProtectedDraftState['mode']) => {
        const action = protectedDraftActions.setMode(mode)
        dispatch(action)
    }, [dispatch])

    const setTokenId = useCallback((tokenId: ProtectedDraftState["tokenId"]) => {
        const action = protectedDraftActions.setTokenId(tokenId)
        dispatch(action)
    }, [dispatch])

    const setDraftId = useCallback((id: ProtectedDraftState['draftId']) => {
        const action = protectedDraftActions.setDraftId(id)
        dispatch(action)
    }, [dispatch])

    return {
        state,
        actions: {
            setMode,
            setDraftId,
            setTokenId
        }
    }
}