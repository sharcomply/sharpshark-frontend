import { useQuery } from 'react-query';
import { backend } from 'api';


export default function useProtected(id?: string) {
    const { data: protect, ...options } = useQuery([ 'PROTECTED_ITEM', id], () => backend.drafts.getDraft(id), {
        enabled: !!id,
        retry: false,
        keepPreviousData: true
    });

    return {
        protect,
        ...options
    };
}
