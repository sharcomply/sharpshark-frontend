import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { validationSchema } from "../validationSchema";
import { useEffect } from "react";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";

export interface DraftUpdateFormState {
    title: string;
    text: string;
    file?: File;
    image?: any;
    type: "text" | "image";
    fullTitle: string;
    authors: string[];
    coAuthors: string[];
    participants: string[];
    additionalImages: string[]
}

export const useDraftUpdateForm = (initialValues: DraftUpdateFormState) => {
    const { actions: { open, close, error } } = useDraftAlert()

    const form = useForm<DraftUpdateFormState>({
        resolver: yupResolver(validationSchema),
        defaultValues: initialValues
    })

    useEffect(() => {
        const errors = Object.values(form.formState.errors)[0]
        if (!errors) return

        open()
        error('Validation Error', errors.message || "")

        return () => {
            close()
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [error, close, open, error, form.formState.errors])

    return {
        form
    }
}
