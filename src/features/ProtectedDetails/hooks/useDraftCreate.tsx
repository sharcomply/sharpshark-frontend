import { useMutation, useQueryClient } from "react-query";
import { backend } from "api";
import { useCallback, useState } from "react";
import { DraftUpdateFormState } from "./useDraftUpdateForm";
import { useProtectedDraftStore } from "./useProtectedDraftStore";

export const useDraftCreate = () => {
    const queryClient = useQueryClient();
    const {
        state: { mode, draftId },
        actions: { setDraftId },
    } = useProtectedDraftStore();
    const [isLoading, setLoading] = useState(false);

    const onSuccess = () => Promise.all([queryClient.invalidateQueries([`PROTECTED_LIST`]), queryClient.invalidateQueries(["PROTECTED_ITEM"])]);

    const { mutateAsync: saveDraft, isLoading: createLoading } = useMutation(backend.drafts.saveDraft, {
        onSuccess,
    });

    const { mutateAsync: updateDraft, isLoading: updateLoading } = useMutation(backend.drafts.editDraft, {
        onSuccess,
    });

    const handleSave = useCallback(
        async (form: DraftUpdateFormState) => {
            const payload = { _id: draftId! };

            if (mode === "new") {
                const { _id } = await saveDraft({
                    title: form.title,
                    type: form.type,
                    fullTitle: form.fullTitle,
                    authors: form.authors.filter((i) => i.length),
                    coAuthors: form.coAuthors.filter((i) => i.length),
                    participants: form.participants.filter((i) => i.length),
                    parentId: draftId,
                });

                Object.assign(payload, { _id });
            }

            return payload;
        },
        [mode, saveDraft, draftId]
    );

    const create = useCallback(
        async (form: DraftUpdateFormState) => {
            const payload = {
                uploadFile: form.file || form.image,
                fileExt: "txt",
            };

            setLoading(true);

            if (form.type === "text" && !form.file) {
                const blob = new Blob([form.text], { type: "text/plain" });
                payload.uploadFile = new File([blob], form.title);
            }

            if (form.type === "image" && form.image) {
                payload.fileExt = form.image.type.split("/")[1];
            }

            try {
                // Save Draft
                const { _id } = await handleSave(form);

                // Get Links for Upload And Download
                const { uploadLink, downloadLink } = await backend.drafts.getFileUploadLink(_id, payload.fileExt);

                // Upload Link
                await backend.drafts.uploadFile(uploadLink.url, payload.uploadFile!, uploadLink.fields);

                // update draft using link to file
                await updateDraft({
                    _id,
                    title: form.title,
                    linkUrl: downloadLink,
                });

                setDraftId(_id);
                setLoading(false);

                return await backend.drafts.getDraft(_id);
            } catch (e) {
                setLoading(false);
                console.log("[DRAFT_SAVE]", e);
                throw e;
            }
        },
        [handleSave, setDraftId, updateDraft]
    );

    return {
        create,
        loading: createLoading || updateLoading || isLoading,
    };
};
