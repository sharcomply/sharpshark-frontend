import {useQuery} from "react-query";
import {backend} from "api";
import {useParams} from "react-router-dom";

export default function useCurrentProtected() {
    const { protectId } = useParams();
    const { data: protect, ...options } = useQuery(["PROTECTED_ITEM", protectId], () => backend.drafts.getDraft(protectId), {
        enabled: !!protectId,
        retry: true,
        keepPreviousData: true,
        refetchOnWindowFocus: false
    });

    return {
        protect,
        ...options,
    };
}
