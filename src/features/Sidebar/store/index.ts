import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Types
export enum SidebarKeys {
    DRAFTS = 'drafts',
    PROTECTS = 'protects',
    ALERTS = 'alerts',
    DISPUTES = 'disputes'
}

export type SidebarValues = {
    total?: number;
    unread?: number;
    type?: 'default' | 'warn' | 'danger'
}

export type SidebarState = {
    [item in SidebarKeys]: SidebarValues
}

// Initial State
const initialState: SidebarState = {
    [SidebarKeys.DRAFTS]: { total: 0, unread: 0, type: 'default' },
    [SidebarKeys.ALERTS]: { total: 0, unread: 0, type: 'default' },
    [SidebarKeys.DISPUTES]: { total: 0, unread: 0, type: 'default' },
    [SidebarKeys.PROTECTS]: { total: 0, unread: 0, type: 'default' }
};

// Slice
const sidebarSlice = createSlice({
    name: "sidebar",
    initialState,
    reducers: {
        updateIndicators: (state, action: PayloadAction<{ type: SidebarKeys, indicators: SidebarValues }>) => ({
            ...state,
            [action.payload.type]: {
                ...state.drafts,
                ...action.payload.indicators
            }
        }),
        clearStore: () => ({
            ...initialState
        })
    },
});

export const sidebarReducers = sidebarSlice.reducer
export const sidebarActions = sidebarSlice.actions
