import styled from "@emotion/styled";
import { Icon } from "@iconify/react";

import NavItem from "./components/NavItem";
import Section from "./components/Section";
import { Box, Stack } from "@mantine/core";
import { useSidebarStore } from "./hooks/useSidebarStore";
import { useEffect } from "react";
import { useSidebarCounts } from "./hooks/useSidebarCounts";
import { SharkButton } from "components/SharkButton";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import useAuthStore from "features/Auth/hooks/useAuthStore";

const appVersion = 1.06
console.log(`APP VERSION`, appVersion)

// Component
export default function Sidebar() {
    const { data } = useSidebarCounts();
    const { drafts, protects, alerts, disputes, setDisputesIndicators, setAlertsIndicators, setDraftsIndicators, setProtectsIndicators, clearStore} = useSidebarStore();
    const { t } = useTranslation();
    const { isLoggedIn, connected } = useAuthStore();

    useEffect(() => {
        if (!isLoggedIn || !connected) {
            clearStore()
        } else {
            setDraftsIndicators({total: data?.counts.drafts});
            setProtectsIndicators({total: data?.counts.protects});
            setAlertsIndicators({total: data?.counts.alerts, unread: data?.counts.unreadAlerts});
            setDisputesIndicators({total: data?.counts.disputes});
        }
    }, [data, setDraftsIndicators, setAlertsIndicators, setProtectsIndicators, setDisputesIndicators, isLoggedIn, connected]);

    return (
        <SidebarRoot id="sidebar" spacing={0}>
            {/* Navigation */}
            <NavItem icon={<Icon icon={"ic:round-notes"} />} title={t("sidebar.drafts_btn")} to={"/drafts"} indicators={drafts} />
            <NavItem icon={<Icon icon={"ic:round-security"} />} title={t("sidebar.protected_btn")} to={"/protected"} indicators={protects} />

            <Box mx={16} sx={{ height: 1, backgroundColor: "#DDDEEE", opacity: 0.1 }} />
            <Section title="Copyright protection">
                <NavItem
                    icon={<Icon icon={"ic:round-copyright"} />}
                    title={t("sidebar.alerts_btn")}
                    to={"/alerts"}
                    indicators={alerts}
                    unread={alerts.unread}
                />

                <NavItem icon={<Icon icon={"ic:round-call-made"} />} title={t("sidebar.disputes_btn")} to={"/disputes/draft-website-owner"} indicators={disputes} />
            </Section>

            <Box mx={16} sx={{ height: 1, backgroundColor: "#DDDEEE", opacity: 0.1 }} />

            <Section containerProps={{ mt: 32, px: 16 }}>
                <Link to={"/drafts/create"}>
                    <SharkButton variant={"filled"} buttonType="primary" width={"100%"}>
                        {t("sidebar.new-document_btn")}
                    </SharkButton>
                </Link>
            </Section>

            {/* Sidebar Footer */}
            <Stack mt={"auto"}>
                <NavItem icon={<Icon icon={"ic:round-info"} />} title={t("sidebar.how-it-works_link")} to={"https://sharpshark.io/#faq"} external={true} />
              <div style={{color: '#9b9fbc', marginLeft: 20, marginBottom: 12}}>Version: {appVersion}</div>
            </Stack>
        </SidebarRoot>
    );
}


// Styling
const SidebarRoot = styled(Stack)`
    display: flex;
    flex-direction: column;
    width: 290px;
    min-width: 290px;
    background-color: #32313e;
    overflow: auto;
`;
