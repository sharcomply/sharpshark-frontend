import styled from "@emotion/styled"
import { ReactNode } from "react";
import { Stack, StackProps } from '@mantine/core';

// Props
interface Props {
    title?: string;
    children?: ReactNode;
    containerProps?: StackProps;
}

// Component
export default function Section({ title, children, containerProps }: Props) {
    return (
        <Stack spacing={0} {...containerProps}>
            {Boolean(title) && (
                <SectionName>
                    <span>{title}</span>
                </SectionName>
            )}
            {children}
        </Stack>
    )
}

// Styling

const SectionName = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    margin: 20px  32px 12px ;

    span {
        color: #9b9fbc;
        font-weight: 500;
        font-size: 12px;
        line-height: 1.2;
        letter-spacing: .3px;
        text-transform: uppercase;
        opacity: .7;
    }
`