import { css } from "@emotion/react";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";

// Props
interface Props {
    title: string;
    to?: string;
    onClick?: () => void;
    variant?: 'filled' | 'outlined'
}

// Component
export default function Button({ title, to, onClick, variant = 'filled' }: Props) {
    if (to) {
        return (
            <NavLinkRoot to={to} className={variant}>
                <span>{title}</span>
            </NavLinkRoot>
        );
    }

    return (
        <ButtonRoot onClick={onClick} className={variant}>
            <span>{title}</span>
        </ButtonRoot>
    );
}

// Styling
const RootCSS = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: relative;
    
    height: 48px;
    padding: 14px 24px;
    border-radius: 4px;
    cursor: pointer;
    
    font-weight: 500;
    font-size: 14px;
    line-height: 20px;
    text-align: center;
    
    transition-duration: 200ms;
    transition-property: color, background-color, box-shadow, border;
    transition-timing-function: linear;
    
    &.filled {
        color: #fff;
        background-color: #4700E5;
        box-shadow: 0 4px 12px rgba(55, 81, 255, 0.24);
    }

    &.filled:hover {
        color: #fff;
        background-color: #1C00A6;
        box-shadow: 0 6px 24px rgba(37, 61, 217, 0.32);
    }

    &.filled:focus {
        color: #fff;
        background-color: #4700E5;
        box-shadow: 0 2px 6px rgba(37, 61, 217, 0.32);
    }

    
    &.outlined {
        color: #4700E5;
        background-color: transparent;
        border: 1px solid #4700E5;
    }

    &.outlined:hover {
        color: #1C00A6;
        background-color: transparent;
        border: 1px solid #1C00A6;
    }

    &.outlined:focus {
        color: #FFFFFF;
        background-color: #4700E5;
        border: 1px solid #4700E5;
    }
`;

const ButtonRoot = styled.button`
    ${RootCSS};
`;

const NavLinkRoot = styled(Link)`
    ${RootCSS};
`;

