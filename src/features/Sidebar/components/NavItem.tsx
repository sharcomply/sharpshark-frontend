import { css } from "@emotion/react";
import styled from "@emotion/styled";
import clsx from "clsx";
import { ReactNode } from "react";
import { Link, useMatch } from "react-router-dom";
import { Box, Group, Stack, Text } from "@mantine/core";
import { SidebarValues } from "../store";

// Props
interface Props {
    icon: ReactNode;
    title: string;
    to: string;
    description?: string;
    external?: boolean;
    indicators?: SidebarValues;
    unread?: number;
}

const badgeColors = {
    warn: "#FFC933",
    danger: "#E60C54",
    default: "transparent",
};

// Component
export default function NavItem({ icon, title, to, description, external, indicators, unread }: Props) {
    return (
        <ExtendedLink external={external} to={to}>
            <Group px={32} sx={{ height: 64 }} position={"apart"}>
                <Group align={"center"} spacing={24}>
                    <Stack align={"center"} sx={{ fontSize: 16 }}>
                        {icon}
                    </Stack>
                    <Text size={"md"} sx={{ lineHeight: "21px" }} dangerouslySetInnerHTML={{ __html: title }} />
                </Group>

                <Group align={"center"}>
                    {!!indicators && unread! > 0 && (
                        <Box
                            px={12}
                            py={4}
                            sx={{
                                backgroundColor: indicators?.type ? badgeColors[indicators.type] : badgeColors.default,
                                borderRadius: 100,
                                color: "#ffffff",
                                height: 24,
                            }}
                        >
                            <Badge bgColor={badgeColors.default}>
                                <Text sx={{ fontSize: 12, lineHeight: "16px", fontWeight: 500 }}>
                                    {unread} / {indicators!.total}
                                </Text>
                            </Badge>
                        </Box>
                    )}
                    {!!indicators && !unread && (
                        <Text size={"md"} sx={{ lineHeight: "21px" }}>
                            {indicators.total}
                        </Text>
                    )}
                </Group>
            </Group>

            {description && (
                <Group px={32} pb={16}>
                    <Text size={"sm"}>{description}</Text>
                </Group>
            )}
        </ExtendedLink>
    );
}

const ExtendedLink = ({ children, to, external }: any) => {
    const match = useMatch({ path: to });
    const ExternalLink = (
        <NavItemRootExt href={to} target="_blank">
            {children}
        </NavItemRootExt>
    );

    const AppLink = (
        <NavItemRoot className={clsx({ active: Boolean(match) })} to={to}>
            {children}
        </NavItemRoot>
    );

    return external ? ExternalLink : AppLink;
};

// Styling
const RootCSS = css`
    display: flex;
    flex-direction: column;
    position: relative;
    width: 100%;
    color: #9b9fbc;

    transition: color 0.2s ease-in-out;

    &::after {
        content: "";
        position: absolute;
        width: calc(100% - 3px);
        height: 100%;
        left: 0;
        top: 0;
        background-color: rgba(159, 162, 180, 0.08);
        opacity: 0;
        border-left: 3px solid #dde2ff;

        transition: all 0.2s ease-in-out;
    }

    &.active::after {
        opacity: 1;
    }

    &:hover::after {
        opacity: 1;
    }

    &.active {
        color: #dde2ff;
    }

    &:hover {
        color: #dde2ff;
    }
`;

const NavItemRoot = styled(Link)`
    ${RootCSS};
`;

const NavItemRootExt = styled.a`
    ${RootCSS};
`;

const Badge = styled.div<any>`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 4px 12px 4px 12px;
    background-color: #e60c54;
    border-radius: 100px;
    color: #fff;
    font-size: 12px;
    font-weight: 400px;
`;
