import { useQuery } from 'react-query';
import { backend } from 'api';


export const useSidebarCounts = () => {
    const { data, refetch, isLoading, isError, remove } = useQuery([ 'SIDEBAR_COUNTS' ], () => backend.constants.getSidebarCounts(), {
        enabled: true,
        keepPreviousData: true,
    });
    
    return {
        data,
        refetch,
        isLoading,
        isError,
        remove
    };
};
