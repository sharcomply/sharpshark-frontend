import { useAppDispatch, useAppSelector } from 'store';
import { useCallback } from 'react';
import { sidebarActions, SidebarKeys, SidebarValues } from '../store';


export function useSidebarStore() {
    const state = useAppSelector((state) => state.sidebar)
    const dispatch = useAppDispatch()

    const setDraftsIndicators = useCallback((indicators: SidebarValues) => {
        const action = sidebarActions.updateIndicators({
            type: SidebarKeys.DRAFTS,
            indicators
        })

        dispatch(action)
    }, [dispatch])

    const setProtectsIndicators = useCallback((indicators: SidebarValues) => {
        const action = sidebarActions.updateIndicators({
            type: SidebarKeys.PROTECTS,
            indicators
        })

        dispatch(action)
    }, [dispatch])

    const setAlertsIndicators = useCallback((indicators: SidebarValues) => {
        const action = sidebarActions.updateIndicators({
            type: SidebarKeys.ALERTS,
            indicators
        })

        dispatch(action)
    }, [dispatch])

    const setDisputesIndicators = useCallback((indicators: SidebarValues) => {
        const action = sidebarActions.updateIndicators({
            type: SidebarKeys.DISPUTES,
            indicators
        })

        dispatch(action)
    }, [dispatch])

    const clearStore = useCallback(() => {
        const action = sidebarActions.clearStore()
        dispatch(action)
    }, [dispatch])

    return {
        ...state,
        setDraftsIndicators,
        setProtectsIndicators,
        setAlertsIndicators,
        setDisputesIndicators,
        clearStore
    }
}