import { ModalContent } from "../interfaces";
import AlertPreview from "assets/images/welcome/AlertPreview.jpg";
import CertificatePreview from "assets/images/welcome/CertificatePreview.jpg";
import AlertListPreview from "assets/images/welcome/AlertListPreview.jpg";
import { ReactNode } from "react";
import { Image } from "@mantine/core";
import { TFunction } from "react-i18next";

export interface WelcomeItem {
    id: number;
    title: (t: TFunction<"translation", undefined>) => string;
    description: (t: TFunction<"translation", undefined>) => string;
    image?: ReactNode;
    contentType: ModalContent;
    submitText: (t: TFunction<"translation", undefined>) => string;
}

// Welcome Data
export const WelcomeData: WelcomeItem[] = [
    {
        id: 1,
        title: (t) => t("welcome.title_1"),
        description: (t) => t("welcome.description_1"),
        image: <Image width={360} height={220} src={AlertPreview} sx={{ border: "1px solid transparent", borderRadius: 4 }} fit={"none" as any} />,
        contentType: ModalContent.Welcome,
        submitText: (t) => t("buttons.got_it_btn"),
    },
    {
        id: 2,
        title: (t) => t("welcome.title_2"),
        description: (t) => t("welcome.description_2"),
        image: <Image width={360} height={220} src={CertificatePreview} sx={{ border: "1px solid #DDDEEE", borderRadius: 4 }} />,
        contentType: ModalContent.Protect,
        submitText: (t) => t("buttons.nice_btn"),
    },
    {
        id: 3,
        title: (t) => t("welcome.title_3"),
        description: (t) => t("welcome.description_3"),
        image: <Image width={360} height={220} src={AlertListPreview} sx={{ border: "1px solid #DDDEEE", borderRadius: 4 }} />,
        contentType: ModalContent.Detect,
        submitText: (t) => t("buttons.all_clear_btn"),
    },
];
