interface Props {
    label: string;
    value: string;
}

const licenceData: Props[] = [
    // {
    //   value: "© all rights reserved, commercial distribution",
    //   label: "© all rights reserved, commercial distribution",
    // },
    // { value: "© all rights reserved, no distribution", label: "© all rights reserved, no distribution" },
    // { value: "Free-to-use", label: "Free-to-use" },
    {
        value: "Time Machine © 2022 by H. G. Wells is licensed under CC0 1.0: https://creativecommons.org/publicdomain/zero/1.0/",
        label: "CC0 1.0",
    },
    {
        value: "Time Machine © 2022 by H. G. Wells is licensed under CC BY 4.0: https://creativecommons.org/licenses/by/4.0/",
        label: "CC BY 4.0",
    },
    {
        value: "Time Machine © 2022 by H. G. Wells is licensed under CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/",
        label: " CC BY-SA 4.0",
    },
    {
        value: "Time Machine © 2022 by H. G. Wells is licensed under CC BY-ND 4.0: https://creativecommons.org/licenses/by-nd/4.0/",
        label: "CC BY-ND 4.0",
    },
    {
        value: "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC 4.0: https://creativecommons.org/licenses/by-nc/4.0/",
        label: "CC BY-NC 4.0",
    },
    {
        value: "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/",
        label: "CC BY-NC-SA 4.0",
    },
    {
        value: "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-ND 4.0: https://creativecommons.org/licenses/by-nc-nd/4.0/",
        label: "CC BY-NC-ND 4.0",
    },
];

export default licenceData;
