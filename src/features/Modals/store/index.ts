import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ReactNode } from "react";
import { ModalContent, ModalType } from "../interfaces";

// Types
export interface ModalState {
  open: boolean;
  type: ModalType | null;
  content: ModalContent | null;
  modaContent?: string | ReactNode;
  modalStatus?: string | ReactNode;
}

// Initial State
const initialState: ModalState = {
  open: false,
  type: null,
  content: null,
  modaContent: undefined,
  modalStatus: undefined
};

// Slice
const appModalSlice = createSlice({
  name: "app-modal",
  initialState,
  reducers: {
    open: (_, action: PayloadAction<{ type: ModalType; content: ModalContent }>) => ({
      open: true,
      type: action.payload.type,
      content: action.payload.content,
    }),
    switch: (state, action: PayloadAction<ModalContent>) => ({
      ...state,
      content: action.payload,
    }),
    setModalContent: (state, action: PayloadAction<ModalState["modaContent"]>) => {
      state.modaContent = action.payload
    },
    setModalStatus: (state, action: PayloadAction<ModalState["modalStatus"]>) => {
      state.modalStatus = action.payload
    },

    close: (state) => ({
      ...state,
      open: false,
    }),
  },
});


export const appModalReducers = appModalSlice.reducer
export const appModalActions = appModalSlice.actions
