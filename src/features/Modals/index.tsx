import React, { useEffect } from "react";

import AuthModals from "./containers/AuthModals";
import { useModal } from "./hooks/useModal";
import { ModalContent, ModalType } from "./interfaces";
import { getWelcomeWatched } from "helpers/welcomeStorage";
import WelcomeModals from "./containers/WelcomeModals";
import SettingsModals from "./containers/SettingsModals";
import SharedModals from "./containers/SharedModals";
import ProtectedModals from "./containers/ProtectedModals";
import DraftModals from "./containers/DraftModals";
import DisputesModals from "./containers/DisputesModal";
import GeneralModals from "./containers/GeneralModals";

// Component
export default function ModalContainer() {
    const { handleOpen } = useModal(ModalType.WELCOME);

    useEffect(() => {
        const watchSkip = getWelcomeWatched();

        if (Boolean(watchSkip)) return;

        handleOpen({
            type: ModalType.WELCOME,
            content: ModalContent.Welcome,
        });
    }, [handleOpen]);

    return (
        <React.Fragment>
            <AuthModals />
            <DraftModals />
            <WelcomeModals />
            <SettingsModals />
            <SharedModals />
            <ProtectedModals />
            <DisputesModals />
            <GeneralModals/>
        </React.Fragment>
    );
}
