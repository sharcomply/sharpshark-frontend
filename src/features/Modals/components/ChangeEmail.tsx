import styled from "@emotion/styled";
import { sendEmailVerification } from "api/requests/auth";
import { Input } from "components/Input";
import TextField from "components/TextField";
import { useWhoAmi } from "features/Auth/hooks/useWhoAmi";
import useSettingStore from "features/Settings/hooks/useSettingStore";
import { useCallback, useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import closeIcon from "../../../assets/images/close-icon.svg";
import { useModal } from "../hooks/useModal";
import { ChangeEmailTypes } from "../interfaces/formTypes";
import { useDraftAlert } from "../../DraftDetails/hooks/useDraftAlert";

// Component
export default function ChangeEmail() {
    const { handleClose } = useModal();
    const { t } = useTranslation();
    const [loading, setloading] = useState(false);
    const { register, handleSubmit } = useForm<ChangeEmailTypes>();
    const { setEmailAddress } = useSettingStore();
    const alert = useDraftAlert();

    const { refetch } = useWhoAmi();

    const onSubmit = useCallback(
        async (form: ChangeEmailTypes) => {
            try {
                alert.actions.close();
                setloading(true);
                await sendEmailVerification({ email: form.email });

                const response = await refetch();
                setEmailAddress({ address: form.email, isEmailVerified: response.data?.user.isEmailVerified });

                setloading(false);
                handleClose();
            } catch (error: any) {
                setloading(false);
                const err = await error.response.json();
                if (err.data.message.includes("E11000")) {
                    alert.actions.open();
                    alert.actions.error(t("Validation Error!"), t("An email with this name already exists"));
                }
            }
        },
        [handleClose, refetch, setEmailAddress]
    );
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <ModalHeader>
                <h2>{t("modals.email.title")}</h2>
                <CloseButton onClick={handleClose}>
                    <img src={closeIcon} alt=""></img>
                </CloseButton>
            </ModalHeader>

            <Text>{t("modals.email.set_email_description")}</Text>

            <TextField label={t("textfields.lables.your_email")}>
                {" "}
                <Text>{t("modals.email.we_fetch_associated")}</Text>
                <Input placeholder={t("modals.email.input_placeholder")} {...register("email", { required: true })} />
            </TextField>

            <Button color={"#4700e5"} hoverColor={"#1c00a6"} onClick={handleClose} disabled={loading}>
                {t("buttons.change-email_btn")}
            </Button>
        </form>
    );
}

// Styling
const ModalHeader = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 20px;
    position: relative;

    & h2 {
        display: flex;
        font-size: 28px;
        margin: 0;
        width: 100%;
        line-height: 1.3;
        font-weight: 700;
    }

    & a {
        display: flex;
        font-size: 60px;
    }
`;

const Text = styled.div`
    font-size: 14px;
    line-height: 1.5;
    color: #666892;
    display: block;
    margin: 0.5rem 0 1rem 0;
`;

const Button = styled.button<{ color: string; hoverColor: string }>`
    display: flex;
    width: 100%;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    color: #fff;
    border: 1px solid transparent;

    padding: 12px 16px;
    margin-top: 8px;
    margin-bottom: 8px;
    border-radius: 4px;

    font-weight: 500;
    font-size: 14px;
    line-height: 20px;

    user-select: none;
    transition: background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

    ${({ color }) => `
        background-color: ${color};
        border-color: ${color};
    `}

    &:hover {
        ${({ hoverColor }) => `
            background-color: ${hoverColor};
            border-color: ${hoverColor};
        `}
    }

    &:disabled {
        opacity: 0.6;
    }
`;

const CloseButton = styled.button`
    cursor: pointer;
    position: absolute;
    width: 16px;
    height: 16px;
    right: -75px;
    top: -25px;
`;
