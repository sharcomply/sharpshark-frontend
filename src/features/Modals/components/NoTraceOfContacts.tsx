import styled from "@emotion/styled";
import { Checkbox, Group, Stack, Text } from "@mantine/core";
import Icon from "components/Icon";
import { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useModal } from "../hooks/useModal";
import Section from "../../Sidebar/components/Section";
import { SharkButton } from "../../../components/SharkButton";
import useDisputesStore from "features/DisputesDetail/hooks/useDisputesStore";
import { ModalContent } from "../interfaces";

// Component
export default function NoTraceOfContacts() {
    const navigate = useNavigate();
    const { handleClose } = useModal();
    const [checked, setChecked] = useState({ one: false, two: false });
    const { setDisputModalResponse } = useDisputesStore();

    const { t } = useTranslation();
    const closeHandler = useCallback(() => {
        handleClose();
        navigate(
            {
                hash: undefined,
            },
            { replace: true }
        );
    }, [handleClose, navigate]);

    const onSubmit = useCallback(() => {
        setDisputModalResponse(ModalContent.NoTraceOfContacts);
    }, [setDisputModalResponse, closeHandler]);

    return (
        <Stack spacing={8}>
            <Group onClick={closeHandler} p={2} sx={{ position: "absolute", right: "-36px", top: 0, backgroundColor: "#fff", borderRadius: 100 }}>
                <Icon icon={"ep:close-bold"} color={"#666892"} size={16} />
            </Group>
            <Stack pt={32} px={32} spacing={4}>
                <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 24, lineHeight: "30px" }}>
                    {t("No trace of contacts, right?")}
                </Text>
                <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px", marginBottom: "24px" }}>
                    {t(
                        "It’s impossible to find their contacts anywhere, right? If so, we will now prepare a claim for the provider so you can send it immediately"
                    )}
                </Text>
                <Section>
                    <Checkbox
                        onClick={() => setChecked({ one: !checked.one, two: checked.two })}
                        label="I can’t find any contacts and want to notify their provider"
                    />
                </Section>
                <Section>
                    <Checkbox
                        onClick={() => setChecked({ one: checked.one, two: !checked.two })}
                        label="I checked their privacy policy and cookies agreement: there are no contacts mentioned there"
                    />
                </Section>
            </Stack>
            <Footer>
                <Group spacing={12}>
                    <Group spacing={8}></Group>
                    <SharkButton variant="outlined" buttonType="secondary" px={16} py={11} onClick={closeHandler}>
                        × Cancel
                    </SharkButton>
                    <SharkButton variant="filled" buttonType="secondary" px={16} py={11} onClick={onSubmit} disabled={!(checked.one && checked.two)}>
                        Let’s send it!
                    </SharkButton>
                </Group>
            </Footer>
        </Stack>
    );
}
const Footer = styled(Group)`
    position: relative;
    background-color: #ffffff;
    border-top: 1px solid #dddeee;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
    padding: 14px 32px;
    height: 103px;
    margin-top: 30px;
    overflow: hidden;
    justify-content: flex-end;
`;
