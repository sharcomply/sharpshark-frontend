import { useMemo, useCallback } from "react";
import { Group, Stack, Text } from "@mantine/core";
import styled from "@emotion/styled";
import clsx from "clsx";

import { WelcomeData, WelcomeItem } from "../data/welcome.data";
import { ReactComponent as SharpLogo } from "assets/images/sharp-logo.svg";
import { ModalContent } from "../interfaces";
import { setWelcomeWatched } from "../../../helpers/welcomeStorage";
import { SharkButton } from "components/SharkButton";
import { useTranslation } from "react-i18next";

// Props
interface WelcomeContentProps {
    item: WelcomeItem;
    onSubmit: () => void;
    onSwitch: (contentType: ModalContent) => void;
}

// Component
export default function WelcomeContent({ item, onSubmit, onSwitch }: WelcomeContentProps) {
    const {t} = useTranslation()
    const contents = useMemo(() => {
        return WelcomeData.map((modal) => ({ id: modal.id, type: modal.contentType }))
    }, []);

    const nextContent = useMemo(() => {
        return WelcomeData.find((modal) => modal.id === item.id + 1);
    }, [item.id]);

    const handleSubmit = useCallback(() => {
        if (nextContent) {
            return onSwitch(nextContent.contentType);
        }

        onSubmit();
        setWelcomeWatched("true");
    }, [nextContent, onSubmit, onSwitch]);

    return (
        <Stack spacing={0}>
            {/* Header */}
            <Stack spacing={24}>
                <Stack spacing={0} align={"center"} px={32} pt={32}>
                    <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 28, lineHeight: "40px" }} align={"center"}>
                        {item.title(t)}
                    </Text>
                    <SharpLogo />
                </Stack>
                <Stack spacing={0} align={"center"} px={16}>
                    <Text color={"#0A001E"} sx={{ fontSize: 16, lineHeight: "20px", minHeight: 60 }} align={"center"}>
                        {item.description(t)}
                    </Text>
                </Stack>
            </Stack>

            {/* Picture */}
            <Stack px={60} py={42}>
                {item.image}
            </Stack>

            <Stack pb={32} px={32}>
                <Group position="apart">
                    <div style={{ display: "flex" }}>
                        {contents.map((content) => (
                            <RadioButton key={content.id} onClick={() => onSwitch(content.type)} className={clsx({ active: item.id === content.id })} />
                        ))}
                    </div>

                    <SharkButton px={16} variant="filled" size="medium" buttonType="primary" sx={{ paddingLeft: 16, paddingRight: 16 }} onClick={handleSubmit}>{item.submitText(t)}</SharkButton>
                </Group>
            </Stack>
        </Stack>
    );
}

const RadioButton = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    margin-right: 7px;
    position: relative;
    border-radius: 50%;
    min-width: 13px;
    height: 13px;
    border: 1px solid #4700e5;
    padding: 6px;
    box-shadow: inset 0 0 1px rgb(0 0 0 / 10%);

    &.active {
        &::before {
            content: "";
            position: absolute;
            border-radius: 50%;
            width: 8px;
            height: 8px;
            background-color: #4700e5;
        }
    }
`;
