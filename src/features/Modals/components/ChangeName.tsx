import styled from "@emotion/styled";
import { Input } from "components/Input";
import TextField from "components/TextField";
import { useTranslation } from "react-i18next";
import closeIcon from "../../../assets/images/close-icon.svg";
import { useModal } from "../hooks/useModal";

// Component
export default function ChangeName() {
    const { handleClose } = useModal();
    const { t } = useTranslation();
    return (
        <>
            <ModalHeader>
                <h2>{t("settings.titles.change_name")}</h2>
                <CloseButton onClick={handleClose}>
                    <img src={closeIcon} alt=""></img>
                </CloseButton>
            </ModalHeader>

            <TextField label={t("textfields.lables.name_or_pen_name")}>
                {" "}
                <Text>{t("textfields.we_sent_email_code", { seconds: "59" })}</Text>
                <Input placeholder={t("textfields.placeholders.code")} />
            </TextField>

            <Button color={"#4700e5"} hoverColor={"#1c00a6"} onClick={handleClose} disabled>
                {t("buttons.change_name_btn")}
            </Button>
        </>
    );
}

// Styling
const ModalHeader = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 20px;
    position: relative;

    & h2 {
        display: flex;
        font-size: 28px;
        margin: 0;
        width: 100%;
        line-height: 1.3;
        font-weight: 700;
    }

    & a {
        display: flex;
        font-size: 60px;
    }
`;

const Text = styled.div`
    font-size: 14px;
    line-height: 1.5;
    color: #666892;
    display: block;
    margin: 0.5rem 0 1rem 0;
`;

const Button = styled.button<{ color: string; hoverColor: string }>`
    display: flex;
    width: 100%;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    color: #fff;
    border: 1px solid transparent;

    padding: 12px 16px;
    margin-top: 8px;
    margin-bottom: 8px;
    border-radius: 4px;

    font-weight: 500;
    font-size: 14px;
    line-height: 20px;

    user-select: none;
    transition: background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

    ${({ color }) => `
        background-color: ${color};
        border-color: ${color};
    `}

    &:hover {
        ${({ hoverColor }) => `
            background-color: ${hoverColor};
            border-color: ${hoverColor};
        `}
    }

    &:disabled {
        opacity: 0.6;
    }
`;

const CloseButton = styled.button`
    cursor: pointer;
    position: absolute;
    width: 16px;
    height: 16px;
    right: -75px;
    top: -25px;
`;
