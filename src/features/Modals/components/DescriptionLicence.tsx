import { Checkbox, Group, Stack, Switch, Text } from "@mantine/core";
import Icon from "components/Icon";
import { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { useModal } from "../hooks/useModal";
import styled from "@emotion/styled";
import { SharkButton } from "../../../components/SharkButton";
import { ModalContent } from "../interfaces";
import ArrBack from "../../Icons/ArrBack";
import { descriptionLicence } from "./descriptionLicense";

interface Props {
    switchAttribution: { switch: boolean; disable: boolean };
    switchCommercialUsage: { switch: boolean; disable: boolean };
    switchAdaptations: { switch: boolean; disable: boolean };
    checkboxAdaptation: { checked: boolean; disable: boolean };
    licenceType: string;
}
// Component
export default function DescriptionLicence({ switchAttribution, switchCommercialUsage, switchAdaptations, checkboxAdaptation, licenceType }: Props) {
    const { handleSwitch } = useModal();
    const { t } = useTranslation();
    const [mainCheckboxSharp, setMainCheckboxSharp] = useState(false);
    const [mainCheckboxCC1, setMainCheckboxCC1] = useState(false);
    const [mainCheckboxCC2, setMainCheckboxCC2] = useState(false);
    const [mainCheckboxCC3, setMainCheckboxCC3] = useState(false);
    const [switch1, setSwitch1] = useState(switchAttribution.switch);
    const [switch2, setSwitch2] = useState(switchCommercialUsage.switch);
    const [switch3, setSwitch3] = useState(switchAdaptations.switch);
    const [checkboxCC, setCheckboxCC] = useState(checkboxAdaptation.checked);
    const closeHandler = useCallback(() => {
        handleSwitch(ModalContent.BeforeCloseSetLicence);
    }, [handleSwitch]);

    // const onSumbit = useCallback(
    //     (values: LicenseParameters) => {
    //         setLicenceParameters({ type: values.licenseHash, price: values.amountWei });
    //         if (!!draftStatusBeforeProtect && draftStatusBeforeProtect === "upgrade") {
    //             handleSwitch(ModalContent.PaymentChoice);
    //         } else {
    //             handleSwitch(ModalContent.Preferences);
    //         }
    //     },
    //     [setLicenceParameters, handleSwitch]
    // );

    return (
        <Stack spacing={0}>
            <Group onClick={closeHandler} p={2} sx={{ position: "absolute", right: "-36px", top: 0, backgroundColor: "#fff", borderRadius: 100 }}>
                <Icon icon={"ep:close-bold"} color={"#666892"} size={16} />
            </Group>

            <Group
                spacing={0}
                style={{
                    display: "flex",
                    alignItems: "stretch",
                    backgroundColor: "#F7F8FF",
                    borderRadius: "12px",
                    overflow: "hidden",
                }}
            >
                <Group
                    spacing={0}
                    py={32}
                    direction={"column"}
                    style={{
                        boxSizing: "border-box",
                        height: "100%",
                        width: "440px",
                        borderRight: "1px solid #DDDEEE",
                        backgroundColor: "#fff",
                    }}
                >
                    <Group ml={32} pb={17} position="apart" align={"center"}>
                        <CloseButton>
                            <span className="icon">
                                <ArrBack />
                            </span>
                            <span className="text">{t("buttons.back_btn")}</span>
                        </CloseButton>

                        <Text color="#252733" weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                            {t("Creative Commons license ")}
                        </Text>
                    </Group>
                    <Group pl={32} spacing={24} style={{ boxSizing: "border-box", borderTop: "1px solid #DDDEEE", width: "100%" }}>
                        <Group spacing={10} pt={27} style={{ width: "440px" }}>
                            <Text color="#252733" weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("How should others use your work?")}
                            </Text>
                        </Group>
                        <Group direction={"row"} noWrap={true} align={"start"}>
                            <Switch
                                size="md"
                                checked={switch1}
                                disabled={switchAttribution.disable}
                                onClick={() => {
                                    setSwitch1(!switch1);
                                }}
                            />
                            <Group spacing={0} direction={"column"}>
                                <Text weight={"500"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                                    {t("Attribution")}
                                </Text>
                                <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                                    {t("Anyone can use it, even not giving attribution to you")}
                                </Text>
                            </Group>
                        </Group>
                        <Group direction={"row"} noWrap={true} align={"start"}>
                            <Switch
                                size="md"
                                checked={switch2}
                                disabled={switchCommercialUsage.disable}
                                onClick={() => {
                                    setSwitch2(!switch2);
                                }}
                            />
                            <Group spacing={0} direction={"column"}>
                                <Text weight={"500"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                                    {t("Commercial usage")}
                                </Text>
                                <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                                    {t("Others can use it for commercial purposes, too")}
                                </Text>
                            </Group>
                        </Group>
                        <Group direction={"row"} noWrap={true} align={"start"}>
                            <Switch
                                size="md"
                                checked={switch3}
                                disabled={switchAdaptations.disable}
                                onClick={() => {
                                    setSwitch3(!switch3);
                                }}
                            />
                            <Group spacing={0} direction={"column"}>
                                <Text weight={"500"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                                    {t("Adaptations")}
                                </Text>
                                <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                                    {t("Others can remix, build on, and adapt the work")}
                                </Text>
                                <Group noWrap spacing={0} align={"start"} pt={16} mr={65}>
                                    <Checkbox
                                        checked={checkboxCC}
                                        disabled={checkboxAdaptation.disable}
                                        onClick={() => {
                                            setCheckboxCC(!checkboxCC);
                                        }}
                                    />
                                    <Label>Others must use the same CC license if they share the adapted work.</Label>
                                </Group>
                            </Group>
                        </Group>
                        <Group spacing={0} pb={23} style={{ borderTop: "1px solid #DDDEEE" }}>
                            <Group noWrap spacing={0} align={"start"} pt={16}>
                                <Checkbox checked={mainCheckboxCC1} onChange={() => setMainCheckboxCC1(!mainCheckboxCC1)} />
                                <Label>I own or have authority to license the work.</Label>
                            </Group>
                            <Group noWrap spacing={0} align={"start"} pt={16}>
                                <Checkbox checked={mainCheckboxCC2} onChange={() => setMainCheckboxCC2(!mainCheckboxCC2)} />
                                <Label>I understand that CC licensing is not revocable.</Label>
                            </Group>
                            <Group noWrap spacing={0} align={"start"} pt={16}>
                                <Checkbox checked={mainCheckboxCC3} onChange={() => setMainCheckboxCC3(!mainCheckboxCC3)} />
                                <Label>
                                    I have read and understand{" "}
                                    <a href="https://creativecommons.org/" target={"__blank"} style={{ color: "blue", textDecoration: "underline" }}>
                                        the terms of the license.
                                    </a>
                                </Label>
                            </Group>
                        </Group>
                    </Group>
                    <Group spacing={0}>
                        <Group noWrap spacing={0} px={32} py={16} align={"start"} style={{ borderTop: "1px solid #DDDEEE" }}>
                            <Checkbox checked={mainCheckboxSharp} onChange={() => setMainCheckboxSharp(!mainCheckboxSharp)} />
                            <Label>
                                I’m okay with the content going into public databases and can’t be deleted,{" "}
                                <a href="https://creativecommons.org/" target={"__blank"} style={{ color: "blue", textDecoration: "underline" }}>
                                    I have read about it
                                </a>
                            </Label>
                        </Group>
                        <Group spacing={12} pt={32} position={"right"} px={32} style={{ width: "100%" }}>
                            <SharkButton onClick={closeHandler} type={"button"} variant={"outlined"} buttonType={"secondary"}>
                                × Cancel
                            </SharkButton>
                            <SharkButton
                                disabled={!mainCheckboxSharp || !mainCheckboxCC1 || !mainCheckboxCC2 || !mainCheckboxCC3}
                                variant={"filled"}
                                buttonType={"primary"}
                            >
                                Protect
                            </SharkButton>
                        </Group>
                    </Group>
                </Group>
                <Group
                    px={32}
                    style={{
                        display: "flex",
                        width: "390px",
                        height: "100%",
                        backgroundColor: "#F7F8FF",
                        borderTopRightRadius: "12px",
                        borderBottomRightRadius: "12px",
                    }}
                >
                    <Group direction={"column"} pt={32}>
                        {descriptionLicence(licenceType)}
                        <a
                            href="https://en.wikipedia.org/wiki/Copyright_notice"
                            target="_blank"
                            style={{
                                marginRight: "8px",
                                fontWeight: "400",
                                fontSize: "16px",
                                lineHeight: "20px",
                                color: "#4700E5",
                                textDecoration: "underline",
                            }}
                            rel="noreferrer"
                        >
                            <Group spacing={4}>
                                See the license info
                                <Icon icon="ic:baseline-launch" color="#4700E5" />
                            </Group>
                        </a>
                    </Group>
                </Group>
            </Group>
        </Stack>
    );
}
const Label = styled.span`
    font-weight: 500;
    font-size: 14px;
    line-height: 20px;
    margin-left: 8px;
`;

const CloseButton = styled.a`
    display: flex;
    align-items: center;
    cursor: pointer;
    color: #4700e5;

    & .text {
        display: flex;
        align-items: center;
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }

    & .icon {
        display: flex;
        align-items: center;
        margin-right: 8px;
        font-size: 12px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }
`;
