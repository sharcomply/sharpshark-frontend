import styled from "@emotion/styled";
import { yupResolver } from "@hookform/resolvers/yup";
import { Group, NumberInput, Stack, Text } from "@mantine/core";
import ArrBack from "features/Icons/ArrBack";
import Logo from "features/Icons/Logo";
import { Controller, useForm } from "react-hook-form";
import { useModal } from "../hooks/useModal";
import { object, number } from "yup";
import { useCallback, useState } from "react";

import TextField from "components/TextField";
import { SharkButton } from "components/SharkButton";
import { useStartTracking } from "../../ProtectedDetails/hooks/useStartTracking";
import { useTranslation } from "react-i18next";
import useAuthStore from "features/Auth/hooks/useAuthStore";
// import { ModalContent, ModalType } from "../interfaces";
import { useWhoAmi } from "../../Auth/hooks/useWhoAmi";
import { useNearContext } from "../../../providers/NearProvider";
import { useDraftAlert } from "../../DraftDetails/hooks/useDraftAlert";

// Validation Schema
const validationSchema = object().shape({
    scanTotal: number().required("Required").min(1, "Must be greater than or equal to 1"),
});

// Component
export default function StartMonitoring() {
    const form = useForm<{ scanTotal: number }>({
        resolver: yupResolver(validationSchema),
        defaultValues: {
            scanTotal: undefined,
        },
    });

    const { startTrakingWithFiat, startTrakingWithMetaMask, isLoadingFiat, isNearLoading, isLoadingMetaMask, startTrakingWithNear } = useStartTracking();
    const { t } = useTranslation();
    const {
        handleClose,
        // handleOpen
    } = useModal();
    const { data } = useWhoAmi();
    const [disable, setDisable] = useState(true);
    const { network } = useAuthStore();
    const Near = useNearContext();
    const alert = useDraftAlert();

    const onSubmitWithMetaMask = useCallback(
        (form: { scanTotal: number }) => {
            startTrakingWithMetaMask(form.scanTotal);
            handleClose();
        },
        [handleClose, startTrakingWithMetaMask]
    );

    const onSubmitWithFiat = useCallback(
        (form: { scanTotal: number }) => {
            startTrakingWithFiat(form.scanTotal);
            handleClose();
        },
        [handleClose, startTrakingWithFiat]
    );

    const onSubmitNear = useCallback(
        (form: { scanTotal: number }) => {
            if (!data?.user.walletAddressNear) {
                // handleOpen({ type: ModalType.SETTINGS, content: ModalContent.ChangeNearAddress });
                return;
            }
            if (!Near?.WalletConnection.account().accountId) {
                alert.actions.open();
                alert.actions.error(t("Connect Near address!"), t("You should connect the near address"));
                handleClose();
                return;
            } else if (data?.user.walletAddressNear !== Near?.WalletConnection.account().accountId) {
                alert.actions.open();
                alert.actions.error(t("Wrong near address"), t("Your connected wallet don't match to your address that linked to account"));
                handleClose();
                return;
            }
            startTrakingWithNear(form.scanTotal);
            handleClose();
        },
        [handleClose, startTrakingWithFiat, data]
    );

    const handleMainPageClick = () => {
        window.open(window.location.origin, "_blank", "noopener,noreferrer");
    };

    return (
        <Stack spacing={34} p={32} sx={{ width: 380 }}>
            {/* Controls */}
            <Group position="apart" align={"center"}>
                <CloseButton onClick={() => handleClose()}>
                    <span className="icon">
                        <ArrBack />
                    </span>
                    <span className="text">{t("buttons.back_btn")}</span>
                </CloseButton>
                <a type="button" onClick={handleMainPageClick}>
                    <Logo height={30} width={30} />
                </a>
            </Group>

            {/* Header */}
            <Stack spacing={4}>
                <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 24, lineHeight: "30px" }}>
                    {t("modals.monitoring.title")}
                </Text>
                <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                    {t("modals.monitoring.descriptions")}
                </Text>
            </Stack>

            {/* Content */}
            <form
                onSubmit={(e) => {
                    e.preventDefault();
                }}
            >
                <Stack>
                    <Controller
                        name="scanTotal"
                        control={form.control}
                        render={({ field, fieldState }) => (
                            <TextField label={t("textfields.lables.days")} description={t("textfields.descriptions.how_many_days")}>
                                <NumberInput
                                    placeholder={t("textfields.placeholders.days_count")}
                                    {...field}
                                    value={field.value}
                                    hideControls
                                    parser={(value) => value?.replace(/\D/g, "")}
                                    onChange={(value) => field.onChange(value)}
                                    onInput={(event: any) => {
                                        event.target.value < 1 ? setDisable(true) : setDisable(false);
                                    }}
                                />
                                {fieldState.error?.message && (
                                    <Text mt={4} size="sm" color={"#E50C54"}>
                                        {fieldState.error?.message}
                                    </Text>
                                )}
                            </TextField>
                        )}
                    />
                </Stack>
            </form>

            <Stack>
                {network !== "near" ? (
                    <SharkButton
                        onClick={form.handleSubmit(onSubmitWithMetaMask)}
                        buttonType="primary"
                        type="button"
                        variant="filled"
                        loading={isLoadingMetaMask}
                        disabled={isLoadingFiat || isLoadingMetaMask || isNearLoading || disable}
                    >
                        Pay with MetaMask
                    </SharkButton>
                ) : (
                    <SharkButton
                        onClick={form.handleSubmit(onSubmitNear)}
                        buttonType="primary"
                        type="button"
                        variant="filled"
                        loading={isNearLoading}
                        disabled={isLoadingFiat || isLoadingMetaMask || isNearLoading || disable}
                    >
                        Pay with Near
                    </SharkButton>
                )}
                <SharkButton
                    onClick={form.handleSubmit(onSubmitWithFiat)}
                    buttonType="primary"
                    type="button"
                    variant="filled"
                    loading={isLoadingFiat}
                    disabled={isLoadingFiat || isLoadingMetaMask || isNearLoading || disable}
                >
                    Pay with Fiat
                </SharkButton>
            </Stack>
        </Stack>
    );
}

// Styling
const CloseButton = styled.a`
    display: flex;
    align-items: center;
    cursor: pointer;
    color: #4700e5;

    & .text {
        display: flex;
        align-items: center;
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }

    & .icon {
        display: flex;
        align-items: center;
        margin-right: 8px;
        font-size: 12px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }
`;
