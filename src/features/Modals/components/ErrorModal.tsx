// Component
import {useCallback, useState} from "react";
import styled from "@emotion/styled";
import {useModal} from "../hooks/useModal";
import {useTranslation} from "react-i18next";
import ArrBack from "../../Icons/ArrBack";

export default function ErrorModal() {
  const [loading, setLoading] = useState(false);
  const {handleClose} = useModal();
  const {t} = useTranslation();

  const handleCloseModal = useCallback(() => {
    setLoading(true);
    handleClose();
    setLoading(false)
  }, [setLoading, handleClose]);

  return (
    <>
      <ModalControls>
        <CloseButton onClick={handleClose}>
    <span className="icon">
      <ArrBack/>
      </span>
          <span className="text">{t("buttons.back_btn")}</span>
        </CloseButton>
      </ModalControls>

      <ModalHeader>
        <h2 dangerouslySetInnerHTML={{__html: t('statuses.smt_went_wrong')}}/>
      </ModalHeader>

      <ModalBody>
        <Text dangerouslySetInnerHTML={{__html: t('errors.messages.loadingFailed')}}>
          </Text>
      </ModalBody>


      <ButtonGroup>
        <Button disabled={loading} color={'#4700E5'} hoverColor={'#1C00A6'} onClick={handleCloseModal}>
          Close
        </Button>
      </ButtonGroup>
    </>
  );
}

// Styling
const ModalControls = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 12px;
`;

const CloseButton = styled.button`
  display: flex;
  align-items: center;
  cursor: pointer;
  color: #4700e5;

  & .text {
    display: flex;
    align-items: center;
    font-size: 14px;
    font-weight: 500;
    letter-spacing: 0.3px;
    line-height: 16px;
  }

  & .icon {
    display: flex;
    align-items: center;
    margin-right: 8px;
    font-size: 12px;
    font-weight: 500;
    letter-spacing: 0.3px;
    line-height: 16px;
  }
`;

const ModalHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  margin-bottom: 25px;

  & h2 {
    font-size: 22px;
    width: 100%;
    //max-width: 170px;
    margin: 0;
    width: 100%;
    line-height: 1.3;
    font-weight: 700;
    text-align: center;
  }

  & a {
    display: flex;
    font-size: 60px;
  }
`;

const ModalBody = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

const Text = styled.div`
  font-size: 14px;
  line-height: 1.5;
  text-align: center;
  color: #666892;
  display: block;
`;

const ButtonGroup = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 16px;
`;

const Button = styled.button<{ color: string; hoverColor: string }>`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  color: #fff;
  border: 1px solid transparent;

  padding: 12px 16px;
  margin-top: 8px;
  margin-bottom: 8px;
  border-radius: 4px;

  font-weight: 500;
  font-size: 14px;
  line-height: 20px;

  user-select: none;
  transition: background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

  &:disabled {
    opacity: 0.6;
  }

  ${({color}) => `
        background-color: ${color};
        border-color: ${color};
    `}
  &:hover {
    ${({hoverColor}) => `
            background-color: ${hoverColor};
            border-color: ${hoverColor};
        `}
  }
`;
