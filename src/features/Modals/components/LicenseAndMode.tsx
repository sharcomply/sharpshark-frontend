import { Checkbox, Group, NumberInput, Select, Stack, Text } from "@mantine/core";
import Icon from "components/Icon";
import { useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useModal } from "../hooks/useModal";
import styled from "@emotion/styled";
import { SharkButton } from "../../../components/SharkButton";
import useDraftStore from "../../DraftDetails/hooks/useDraftStore";
import { Controller, useForm } from "react-hook-form";
import { LicenseParameters } from "../interfaces/formTypes";
import { yupResolver } from "@hookform/resolvers/yup";

import * as yup from "yup";
import { ModalContent } from "../interfaces";
import RadioButton from "../../DisputesCollectingEvidence/components/RadioButton";
import licenceData from "../data/licence.data";

const schema = yup.object({
    licenseHash: yup.string().required().default("© all rights reserved, commercial distribution"),
    // amountWei: yup.number().when("licenseHash", { is: "Copyrighted (pay-to-use)", then: yup.number().required() }),
});
// Component
export default function LicenseAndMode() {
    const { handleSwitch } = useModal();
    const { t } = useTranslation();
    const { setLicenceParameters, draftStatusBeforeProtect, licence } = useDraftStore();
    const [radioButton, setRadioButton] = useState(
        licence.type !== undefined && licence.type.length ? licence.type : "© all rights reserved, commercial distribution"
    );
    const { control, watch, setValue } = useForm<LicenseParameters>({
        resolver: yupResolver(schema),
        defaultValues: {
            licenseHash: licence.type !== undefined && licence.type.length ? licence.type : "© all rights reserved, commercial distribution",
            amountWei: licence.price,
        },
    });

    const currentLicense = watch("licenseHash");

    useEffect(() => {
        if (
            watch("licenseHash") === "Time Machine © 2022 by H. G. Wells is licensed under CC0 1.0: https://creativecommons.org/publicdomain/zero/1.0/" ||
            watch("licenseHash") === "Time Machine © 2022 by H. G. Wells is licensed under CC BY 4.0: https://creativecommons.org/licenses/by/4.0/" ||
            watch("licenseHash") === "Time Machine © 2022 by H. G. Wells is licensed under CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/" ||
            watch("licenseHash") === "Time Machine © 2022 by H. G. Wells is licensed under CC BY-ND 4.0: https://creativecommons.org/licenses/by-nd/4.0/" ||
            watch("licenseHash") === "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC 4.0: https://creativecommons.org/licenses/by-nc/4.0/" ||
            watch("licenseHash") ===
                "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/" ||
            watch("licenseHash") === "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-ND 4.0: https://creativecommons.org/licenses/by-nc-nd/4.0/"
        ) {
            setRadioButton("Public, non-commercial distribution");
        }
    }, []);

    useEffect(() => {
        if (radioButton === "Public, non-commercial distribution") {
            setValue(
                "licenseHash",
                currentLicense === "Time Machine © 2022 by H. G. Wells is licensed under CC0 1.0: https://creativecommons.org/publicdomain/zero/1.0/" ||
                    currentLicense === "Time Machine © 2022 by H. G. Wells is licensed under CC BY 4.0: https://creativecommons.org/licenses/by/4.0/" ||
                    currentLicense === "Time Machine © 2022 by H. G. Wells is licensed under CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/" ||
                    currentLicense === "Time Machine © 2022 by H. G. Wells is licensed under CC BY-ND 4.0: https://creativecommons.org/licenses/by-nd/4.0/" ||
                    currentLicense === "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC 4.0: https://creativecommons.org/licenses/by-nc/4.0/" ||
                    currentLicense ===
                        "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/" ||
                    currentLicense ===
                        "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-ND 4.0: https://creativecommons.org/licenses/by-nc-nd/4.0/"
                    ? currentLicense
                    : "Time Machine © 2022 by H. G. Wells is licensed under CC BY 4.0: https://creativecommons.org/licenses/by/4.0/"
            );
        }
        if (watch("licenseHash") !== "© all rights reserved, commercial distribution") {
            setValue("amountWei", "");
        }
    }, [radioButton, setValue, watch]);

    const [mainCheckboxSharp, setMainCheckboxSharp] = useState(false);

    const closeHandler = useCallback(() => {
        setLicenceParameters({ type: watch("licenseHash"), price: watch("amountWei") });
        handleSwitch(ModalContent.BeforeCloseSetLicence);
    }, [handleSwitch]);

    const changeRightCC = () => {
        switch (watch("licenseHash")) {
            case "Time Machine © 2022 by H. G. Wells is licensed under CC0 1.0: https://creativecommons.org/publicdomain/zero/1.0/":
                return (
                    <Group>
                        <Group style={{ width: "100%" }} position={"apart"} align={"start"} noWrap={true}>
                            <Group direction={"column"} spacing={0} style={{ width: "60%" }}>
                                <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                    CC0 1.0
                                </Text>
                                <Text color={"#666892"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                    1.0 Universal
                                </Text>
                            </Group>
                            <Group>
                                <Icon icon="bi:cc-circle" color="#000" size={48} />
                                <Icon icon="cib:creative-commons-zero" color="#000" size={48} />
                            </Group>
                        </Group>
                        <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                            {t(
                                "By marking the work with a CC0 public domain dedication, the creator is giving up their copyright and allowing reusers to distribute, remix, adapt, and build upon the material in any medium or format, even for commercial purposes."
                            )}
                        </Text>
                    </Group>
                );
            case "Time Machine © 2022 by H. G. Wells is licensed under CC BY 4.0: https://creativecommons.org/licenses/by/4.0/":
                return (
                    <Group>
                        <Group style={{ width: "100%" }} position={"apart"} align={"start"} noWrap={true}>
                            <Group direction={"column"} spacing={0} style={{ width: "60%" }}>
                                <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                    CC BY 4.0
                                </Text>
                                <Text color={"#666892"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                    Attribution 4.0 International
                                </Text>
                            </Group>
                            <Group>
                                <Icon icon="bi:cc-circle" color="#000" size={48} />
                                <Icon icon="cib:creative-commons-by" color="#000" size={48} />
                            </Group>
                        </Group>
                        <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                            {t(
                                "This license requires that reusers give credit to the creator. It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, even for commercial purposes."
                            )}
                        </Text>
                    </Group>
                );
            case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/":
                return (
                    <Group>
                        <Group style={{ width: "100%" }} position={"apart"} align={"start"} noWrap={true}>
                            <Group direction={"column"} spacing={0} style={{ width: "60%" }}>
                                <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                    CC BY-SA 4.0
                                </Text>
                                <Text color={"#666892"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                    Attribution-ShareAlike 4.0 International
                                </Text>
                            </Group>
                            <Group position={"right"}>
                                <Icon icon="bi:cc-circle" color="#000" size={48} />
                                <Icon icon="cib:creative-commons-by" color="#000" size={48} />
                                <Icon icon="cib:creative-commons-sa" color="#000" size={48} />
                            </Group>
                        </Group>
                        <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                            {t(
                                "This license requires that reusers give credit to the creator. It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, even for commercial purposes. If others remix, adapt, or build upon the material, they must license the modified material under identical terms."
                            )}
                        </Text>
                    </Group>
                );

            case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-ND 4.0: https://creativecommons.org/licenses/by-nd/4.0/":
                return (
                    <Group>
                        <Group style={{ width: "100%" }} position={"apart"} align={"start"} noWrap={true}>
                            <Group direction={"column"} spacing={0} style={{ width: "60%" }}>
                                <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                    CC BY-ND 4.0
                                </Text>
                                <Text color={"#666892"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                    Attribution-NoDerivatives 4.0 International
                                </Text>
                            </Group>
                            <Group position={"right"}>
                                <Icon icon="bi:cc-circle" color="#000" size={48} />
                                <Icon icon="cib:creative-commons-by" color="#000" size={48} />
                                <Icon icon="cib:creative-commons-nd" color="#000" size={48} />
                            </Group>
                        </Group>
                        <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                            {t(
                                "This license requires that reusers give credit to the creator. It allows reusers to copy and distribute the material in any medium or format in unadapted form only, even for commercial purposes."
                            )}
                        </Text>
                    </Group>
                );

            case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC 4.0: https://creativecommons.org/licenses/by-nc/4.0/":
                return (
                    <Group>
                        <Group style={{ width: "100%" }} position={"apart"} align={"start"} noWrap={true}>
                            <Group direction={"column"} spacing={0} style={{ width: "60%" }}>
                                <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                    CC BY-NC 4.0
                                </Text>
                                <Text color={"#666892"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                    Attribution-NonCommercial 4.0 International
                                </Text>
                            </Group>
                            <Group position={"right"}>
                                <Icon icon="bi:cc-circle" color="#000" size={48} />
                                <Icon icon="cib:creative-commons-by" color="#000" size={48} />
                                <Icon icon="cib:creative-commons-nc" color="#000" size={48} />
                            </Group>
                        </Group>
                        <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                            {t(
                                "This license requires that reusers give credit to the creator. It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, for noncommercial purposes only."
                            )}
                        </Text>
                    </Group>
                );
            case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/":
                return (
                    <Group>
                        <Group style={{ width: "100%" }} position={"apart"} align={"start"} noWrap={true}>
                            <Group direction={"column"} spacing={0} style={{ width: "100%" }}>
                                <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                    CC BY-NC-SA 4.0
                                </Text>
                                <Text color={"#666892"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                    Attribution-NonCommercial-ShareAlike 4.0 International
                                </Text>
                            </Group>
                            <Group position={"right"}>
                                <Icon icon="bi:cc-circle" color="#000" size={48} />
                                <Icon icon="cib:creative-commons-by" color="#000" size={48} />
                                <Icon icon="cib:creative-commons-nc" color="#000" size={48} />
                                <Icon icon="cib:creative-commons-sa" color="#000" size={48} />
                            </Group>
                        </Group>
                        <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                            {t(
                                "This license requires that reusers give credit to the creator. It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, for noncommercial purposes only. If others modify or adapt the material, they must license the modified material under identical terms."
                            )}
                        </Text>
                    </Group>
                );
            case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-ND 4.0: https://creativecommons.org/licenses/by-nc-nd/4.0/":
                return (
                    <Group>
                        <Group style={{ width: "100%" }} position={"apart"} align={"start"} noWrap={true}>
                            <Group direction={"column"} spacing={0} style={{ width: "100%" }}>
                                <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                    CC BY-NC-ND 4.0
                                </Text>
                                <Text color={"#666892"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                    Attribution-NonCommercial-NoDerivatives 4.0 International
                                </Text>
                            </Group>
                            <Group position={"right"}>
                                <Icon icon="bi:cc-circle" color="#000" size={48} />
                                <Icon icon="cib:creative-commons-by" color="#000" size={48} />
                                <Icon icon="cib:creative-commons-nc" color="#000" size={48} />
                                <Icon icon="cib:creative-commons-nd" color="#000" size={48} />
                            </Group>
                        </Group>
                        <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                            {t(
                                "This license requires that reusers give credit to the creator. It allows reusers to copy and distribute the material in any medium or format in unadapted form and for noncommercial purposes only."
                            )}
                        </Text>
                    </Group>
                );
            default:
                break;
        }
    };
    const onSubmit = useCallback(() => {
        setLicenceParameters({ type: watch("licenseHash"), price: watch("amountWei") });
        if (!!draftStatusBeforeProtect && draftStatusBeforeProtect === "upgrade") {
            handleSwitch(ModalContent.PaymentChoice);
        } else {
            handleSwitch(ModalContent.Preferences);
        }
    }, [draftStatusBeforeProtect, handleSwitch, setLicenceParameters, watch]);

    return (
        <Stack spacing={0}>
            <Group onClick={closeHandler} p={2} sx={{ position: "absolute", right: "-36px", top: 0, backgroundColor: "#fff", borderRadius: 100 }}>
                <Icon icon={"ep:close-bold"} color={"#666892"} size={16} />
            </Group>

            <Group
                spacing={0}
                style={{
                    display: "flex",
                    alignItems: "stretch",
                    backgroundColor: "#F7F8FF",
                    borderRadius: "12px",
                    overflow: "hidden",
                }}
            >
                <Group
                    spacing={0}
                    pt={32}
                    pb={64}
                    direction={"column"}
                    style={{
                        boxSizing: "border-box",
                        height: "100%",
                        width: "440px",
                        borderRight: "1px solid #DDDEEE",
                        backgroundColor: "#fff",
                    }}
                >
                    <Text pl={32} pb={17} color="#252733" weight={"700"} sx={{ fontSize: 24, lineHeight: "30px" }}>
                        {t("License & mode")}
                    </Text>
                    <form
                        onSubmit={(event) => {
                            event.preventDefault();
                        }}
                        style={{ borderTop: "1px solid #DDDEEE" }}
                    >
                        <Group spacing={0} style={{ width: "440px" }}>
                            <Group
                                spacing={10}
                                pl={32}
                                pt={26}
                                direction={"column"}
                                style={{
                                    width: "100%",
                                    background: radioButton === "© all rights reserved, commercial distribution" ? "#EDEEFF" : "none",
                                }}
                                position={"apart"}
                            >
                                <Group pb={26} style={{ width: "100%", borderBottom: "1px solid #DDDEEE" }}>
                                    <Group>
                                        <Controller
                                            name="licenseHash"
                                            control={control}
                                            // defaultValue="Copyrighted (pay-to-use)"
                                            render={({ field }) => (
                                                <Group spacing={0}>
                                                    <RadioButton
                                                        {...field}
                                                        // disabled={loading}
                                                        // defaultChecked={true}
                                                        onClick={() => {
                                                            setRadioButton("© all rights reserved, commercial distribution");
                                                        }}
                                                        checked={radioButton === "© all rights reserved, commercial distribution"}
                                                        type="radio"
                                                        value="© all rights reserved, commercial distribution"
                                                    />
                                                    <Label>Public, commercial distribution</Label>
                                                </Group>
                                            )}
                                        />
                                    </Group>
                                    <Group spacing={0} direction={"column"} style={{ marginLeft: "35px" }}>
                                        <SubTitle>Reuse price</SubTitle>
                                        <Controller
                                            name="amountWei"
                                            control={control}
                                            render={({ field }) => (
                                                <NumberInput
                                                    disabled={watch("licenseHash") !== "© all rights reserved, commercial distribution"}
                                                    {...field}
                                                    value={Number(field.value || 0)}
                                                    placeholder="10"
                                                    onInput={(event: any) => {
                                                        if (event.target.value > 1000000) {
                                                            event.target.value = 1000000;
                                                        } else if (event.target.value < 0) {
                                                            event.target.value = 0;
                                                        }
                                                    }}
                                                    styles={{ rightSection: { marginRight: "15px", color: "#666892" } }}
                                                    style={{ width: "250px", margin: "6px 0px" }}
                                                    rightSection={<Text style={{ wordBreak: "keep-all" }}>US$</Text>}
                                                    hideControls
                                                    parser={(value) => value?.replace(/\D/g, "")}
                                                    onChange={(value) => field.onChange(value)}
                                                />
                                            )}
                                        />
                                    </Group>
                                </Group>
                            </Group>
                            <Group
                                spacing={10}
                                pl={32}
                                pt={26}
                                direction={"column"}
                                style={{ width: "100%", background: radioButton === "© all rights reserved, no distribution" ? "#EDEEFF" : "none" }}
                                position={"apart"}
                            >
                                <Group pb={26} style={{ width: "100%", borderBottom: "1px solid #DDDEEE" }}>
                                    <Group>
                                        <Controller
                                            name="licenseHash"
                                            control={control}
                                            // defaultValue="Copyrighted (pay-to-use)"
                                            render={({ field }) => (
                                                <Group spacing={0}>
                                                    <RadioButton
                                                        {...field}
                                                        // disabled={loading}
                                                        defaultChecked={false}
                                                        onClick={() => {
                                                            setRadioButton("© all rights reserved, no distribution");
                                                        }}
                                                        checked={radioButton === "© all rights reserved, no distribution"}
                                                        type="radio"
                                                        value="© all rights reserved, no distribution"
                                                    />
                                                    <Label>Public, no distribution</Label>
                                                </Group>
                                            )}
                                        />
                                    </Group>
                                </Group>
                            </Group>
                            <Group
                                spacing={10}
                                pl={32}
                                pt={26}
                                direction={"column"}
                                style={{ width: "100%", background: radioButton === "Public, non-commercial distribution" ? "#EDEEFF" : "none" }}
                                position={"apart"}
                            >
                                <Group pb={26} style={{ width: "100%", borderBottom: "1px solid #DDDEEE" }}>
                                    <Group>
                                        <Controller
                                            name="licenseHash"
                                            control={control}
                                            render={({ field }) => (
                                                <Group spacing={0}>
                                                    <RadioButton
                                                        {...field}
                                                        // disabled={loading}
                                                        onClick={() => {
                                                            setRadioButton("Public, non-commercial distribution");
                                                        }}
                                                        checked={radioButton === "Public, non-commercial distribution"}
                                                        type="radio"
                                                        value="Public, non-commercial distribution"
                                                    />
                                                    <Label>Public, non-commercial distribution</Label>
                                                </Group>
                                            )}
                                        />
                                    </Group>

                                    <Group spacing={0} direction={"column"} style={{ marginLeft: "35px" }}>
                                        <SubTitle>Creative commons license</SubTitle>
                                        <Controller
                                            name="licenseHash"
                                            control={control}
                                            render={({ field }) => (
                                                <Select
                                                    {...field}
                                                    style={{ width: "250px" }}
                                                    disabled={
                                                        watch("licenseHash") !== "Public, non-commercial distribution" &&
                                                        watch("licenseHash") !==
                                                            "Time Machine © 2022 by H. G. Wells is licensed under CC0 1.0: https://creativecommons.org/publicdomain/zero/1.0/" &&
                                                        watch("licenseHash") !==
                                                            "Time Machine © 2022 by H. G. Wells is licensed under CC BY 4.0: https://creativecommons.org/licenses/by/4.0/" &&
                                                        watch("licenseHash") !==
                                                            "Time Machine © 2022 by H. G. Wells is licensed under CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/" &&
                                                        watch("licenseHash") !==
                                                            "Time Machine © 2022 by H. G. Wells is licensed under CC BY-ND 4.0: https://creativecommons.org/licenses/by-nd/4.0/" &&
                                                        watch("licenseHash") !==
                                                            "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC 4.0: https://creativecommons.org/licenses/by-nc/4.0/" &&
                                                        watch("licenseHash") !==
                                                            "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/" &&
                                                        watch("licenseHash") !==
                                                            "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-ND 4.0: https://creativecommons.org/licenses/by-nc-nd/4.0/"
                                                    }
                                                    onChange={(e) => {
                                                        field.onChange(e);
                                                        // handleChangeType();
                                                    }}
                                                    data={licenceData}
                                                    styles={{
                                                        rightSection: { pointerEvents: "none" },
                                                        item: { background: "transparent !important", color: "#0A001E !important" },
                                                    }}
                                                    rightSection={<Icon icon="material-symbols:arrow-drop-down-rounded" color="#666892" hoverColor="#666892" />}
                                                    required={true}
                                                />
                                            )}
                                        />
                                        <a
                                            href="https://chooser-beta.creativecommons.org/"
                                            target={"__blank"}
                                            style={{ color: "blue", textDecoration: "underline", paddingTop: "8px" }}
                                        >
                                            I need help choosing a license
                                        </a>
                                    </Group>
                                </Group>
                            </Group>
                            <Group
                                spacing={10}
                                pl={32}
                                pt={26}
                                direction={"column"}
                                style={{ width: "100%", background: radioButton === "Private, no distribution" ? "#EDEEFF" : "none" }}
                                position={"apart"}
                            >
                                <Group pb={26} style={{ width: "100" }} position={"apart"}>
                                    <Controller
                                        name="licenseHash"
                                        control={control}
                                        // defaultValue="Copyrighted (pay-to-use)"
                                        render={({ field }) => (
                                            <Group spacing={0}>
                                                <RadioButton
                                                    {...field}
                                                    disabled={true}
                                                    onClick={() => {
                                                        setRadioButton("Private, no distribution");
                                                    }}
                                                    checked={radioButton === "Private, no distribution"}
                                                    type="radio"
                                                    value="Private, no distribution"
                                                />
                                                <Label style={{ color: "#B8BDE0" }}>Private, no distribution</Label>
                                            </Group>
                                        )}
                                    />
                                    <SharkButton
                                        disabled={true}
                                        variant={"filled"}
                                        buttonType={"primary"}
                                        size={"small"}
                                        style={{ borderRadius: "24px", height: "24px", marginRight: "32px" }}
                                    >
                                        soon
                                    </SharkButton>
                                </Group>
                            </Group>
                        </Group>

                        <Group noWrap spacing={0} px={32} py={16} align={"start"} style={{ borderTop: "1px solid #DDDEEE" }}>
                            <Checkbox checked={mainCheckboxSharp} onChange={() => setMainCheckboxSharp(!mainCheckboxSharp)} />
                            <Label>
                                I’m okay with the content going into public databases and can’t be deleted,{" "}
                                <a href="https://creativecommons.org/" target={"__blank"} style={{ color: "blue", textDecoration: "underline" }}>
                                    I have read about it
                                </a>
                            </Label>
                        </Group>
                        <Group spacing={12} pt={32} position={"right"} px={32} style={{ width: "100%" }}>
                            <SharkButton onClick={closeHandler} type={"button"} variant={"outlined"} buttonType={"secondary"}>
                                × Cancel
                            </SharkButton>
                            <SharkButton
                                disabled={
                                    !mainCheckboxSharp ||
                                    !watch("licenseHash") ||
                                    (watch("licenseHash") === "© all rights reserved, commercial distribution" &&
                                        (!watch("amountWei") || watch("amountWei") === "0"))
                                }
                                variant={"filled"}
                                buttonType={"primary"}
                                onClick={onSubmit}
                            >
                                Protect
                            </SharkButton>
                        </Group>
                    </form>
                </Group>
                <Group
                    px={32}
                    style={{
                        display: "flex",
                        width: "390px",
                        height: "100%",
                        backgroundColor: "#F7F8FF",
                        borderTopRightRadius: "12px",
                        borderBottomRightRadius: "12px",
                    }}
                >
                    {(watch("licenseHash") === "© all rights reserved, commercial distribution" ||
                        watch("licenseHash") === "© all rights reserved, no distribution" ||
                        watch("licenseHash") === undefined) && (
                        <Group direction={"column"} pt={32}>
                            <Group style={{ width: "100%" }} position={"apart"}>
                                <Group direction={"column"} spacing={0}>
                                    <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 24, lineHeight: "30px" }}>
                                        {"Copyright:"}
                                    </Text>
                                    <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 24, lineHeight: "30px" }}>
                                        {t("All rights reserved")}
                                    </Text>
                                </Group>
                                <Icon icon="ic:round-copyright" color="#000" size={64} />
                            </Group>
                            {(watch("licenseHash") === "© all rights reserved, commercial distribution" || watch("licenseHash") === undefined) && (
                                <>
                                    <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                                        {t(
                                            "You keep all the rights for this work. Nobody can reuse, distribute, adapt, or resell any part of this work or its adaprations unless they pay for it / get your written permission beforehand."
                                        )}
                                    </Text>
                                    <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                                        {t(
                                            "The only one way your work can be used freely is “fair use” — it’s when a small portion of it is quoted and you’re properly mentioned as its rightsholder."
                                        )}
                                    </Text>
                                    <a
                                        href="https://en.wikipedia.org/wiki/Copyright_notice"
                                        target="_blank"
                                        style={{
                                            marginRight: "8px",
                                            fontWeight: "400",
                                            fontSize: "16px",
                                            lineHeight: "20px",
                                            color: "#4700E5",
                                            textDecoration: "underline",
                                        }}
                                        rel="noreferrer"
                                    >
                                        <Group spacing={4}>
                                            See the license info
                                            <Icon icon="ic:baseline-launch" color="#4700E5" />
                                        </Group>
                                    </a>
                                </>
                            )}
                            {watch("licenseHash") === "© all rights reserved, no distribution" && (
                                <>
                                    <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                                        {t(
                                            "You keep all the rights for this work. Nobody can reuse, distribute, adapt, or resell any part of this work or its adaprations unless they get your written permission beforehand."
                                        )}
                                    </Text>
                                    <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                                        {t(
                                            "The only one way your work can be used freely is “fair use” — it’s when a small portion of it is quoted and you’re properly mentioned as its rightsholder."
                                        )}
                                    </Text>
                                    <a
                                        href="https://en.wikipedia.org/wiki/Copyright_notice"
                                        target="_blank"
                                        style={{
                                            marginRight: "8px",
                                            fontWeight: "400",
                                            fontSize: "16px",
                                            lineHeight: "20px",
                                            color: "#4700E5",
                                            textDecoration: "underline",
                                        }}
                                        rel="noreferrer"
                                    >
                                        <Group spacing={4}>
                                            See the license info
                                            <Icon icon="ic:baseline-launch" color="#4700E5" />
                                        </Group>
                                    </a>
                                </>
                            )}
                        </Group>
                    )}
                    {radioButton === "Public, non-commercial distribution" &&
                        (watch("licenseHash") ===
                            "Time Machine © 2022 by H. G. Wells is licensed under CC0 1.0: https://creativecommons.org/publicdomain/zero/1.0/" ||
                            watch("licenseHash") ===
                                "Time Machine © 2022 by H. G. Wells is licensed under CC BY 4.0: https://creativecommons.org/licenses/by/4.0/" ||
                            watch("licenseHash") ===
                                "Time Machine © 2022 by H. G. Wells is licensed under CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/" ||
                            watch("licenseHash") ===
                                "Time Machine © 2022 by H. G. Wells is licensed under CC BY-ND 4.0: https://creativecommons.org/licenses/by-nd/4.0/" ||
                            watch("licenseHash") ===
                                "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC 4.0: https://creativecommons.org/licenses/by-nc/4.0/" ||
                            watch("licenseHash") ===
                                "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/" ||
                            watch("licenseHash") ===
                                "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-ND 4.0: https://creativecommons.org/licenses/by-nc-nd/4.0/") && (
                            <Group direction={"column"} pt={32}>
                                <Group style={{ width: "100%" }} position={"apart"}>
                                    <Group direction={"column"} spacing={0}>
                                        <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                            {"Creative Commons:"}
                                        </Text>
                                        <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                            {t("Openly licensed content")}
                                        </Text>
                                    </Group>
                                    <Icon icon="bi:cc-circle" color="#000" size={48} />
                                </Group>
                                <Group>
                                    <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                                        {t(
                                            'Creative Commons legal tools give everyone from individual creators to large companies a simple, standardized way to grant copyright permissions to their creative work. They are designed to forge a balance inside the traditional "all rights reserved" setting that copyright law creates.'
                                        )}
                                    </Text>
                                </Group>
                                {changeRightCC()}
                                <a
                                    href="https://en.wikipedia.org/wiki/Copyright_notice"
                                    target="_blank"
                                    style={{
                                        marginRight: "8px",
                                        fontWeight: "400",
                                        fontSize: "16px",
                                        lineHeight: "20px",
                                        color: "#4700E5",
                                        textDecoration: "underline",
                                    }}
                                    rel="noreferrer"
                                >
                                    <Group spacing={4}>
                                        See the license info
                                        <Icon icon="ic:baseline-launch" color="#4700E5" />
                                    </Group>
                                </a>
                            </Group>
                        )}
                    <Group style={{ width: "100%" }} position={"apart"}>
                        <Group direction={"column"} spacing={0}>
                            <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 24, lineHeight: "30px" }}>
                                {"Mode:"}
                            </Text>
                            <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 24, lineHeight: "30px" }}>
                                {t("Public (unlocked)")}
                            </Text>
                        </Group>
                        <Icon icon="material-symbols:lock-open-rounded" color="#000" size={40} />
                    </Group>
                    <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                        {t("Your work goes to public distributed database as is and can’t be deleted.")}
                    </Text>
                </Group>
            </Group>
        </Stack>
    );
}
const Label = styled.span`
    font-weight: 500;
    font-size: 14px;
    line-height: 20px;
    margin-left: 8px;
`;
const SubTitle = styled.span`
    font-weight: 700;
    font-size: 12px;
    line-height: 16px;
    text-transform: uppercase;
`;
