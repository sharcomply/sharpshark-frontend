import styled from "@emotion/styled";
import { Input } from "components/Input";
import TextField from "components/TextField";
import useApplicationLoader from "features/ApplicationLoader/hooks/useApplicationLoader";
import ArrBack from "features/Icons/ArrBack";
import Logo from "features/Icons/Logo";
import { fakeRequest } from "helpers/fakeRequest";
import { SubmitHandler, useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useModal } from "../hooks/useModal";
import { ModalContent } from "../interfaces";
import { ForgotPasswordInputTypes } from "../interfaces/formTypes";

// Component
export default function ForgotPassword() {
    const { handleSwitch, handleClose } = useModal();
    const loader = useApplicationLoader();
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<ForgotPasswordInputTypes>();

    const onSubmit: SubmitHandler<ForgotPasswordInputTypes> = async () => {
        try {
            loader.start();

            await fakeRequest();

            loader.stop();
            handleClose();
            reset();
        } catch (e) {
            loader.stop();
        }
    };

    const {t} = useTranslation()

    return (
        <>
            <ModalControls>
                <CloseButton onClick={() => handleSwitch(ModalContent.SignIn)}>
                    <span className="icon">
                        <ArrBack />
                    </span>
                    <span className="text">{t("buttons.back_btn")}</span>
                </CloseButton>
                <a href="/" target={"_blank"}>
                    <Logo />
                </a>
            </ModalControls>

            <Text>{t("modals.password.reset_password_text")}</Text>

            <Group onSubmit={handleSubmit(onSubmit)}>
                <TextField label={t("textfields.lables.email")} errorText={errors.email?.message}>
                    <Input placeholder={t("textfields.placeholders.email_address")} {...register("email", { required: true })} />
                </TextField>

                <Button color={"#4700e5"} hoverColor={"#1c00a6"} type="submit">
                    {t('buttons.reset_password_btn')}
                </Button>
            </Group>

            <AdditionalInfo>
                {t("modals.password.dont_have_account")}
                <LinkButton onClick={() => handleSwitch(ModalContent.SignIn)}>Sign in</LinkButton>
                {t("common.or")}
                <LinkButton onClick={() => handleSwitch(ModalContent.SignUp)}>Sign up</LinkButton>
            </AdditionalInfo>
        </>
    );
}

// Styling
const ModalControls = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 12px;

    & a {
        display: flex;
        font-size: 30px;
    }
`;

const CloseButton = styled.button`
    display: flex;
    align-items: center;
    cursor: pointer;
    color: #4700e5;

    & .text {
        display: flex;
        align-items: center;
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }

    & .icon {
        display: flex;
        align-items: center;
        margin-right: 8px;
        font-size: 12px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }
`;

const Text = styled.div`
    font-size: 14px;
    line-height: 1;
    text-align: center;
    color: #666892;
    display: block;
`;

const Group = styled.form`
    display: flex;
    flex-direction: column;
    padding-top: 26px;
`;

const Button = styled.button<{ color?: string; hoverColor?: string }>`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    color: #fff;
    border: 1px solid transparent;

    padding: 12px 16px;
    margin-top: 8px;
    margin-bottom: 8px;
    border-radius: 4px;

    font-weight: 500;
    font-size: 14px;
    line-height: 20px;

    user-select: none;
    transition: background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

    ${({ color }) =>
        color &&
        `
        background-color: ${color};
        border-color: ${color};
    `}

    &:hover {
        ${({ hoverColor }) =>
            hoverColor &&
            `
            background-color: ${hoverColor};
            border-color: ${hoverColor};
        `}
    }
`;

const AdditionalInfo = styled.div`
    margin-top: 12px;
    margin-bottom: 16px;

    text-align: center;
    line-height: 18px;
    font-size: 14px;
    color: #666892;
`;

const LinkButton = styled.button`
    color: #4700e5;
    cursor: pointer;
    text-decoration: none;
    background-color: transparent;
    transition: color 0.2s ease-in-out;
    margin-left: 4px;
    margin-right: 4px;
    font-size: 14px;
    text-decoration: underline;
    position: relative;

    &:hover::before {
        content: "";
        position: absolute;
        bottom: 1px;
        width: 100%;
        height: 1px;
        background-color: #4700e5;
    }
`;
