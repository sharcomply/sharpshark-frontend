import { Group, Loader, Stack, Text } from "@mantine/core";
import { Table } from "components/DataTable";
import Icon from "components/Icon";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import { ScanItem } from "features/Scan/components/ScanItem";
import { TableHead } from "features/Scan/components/TableHead";
import useScan from "features/Scan/hooks/useScan";
import { useCallback, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useModal } from "../hooks/useModal";

// Component
export default function ExistingPublications() {
    const navigate = useNavigate();
    const { handleClose } = useModal();
    const { scanId } = useDraftStore();
    const { scan, isLoading } = useScan(scanId);
    const {t} = useTranslation()
    const closeHandler = useCallback(() => {
        handleClose();
        navigate(
            {
                hash: undefined,
            },
            { replace: true }
        );
    }, [handleClose, navigate]);

    useEffect(() => {
        console.log(scanId);
    }, [scanId]);

    return (
        <Stack spacing={8}>
            <Group onClick={closeHandler} p={2} sx={{ position: "absolute", right: "-36px", top: 0, backgroundColor: "#fff", borderRadius: 100 }}>
                <Icon icon={"ep:close-bold"} color={"#666892"} size={16} />
            </Group>

            {/* Header */}
            <Stack pt={32} px={32} spacing={4}>
                <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 24, lineHeight: "30px" }}>
                    {t("modals.publications.similar_content_title")}
                </Text>
                <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                {t("modals.publications.smimilar_content_text")}
                </Text>
            </Stack>
            {/* Content */}
            <Stack pb={32} spacing={4}>
                <Table>
                    <TableHead
                        items={[
                            { title: t("table.titles.title"), width: "20%" },
                            { title: "", width: "30%" },
                            { title: t("table.titles.url"), width: "35%" },
                            { title: t("table.titles.similarity"), width: "15%" },
                        ]}
                    />

                    <tbody>
                        {scan?.similarContent.map((item) => {
                            return <ScanItem key={item._id} item={item} />;
                        })}

                        {!isLoading && !scan?.similarContent.length && (
                            <tr style={{ borderTop: "1px solid #DDDEEE" }}>
                                <td>
                                    <Group pr={16} pl={32} position="apart" align="center" sx={{ height: 42 }}>
                                        <Text size={"sm"} color={"#666892"} weight={"400"} sx={{ whiteSpace: "nowrap" }}>
                                            {t("table.empty-table")}
                                        </Text>
                                    </Group>
                                </td>
                                <td></td>
                            </tr>
                        )}

                        {!!isLoading && (
                            <tr style={{ borderTop: "1px solid #DDDEEE" }}>
                                <td>
                                    <Group pr={16} pl={32} position="apart" align="center" sx={{ height: 42 }}>
                                        <Loader color={"#666892"} size={"sm"} />
                                    </Group>
                                </td>
                                <td></td>
                            </tr>
                        )}
                    </tbody>
                </Table>
            </Stack>
        </Stack>
    );
}
