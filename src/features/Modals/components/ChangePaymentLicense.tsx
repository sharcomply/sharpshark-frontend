import styled from "@emotion/styled";
import { Stack, Text } from "@mantine/core";
import { useWeb3React } from "@web3-react/core";
import { getCertificatePaymentLink } from "api/requests/drafts";
import { connector } from "blockchain/connector";
import { useContract } from "blockchain/hooks/useContract";
import { SharkButton } from "components/SharkButton";
import { useWhoAmi } from "features/Auth/hooks/useWhoAmi";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import ArrBack from "features/Icons/ArrBack";
import Logo from "features/Icons/Logo";
import { useProtectedDraftStore } from "features/ProtectedDetails/hooks/useProtectedDraftStore";
import { useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useModal } from "../hooks/useModal";
import { ModalContent } from "../interfaces";
import { useNearContext } from "../../../providers/NearProvider";
import { utils } from "near-api-external-link-fork";
import { changeMethods, viewMethods } from "constants/NearContract";
import { useCrossTabState } from "hooks/useCrossTbState";
import { NEAR_CONTRACT } from "../../../constants/providers";
import { getNetworkBySymbol } from "constants/networks";
import { getNetworkById } from "constants/networks";

// Component
export default function ChangePaymentLicense() {
    const { t } = useTranslation();
    const contract = useContract();
    const { handleClose, setModalContent, handleSwitch } = useModal();
    const {
        state: { tokenId },
    } = useProtectedDraftStore();
    const { activate, account, deactivate, chainId } = useWeb3React();
    const { ...whoami } = useWhoAmi();
    const { draftId, mintedNetworks } = useDraftStore();

    const [loadingMetaMask, setLoadingMetaMask] = useState(false);
    const [loadingFiat, setLoadingFiat] = useState(false);
    const Near = useNearContext();
    const [nearConnectToPay, setNearConnectToPay] = useCrossTabState("nearConnectToPay", "");
    const [networkError, setNetworkError] = useState(false);

    const onCloseModal = useCallback(() => {
        handleClose();
    }, [handleClose]);

    const NearLogin = useCallback(async () => {
        await Near?.WalletConnection.requestSignIn({
            contractId: NEAR_CONTRACT,
            methodNames: [...viewMethods, ...changeMethods],
            successUrl: `${window.location.href}?nearConnectToPay=true`,
        });
    }, [Near]);

    const handlePayByMetaMask = useCallback(async () => {
        try {
            setLoadingMetaMask(true);
            setNetworkError(false);

            if (!account) {
                await activate(connector);
            }

            if (account) {
                const price = await contract.getTokenInfo(tokenId!);

                const certificateMintedNetwork = getNetworkBySymbol(mintedNetworks);
                const currentNetwork = getNetworkById(chainId?.toString());

                if (currentNetwork !== certificateMintedNetwork) {
                    setNetworkError(true);
                }

                const { wait } = await contract.buyCert(tokenId!, { value: price[0].toString() });
                await wait();

                console.log("BUY", price[0].toString());
                setModalContent(t("modals.purchase_success", { tokenId: tokenId }));
                handleSwitch(ModalContent.CustomModal);
                setLoadingMetaMask(false);
                deactivate();
            }
        } catch (error) {
            console.log(error);
            setLoadingMetaMask(false);
        }
    }, [contract, activate, account, deactivate, tokenId, setModalContent, handleSwitch]);

    useEffect(() => {
        if (!account) return;
        if (!!whoami.data?.user) return;
        handlePayByMetaMask();
    }, [account]);

    const handlePayByFiat = useCallback(async () => {
        try {
            setLoadingFiat(true);
            console.log(draftId);
            await getCertificatePaymentLink({
                id: draftId!,
                network: mintedNetworks!,
                successUrl: `${window.location.href}?stripePayLicenceSucces=true`,
                cancelUrl: `${window.location.href}?stripePayLicenceError=true`,
            }).then((data) => {
                window.open(data.url);
            });
            setLoadingFiat(false);
            handleClose();
        } catch (error) {
            setLoadingFiat(false);
        }
    }, [draftId, handleClose]);

    const handlePayByNear = useCallback(async () => {
        try {
            setNearConnectToPay("false");
            setLoadingMetaMask(true);
            if (Near?.WalletConnection.account().accountId) {
                const tokenInfo = await Near?.Contract.get_token_info({ token_id: tokenId });
                console.log("PRICE_LICENCE", tokenInfo);

                const amountInNEAR = utils.format.formatNearAmount(tokenInfo.sale_amount);
                const amountInYocto = utils.format.parseNearAmount(`${Number(amountInNEAR) + 0.05}`);
                handleClose();
                localStorage.setItem(`${window.location.href}`, 'true')
                const res = await Near?.Contract.buy_cert({
                    amount: amountInYocto,
                    args: { token_id: tokenId },
                    callbackUrl: `${window.location.href}?nearPayLicenceSucces=true`,
                });
                console.log(res);
            } else {
                setLoadingMetaMask(true);
                await NearLogin();
            }
        } catch (error) {
            console.log(error);
            setLoadingMetaMask(false);
        }
    }, [contract, account, tokenId, setModalContent, handleSwitch, whoami]);

    useEffect(() => {
        let timer1: any;
        if (nearConnectToPay === "true") {
            timer1 = setTimeout(handlePayByNear, 3000);
        }
        return () => {
            clearTimeout(timer1);
        };
    }, [nearConnectToPay, handlePayByNear]);

    return (
        <ModalContainer>
            <ModalControls>
                <CloseButton onClick={onCloseModal}>
                    <span className="icon">
                        <ArrBack />
                    </span>
                    <span className="text">{t("buttons.back_btn")}</span>
                </CloseButton>
                <a href="/" target={"_blank"}>
                    <Logo />
                </a>
            </ModalControls>

            <ModalHeader>
                <h2>Buy licence</h2>
            </ModalHeader>

            <Stack mt={32} spacing={16} align="center">
                {mintedNetworks === "near" ? (
                    <SharkButton
                        onClick={handlePayByNear}
                        loading={loadingMetaMask}
                        disabled={loadingMetaMask || loadingFiat}
                        buttonType={"secondary"}
                        variant={"filled"}
                    >
                        Pay with near
                    </SharkButton>
                ) : (
                    <SharkButton
                        onClick={handlePayByMetaMask}
                        loading={loadingMetaMask}
                        disabled={loadingMetaMask || loadingFiat}
                        buttonType={"secondary"}
                        variant={"filled"}
                    >
                        {t("buttons.metamask_pay_btn")}
                    </SharkButton>
                )}

                <SharkButton
                    loading={loadingFiat}
                    disabled={loadingMetaMask || loadingFiat}
                    onClick={handlePayByFiat}
                    buttonType={"secondary"}
                    variant={"filled"}
                >
                    {t("buttons.fiat_pay_btn")}
                </SharkButton>

                {networkError && (
                    <Text align={"center"} color={"red"}>
                        {t("certificate.network_error")}
                    </Text>
                )}
            </Stack>
        </ModalContainer>
    );
}

// Styling

const ModalControls = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;

    & a {
        display: flex;
        font-size: 30px;
    }
`;

const CloseButton = styled.button`
    display: flex;
    align-items: center;
    cursor: pointer;
    color: #4700e5;

    & .text {
        display: flex;
        align-items: center;
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }

    & .icon {
        display: flex;
        align-items: center;
        margin-right: 8px;
        font-size: 12px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }
`;

const ModalContainer = styled.div`
    /* width: 400px; */
    padding: 32px;
`;

const ModalHeader = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    position: relative;
    margin-top: 25px;

    & h2 {
        font-size: 24px;
        line-height: 30px;
        font-weight: 700;
    }

    & a {
        display: flex;
        font-size: 60px;
    }
`;
