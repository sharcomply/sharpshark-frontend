import styled from "@emotion/styled";
import { Checkbox, Group, Stack, Text } from "@mantine/core";
import Icon from "components/Icon";
import { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useModal } from "../hooks/useModal";
import Section from "../../Sidebar/components/Section";
import { SharkButton } from "../../../components/SharkButton";
import useDisputesStore from "../../DisputesDetail/hooks/useDisputesStore";
import { ModalContent } from "../interfaces";

// Component
export default function TakeItToCourt() {
    const navigate = useNavigate();
    const { handleClose } = useModal();
    const { setDisputModalResponse } = useDisputesStore();
    const { t } = useTranslation();
    const [checked, setChecked] = useState(false);
    const closeHandler = useCallback(() => {
        handleClose();
        navigate(
            {
                hash: undefined,
            },
            { replace: true }
        );
    }, [handleClose, navigate]);

    const onSubmit = useCallback(() => {
        setDisputModalResponse(ModalContent.TakeItToCourt);
        closeHandler();
    }, [setDisputModalResponse, closeHandler]);

    return (
        <Stack spacing={8}>
            <Group onClick={closeHandler} p={2} sx={{ position: "absolute", right: "-36px", top: 0, backgroundColor: "#fff", borderRadius: 100 }}>
                <Icon icon={"ep:close-bold"} color={"#666892"} size={16} />
            </Group>
            <Stack pt={32} px={32} spacing={4}>
                <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 24, lineHeight: "30px" }}>
                    {t("Do you want to take it to court?")}
                </Text>
                <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                    {t(
                        "If search engine didn’t react in 14 business days, you can take this case to the court. For this, you will need help of a qualified lawyer."
                    )}
                </Text>
                <br />
                <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px", marginBottom: "24px" }}>
                    {t("We can provide you with contacts of our affiliated lawyer and send all the previously created evidence so you can use it at court.")}
                </Text>
                <Section>
                    <Checkbox onClick={() => setChecked(!checked)} label="I’d like to proceed with the case outside of SharpShark" />
                </Section>
            </Stack>
            <Footer>
                <Group spacing={12}>
                    <Group spacing={8}></Group>
                    <SharkButton variant="outlined" buttonType="secondary" px={16} py={11}>
                        × Cancel
                    </SharkButton>
                    <SharkButton disabled={!checked} variant="filled" buttonType="secondary" onClick={onSubmit} px={16} py={11}>
                        Get case materials and lawyers’ info
                    </SharkButton>
                </Group>
            </Footer>
        </Stack>
    );
}
const Footer = styled(Group)`
    position: relative;
    background-color: #ffffff;
    border-top: 1px solid #dddeee;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
    padding: 14px 32px;
    height: 103px;
    margin-top: 30px;
    overflow: hidden;
    justify-content: flex-end;
`;
