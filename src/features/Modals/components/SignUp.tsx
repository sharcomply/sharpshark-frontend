import styled from "@emotion/styled";
import ArrBack from "features/Icons/ArrBack";
import Logo from "features/Icons/Logo";
import { useTranslation } from "react-i18next";
import { useModal } from "../hooks/useModal";
import { ModalContent } from "../interfaces";

// Component
export default function SignUp() {
    const { handleSwitch } = useModal();
    const {t} = useTranslation()
    return (
        <>
            <ModalControls>
                <CloseButton onClick={() => handleSwitch(ModalContent.SignIn)}>
                    <span className="icon">
                        <ArrBack />
                    </span>
                    <span className="text">{t("buttons.back_btn")}</span>
                </CloseButton>
            </ModalControls>

            <ModalHeader>
                <h2 dangerouslySetInnerHTML={{__html: t("modals.sign_up.title")}}/>
                <a href="/" target={"_blank"}>
                    <Logo />
                </a>
            </ModalHeader>

            <Text>
                <span>{t("modals.sign_up.lets_meet")}</span>
                <span className="bold-text">{t("modals.sign_up.you_are")}</span>
            </Text>

            <ButtonGroup>
                <OutlineButton color={"#4700e5"} hoverColor={"#1c00a6"} onClick={() => handleSwitch(ModalContent.TeamSignUp)}>
                    {t("modals.sign_up.head_of_team")}
                </OutlineButton>
                <Button color={"#4700e5"} hoverColor={"#1c00a6"} onClick={() => handleSwitch(ModalContent.IndividualSignUp)}>
                    {t("modals.sign_up.individual")}
                </Button>
            </ButtonGroup>

            <AdditionalInfo>
                {t("modals.sign_up.have_an_account")}
                <LinkButton onClick={() => handleSwitch(ModalContent.SignIn)}>{t("buttons.sign_in_btn")}</LinkButton>
            </AdditionalInfo>
        </>
    );
}

// Styling
const ModalControls = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    margin-bottom: 12px;
`;

const CloseButton = styled.button`
    display: flex;
    align-items: center;
    cursor: pointer;
    color: #4700e5;

    & .text {
        display: flex;
        align-items: center;
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }

    & .icon {
        display: flex;
        align-items: center;
        margin-right: 8px;
        font-size: 12px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }
`;

const ModalHeader = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 25px;

    & h2 {
        display: flex;
        font-size: 28px;
        max-width: 170px;
        margin: 0;
        width: 100%;
        line-height: 1.3;
        font-weight: 700;
    }

    & a {
        display: flex;
        font-size: 60px;
    }
`;

const Text = styled.div`
    font-size: 14px;
    line-height: 1;
    text-align: center;
    color: #666892;
    display: block;

    & span.bold-text {
        font-size: 12px;
        text-transform: uppercase;
        font-weight: 700;
        color: #0a001e;
        display: block;
        margin-top: 25px;
    }
`;

const ButtonGroup = styled.div`
    display: flex;
    flex-direction: column;
    padding-top: 26px;
`;

const Button = styled.button<{ color?: string; hoverColor?: string }>`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    color: #fff;
    border: 1px solid transparent;

    padding: 12px 16px;
    margin-top: 8px;
    margin-bottom: 8px;
    border-radius: 4px;

    font-weight: 500;
    font-size: 14px;
    line-height: 20px;

    user-select: none;
    transition: background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

    ${({ color }) =>
        color &&
        `
        background-color: ${color};
        border-color: ${color};
    `}

    &:hover {
        ${({ hoverColor }) =>
            hoverColor &&
            `
            background-color: ${hoverColor};
            border-color: ${hoverColor};
        `}
    }
`;

const OutlineButton = styled(Button)`
    color: #4700e5;
    background: #fcfdfe;
    border: 1px solid #dddeee;

    &:hover {
        background: #fcfdfe;
        border: 1px solid #4700e5;
    }
`;

const AdditionalInfo = styled.div`
    margin-top: 12px;
    margin-bottom: 16px;

    text-align: center;
    line-height: 18px;
    font-size: 14px;
    color: #666892;
`;

const LinkButton = styled.button`
    color: #4700e5;
    cursor: pointer;
    text-decoration: none;
    background-color: transparent;
    transition: color 0.2s ease-in-out;
    margin-left: 4px;
    font-size: 14px;
    text-decoration: underline;
    position: relative;

    &:hover::before {
        content: "";
        position: absolute;
        bottom: 1px;
        width: 100%;
        height: 1px;
        background-color: #4700e5;
    }
`;
