import styled from "@emotion/styled";
import { Group, Stack, Text, Textarea } from "@mantine/core";
import Icon from "components/Icon";
import { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useModal } from "../hooks/useModal";
import CopyButton from "./CopyButton";
import ReactDOMServer from "react-dom/server";
import { UseTheContentButton } from "./UseTheContentButton";
import { UseTheContentLink } from "./UseTheContentLink";
import useDraft from "../../DraftDetails/hooks/useDraft";
import { useProtectedDraftStore } from "../../ProtectedDetails/hooks/useProtectedDraftStore";

// Component
export default function UsingYourContent() {
    const navigate = useNavigate();
    const { handleClose } = useModal();
    const { t } = useTranslation();
    const [htmlString, setHtmlString] = useState("");
    const closeHandler = useCallback(() => {
        handleClose();
        navigate(
            {
                hash: undefined,
            },
            { replace: true }
        );
    }, [handleClose, navigate]);

    const { state } = useProtectedDraftStore();
    const { draft } = useDraft(state.draftId);

    const tokenId = draft?.tokenId;

    const handleButtonCopyClick = () => {
        setHtmlString(ReactDOMServer.renderToString(<UseTheContentButton id={tokenId} />));
        navigator.clipboard.writeText(ReactDOMServer.renderToString(<UseTheContentButton id={tokenId} />));
    };
    const handleLinkCopyClick = () => {
        setHtmlString(ReactDOMServer.renderToString(<UseTheContentLink id={tokenId} />));
        navigator.clipboard.writeText(ReactDOMServer.renderToString(<UseTheContentLink id={tokenId} />));
    };

    return (
        <Stack spacing={8}>
            <Group onClick={closeHandler} p={2} sx={{ position: "absolute", right: "-36px", top: 0, backgroundColor: "#fff", borderRadius: 100 }}>
                <Icon icon={"ep:close-bold"} color={"#666892"} size={16} />
            </Group>
            <Stack p={32} sx={{ width: 480 }}>
                {/* Header */}
                <Stack spacing={4}>
                    <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 24, lineHeight: "30px" }}>
                        {t("modals.usingYourContent.title")}
                    </Text>
                    <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                        {t("modals.usingYourContent.text")}
                    </Text>
                </Stack>
                {/* Content */}
                <Stack spacing={8}>
                    <Stack>
                        <Group position={"apart"}>
                            <Group>
                                <UseTheContentLink id={tokenId} />
                            </Group>

                            <CopyButton onClick={handleLinkCopyClick}>Copy link</CopyButton>
                        </Group>
                        <Group position={"apart"}>
                            <Group>
                                <UseTheContentButton id={tokenId} />
                            </Group>
                            <CopyButton onClick={handleButtonCopyClick}>Copy link</CopyButton>
                        </Group>
                    </Stack>
                    <TextArea
                        defaultValue={htmlString}
                        disabled={true}
                        minRows={2}
                        id="text"
                        placeholder="HTML code"
                        autosize
                        variant={"unstyled"}
                        styles={{
                            root: { flex: 1 },
                            wrapper: { height: "100%", zIndex: 1 },
                            input: { minHeight: "100%!important", height: "100%!important", overflow: "hidden" },
                        }}
                    />
                </Stack>
            </Stack>
        </Stack>
    );
}
const TextArea = styled(Textarea)`
    border: 1px solid #dddeee;
    border-radius: 8px;
    //padding: 8px 16px;

    #text {
        padding: 8px 16px;
        font-weight: 400;
        font-size: 16px;
        line-height: 20px;
        color: #0a001e;
    }

    #text::placeholder {
        font-weight: 400;
        font-size: 16px;
        line-height: 20px;
        color: #666892;
    }
`;
