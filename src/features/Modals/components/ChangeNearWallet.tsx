import styled from "@emotion/styled";
import { Input } from "components/Input";
import TextField from "components/TextField";
import { useWhoAmi } from "features/Auth/hooks/useWhoAmi";
import useSettingStore from "features/Settings/hooks/useSettingStore";
import { useCallback, useState } from "react";
import { useForm } from "react-hook-form";
import closeIcon from "../../../assets/images/close-icon.svg";
import { useModal } from "../hooks/useModal";
import { ChangeNearAddressTypes} from "../interfaces/formTypes";
import {changeNearAddressRequest} from "../../../api/requests/auth";
import {useNearContext} from "../../../providers/NearProvider";

// Component
export default function ChangeNearWallet() {
    const { handleClose } = useModal();
    const [loading, setloading] = useState(false);
    const { register, handleSubmit } = useForm<ChangeNearAddressTypes>();
    const { setNearAddress } = useSettingStore();
    const {data} = useWhoAmi()
    const Near = useNearContext()

    const { refetch } = useWhoAmi();

    const onSubmit = useCallback(
        async (form: ChangeNearAddressTypes) => {
            try {
                setloading(true);

                const response = await changeNearAddressRequest(form);
                console.log(response)
                await refetch()
                setNearAddress(form.walletAddressNear );

                setloading(false);
                handleClose();
            } catch (error) {
                setloading(false);
            }
        },
        [handleClose, refetch, setNearAddress]
    );
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <ModalHeader>
                <h2>Add near address</h2>
                <p>You wouldn't be able change this <br/> address in future</p>
                <CloseButton onClick={handleClose}>
                    <img src={closeIcon} alt=""></img>
                </CloseButton>
            </ModalHeader>

            <TextField label={"Near Address"}>
                <Input  defaultValue={data?.user.walletAddressNear || Near?.WalletConnection.account().accountId} placeholder={"New Near Address"} {...register("walletAddressNear", { required: true })} />
            </TextField>

            <Button type={"submit"} color={"#4700e5"} hoverColor={"#1c00a6"} onClick={handleClose} disabled={loading}>
                Confirm
            </Button>
        </form>
    );
}

// Styling
const ModalHeader = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    margin-bottom: 20px;
    position: relative;

    & h2 {
        display: flex;
        font-size: 28px;
        margin: 0;
        width: 100%;
        line-height: 1.3;
        font-weight: 700;
    }

    & a {
        display: flex;
        font-size: 60px;
    }
`;

const Button = styled.button<{ color: string; hoverColor: string }>`
    display: flex;
    width: 100%;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    color: #fff;
    border: 1px solid transparent;

    padding: 12px 16px;
    margin-top: 8px;
    margin-bottom: 8px;
    border-radius: 4px;

    font-weight: 500;
    font-size: 14px;
    line-height: 20px;

    user-select: none;
    transition: background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

    ${({ color }) => `
        background-color: ${color};
        border-color: ${color};
    `}

    &:hover {
        ${({ hoverColor }) => `
            background-color: ${hoverColor};
            border-color: ${hoverColor};
        `}
    }

    &:disabled {
        opacity: 0.6;
    }
`;

const CloseButton = styled.div`
    cursor: pointer;
    position: absolute;
    width: 16px;
    height: 16px;
    right: -75px;
    top: -25px;
`;
