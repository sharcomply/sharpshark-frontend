import styled from "@emotion/styled";
import { sendKYCVerification } from "api/requests/auth";
import { Input } from "components/Input";
import SelectField from "components/SelectField";
import { country } from "components/SelectField/data";
import { SharkButton } from "components/SharkButton";
import TextField from "components/TextField";
import ArrBack from "features/Icons/ArrBack";
import Logo from "features/Icons/Logo";
import { setVerificationStorage } from "helpers/verificationStorage";
import { useCallback, useEffect, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { useModal } from "../hooks/useModal";
import { KYCVerificationTypes } from "../interfaces/formTypes";
import { object, string } from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { Alert } from "../../DeletedDraftDetails/components/Alert";
import { useTranslation } from "react-i18next";

import { useCountriesList } from "../hooks/useCountriesList";
import { useWhoAmi } from "features/Auth/hooks/useWhoAmi";
import useAuthStore from "features/Auth/hooks/useAuthStore";

export const validationSchema = object().shape({
    email: string().required("Required field").max(100),
    country: string().required("Required field").max(100),
});

// Component
export default function VerificationForm() {
    const { handleClose } = useModal();
    const { t } = useTranslation();
    const [messageError, setMessageError] = useState("");
    const { refetch } = useWhoAmi();
    const { user } = useAuthStore();
    const {
        register,
        handleSubmit,
        formState: { errors },
        control,
        reset,
    } = useForm<KYCVerificationTypes>({ resolver: yupResolver(validationSchema) });
    const [loading, setLoading] = useState(false);
    const [countriesList, setCountriesList] = useState<any>(country);
    const { data: countries } = useCountriesList();

    useEffect(() => {
        if (!!countries) setCountriesList(countries);
    }, [countries, countriesList]);

    const onHandlerClose = useCallback(() => {
        handleClose();
        setVerificationStorage("true");
    }, [handleClose]);

    useEffect(() => {
        reset({ email: user?.email, country: user?.country });
    }, [user]);

    const onSubmit: SubmitHandler<KYCVerificationTypes> = async (values) => {
        try {
            setLoading(true);
            setMessageError("");
            const { data } = await sendKYCVerification(values);
            if (data) {
                setVerificationStorage("true");
                handleClose();
            }
            await refetch();
            setLoading(false);
        } catch (e: any) {
            const error = await e.response.json();
            console.log("ERROR MESSAGE", error.data.message);
            setLoading(false);
            if (error.data.message) {
                setMessageError(error.data.message);
            }
        }
    };

    return (
        <>
            <ModalControls>
                <CloseButton onClick={onHandlerClose}>
                    <span className="icon">
                        <ArrBack />
                    </span>
                    <span className="text">Back</span>
                </CloseButton>
                <a href="/" target={"_blank"}>
                    <Logo />
                </a>
            </ModalControls>
            <Text>{t("modals.KYC.title")}</Text>
            {messageError.length > 0 && (
                <Group>
                    <Alert title={t("errors.titles.smth_wrong")} text={messageError} variant={"error"} />
                </Group>
            )}

            <Group onSubmit={handleSubmit(onSubmit)}>
                <TextField label="Email" errorText={errors.email?.message} helpText={t("tooltips.KYC_Email")}>
                    <Input placeholder="Email address" {...register("email")} />
                </TextField>

                <SelectField
                    name="country"
                    label="Country"
                    placeholder="United States"
                    data={countriesList}
                    control={control}
                    errorText={errors.country?.message}
                />

                <SharkButton buttonType="secondary" disabled={loading} loading={loading} variant="filled" type="submit">
                    {t("buttons.modal_kyc_btn")}
                </SharkButton>
            </Group>
        </>
    );
}

// Styling
const ModalControls = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 12px;

    & a {
        display: flex;
        font-size: 30px;
    }
`;

const Text = styled.div`
    font-size: 14px;
    line-height: 1;
    text-align: center;
    color: #666892;
    display: block;
`;

const Group = styled.form`
    display: flex;
    flex-direction: column;
    padding-top: 26px;
`;

const CloseButton = styled.button`
    display: flex;
    align-items: center;
    cursor: pointer;
    color: #4700e5;

    & .text {
        display: flex;
        align-items: center;
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }

    & .icon {
        display: flex;
        align-items: center;
        margin-right: 8px;
        font-size: 12px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }
`;

