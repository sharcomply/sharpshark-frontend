import styled from "@emotion/styled";
import { Select, Grid, Input, Checkbox, Overlay, Center, Stack } from "@mantine/core";
import { backend } from "api";
// import { Button, OutlineButton } from "components/Button";
import Icon from "components/Icon";
import { RadioInput } from "components/Input";
import { SharkButton } from "components/SharkButton";
import { useDraftOverlay } from "features/DraftDetails/hooks/useDraftOverlay";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import { useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useModal } from "../hooks/useModal";
import { ModalContent } from "../interfaces";
// import { ModalType } from "../interfaces";
import { useContract } from "../../../blockchain/hooks/useContract";
import { getNetworkById } from "../../../constants/networks";
import { useNearContext } from "providers/NearProvider";
import useAuthStore from "features/Auth/hooks/useAuthStore";
import { useWhoAmi } from "../../Auth/hooks/useWhoAmi";
import { useDraftAlert } from "../../DraftDetails/hooks/useDraftAlert";

// Component
export default function Preferences() {
    const [mode, setMode] = useState<string>("unlocked");
    const {
        handleClose,
        handleSwitch,
        //  handleOpen
    } = useModal();
    const { data, refetch } = useWhoAmi();

    useEffect(()=>{
       if (!data?.user.walletAddressNear) {
           refetch()
       }
    },[])

    const navigate = useNavigate();
    const draftStore = useDraftStore();
    const overlay = useDraftOverlay();

    const { t } = useTranslation();
    const contract = useContract();
    const { network } = useAuthStore();
    const Near = useNearContext();
    const alert = useDraftAlert();

    const onSetPaymentPayload = useCallback(async () => {
        draftStore.setPaymentMethod("Stripe");
        if (!draftStore.draftId) return;

        // openMessage();
        draftStore.setProtectLoading({ fiat: true });
        draftStore.showDraftAlert(false);

        try {
            if (draftStore.draftType === "image") {
                const { requestId } = await backend.scans.startScan({ documentId: draftStore.draftId, network: getNetworkById(network)?.symbol });

                const { url } = await backend.scans.getScanPaymentLink({
                    requestId,
                    successUrl: `${window.location.href}?stripeSucces=true`,
                    cancelUrl: `${window.location.href}?stripeSucces=true`,
                    network: getNetworkById(network)?.symbol || "",
                });

                window.open(url);

                handleClose();
                draftStore.showDraftAlert(false);
                draftStore.setDraftStatus({
                    type: "warn",
                    message: t("statuses.protecting"),
                });

                overlay.actions.open();
                overlay.actions.setPendingPaymentFiat(url);
                draftStore.showDraftAlert(true);
                draftStore.setDraftAlert({
                    type: "warn",
                    title: t("warn.titles.do_not_exit_protection"),
                    message: t("warn.messages.do_not_exit_protection"),
                });
            } else {
                await backend.scans.startEstimateDocument({ documentId: draftStore.draftId });
            }
        } catch (e: any) {
            // Close overlay and clean state
            const error = await e.response.json();
            overlay.actions.close();
            overlay.actions.clear();
            // closeMessage();
            draftStore.setProtectLoading({ fiat: false });
            draftStore.setDraftStatus({
                type: "error",
                message: t("statuses.protect_failed"),
            });
            draftStore.showDraftAlert(true);
            draftStore.setDraftAlert({
                type: "error",
                title: t("errors.titles.smth_wrong"),
                message: error.data.message,
            });
            handleClose();

            console.log("[PROTECT] ", e);
        }
    }, [draftStore, overlay.actions, handleClose, t]);

    const closeNear = useCallback(() => {
        overlay.actions.close();
        overlay.actions.clear();
        draftStore.setDraftStatus({
            type: "error",
            message: t("statuses.protect_failed"),
        });
        draftStore.showDraftAlert(true);
        draftStore.setDraftAlert({
            type: "error",
            title: t("errors.titles.protection_canceled"),
            message: t("errors.messages.protection_canceled"),
        });
    }, [overlay, draftStore]);

    const onNearProtect = useCallback(async () => {
        if (!data?.user.walletAddressNear) {
            // handleOpen({ type: ModalType.SETTINGS, content: ModalContent.ChangeNearAddress });
            return;
        }
        if (!Near?.WalletConnection.account().accountId) {
            alert.actions.open();
            alert.actions.error(t("Connect Near address!"), t("You should connect the near address"));
            handleClose();
            return;
        } else if (data?.user.walletAddressNear !== Near?.WalletConnection.account().accountId) {
            alert.actions.open();
            alert.actions.error(t("Wrong near address"), t("Your connected wallet don't match to your address that linked to account"));
            handleClose();
            return;
        }
        if (!draftStore.draftId) return;
        draftStore.setPaymentMethod("Near");
        handleClose();

        draftStore.showDraftAlert(false);
        draftStore.setDraftStatus({
            type: "warn",
            message: t("statuses.protecting"),
        });

        // Open Overlay and Show Pernding Payment message
        // openMessage();
        draftStore.showDraftAlert(true);
        draftStore.setDraftAlert({
            type: "warn",
            title: t("warn.titles.do_not_exit_protection_near"),
            message: t("warn.messages.do_not_exit_protection_near"),
        });
        overlay.actions.open();
        overlay.actions.setPendingPayment("near", closeNear);
        try {
            if (draftStore.draftType === "image") {
                const { requestId, toBlockchain } = await backend.scans.startScan({ documentId: draftStore.draftId, network: "near" });
                await Near?.Contract?.pay_request({
                    amount: toBlockchain.priceInWei,
                    args: {
                        amount: toBlockchain.priceInWei,
                        request_id: requestId,
                        expire: toBlockchain.expire as string,
                        signature: toBlockchain.signature as number[],
                        v: toBlockchain.v!,
                    },
                });
            } else {
                const { success } = await backend.scans.startEstimateDocument({ documentId: draftStore.draftId });
                console.log(`estimate`, success);
            }
        } catch (error) {
            console.log(error);
        }
    }, [draftStore, handleClose, overlay.actions, t, data]);

    const onProtect = useCallback(async () => {
        if (!draftStore.draftId) return;
        draftStore.setPaymentMethod("Metamask");
        handleClose();

        draftStore.showDraftAlert(false);
        draftStore.setDraftStatus({
            type: "warn",
            message: t("statuses.protecting"),
        });

        // Open Overlay and Show Pernding Payment message
        // openMessage();
        overlay.actions.open();
        overlay.actions.setPendingPayment();
        draftStore.showDraftAlert(true);
        draftStore.setDraftAlert({
            type: "warn",
            title: t("warn.titles.do_not_exit_protection"),
            message: t("warn.messages.do_not_exit_protection"),
        });
        try {
            if (draftStore.draftType === "image") {
                const { toBlockchain } = await backend.scans.startScan({ documentId: draftStore.draftId, network: getNetworkById(network)?.symbol });
                const tx = await contract.payRequest(toBlockchain.requestId, toBlockchain.expire, toBlockchain.signature, { value: toBlockchain.priceInWei });
                await tx.wait();
            } else {
                const { success } = await backend.scans.startEstimateDocument({ documentId: draftStore.draftId });
                console.log(`estimate`, success);
            }
        } catch (error: any) {
            console.log(error);
            overlay.actions.close();
            overlay.actions.clear();
            // closeMessage();
            draftStore.setDraftStatus({
                type: "error",
                message: t("statuses.protect_failed"),
            });
            draftStore.setProtectLoading({ metaMask: false });

            // code: 4001, message: 'MetaMask Tx Signature: User denied transaction signature.'
            if (error.code === 4001 || error.code === "ACTION_REJECTED") {
                draftStore.showDraftAlert(true);
                draftStore.setDraftAlert({
                    type: "error",
                    title: t("errors.titles.protection_canceled"),
                    message: t("errors.messages.protection_canceled"),
                });
            }
        }
    }, [draftStore, handleClose, t, overlay.actions, contract]);

    return (
        <ModalContainer>
            {/* COMING SOON CONTENT */}
            <ModalHeader>
                <h2>{t("modals.preferences.title")}</h2>

                <SetUpContainer>
                    {" "}
                    <UnderlinedText>{t("modals.preferences.how_do_i_set")}</UnderlinedText>
                    <Icon icon={"mdi:launch"} color={"#666892"} hoverColor={"#666892"} />
                    <Text>{t("common.coming_soon")}</Text>
                </SetUpContainer>
            </ModalHeader>
            <ModalData>
                <ModeInfo>
                    <Icon
                        icon={mode === "unlocked" ? "material-symbols:lock-open-rounded" : "material-symbols:lock"}
                        color={"#666892"}
                        hoverColor={"#666892"}
                        size={25}
                    />
                    <h3>{t("modals.preferences.mode")}</h3>
                </ModeInfo>

                <RadioRow isActive={mode === "unlocked"}>
                    <RadioInput
                        value="unlocked"
                        type="radio"
                        id="unlocked"
                        name="radio-group"
                        onChange={() => setMode("unlocked")}
                        checked={mode === "unlocked"}
                    />
                    <label htmlFor="unlocked">Unlocked</label>
                    <Text>{t("modals.preferences.every_persson_can_find_content")}</Text>
                </RadioRow>
                <RadioRow isActive={mode === "locked"}>
                    <RadioInput value="locked" type="radio" id="locked" name="radio-group" onChange={() => setMode("locked")} checked={mode === "locked"} />
                    <label htmlFor="test2">{t("statuses.locked")}</label>
                    <Text>{t("modals.preferences.password_protected")}</Text>
                </RadioRow>

                {mode === "unlocked" && (
                    <LicenseInfo>
                        <h3>{t("modals.preferences.license_title")}</h3>
                        <Text>{t("modals.preferences.license_description")}</Text>

                        <ReadMoreContainer>
                            {" "}
                            <UnderlinedText>{t("common.read_faq")}</UnderlinedText>
                            <Icon icon={"mdi:launch"} color={"#666892"} hoverColor={"#666892"} />
                            <Text>{t("common.coming_soon")}</Text>
                        </ReadMoreContainer>

                        <Grid>
                            <Grid.Col span={6}>
                                <Label>{t("textfields.lables.type")}</Label>
                                <Select
                                    defaultValue="copyright"
                                    data={[{ value: "copyright", label: "Copyrighted (pay-to-use)" }]}
                                    styles={{
                                        rightSection: { pointerEvents: "none" },
                                        item: { background: "transparent !important", color: "#0A001E !important" },
                                    }}
                                    rightSection={<Icon icon="material-symbols:arrow-drop-down-rounded" color="#666892" hoverColor="#666892" />}
                                />
                            </Grid.Col>
                        </Grid>
                        <Grid>
                            <Grid.Col span={6}>
                                <Label>{t("common.price")}</Label>
                                <Input placeholder="10" styles={{ rightSection: { marginRight: "5px" } }} rightSection={<Text>US$</Text>} />
                            </Grid.Col>
                        </Grid>
                    </LicenseInfo>
                )}
                <CheckboxContainer>
                    <CheckboxRow>
                        <Checkbox disabled />
                        <Text>
                            {t("modals.preferences.show_name_certificate")}
                            <br /> <u onClick={() => handleSwitch(ModalContent.SetEmail)}>{t("modals.preferences.confirm_email")}</u>
                        </Text>
                    </CheckboxRow>
                    <CheckboxRow>
                        <Checkbox />
                        <Text color="#0A001E" dangerouslySetInnerHTML={{ __html: t("modals.preferences.have_read_terms") }}></Text>
                    </CheckboxRow>
                </CheckboxContainer>
            </ModalData>
            {/* <ModalFooter>
                    <ButtonGroup>
                        <OutlineButton onClick={handleClose}>Cancel</OutlineButton>
                        <Button onClick={onProtect} color={"#4700e5"} hoverColor={"#1c00a6"}>
                            Protect
                        </Button>
                    </ButtonGroup>
                </ModalFooter> */}

            {/* Overlay */}
            <Overlay opacity={0.97} zIndex={5} />

            {/* Protect */}
            <Center sx={{ position: "absolute", inset: 0, zIndex: 10 }}>
                {!draftStore.draftId ? (
                    <Stack align={"center"}>
                        <Text
                            style={{ textAlign: "center", fontSize: "16px", lineHeight: "26px" }}
                            color="red"
                            dangerouslySetInnerHTML={{ __html: t("modals.preferences.no_draft_id") }}
                        />
                        <SharkButton onClick={() => navigate(0)} variant="outlined" buttonType="primary">
                            {t("buttons.reload_page_btn")}
                        </SharkButton>
                    </Stack>
                ) : (
                    <Stack spacing={16} align="center">
                        {network === "near" ? (
                            <SharkButton
                                loading={draftStore.protectLoading.fiat}
                                disabled={draftStore.protectLoading.fiat || draftStore.protectLoading.metaMask}
                                onClick={onNearProtect}
                                buttonType={"secondary"}
                                variant={"filled"}
                            >
                                {"Pay with Near"}
                            </SharkButton>
                        ) : (
                            <SharkButton
                                loading={draftStore.protectLoading.metaMask}
                                disabled={draftStore.protectLoading.fiat || draftStore.protectLoading.metaMask}
                                onClick={onProtect}
                                buttonType={"secondary"}
                                variant={"filled"}
                            >
                                {t("buttons.metamask_pay_btn")}
                            </SharkButton>
                        )}
                        <SharkButton
                            loading={draftStore.protectLoading.fiat}
                            disabled={draftStore.protectLoading.fiat || draftStore.protectLoading.metaMask}
                            onClick={onSetPaymentPayload}
                            buttonType={"secondary"}
                            variant={"filled"}
                        >
                            {t("buttons.fiat_pay_btn")}
                        </SharkButton>
                    </Stack>
                )}
            </Center>
        </ModalContainer>
    );
}

// Styling

const ModalContainer = styled.div`
    padding: 32px 0 0 0;
    position: relative;
    border-radius: 12px;
    overflow: hidden;
`;

const ModalHeader = styled.div`
    padding: 0 32px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 25px;

    & h2 {
        font-size: 24px;
        line-height: 30px;
        font-weight: 700;
    }
`;

const SetUpContainer = styled.div`
    cursor: pointer;
    display: flex;
    margin-top: 5px;
    margin-left: 170px;

    & div {
        margin-right: 5px;
    }
`;

const UnderlinedText = styled.div`
    font-size: 14px;
    line-height: 20px;
    text-decoration-line: underline;
    color: #666892;
`;

const ModalData = styled.div`
    margin-top: 33.5px;
`;

const ModeInfo = styled.div`
    display: flex;
    align-items: center;
    padding: 0 32px 16px 32px;
    border-bottom: 1px solid #dddeee;

    & h3 {
        font-size: 20px;
        line-height: 30px;
        font-weight: 700;
        margin-left: 8px;
        margin-top: 3px;
    }
`;

const LicenseInfo = styled.div`
    margin-top: 32px;
    padding: 0 32px;

    & h3 {
        font-size: 20px;
        line-height: 30px;
        font-weight: 700;
    }
`;

const Text = styled.div<{ color?: string }>`
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;

    & u {
        color: #4700e5;
        cursor: pointer;
    }

    ${({ color }) => `
        color: ${color ? color : "#666892"};
    `}
`;

const ReadMoreContainer = styled.div`
    cursor: pointer;
    display: flex;

    & div {
        margin-right: 5px;
    }
`;

const Label = styled.div`
    font-weight: 700;
    font-size: 12px;
    line-height: 16px;
    letter-spacing: 0.2px;
    margin-top: 16px;
    margin-bottom: 8px;
`;

const RadioRow = styled.div<{ isActive?: boolean }>`
    display: flex;
    align-items: center;
    height: 20px;
    padding: 32px;
    transition: all 0.5s linear;
    border: 1px solid #dddeee;

    & label {
        font-size: 14px;
        line-height: 20px;
        margin-left: 16px;
        font-weight: 400;
        color: #0a001e;
        width: 112px;
    }

    ${({ isActive }) => `
        background-color: ${isActive ? "#EDEEFF" : "#fff"};
        border: ${isActive ? "unset" : "1px solid #dddeeeto"};
    `}
`;

const CheckboxRow = styled.div`
    display: flex;
    align-items: start;
    margin-top: 32px;
    padding: 0 32px;

    & div:first-child {
        margin-right: 8px;
    }
`;

// const ModalFooter = styled.div`
//     border-top: 1px solid #dfe0eb;
// `;

// const ButtonGroup = styled.div`
//     display: flex;
//     justify-content: end;
//     padding: 32px;

//     & button {
//         margin: 0 5px;
//     }
// `;

const CheckboxContainer = styled.div`
    padding-bottom: 32px;
`;
