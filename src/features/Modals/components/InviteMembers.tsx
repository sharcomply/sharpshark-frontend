import styled from "@emotion/styled";
import { Grid } from "@mantine/core";
import { Input } from "components/Input";
import TextField from "components/TextField";
import closeIcon from "../../../assets/images/close-icon.svg";
import { useModal } from "../hooks/useModal";
import minusCircleRed from "../../../assets/images/minus-circle-red.svg";
import { useTranslation } from "react-i18next";

// Component
export default function InviteMembers() {
    const { handleClose } = useModal();
    const {t} = useTranslation()
    return (
        <>
            <ModalContainer>
                <ModalHeader>
                    <h2>{t("modals.invite_member.title")}</h2>
                    <CloseButton onClick={handleClose}>
                        <img src={closeIcon} alt=""></img>
                    </CloseButton>
                </ModalHeader>

                <Text>
                    {t("modals.invite_member.description")}
                </Text>

                <Grid justify="space-between">
                    <Grid.Col span={6}>
                        <TextField label={t("textfields.lables.email")}>
                            {" "}
                            <Input placeholder={t("textfields.placeholders.email_address")} />
                        </TextField>
                    </Grid.Col>
                    <Grid.Col span={6}>
                        <TextField label={t("textfields.lables.name_optional")}>
                            {" "}
                            <Grid justify="space-between" align="center">
                                <Grid.Col span={11}>
                                    <Input placeholder={t("textfields.placeholders.name")} />
                                </Grid.Col>
                                <a href="/#">
                                    <img src={minusCircleRed} width={28} height={28} alt="" />
                                </a>
                            </Grid>
                        </TextField>
                    </Grid.Col>
                </Grid>
            </ModalContainer>

            <ButtonsContainer>
                <Grid justify="end">
                    <Grid.Col span={3}>
                        <Grid justify="space-between">
                            <Grid.Col span={6}>
                                <OutlineButton color={"#4700e5"} hoverColor={"#1c00a6"} onClick={handleClose}>
                                    {t("buttons.cancel_btn")}
                                </OutlineButton>
                            </Grid.Col>
                            <Grid.Col span={6}>
                                <Button color={"#4700e5"} hoverColor={"#1c00a6"} onClick={handleClose}>
                                    {t("buttons.save_btn")}
                                </Button>
                            </Grid.Col>
                        </Grid>
                    </Grid.Col>
                </Grid>
            </ButtonsContainer>
        </>
    );
}

// Styling

const ModalContainer = styled.div`
    padding: 32px;
`;

const ModalHeader = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 8px;
    position: relative;

    & h2 {
        display: flex;
        font-size: 28px;
        margin: 0;
        width: 100%;
        line-height: 1.3;
        font-weight: 700;
    }

    & a {
        display: flex;
        font-size: 60px;
    }
`;

const Text = styled.div`
    font-size: 14px;
    line-height: 1.5;
    color: #666892;
    display: block;
    margin: 0.5rem 0 1rem 0;
`;

const Button = styled.button<{ color: string; hoverColor: string }>`
    display: flex;
    width: 100%;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    color: #fff;
    border: 1px solid transparent;

    padding: 12px 8px;
    border-radius: 4px;

    font-weight: 500;
    font-size: 14px;
    line-height: 20px;

    user-select: none;
    transition: background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

    ${({ color }) => `
        background-color: ${color};
        border-color: ${color};
    `}

    &:hover {
        ${({ hoverColor }) => `
            background-color: ${hoverColor};
            border-color: ${hoverColor};
        `}
    }
`;

const CloseButton = styled.button`
    cursor: pointer;
    position: absolute;
    width: 16px;
    height: 16px;
    right: -75px;
    top: -25px;
`;

const ButtonsContainer = styled.div`
    border-top: 1px solid #dee2e6;
    padding: 16px 31px 20px 32px;
`;

const OutlineButton = styled(Button)`
    color: #4700e5;
    background: #fcfdfe;
    border: 1px solid #dddeee;

    &:hover {
        background: #fcfdfe;
        border: 1px solid #4700e5;
    }
`;
