import styled from "@emotion/styled";
import { Button } from "components/Button";
import { Input } from "components/Input";
import TextField from "components/TextField";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import ArrBack from "features/Icons/ArrBack";
import Logo from "features/Icons/Logo";
import { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { useModal } from "../hooks/useModal";

// Component
export default function TextEditorAddImage() {
    const { handleClose } = useModal();
    const { t } = useTranslation();
    const { setImageInTextEditor } = useDraftStore();
    const [value, setValue] = useState("");

    const addImage = useCallback(() => {
        setImageInTextEditor(value);
        handleClose();
    }, [setImageInTextEditor, handleClose, value]);

    return (
        <ModalContainer>
            <ModalControls>
                <CloseButton onClick={() => handleClose()}>
                    <span className="icon">
                        <ArrBack />
                    </span>
                    <span className="text">{t("buttons.back_btn")}</span>
                </CloseButton>
                <a href="/" target={"_blank"}>
                    <Logo />
                </a>
            </ModalControls>

            <ModalHeader>
                <h2>Would you like to add a picture ?</h2>
            </ModalHeader>
            <Description></Description>
            <InputContainer>
                <TextField label={"URL"}>
                    <InputRow>
                        <Input value={value} onChange={(e) => setValue(e.target.value)} placeholder="https://example.npg" />
                    </InputRow>
                </TextField>
            </InputContainer>
            <ModalFooter>
                <Button color={"#4700e5"} hoverColor={"#1c00a6"} width="100%" onClick={addImage} disabled={!value}>
                    Add
                </Button>
            </ModalFooter>
        </ModalContainer>
    );
}

// Styling

const ModalContainer = styled.div`
    padding: 32px;
`;

const ModalControls = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;

    & a {
        display: flex;
        font-size: 30px;
    }
`;

const CloseButton = styled.button`
    display: flex;
    align-items: center;
    cursor: pointer;
    color: #4700e5;

    & .text {
        display: flex;
        align-items: center;
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }

    & .icon {
        display: flex;
        align-items: center;
        margin-right: 8px;
        font-size: 12px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }
`;

const ModalHeader = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    position: relative;
    margin-top: 25px;

    & h2 {
        font-size: 24px;
        line-height: 30px;
        font-weight: 700;
    }

    & a {
        display: flex;
        font-size: 60px;
    }
`;

const Text = styled.div`
    font-size: 14px;
    line-height: 20px;
    color: #666892;
    display: block;
`;

const Description = styled(Text)`
    width: 314px;
    margin-top: 4px;
`;

const InputContainer = styled.div`
    margin-top: 24px;
`;

const InputRow = styled.div`
    margin-left: -3px;
    margin-top: 8px;
`;

const ModalFooter = styled.div`
    text-align: center;

    & button {
        margin-top: 0;
        margin-bottom: 24px;
    }
`;
