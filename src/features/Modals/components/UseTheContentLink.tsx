interface Props {
    id?: string;
}
export const UseTheContentLink = ({ id }: Props) => {
    return (
        <p style={{ color: "blue", textDecoration: "underline", cursor: "pointer", margin: "0" }}>
            &#10022;
            <a
                href={`${window.location.origin}/certificate/${id}`}
                style={{ fontStyle: "normal", fontWeight: "400", fontSize: "14px", lineHeight: "20px", fontFamily: "Sans-serif" }}
            >
                {" "}
                Use the content
            </a>
        </p>
    );
};
