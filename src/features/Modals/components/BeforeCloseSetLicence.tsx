import styled from "@emotion/styled";
import { SharkButton } from "components/SharkButton";
import Logo from "features/Icons/Logo";
import { useModal } from "../hooks/useModal";
import { ModalContent } from "../interfaces";

// Component
export default function BeforeCloseSetLicence() {
    const { handleClose, handleSwitch } = useModal();

    return (
        <ModalContainer>
            <ModalControls>
                <a href="/" target={"_blank"}>
                    <Logo />
                </a>
            </ModalControls>

            <Text> You sure? We'll keep the content but you'll have to start over the licensing</Text>
            <div style={{ display: "flex", justifyContent: "space-between", width: "100%", marginTop: "32px" }}>
                <SharkButton onClick={() => handleSwitch(ModalContent.LicenseAndMode)} type={"button"} variant={"outlined"} buttonType={"secondary"}>
                    Back
                </SharkButton>
                <SharkButton onClick={handleClose} variant={"filled"} buttonType={"primary"} width={"230px"} p={15}>
                    Save as draft, don't save licensing options
                </SharkButton>
            </div>
        </ModalContainer>
    );
}

// Styling

const ModalContainer = styled.div`
    padding: 32px;
    width: 440px;
`;

const ModalControls = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 32px;
    & a {
        display: flex;
        font-size: 30px;
    }
`;

const Text = styled.div`
    font-size: 14px;
    line-height: 20px;
    color: #666892;
    display: block;
`;
