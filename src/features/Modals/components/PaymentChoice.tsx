import styled from "@emotion/styled";
import { Stack } from "@mantine/core";
import { SharkButton } from "components/SharkButton";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import ArrBack from "features/Icons/ArrBack";
import Logo from "features/Icons/Logo";
import { useDraftVersionStore } from "features/NewDraftVersion/hooks/useDraftVersionStore";
import { useDraftVersionUpgrade } from "features/NewDraftVersion/hooks/useDraftVersionUpgrade";
import { useCallback, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useModal } from "../hooks/useModal";
import useAuthStore from "../../Auth/hooks/useAuthStore";
// import { ModalContent, ModalType } from "../interfaces";
import { useWhoAmi } from "../../Auth/hooks/useWhoAmi";

// Component
export default function PaymentChoice() {
    const { setProtectLoading, protectLoading, setDraftStatusBeforeProtect } = useDraftStore();
    const { newForm } = useDraftVersionStore();
    const { upgradeWithFiat, upgradeWithMetaMask, upgradeWithNear } = useDraftVersionUpgrade();
    const { handleClose } = useModal();
    const { data } = useWhoAmi();
    // const { handleOpen } = useModal();

    const { network } = useAuthStore();
    const { t } = useTranslation();

    const onCloseModal = useCallback(() => {
        handleClose();
    }, [handleClose]);

    const onUpgradeWithMetaMask = useCallback(() => {
        setProtectLoading({ metaMask: true });
        upgradeWithMetaMask(newForm);
    }, [setProtectLoading, upgradeWithMetaMask, newForm]);

    const onUpgradeWithFiat = useCallback(() => {
        setProtectLoading({ fiat: true });
        upgradeWithFiat(newForm);
    }, [setProtectLoading, upgradeWithFiat, newForm]);

    const onUpgradeWithNear = useCallback(() => {
        if (!data?.user.walletAddressNear) {
            // handleOpen({ type: ModalType.SETTINGS, content: ModalContent.ChangeNearAddress });
            return;
        }

        setProtectLoading({ near: true });
        upgradeWithNear(newForm);
    }, [setProtectLoading, upgradeWithNear, newForm, data]);

    useEffect(() => {
        setDraftStatusBeforeProtect(undefined);
    }, []);

    return (
        <ModalContainer>
            <ModalControls>
                <CloseButton onClick={onCloseModal}>
                    <span className="icon">
                        <ArrBack />
                    </span>
                    <span className="text">{t("buttons.back_btn")}</span>
                </CloseButton>
                <a href="/" target={"_blank"}>
                    <Logo />
                </a>
            </ModalControls>

            <ModalHeader>
                <h2>{t("modals.paymeyment.title")}</h2>
            </ModalHeader>

            <Stack mt={32} spacing={16} align="center">
                {network != "near" ? (
                    <SharkButton
                        onClick={onUpgradeWithMetaMask}
                        loading={protectLoading.metaMask}
                        disabled={protectLoading.fiat || protectLoading.metaMask || protectLoading.near}
                        buttonType={"secondary"}
                        variant={"filled"}
                    >
                        {t("buttons.metamask_pay_btn")}
                    </SharkButton>
                ) : (
                    <SharkButton
                        loading={protectLoading.near}
                        disabled={protectLoading.near || protectLoading.metaMask || protectLoading.fiat}
                        onClick={onUpgradeWithNear}
                        buttonType={"secondary"}
                        variant={"filled"}
                    >
                        Pay with Near
                    </SharkButton>
                )}

                <SharkButton
                    loading={protectLoading.fiat}
                    disabled={protectLoading.fiat || protectLoading.metaMask || protectLoading.near}
                    onClick={onUpgradeWithFiat}
                    buttonType={"secondary"}
                    variant={"filled"}
                >
                    {t("buttons.fiat_pay_btn")}
                </SharkButton>
            </Stack>
        </ModalContainer>
    );
}

// Styling

const ModalControls = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;

    & a {
        display: flex;
        font-size: 30px;
    }
`;

const CloseButton = styled.button`
    display: flex;
    align-items: center;
    cursor: pointer;
    color: #4700e5;

    & .text {
        display: flex;
        align-items: center;
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }

    & .icon {
        display: flex;
        align-items: center;
        margin-right: 8px;
        font-size: 12px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }
`;

const ModalContainer = styled.div`
    padding: 32px;
`;

const ModalHeader = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    position: relative;
    margin-top: 25px;

    & h2 {
        font-size: 24px;
        line-height: 30px;
        font-weight: 700;
    }

    & a {
        display: flex;
        font-size: 60px;
    }
`;
