import styled from "@emotion/styled";
import { ReactNode } from "react";
import Icon from "../../../components/Icon";
import { Group } from "@mantine/core";

interface Props {
    children?: ReactNode;
    onClick?: () => void;
    disabled?: boolean;
}
const CopyButton = ({ children, onClick, disabled }: Props) => {
    return (
        <>
            <Button type="button" disabled={disabled} onClick={onClick}>
                <Group spacing={9}>
                    <Icon icon={"clarity:copy-line"} color={"#666892"} />
                    {children}
                </Group>
            </Button>
        </>
    );
};

export default CopyButton;

const Button = styled.button<any>`
    display: flex;
    align-items: center;
    padding: 5px 8px;
    width: 105px;
    height: 30px;
    border: 1px solid #dddeee;
    border-radius: 4px;
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;
    color: #666892;
    cursor: pointer;
    :disabled {
        background-color: #f7f8f9;
        cursor: default;
    }
`;
