import styled from "@emotion/styled";
import { Input } from "components/Input";
import TextField from "components/TextField";
import useApplicationLoader from "features/ApplicationLoader/hooks/useApplicationLoader";
import ArrBack from "features/Icons/ArrBack";
import Logo from "features/Icons/Logo";
import { fakeRequest } from "helpers/fakeRequest";
import { SubmitHandler, useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useModal } from "../hooks/useModal";
import { ModalContent } from "../interfaces";
import { SignUpInputTypes } from "../interfaces/formTypes";

// Component
export default function IndividualSignUp() {
    const { handleSwitch, handleClose } = useModal();
    const loader = useApplicationLoader();
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<SignUpInputTypes>();

    const onSubmit: SubmitHandler<SignUpInputTypes> = async () => {
        try {
            loader.start();

            await fakeRequest();
            
            loader.stop();
            handleClose();
            reset();
        } catch (e) {
            loader.stop();
        }
    };

    const {t} = useTranslation()

    return (
        <>
            <ModalControls>
                <CloseButton onClick={() => handleSwitch(ModalContent.SignUp)}>
                    <span className="icon">
                        <ArrBack />
                    </span>
                    <span className="text">{t("buttons.back_btn")}</span>
                </CloseButton>
                <a href="/" target={"_blank"}>
                    <Logo />
                </a>
            </ModalControls>

            <Group onSubmit={handleSubmit(onSubmit)}>
                <TextField label={t("textfields.lables.fullname")} errorText={errors.fullName?.message}>
                    <Input placeholder={t("textfields.lables.fullname")} {...register("fullName", { required: true })} />
                </TextField>
                <TextField label={t("textfields.lables.personal_email")} errorText={errors.email?.message}>
                    <Input placeholder="your@email.com" {...register("email", { required: true })} />
                </TextField>
                <TextField
                    label={t("textfields.lables.create_repeat_password")}
                    description={t("textfields.descriptions.password_field")}
                    errorText={errors.password?.message || errors.confirmPassword?.message}
                >
                    <Input
                        placeholder={t("textfields.placeholders.your_password")}
                        radius={"8px 8px 0px 0px"}
                        {...register("password", { required: true })}
                    />
                    <Input
                        placeholder={t("textfields.placeholders.repeat_password")}
                        radius={"0px 0px 8px 8px"}
                        {...register("confirmPassword", { required: true })}
                    />
                </TextField>

                <Button color={"#4700e5"} hoverColor={"#1c00a6"} type="submit">
                    {t("buttons.sign_up_btn")}
                </Button>
                <Button color={"#dc3545"} hoverColor={"#c82333"} type="button">
                {t("buttons.google_sign_up_btn")}
                </Button>
            </Group>
        </>
    );
}

// Styling
const ModalControls = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 12px;

    & a {
        display: flex;
        font-size: 30px;
    }
`;

const CloseButton = styled.button`
    display: flex;
    align-items: center;
    cursor: pointer;
    color: #4700e5;

    & .text {
        display: flex;
        align-items: center;
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }

    & .icon {
        display: flex;
        align-items: center;
        margin-right: 8px;
        font-size: 12px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }
`;

const Group = styled.form`
    display: flex;
    flex-direction: column;
    padding-top: 26px;
`;

const Button = styled.button<{ color?: string; hoverColor?: string }>`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    color: #fff;
    border: 1px solid transparent;

    padding: 12px 16px;
    margin-top: 8px;
    margin-bottom: 8px;
    border-radius: 4px;

    font-weight: 500;
    font-size: 14px;
    line-height: 20px;

    user-select: none;
    transition: background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

    ${({ color }) =>
        color &&
        `
        background-color: ${color};
        border-color: ${color};
    `}

    &:hover {
        ${({ hoverColor }) =>
            hoverColor &&
            `
            background-color: ${hoverColor};
            border-color: ${hoverColor};
        `}
    }
`;
