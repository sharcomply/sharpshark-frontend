import styled from '@emotion/styled';
import ArrBack from 'features/Icons/ArrBack';
import Logo from 'features/Icons/Logo';
import {useModal} from '../hooks/useModal';
import {useCallback, useState} from 'react';
import {useAuth} from '../../Auth/hooks/useAuth';
import {useTranslation} from 'react-i18next';
import {Checkbox, Group} from "@mantine/core";

// Component
export default function SignIn() {
    const [loading, setLoading] = useState(false);
    const [checked, setChecked] = useState(false)
    const {handleClose} = useModal();
    const {connectWallet} = useAuth();
    const {t} = useTranslation()
    const handleLogin = useCallback(() => {
        setLoading(true);
        connectWallet()
            .finally(() => {
                setLoading(false);
                handleClose();
            });
    }, [connectWallet, handleClose]);

    return (
        <>
            <ModalControls>
                <CloseButton onClick={handleClose}>
                    <span className="icon">
                        <ArrBack/>
                    </span>
                    <span className="text">{t("buttons.back_btn")}</span>
                </CloseButton>
            </ModalControls>

            <ModalHeader>
                <h2 dangerouslySetInnerHTML={{__html: t("modals.login.sign_in")}}/>
                <a href="/" target={'_blank'}>
                    <Logo/>
                </a>
            </ModalHeader>

            <Text>{t("modals.login.use_metamask_text")}</Text>
            <Group mt={26}>
                <Checkbox onClick={() => setChecked(!checked)}/>
                <Text style={{color: "#0A001E", fontWeight: 600, textAlign: 'start', width: "calc(100% - 36px)", lineHeight: "20px"}}>I have read and I agree with the <a
                    style={{textDecoration: 'underline', color: "#4700E5"}} target={"_blank"} href={"https://www.sharpshark.io/privacy-policy-and-cookies"}>Terms and
                    Conditions</a> </Text>
            </Group>

            <ButtonGroup>
                <Button disabled={!checked || loading} color={'#4700E5'} hoverColor={'#1C00A6'} onClick={handleLogin}>
                    {t("buttons.metamask_login_btn")}
                </Button>
            </ButtonGroup>
        </>
    );
}

// Styling
const ModalControls = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    margin-bottom: 12px;
`;

const CloseButton = styled.button`
    display: flex;
    align-items: center;
    cursor: pointer;
    color: #4700e5;

    & .text {
        display: flex;
        align-items: center;
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }

    & .icon {
        display: flex;
        align-items: center;
        margin-right: 8px;
        font-size: 12px;
        font-weight: 500;
        letter-spacing: 0.3px;
        line-height: 16px;
    }
`;

const ModalHeader = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 25px;

    & h2 {
        display: flex;
        font-size: 28px;
        max-width: 170px;
        margin: 0;
        width: 100%;
        line-height: 1.3;
        font-weight: 700;
    }

    & a {
        display: flex;
        font-size: 60px;
    }
`;

const Text = styled.div`
    font-size: 14px;
    line-height: 1;
    text-align: center;
    color: #666892;
    display: block;
`;

const ButtonGroup = styled.div`
    display: flex;
    flex-direction: column;
    padding-top: 16px;
`;

const Button = styled.button<{ color: string; hoverColor: string }>`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    color: #fff;
    border: 1px solid transparent;

    padding: 12px 16px;
    margin-top: 8px;
    margin-bottom: 8px;
    border-radius: 4px;

    font-weight: 500;
    font-size: 14px;
    line-height: 20px;

    user-select: none;
    transition: background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

    &:disabled {
        opacity: 0.6;
    }

    ${({color}) => `
        background-color: ${color};
        border-color: ${color};
    `}
    &:hover {
        ${({hoverColor}) => `
            background-color: ${hoverColor};
            border-color: ${hoverColor};
        `}
    }
`;
