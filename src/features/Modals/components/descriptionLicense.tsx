import { Group, Text } from "@mantine/core";
import Icon from "../../../components/Icon";
import { t } from "i18next";

export const descriptionLicence = (licenceType: string) => {
    switch (licenceType) {
        case "Time Machine © 2022 by H. G. Wells is licensed under CC0 1.0: https://creativecommons.org/publicdomain/zero/1.0/":
            return (
                <>
                    <Group style={{ width: "100%" }} position={"apart"} align={"start"} noWrap={true}>
                        <Group direction={"column"} spacing={0} style={{ width: "60%" }}>
                            <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("CC0 1.0")}
                            </Text>
                            <Text color={"#666892"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("1.0 Universal")}
                            </Text>
                        </Group>
                        <Group>
                            <Icon icon="bi:cc-circle" color="#000" size={48} />
                            <Icon icon="cib:creative-commons-zero" color="#000" size={48} />
                        </Group>
                    </Group>
                    <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                        {t(
                            "By marking the work with a CC0 public domain dedication, the creator is giving up their copyright and allowing reusers to distribute, remix, adapt, and build upon the material in any medium or format, even for commercial purposes."
                        )}
                    </Text>
                    <Group p={16} style={{ border: "1px solid #4700E5", borderRadius: "4px", background: "#EDEEFF" }}>
                        <Group spacing={16} noWrap={true}>
                            <Icon icon="ic:outline-info" color={"#4700E5"} />
                            <Text color={"#0A001E"} weight={"500"} sx={{ fontSize: 16, lineHeight: "20px" }}>
                                {t("You won’t be able to track the work")}
                            </Text>
                        </Group>
                        <Text color={"#0A001E"} weight={"291"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                            {t(
                                "Please, note that if you lisense this work as public domain, SharpShark won’t monitor its usage and you won’t be able to send any requests to reusers."
                            )}
                        </Text>
                        <Text color={"#0A001E"} weight={"291"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                            {t(
                                "By this, you waive all copyright and related or neighboring rights together with all associated claims and causes of action with respect to this work to the extent possible under the law."
                            )}
                        </Text>
                    </Group>
                    <Group noWrap={true}>
                        <Icon icon="cib:creative-commons-zero" color="#000" size={36} />
                        <p>
                            <span style={{ fontWeight: "700", fontSize: 14, lineHeight: "20px" }}>CC0: </span>
                            <span style={{ fontWeight: "500", fontSize: 14, lineHeight: "20px", color: "#666892" }}>
                                This work has been marked as dedicated to the public domain.
                            </span>
                        </p>
                    </Group>
                </>
            );
        case "Time Machine © 2022 by H. G. Wells is licensed under CC BY 4.0: https://creativecommons.org/licenses/by/4.0/":
            return (
                <>
                    <Group style={{ width: "100%" }} position={"apart"} align={"start"} noWrap={true}>
                        <Group direction={"column"} spacing={0} style={{ width: "60%" }}>
                            <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("CC BY 4.0")}
                            </Text>
                            <Text color={"#666892"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("Attribution 4.0 International")}
                            </Text>
                        </Group>
                        <Group>
                            <Icon icon="bi:cc-circle" color="#000" size={48} />
                            <Icon icon="cib:creative-commons-by" color="#000" size={48} />
                        </Group>
                    </Group>
                    <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                        {t(
                            "This license requires that reusers give credit to the creator. It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, even for commercial purposes."
                        )}
                    </Text>
                    <Group noWrap={true}>
                        <Icon icon="cib:creative-commons-by" color="#000" size={36} />
                        <p>
                            <span style={{ fontWeight: "700", fontSize: 14, lineHeight: "20px" }}>BY: </span>
                            <span style={{ fontWeight: "500", fontSize: 14, lineHeight: "20px", color: "#666892" }}>
                                Credit must be given to you, the creator.
                            </span>
                        </p>
                    </Group>
                </>
            );
        case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/":
            return (
                <>
                    <Group style={{ width: "100%" }} position={"apart"} align={"start"} noWrap={true}>
                        <Group direction={"column"} spacing={0} style={{ width: "60%" }}>
                            <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("CC BY-SA 4.0")}
                            </Text>
                            <Text color={"#666892"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("Attribution-ShareAlike 4.0 International")}
                            </Text>
                        </Group>
                        <Group>
                            <Icon icon="bi:cc-circle" color="#000" size={48} />
                            <Icon icon="cib:creative-commons-by" color="#000" size={48} />
                            <Icon icon="cib:creative-commons-sa" color="#000" size={48} />
                        </Group>
                    </Group>
                    <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                        {t(
                            "This license requires that reusers give credit to the creator. It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, even for commercial purposes. If others remix, adapt, or build upon the material, they must license the modified material under identical terms."
                        )}
                    </Text>
                    <Group noWrap={true}>
                        <Icon icon="cib:creative-commons-by" color="#000" size={36} />
                        <p>
                            <span style={{ fontWeight: "700", fontSize: 14, lineHeight: "20px" }}>BY: </span>
                            <span style={{ fontWeight: "500", fontSize: 14, lineHeight: "20px", color: "#666892" }}>
                                Credit must be given to you, the creator.
                            </span>
                        </p>
                    </Group>
                    <Group noWrap={true}>
                        <Icon icon="cib:creative-commons-sa" color="#000" size={36} />
                        <p>
                            <span style={{ fontWeight: "700", fontSize: 14, lineHeight: "20px" }}>SA: </span>
                            <span style={{ fontWeight: "500", fontSize: 14, lineHeight: "20px", color: "#666892" }}>
                                Adaptations must be shared under the same terms.
                            </span>
                        </p>
                    </Group>
                </>
            );
        case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-ND 4.0: https://creativecommons.org/licenses/by-nd/4.0/":
            return (
                <>
                    <Group style={{ width: "100%" }} position={"apart"} align={"start"} noWrap={true}>
                        <Group direction={"column"} spacing={0} style={{ width: "60%" }}>
                            <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("CC BY-ND 4.0")}
                            </Text>
                            <Text color={"#666892"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("Attribution-NoDerivatives 4.0 International")}
                            </Text>
                        </Group>
                        <Group position={"right"}>
                            <Icon icon="bi:cc-circle" color="#000" size={48} />
                            <Icon icon="cib:creative-commons-by" color="#000" size={48} />
                            <Icon icon="cib:creative-commons-nd" color="#000" size={48} />
                        </Group>
                    </Group>
                    <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                        {t(
                            "This license requires that reusers give credit to the creator. It allows reusers to copy and distribute the material in any medium or format in unadapted form only, even for commercial purposes."
                        )}
                    </Text>
                    <Group noWrap={true}>
                        <Icon icon="cib:creative-commons-by" color="#000" size={36} />
                        <p>
                            <span style={{ fontWeight: "700", fontSize: 14, lineHeight: "20px" }}>BY: </span>
                            <span style={{ fontWeight: "500", fontSize: 14, lineHeight: "20px", color: "#666892" }}>
                                Credit must be given to you, the creator.
                            </span>
                        </p>
                    </Group>
                    <Group noWrap={true}>
                        <Icon icon="cib:creative-commons-nd" color="#000" size={36} />
                        <p>
                            <span style={{ fontWeight: "700", fontSize: 14, lineHeight: "20px" }}>ND: </span>
                            <span style={{ fontWeight: "500", fontSize: 14, lineHeight: "20px", color: "#666892" }}>
                                No derivatives or adaptations of your work are permitted.
                            </span>
                        </p>
                    </Group>
                </>
            );
        case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC 4.0: https://creativecommons.org/licenses/by-nc/4.0/":
            return (
                <>
                    <Group style={{ width: "100%" }} position={"apart"} align={"start"} noWrap={true}>
                        <Group direction={"column"} spacing={0} style={{ width: "60%" }}>
                            <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("CC BY-NC 4.0")}
                            </Text>
                            <Text color={"#666892"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("Attribution-NonCommercial 4.0 International")}
                            </Text>
                        </Group>
                        <Group position={"right"}>
                            <Icon icon="bi:cc-circle" color="#000" size={48} />
                            <Icon icon="cib:creative-commons-by" color="#000" size={48} />
                            <Icon icon="cib:creative-commons-nc" color="#000" size={48} />
                        </Group>
                    </Group>
                    <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                        {t(
                            "This license requires that reusers give credit to the creator. It allows reusers to copy and distribute the material in any medium or format in unadapted form only, even for commercial purposes."
                        )}
                    </Text>
                    <Group noWrap={true}>
                        <Icon icon="cib:creative-commons-by" color="#000" size={36} />
                        <p>
                            <span style={{ fontWeight: "700", fontSize: 14, lineHeight: "20px" }}>BY: </span>
                            <span style={{ fontWeight: "500", fontSize: 14, lineHeight: "20px", color: "#666892" }}>
                                Credit must be given to you, the creator.
                            </span>
                        </p>
                    </Group>
                    <Group noWrap={true}>
                        <Icon icon="cib:creative-commons-nc" color="#000" size={36} />
                        <p>
                            <span style={{ fontWeight: "700", fontSize: 14, lineHeight: "20px" }}>NC: </span>
                            <span style={{ fontWeight: "500", fontSize: 14, lineHeight: "20px", color: "#666892" }}>
                                Only noncommercial use of your work is permitted.
                            </span>
                        </p>
                    </Group>
                </>
            );
        case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/":
            return (
                <>
                    <Group style={{ width: "100%" }} position={"apart"} align={"start"} noWrap={true}>
                        <Group direction={"column"} spacing={0} style={{ width: "60%" }}>
                            <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("CC BY-NC-SA 4.0")}
                            </Text>
                            <Text color={"#666892"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("Attribution-NonCommercial-ShareAlike 4.0 International")}
                            </Text>
                        </Group>
                        <Group position={"right"}>
                            <Icon icon="bi:cc-circle" color="#000" size={48} />
                            <Icon icon="cib:creative-commons-by" color="#000" size={48} />
                            <Icon icon="cib:creative-commons-nc" color="#000" size={48} />
                            <Icon icon="cib:creative-commons-sa" color="#000" size={48} />
                        </Group>
                    </Group>
                    <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                        {t(
                            "This license requires that reusers give credit to the creator. It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, for noncommercial purposes only. If others modify or adapt the material, they must license the modified material under identical terms."
                        )}
                    </Text>
                    <Group noWrap={true}>
                        <Icon icon="cib:creative-commons-by" color="#000" size={36} />
                        <p>
                            <span style={{ fontWeight: "700", fontSize: 14, lineHeight: "20px" }}>BY: </span>
                            <span style={{ fontWeight: "500", fontSize: 14, lineHeight: "20px", color: "#666892" }}>
                                Credit must be given to you, the creator.
                            </span>
                        </p>
                    </Group>
                    <Group noWrap={true}>
                        <Icon icon="cib:creative-commons-nc" color="#000" size={36} />
                        <p>
                            <span style={{ fontWeight: "700", fontSize: 14, lineHeight: "20px" }}>NC: </span>
                            <span style={{ fontWeight: "500", fontSize: 14, lineHeight: "20px", color: "#666892" }}>
                                Only noncommercial use of your work is permitted.
                            </span>
                        </p>
                    </Group>
                    <Group noWrap={true}>
                        <Icon icon="cib:creative-commons-sa" color="#000" size={36} />
                        <p>
                            <span style={{ fontWeight: "700", fontSize: 14, lineHeight: "20px" }}>SA: </span>
                            <span style={{ fontWeight: "500", fontSize: 14, lineHeight: "20px", color: "#666892" }}>
                                Adaptations must be shared under the same terms.
                            </span>
                        </p>
                    </Group>
                </>
            );
        case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-ND 4.0: https://creativecommons.org/licenses/by-nc-nd/4.0/":
            return (
                <>
                    <Group style={{ width: "100%" }} position={"apart"} align={"start"} noWrap={true}>
                        <Group direction={"column"} spacing={0} style={{ width: "60%" }}>
                            <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("CC BY-NC-ND 4.0")}
                            </Text>
                            <Text color={"#666892"} weight={"700"} sx={{ fontSize: 20, lineHeight: "24px" }}>
                                {t("Attribution-NonCommercial-NoDerivatives 4.0 International")}
                            </Text>
                        </Group>
                        <Group position={"right"}>
                            <Icon icon="bi:cc-circle" color="#000" size={48} />
                            <Icon icon="cib:creative-commons-by" color="#000" size={48} />
                            <Icon icon="cib:creative-commons-nc" color="#000" size={48} />
                            <Icon icon="cib:creative-commons-nd" color="#000" size={48} />
                        </Group>
                    </Group>
                    <Text color={"#666892"} weight={"400"} sx={{ fontSize: 14, lineHeight: "20px" }}>
                        {t(
                            "This license requires that reusers give credit to the creator. It allows reusers to copy and distribute the material in any medium or format in unadapted form and for noncommercial purposes only."
                        )}
                    </Text>
                    <Group noWrap={true}>
                        <Icon icon="cib:creative-commons-by" color="#000" size={36} />
                        <p>
                            <span style={{ fontWeight: "700", fontSize: 14, lineHeight: "20px" }}>BY: </span>
                            <span style={{ fontWeight: "500", fontSize: 14, lineHeight: "20px", color: "#666892" }}>
                                Credit must be given to you, the creator.
                            </span>
                        </p>
                    </Group>
                    <Group noWrap={true}>
                        <Icon icon="cib:creative-commons-nc" color="#000" size={36} />
                        <p>
                            <span style={{ fontWeight: "700", fontSize: 14, lineHeight: "20px" }}>NC: </span>
                            <span style={{ fontWeight: "500", fontSize: 14, lineHeight: "20px", color: "#666892" }}>
                                Only noncommercial use of your work is permitted.
                            </span>
                        </p>
                    </Group>
                    <Group noWrap={true}>
                        <Icon icon="cib:creative-commons-nd" color="#000" size={36} />
                        <p>
                            <span style={{ fontWeight: "700", fontSize: 14, lineHeight: "20px" }}>ND: </span>
                            <span style={{ fontWeight: "500", fontSize: 14, lineHeight: "20px", color: "#666892" }}>
                                No derivatives or adaptations of your work are permitted.
                            </span>
                        </p>
                    </Group>
                </>
            );
    }
};
