import { Group, Stack, Text } from "@mantine/core";
import { useCallback } from "react";
import { useNavigate } from "react-router-dom";
import { useModal } from "../hooks/useModal";
import { SharkButton } from "../../../components/SharkButton";
import { useTranslation } from "react-i18next";

// Component
export default function ClaimIsSent() {
    const navigate = useNavigate();
    const { handleClose } = useModal();
    const { t } = useTranslation();

    const closeHandler = useCallback(() => {
        handleClose();
        navigate(
            {
                hash: undefined,
            },
            { replace: true }
        );
    }, [handleClose, navigate]);

    return (
        <Stack spacing={8} pb={40}>
            <Stack pt={40} px={32} spacing={4}>
                <Text color={"#0A001E"} weight={"700"} sx={{ fontSize: 24, lineHeight: "30px" }} align={"center"}>
                    {t("dispute.claim_is_sent_modal.text")}
                </Text>
                <Group position={"center"} mt={20}>
                    <SharkButton width={100} variant="filled" buttonType="secondary" px={16} py={11} onClick={closeHandler}>
                        {t("dispute.claim_is_sent_modal.ok")}
                    </SharkButton>
                </Group>
            </Stack>
        </Stack>
    );
}
