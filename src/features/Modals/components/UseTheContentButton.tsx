import LogoOutline from "../../Icons/LogoOutline";

interface Props {
    id?: string;
}

export const UseTheContentButton = ({ id }: Props) => {
    return (
        <a href={`${window.location.origin}/certificate/${id}`}>
            <button type={"button"} style={{ padding: "12px", border: "1px solid #4700E5", borderRadius: "4px", background: "#DDE2FF" }}>
                <div style={{ display: "flex", alignItems: "center" }}>
                    <LogoOutline color={"#4700E5"} height={20} width={20} />
                    <p
                        style={{
                            margin: "0 0 0 8px",
                            color: "#4700E5",
                            fontStyle: "normal",
                            fontWeight: "400",
                            fontSize: "14px",
                            lineHeight: "20px",
                            cursor: "pointer",
                            fontFamily: "Sans-serif",
                        }}
                    >
                        Use the content
                    </p>
                </div>
            </button>
        </a>
    );
};
