import styled from "@emotion/styled";
import { Group, Stack, Text } from "@mantine/core";
import Icon from "components/Icon";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { SharkButton } from "../../../components/SharkButton";
import useDraftStore from "../../DraftDetails/hooks/useDraftStore";
import {getNetworkBySymbol} from "../../../constants/networks";
import useAuthStore from "../../Auth/hooks/useAuthStore";

type CertificateViewProps = {
    handleClose: any;
    handleCertificateFromCache: any;
};

// Component
export default function CertificateViewModal({ handleClose, handleCertificateFromCache } :CertificateViewProps) {
    const { t } = useTranslation();
    const { mintedNetworks } = useDraftStore()
    const network = getNetworkBySymbol(mintedNetworks)
    const { setCurrentNetwork } = useAuthStore();

    const closeHandler = useCallback(() => {
        handleClose();
    }, [handleClose]);

    const onChangeNetwork = useCallback(async() => {
        if(!network) return
        try {
            await window.ethereum.request({
                method: "wallet_switchEthereumChain",
                params: [{ chainId: network.chainId }],
            });
            setCurrentNetwork(network.value);
            localStorage.setItem("NETWORK_ID", network.value);
            closeHandler()
            window.location.reload();
        } catch (e) {
            console.log(e);
        }
    }, [closeHandler]);

    const onDownloadFromCash = useCallback(() => {
        handleCertificateFromCache();
    }, [closeHandler]);

    return (
        <Stack spacing={8} sx={{width: 440}}>
            <Group onClick={closeHandler} p={2} sx={{ position: "absolute", right: "-36px", top: 0, backgroundColor: "#fff", borderRadius: 100 }}>
                <Icon icon={"ep:close-bold"} color={"#666892"} size={16} />
            </Group>
            <Stack pt={32} px={32} spacing={4}>
                <Text color={"#0A001E"} align={"center"} weight={"700"} sx={{ fontSize: 20, lineHeight: "30px" }}>
                    {t("modals.certificate.from_cache_popup_title")}
                </Text>
            </Stack>
            <Footer>
                <Group spacing={12}>
                    <SharkButton variant="filled" buttonType="secondary" px={16} py={11} onClick={onChangeNetwork}>
                        Change network
                    </SharkButton>
                    <SharkButton variant="filled" buttonType="secondary" px={16} py={11} onClick={onDownloadFromCash}>
                        Download from the cache
                    </SharkButton>
                </Group>
            </Footer>
        </Stack>
    );
}
const Footer = styled(Group)`
    background-color: #ffffff;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
    padding: 14px 32px;
    height: 103px;
    overflow: hidden;
    justify-content: center;
`;
