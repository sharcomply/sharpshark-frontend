import { AppState } from "store";
import { useDispatch, useSelector } from "react-redux";
import { useCallback, useMemo } from "react";
import { appModalActions, ModalState } from "../store";
import { ModalContent, ModalType } from "../interfaces";

export const useModal = (watchType?: ModalType) => {
  const state = useSelector((state: AppState) => state.appModal);
  const dispatch = useDispatch();

  const opened = useMemo(() => {
    return state.open && state.type === watchType
  }, [watchType, state.open, state.type])

  const handleOpen = useCallback(
    (payload: { type: ModalType; content: ModalContent }) => {
      dispatch(appModalActions.open(payload));
    },
    [dispatch]
  );

  const handleSwitch = useCallback(
    (content: ModalContent) => {
      dispatch(appModalActions.switch(content));
    },
    [dispatch]
  );

  const setModalContent = useCallback(
    (modaContent: ModalState["modaContent"]) => {
      dispatch(appModalActions.setModalContent(modaContent));
    },
    [dispatch]
  );

  const setModalStatus = useCallback(
    (modalStatus: ModalState["modalStatus"]) => {
      dispatch(appModalActions.setModalStatus(modalStatus));
    },
    [dispatch]
  );

  const handleClose = useCallback(() => {
    dispatch(appModalActions.close());
  }, [dispatch]);

  return {
    state,
    opened,
    handleOpen,
    handleClose,
    handleSwitch,
    setModalStatus,
    setModalContent
  };
};
