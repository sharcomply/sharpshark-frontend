import { useMemo } from "react";
import { useQuery } from "react-query";
import { backend } from "api";

export const useCountriesList = () => {
    const {
        data: rawData,
        refetch,
        isLoading,
        isError,
        remove,
    } = useQuery(["COUNTRIES_LIST"], () => backend.constants.getCountriesList(), {
        // enabled: true,
        keepPreviousData: true,
        // retry: 1,
    });

    const data = useMemo(() => {
        if (!rawData) return undefined;

        const arr: any = [];
        for (let i of rawData) {
            arr.push({
                value: i.code,
                label: i.name,
            });
        }
        return arr;
    }, [rawData]);

    return {
        rawData,
        data,
        refetch,
        isLoading,
        isError,
        remove,
    };
};
