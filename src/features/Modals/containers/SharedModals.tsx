import { Modal } from "@mantine/core";
import { ModalContent, ModalType } from "../interfaces";
import { useModal } from "../hooks/useModal";
import InviteMembers from "../components/InviteMembers";
import CustomModal from "../components/CustomModal";
import ChangePaymentLicense from "../components/ChangePaymentLicense";
import TextEditorAddImage from "../components/TextEditorAddImage";

export default function SharedModals() {
    const { opened, handleClose, state } = useModal(ModalType.SHARED);

    return (
        <Modal
            opened={opened}
            onClose={handleClose}
            withCloseButton={false}
            overlayOpacity={0.5}
            transition="slide-down"
            transitionDuration={400}
            closeOnClickOutside={false}
            centered
            size={"400px"}
            padding={0}
            radius={12}
        >
            {state.content === ModalContent.InviteMembers && <InviteMembers />}
            {state.content === ModalContent.CustomModal && <CustomModal />}
            {state.content === ModalContent.ChangePaymentLicense && <ChangePaymentLicense />}
            {state.content === ModalContent.TextEditorAddImage && <TextEditorAddImage />}
        </Modal>
    );
}
