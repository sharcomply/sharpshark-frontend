import {Modal} from "@mantine/core";
import {ModalContent, ModalType} from "../interfaces";

import {useModal} from "../hooks/useModal";
import ErrorModal from "../components/ErrorModal";

export default function GeneralModals() {
  const { opened, handleClose, state } = useModal(ModalType.GENERAL);


  return (
    <Modal
      opened={opened}
      onClose={handleClose}
      withCloseButton={false}
      overlayOpacity={0.5}
      transition="slide-down"
      transitionDuration={400}
      closeOnClickOutside={false}
      padding={30}
      size={"390px"}
    >
      {state.content === ModalContent.Error && <ErrorModal/>}
    </Modal>
  );
}
