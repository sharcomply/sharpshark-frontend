import { Modal } from "@mantine/core";
import { ModalContent, ModalType } from "../interfaces";
import { useNavigate } from "react-router-dom";

import { useModal } from "../hooks/useModal";
import TakeItToCourt from "../components/TakeItToCourt";
import { useCallback } from "react";
import ClaimBeSent from "../components/ClaimBeSent";
import SentTheClaim from "../components/SentTheClaim";
import NoTraceOfContacts from "../components/NoTraceOfContacts";
import ClaimIsSent from "../components/ClaimIsSent";

export default function DisputesModals() {
    const navigate = useNavigate();
    const { opened, handleClose, state } = useModal(ModalType.DISPUTE);

    const onClose = useCallback(() => {
        handleClose();
        navigate({ hash: undefined }, { replace: true });
    }, [handleClose, navigate]);

    return (
        <Modal
            opened={opened}
            onClose={onClose}
            withCloseButton={false}
            overlayOpacity={0.5}
            transition="slide-down"
            transitionDuration={400}
            closeOnClickOutside={false}
            padding={0}
            size={"480px"}
        >
            {state.content === ModalContent.TakeItToCourt && <TakeItToCourt />}
            {state.content === ModalContent.ClaimBeSent && <ClaimBeSent />}
            {state.content === ModalContent.SentTheClaim && <SentTheClaim />}
            {state.content === ModalContent.NoTraceOfContacts && <NoTraceOfContacts />}
            {state.content === ModalContent.ClaimIsSent && <ClaimIsSent />}
        </Modal>
    );
}
