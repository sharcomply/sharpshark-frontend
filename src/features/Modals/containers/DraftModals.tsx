import { Modal } from "@mantine/core";
import { ModalContent, ModalType } from "../interfaces";
import { useLocation, useNavigate } from "react-router-dom";

import { useModal } from "../hooks/useModal";
import ExistingPublications from "../components/ExistingPublications";
import { useCallback, useEffect } from "react";

export default function DraftModals() {
    const { hash } = useLocation();
    const navigate = useNavigate();
    const { opened, handleClose, state, handleOpen } = useModal(ModalType.DRAFT);

    useEffect(() => {
        if (hash === "#scan-result") {
            handleOpen({ type: ModalType.DRAFT, content: ModalContent.ExistingPublications });
        }
    }, [handleOpen, hash]);

    const onClose = useCallback(() => {
        handleClose();
        navigate({ hash: undefined }, { replace: true });
    }, [handleClose, navigate]);

    return (
        <Modal
            opened={opened}
            onClose={onClose}
            withCloseButton={false}
            overlayOpacity={0.5}
            transition="slide-down"
            transitionDuration={400}
            closeOnClickOutside={false}
            padding={0}
            size={"760px"}
        >
            {state.content === ModalContent.ExistingPublications && <ExistingPublications />}
        </Modal>
    );
}
