import {Modal} from "@mantine/core";
import {ModalContent, ModalType} from "../interfaces";

import {useModal} from "../hooks/useModal";
import SignIn from "../components/SignIn";
import SignUp from "../components/SignUp";
import LoginByEmail from "../components/LoginByEmail";
import ForgotPassword from "../components/ForgotPassword";
import TeamSignUp from "../components/TeamSignUp";
import IndividualSignUp from "../components/IndividualSignUp";
import VerificationForm from "../components/VerificationForm";
import ErrorModal from "../components/ErrorModal";

export default function AuthModals() {
    const { opened, handleClose, state } = useModal(ModalType.AUTH);



    return (
        <Modal
            opened={opened}
            onClose={handleClose}
            withCloseButton={false}
            overlayOpacity={0.5}
            transition="slide-down"
            transitionDuration={400}
            closeOnClickOutside={false}
            padding={30}
            size={"390px"}
        >
            {state.content === ModalContent.SignIn && <SignIn />}
            {state.content === ModalContent.SignUp && <SignUp />}
            {state.content === ModalContent.LoginByEmail && <LoginByEmail />}
            {state.content === ModalContent.KYCVerification && <VerificationForm />}
            {state.content === ModalContent.ForgotPassword && <ForgotPassword />}
            {state.content === ModalContent.TeamSignUp && <TeamSignUp />}
            {state.content === ModalContent.IndividualSignUp && <IndividualSignUp />}
            {state.content === ModalContent.Error && <ErrorModal/>}
        </Modal>
    );
}
