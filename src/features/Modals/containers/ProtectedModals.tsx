import { Modal } from "@mantine/core";
import { ModalContent, ModalType } from "../interfaces";
import { useModal } from "../hooks/useModal";
import Preferences from "../components/Preferences";
import SetEmail from "../components/SetEmail";
import StartMonitoring from "../components/StartMonitoring";
import PaymentChoice from "../components/PaymentChoice";
import SetLicenseParameters from "../components/SetLicenseParameters";
import UsingYourContent from "../components/UsingYourContent";
import LicenseAndMode from "../components/LicenseAndMode";
import BeforeCloseSetLicence from "../components/BeforeCloseSetLicence";

export default function ProtectedModals() {
    const { opened, handleClose, state } = useModal(ModalType.PROTECTED);

    return (
        <Modal
            opened={opened}
            onClose={handleClose}
            withCloseButton={false}
            overlayOpacity={0.5}
            transition="slide-down"
            transitionDuration={400}
            closeOnClickOutside={true}
            size={"auto"}
            padding={0}
            radius={12}
            styles={{ root: { overflow: "hidden" }, body: { position: "relative" } }}
        >
            {state.content === ModalContent.PaymentChoice && <PaymentChoice />}
            {state.content === ModalContent.SetLicenseParameters && <SetLicenseParameters />}
            {state.content === ModalContent.Preferences && <Preferences />}
            {state.content === ModalContent.SetEmail && <SetEmail />}
            {state.content === ModalContent.StartMonitoring && <StartMonitoring />}
            {state.content === ModalContent.UsingYourContent && <UsingYourContent />}
            {state.content === ModalContent.LicenseAndMode && <LicenseAndMode />}
            {state.content === ModalContent.BeforeCloseSetLicence && <BeforeCloseSetLicence />}
        </Modal>
    );
}
