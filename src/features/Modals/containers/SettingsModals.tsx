import {Modal} from "@mantine/core";
import {ModalContent, ModalType} from "../interfaces";
import {useModal} from "../hooks/useModal";
import ChangeEmail from "../components/ChangeEmail";
import ChangePassword from "../components/ChangePassword";
import ChangeName from "../components/ChangeName";
import AddName from "../components/AddName";
// import ChangeNearWallet from "../components/ChangeNearWallet";

export default function SettingsModals() {
    const { opened, handleClose, state } = useModal(ModalType.SETTINGS);

    return (
        <Modal
            opened={opened}
            onClose={handleClose}
            withCloseButton={false}
            overlayOpacity={0.5}
            transition="slide-down"
            transitionDuration={400}
            closeOnClickOutside={false}
            padding={30}
            size={"380px"}
            centered
            radius={12}
        >
            {state.content === ModalContent.ChangePassword && <ChangePassword />}
            {state.content === ModalContent.ChangeEmail && <ChangeEmail />}
            {state.content === ModalContent.ChangeName && <ChangeName />}
            {state.content === ModalContent.AddName && <AddName />}
            {/* {state.content === ModalContent.ChangeNearAddress && <ChangeNearWallet/>} */}
        </Modal>
    );
}
