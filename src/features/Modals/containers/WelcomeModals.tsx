import { useMemo } from "react";
import styled from '@emotion/styled';
import { Modal } from '@mantine/core';
import { useModal } from '../hooks/useModal';
import { ModalType } from '../interfaces';
import { WelcomeData } from '../data/welcome.data';
import WelcomeContent from '../components/WelcomeContent';

// Component
export default function WelcomeModals() {
    const { opened, handleClose, handleSwitch, state } = useModal(ModalType.WELCOME);
    
    const currentModal = useMemo(() => {
        return WelcomeData.find(({ contentType }) => contentType === state.content)
    }, [state.content])
    
    return (
        <Modal
            opened={opened}
            onClose={handleClose}
            withCloseButton={false}
            overlayOpacity={0.6}
            transition="slide-down"
            transitionDuration={400}
            closeOnClickOutside={false}
            padding={0}
            centered
            size={"480px"}
            overlayColor="#454b73"
            radius={12}
            shadow={"0 8px 16px rgb(0 0 0 / 16%)"}
        >
            <Container>
                {currentModal && (
                    <WelcomeContent item={currentModal} onSubmit={handleClose} onSwitch={handleSwitch} />
                )}
            </Container>
        </Modal>
    )
}

// Styling
const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    min-height: 533px;
`;
