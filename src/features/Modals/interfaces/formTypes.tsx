export interface ForgotPasswordInputTypes {
    email: string;
}

export interface SignUpInputTypes {
    fullName: string;
    email: string;
    password: string;
    confirmPassword: string;
}

export interface LoginInputTypes {
    email: string;
    password: string;
}

export interface KYCVerificationTypes {
    email?: string;
    country?: string;
    language?: string;
}
export interface ChangeEmailTypes {
    email: string;
}

export interface  ChangeNearAddressTypes {
    walletAddressNear: string
}

export interface LicenseParameters {
    amountWei: string;
    licenseHash: string;
}
