export enum ModalType {
    AUTH,
    WELCOME,
    SETTINGS,
    SHARED,
    PROTECTED,
    DRAFT,
    CERTIFICATE,
    DISPUTE,
    GENERAL
}

export enum ModalContent {
    SignIn,
    SignUp,
    LoginByEmail,
    KYCVerification,
    ForgotPassword,
    TeamSignUp,
    IndividualSignUp,
    PaymentChoice,
    SetLicenseParameters,
    Welcome,
    Protect,
    Detect,
    Reject,
    ChangePassword,
    ChangeEmail,
    ChangeName,
    AddName,
    InviteMembers,
    CustomModal,
    TextEditorAddImage,
    ChangePaymentLicense,
    Preferences,
    SetEmail,
    ExistingPublications,
    StartMonitoring,
    UsingYourContent,
    TakeItToCourt,
    ClaimBeSent,
    SentTheClaim,
    NoTraceOfContacts,
    LicenseAndMode,
    BeforeCloseSetLicence,
    ClaimIsSent,
    Error,
    Loading
    // ChangeNearAddress,
}
