import { useQuery } from 'react-query';
import { backend } from 'api';
import { useContract } from 'blockchain/hooks/useContract';
import { SharkContract } from 'blockchain';
import { useParams } from 'react-router-dom';
import {useNearContext} from "../../../providers/NearProvider";
import useDraftStore from "../../DraftDetails/hooks/useDraftStore";

const request = async(contract:SharkContract, id: string) => {

    try {
        const draft = await backend.drafts.getDraft(id)
        const tokenInfo = await contract.getTokenInfo(draft.tokenId);
        return {
            ...draft,
            licenceType: tokenInfo[7],
            price: tokenInfo[0].toString(),
            isSell: tokenInfo[9],
            contentHash: draft.metadata.SourceIPFSHash
        }
    } catch (error) {
        console.log(error)
    }

}

const request_near = async(contract:any, id: string) => {

    try {
        const draft = await backend.drafts.getDraft(id)

        const tokenInfo = await contract.get_token_info({token_id: draft.tokenId});

        return {
            ...draft,
            licenceType: tokenInfo.license_hash,
            price: tokenInfo.sale_amount,
            isSell: tokenInfo.sale_is_enabled,
            contentHash: draft.metadata.SourceIPFSHash
        }
    } catch (error) {
        console.log(error)
    }

}


export function useDetailedDraft() {
    const contract = useContract()
    const Near = useNearContext()
    const {protectId} = useParams()
    const {mintedNetworks} = useDraftStore()

    const { data: draft, ...options } = useQuery(['DETAILED_DRAFT', protectId], mintedNetworks! !== 'near' ? () => request(contract, protectId!) : () =>  request_near(Near?.Contract, protectId!), {
        enabled: !!protectId,
        retry: false,
        refetchOnWindowFocus: false,
        keepPreviousData: true
    });



    return {
        draft,
        ...options
    };
}
