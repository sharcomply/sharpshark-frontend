import styled from "@emotion/styled";
import { Icon } from "@iconify/react";
import { Box, Group, Stack, Table, Text, Tooltip } from "@mantine/core";
import { getConvertFromWei } from "api/requests/drafts";
import { SharkButton } from "components/SharkButton";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import { useModal } from "features/Modals/hooks/useModal";
import { ModalContent, ModalType } from "features/Modals/interfaces";
import { humanizeDate } from "helpers/humanizeDate";
import { useCallback, useEffect, useMemo } from "react";
import { Link, useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useDraftVersionStore } from "features/NewDraftVersion/hooks/useDraftVersionStore";
import { useDetailedDraft } from "./hooks/useDetailedDraft";
import useProtected from "features/ProtectedDetails/hooks/useProtected";
import useAuthStore from "features/Auth/hooks/useAuthStore";
import { getNetworkById, getNetworkBySymbol } from "constants/networks";
import { useDraftAlert } from "../DraftDetails/hooks/useDraftAlert";
import { useUnmount } from "../../hooks/useUnmount";

const ProtectedRights = () => {
    const { licence, setLicenceParameters, setTokenId, mintedNetworks } = useDraftStore();
    const { draft, refetch } = useDetailedDraft();
    const { handleOpen } = useModal();
    const { t } = useTranslation();
    const { setParent, setInitialForm } = useDraftVersionStore();
    const { protectId } = useParams();
    const { protect: draftProtected } = useProtected(protectId);
    const alert = useDraftAlert();
    const { network } = useAuthStore();
    const { setMintedNetworks } = useDraftStore();
    const networkLabel = getNetworkBySymbol(mintedNetworks)?.label;

    useEffect(() => {
        alert.actions.close();
        if (mintedNetworks !== getNetworkById(network)?.symbol) {
            alert.actions.open();
            alert.actions.warn(t("Change Network!"), t(`To change the license, you need to change networks to ${networkLabel}.`));
        }
    }, [mintedNetworks, network, networkLabel, t]);

    useUnmount(() => {
        alert.actions.clear();
        alert.actions.close();
    });

    useEffect(() => {
        setMintedNetworks(draftProtected?.mintedNetworks[0].name);
    }, [draftProtected, setMintedNetworks]);

    const handleOpenModal = useCallback(() => {
        handleOpen({ type: ModalType.PROTECTED, content: ModalContent.SetLicenseParameters });
    }, [handleOpen]);

    const getConvertFromWeiRequest = useCallback(async () => {
        if (!draft) return;
        // if (!Number(draft?.price) || !!licence.price) return;
        const response = await getConvertFromWei({ amount: draft.price?.toString()! });
        switch (mintedNetworks) {
            case "polygon":
                setLicenceParameters({ type: draft?.licenceType, price: response.polygon, isSell: draft?.isSell });
                break;
            case "eth":
                setLicenceParameters({ type: draft?.licenceType, price: response.eth, isSell: draft?.isSell });
                break;
            case "bsc":
                setLicenceParameters({ type: draft?.licenceType, price: response.bsc, isSell: draft?.isSell });
                break;
            case "near":
                if (!response.near) return;
                setLicenceParameters({ type: draft?.licenceType, price: response.near, isSell: draft?.isSell });
                break;
        }
    }, [draft, mintedNetworks, setLicenceParameters]);

    useEffect(() => {
        if (draft?.licenceType) {
            getConvertFromWeiRequest();
        }
    }, [draft, getConvertFromWeiRequest]);

    useEffect(() => {
        if (!draft) return;
        const update = async () => {
            await refetch();
        };

        update();

        if (!licence.price) {
            getConvertFromWeiRequest();
        }

        setTokenId(draft.tokenId);
    }, [refetch, setLicenceParameters, setTokenId, licence]);

    const lastParent = useMemo(() => {
        if (!draft?.versionsHistory) return draft;

        const versionSize = Object.keys(draft.versionsHistory).length;
        const lastParentId = draft.versionsHistory[versionSize];

        return { ...draft, _id: lastParentId };
    }, [draft]);

    const handleUpdate = useCallback(() => {
        if (!lastParent || !draft) return;

        setParent(lastParent);
        setInitialForm({
            title: draft.title,
            fullTitle: draft.metadata?.Title || "",
            type: draft.type || "text",
            text: draft.text || "",
            image: draft.imageFile,
            authors: draft.authors,
            coAuthors: draft.coAuthors,
            participants: draft.participants,
            additionalImages: draft.additionalImages,
        });
    }, [draft, lastParent, setInitialForm, setParent]);

    return (
        <>
            <FormRoot sx={{ minHeight: "380px" }} spacing={0}>
                <Table style={{ borderCollapse: "collapse", tableLayout: "fixed" }}>
                    <thead>
                        <tr>
                            <th style={{ width: "30%", borderBottom: "none" }}></th>
                            <th style={{ width: "5%", borderBottom: "none" }}></th>
                            <th style={{ width: "65%", borderBottom: "none" }}></th>
                        </tr>
                    </thead>
                    <tbody>
                        <>
                            <tr>
                                <td style={{ width: "30%", borderBottom: "none" }}>
                                    <Group px={16} align="center" sx={{ height: 54 }}>
                                        <Text size={"sm"} color={"#666892"} weight={"400"}>
                                            Full title
                                        </Text>
                                    </Group>
                                </td>
                                <td style={{ width: "5%", borderBottom: "none" }}></td>
                                <td style={{ width: "65%", borderBottom: "none" }}>
                                    <Group px={16} align="center">
                                        <Text size={"md"} weight={"500"} sx={{ whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis" }}>
                                            {draft?.fullTitle}
                                        </Text>
                                    </Group>
                                </td>
                            </tr>
                            <tr>
                                <td style={{ width: "30%", borderBottom: "none" }}>
                                    <Group px={16} align="center" sx={{ height: 54 }}>
                                        <Text size={"sm"} color={"#666892"} weight={"400"}>
                                            Mode
                                        </Text>
                                    </Group>
                                </td>
                                <td style={{ width: "5%", borderBottom: "none" }}></td>
                                <td style={{ width: "65%", borderBottom: "none" }}>
                                    <Group px={16} align="center">
                                        <Text size={"md"} color={"#666892"} weight={"500"}>
                                            Coming Soon
                                        </Text>
                                    </Group>
                                </td>
                            </tr>
                            <tr>
                                <td style={{ width: "30%", borderBottom: "none" }}>
                                    <Group px={16} align="center" sx={{ height: 54 }}>
                                        <Text size={"sm"} color={"#666892"} weight={"400"}>
                                            Token Id
                                        </Text>
                                    </Group>
                                </td>
                                <td style={{ width: "5%", borderBottom: "none" }}></td>
                                <td style={{ width: "65%", borderBottom: "none" }}>
                                    <Group px={16} align="center">
                                        <Text size={"md"} color={"#666892"} weight={"500"}>
                                            {draft?.tokenId}
                                        </Text>
                                    </Group>
                                </td>
                            </tr>
                            <tr>
                                <td style={{ width: "30%", borderBottom: "none" }}>
                                    <Group px={16} align="center" sx={{ height: 54 }}>
                                        <Text size={"sm"} color={"#666892"} weight={"400"}>
                                            Metadata Hash
                                        </Text>
                                    </Group>
                                </td>
                                <td style={{ width: "5%", borderBottom: "none" }}></td>
                                <td style={{ width: "65%", borderBottom: "none" }}>
                                    <Group px={16} align="center">
                                        <Text size={"md"} color={"#666892"} weight={"500"}>
                                            {draft?.metadataHash}
                                        </Text>
                                    </Group>
                                </td>
                            </tr>
                            <tr>
                                <td style={{ width: "30%", borderBottom: "none" }}>
                                    <Group px={16} align="center" sx={{ height: 54 }}>
                                        <Text size={"sm"} color={"#666892"} weight={"400"}>
                                            Content Hash
                                        </Text>
                                    </Group>
                                </td>
                                <td style={{ width: "5%", borderBottom: "none" }}></td>
                                <td style={{ width: "65%", borderBottom: "none" }}>
                                    <Group px={16} align="center">
                                        <Text size={"md"} color={"#666892"} weight={"500"}>
                                            {draft?.contentHash}
                                        </Text>
                                    </Group>
                                </td>
                            </tr>
                            <tr>
                                <td style={{ width: "30%", borderBottom: "none" }}>
                                    <Group px={16} align="center" sx={{ height: 54 }}>
                                        <Text size={"sm"} color={"#666892"} weight={"400"}>
                                            Licence
                                        </Text>
                                    </Group>
                                </td>
                                <td style={{ width: "5%", borderBottom: "none" }}></td>
                                <td style={{ width: "65%", borderBottom: "none" }}>
                                    <Group px={16} align="center">
                                        <Text size={"md"} color={!!licence.price ? "#666892" : "dark"} weight={"500"}>
                                            {licence.type ? licence.type : "License not for sale"}
                                        </Text>
                                    </Group>
                                </td>
                            </tr>
                            <tr>
                                <td style={{ width: "30%", borderBottom: "none" }}>
                                    <Group px={16} align="center" sx={{ height: 54 }}>
                                        <Text size={"sm"} color={"#666892"} weight={"400"}>
                                            Price
                                        </Text>
                                    </Group>
                                </td>
                                <td style={{ width: "5%", borderBottom: "none" }}></td>
                                <td style={{ width: "65%", borderBottom: "none" }}>
                                    <Group px={16} align="center">
                                        <PriceContainer>
                                            <Text size={"md"} color={!!licence.price ? "#666892" : "dark"} weight={"500"}>
                                                {licence.isSell ? `US$ ${licence.price}` : "License not for sale"}
                                            </Text>
                                            {mintedNetworks === getNetworkById(network)?.symbol && (
                                                <SharkButton
                                                    buttonType="primary"
                                                    onClick={handleOpenModal}
                                                    variant="filled"
                                                    size="small"
                                                    px={8}
                                                    py={4}
                                                    style={{ marginTop: "20px" }}
                                                >
                                                    {licence.isSell ? "Change preferences" : "Set for sale"}
                                                </SharkButton>
                                            )}
                                        </PriceContainer>
                                    </Group>
                                </td>
                            </tr>
                            <tr>
                                <td style={{ width: "30%", borderBottom: "none" }}>
                                    <Group px={16} align="center" sx={{ height: 54 }}>
                                        <Text size={"sm"} color={"#666892"} weight={"400"}>
                                            Draft created
                                        </Text>
                                    </Group>
                                </td>
                                <td style={{ width: "5%", borderBottom: "none" }}></td>
                                <td style={{ width: "65%", borderBottom: "none" }}>
                                    <Group px={16} align="center">
                                        <Text size={"md"} weight={"500"}>
                                            {humanizeDate(draft?.createdAt)?.fullDate}
                                        </Text>
                                    </Group>
                                </td>
                            </tr>
                            <tr>
                                <td style={{ width: "30%", borderBottom: "none" }}>
                                    <Group px={16} align="center" sx={{ height: 54 }}>
                                        <Text size={"sm"} color={"#666892"} weight={"400"}>
                                            Document protected
                                        </Text>
                                    </Group>
                                </td>
                                <td style={{ width: "5%", borderBottom: "none" }}></td>
                                <td style={{ width: "65%", borderBottom: "none" }}>
                                    <Group px={16} align="center">
                                        <Text size={"md"} weight={"500"}>
                                            {humanizeDate(draft?.updatedAt)?.fullDate}
                                        </Text>
                                    </Group>
                                </td>
                            </tr>
                            {draft?.versionsHistoryObjects &&
                                Object.values(draft?.versionsHistoryObjects!).map((value, index) => (
                                    <tr key={value._id} style={{ height: 54 }}>
                                        <td style={{ width: "30%", borderBottom: "none" }}>
                                            {index === 0 && (
                                                <Group pl={16} style={{ width: "100%" }}>
                                                    <Text size={"sm"} color={"#666892"} weight={"400"}>
                                                        Document updated
                                                    </Text>
                                                </Group>
                                            )}
                                        </td>

                                        <td style={{ width: "5%", borderBottom: "none", padding: 0 }}>
                                            <Text align={"right"} weight={"500"} color={"#666892"}>
                                                v.{value.version}
                                            </Text>
                                        </td>

                                        <td style={{ width: "65%", borderBottom: "none" }}>
                                            <Group position={"left"}>
                                                {value.isMinted && (
                                                    <>
                                                        <Text pl={16} weight={"500"}>
                                                            {humanizeDate(value?.updatedAt).fullDate}
                                                        </Text>
                                                        <Box pl={16}>
                                                            <Link to={`/certificate/${value.tokenId}`}>
                                                                <Text color={"#4700E5"} weight={"500"}>
                                                                    Document {"  "} Certificate
                                                                </Text>
                                                            </Link>
                                                        </Box>
                                                    </>
                                                )}

                                                {!value.isMinted && (
                                                    <>
                                                        <Text pl={16} weight={"500"}>
                                                            In progress
                                                        </Text>
                                                        <Box pl={16}>
                                                            <Link to={`/protected/${value.tokenId}`}>
                                                                <Text color={"#4700E5"} weight={"500"}>
                                                                    Go to draft
                                                                </Text>
                                                            </Link>
                                                        </Box>
                                                    </>
                                                )}
                                            </Group>
                                        </td>
                                    </tr>
                                ))}
                        </>
                    </tbody>
                </Table>

                <Footer px={16}>
                    <Group>
                        <Text size={"sm"} weight={"400"} color={"#666892"}>
                            {t("protected_drafts.protected_on", { date: humanizeDate(draft?.updatedAt, "at").fullDate })},{" "}
                            {t("protected_drafts.updated", { date: humanizeDate(draft?.updatedAt, "at").fullDate })}
                        </Text>
                    </Group>
                    <Group spacing={12}>
                        <Group spacing={8}>
                            <Tooltip
                                label={t("textfields.lables.current_work_version")}
                                position="bottom"
                                placement="center"
                                transition="fade"
                                transitionDuration={300}
                                transitionTimingFunction="ease"
                                withArrow={false}
                                styles={{ body: { backgroundColor: "#666892" } }}
                            >
                                <Icon icon={"ep:question-filled"} color={"#666892"} />
                            </Tooltip>
                            <Text size={"sm"} weight={"400"} color={"#666892"}>
                                v.{draft?.version}
                            </Text>
                        </Group>
                        <SharkButton
                            as={Link}
                            to={`/protected/new`}
                            onClick={handleUpdate}
                            variant="outlined"
                            buttonType="secondary"
                            size="small"
                            px={8}
                            py={4}
                        >
                            {t("buttons.update_this_version")}
                        </SharkButton>
                    </Group>
                </Footer>
            </FormRoot>
        </>
    );
};

export default ProtectedRights;

// Styling
const FormRoot = styled(Stack)`
    position: relative;
    background-color: #fbfcff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    overflow: hidden;
    justify-content: space-between;
`;

const Footer = styled(Group)`
    position: relative;
    background-color: #ffffff;
    border-top: 1px solid #dddeee;
    height: 56px;
    overflow: hidden;
    justify-content: space-between;
`;

const PriceContainer = styled.div`
    display: flex;
    flex-direction: column;
`;
