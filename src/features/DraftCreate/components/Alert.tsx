import styled from "@emotion/styled";
import { Group, Stack, Text } from "@mantine/core";
import Icon from "components/Icon";
import { ReactNode, useMemo } from "react";

// Props
type ComponentProps = {
    variant?: 'error' | string,
    title?: string;
    text?: string | ReactNode;
}

// Component
export const Alert = (props: ComponentProps) => {
    const { text = '', title, variant } = props;
    
    const variables = useMemo(() => {
        let backgroundColor = 'transparent'
        let borderColor = 'transparent'
        let titleColor = '#0A001E'
        let iconColor = '#0A001E'

        if (variant === 'error') {
            backgroundColor = '#FFF2F8'
            borderColor = '#E60C54'
            titleColor = '#E60C54'
            iconColor = '#E60C54'
        }

        return { backgroundColor, borderColor, titleColor, iconColor }
    } ,[variant])

    return (
        <Root px={16} p={16} spacing={4} sx={{ backgroundColor: variables.backgroundColor, borderColor: variables.borderColor }}>
            <Group spacing={16} px={14}>
                <Icon icon="ic:outline-info" color={variables.iconColor} />
                <Text size={'md'} color={variables.titleColor} weight={"500"}>{title}</Text>
            </Group>
            <Group px={48} position={'apart'}>
                <StyledText size={'sm'} color={'#666892'}>{text}</StyledText>
            </Group>
        </Root>
    )
}

// Styling
const Root = styled(Stack)`
    border-width: 1px;
    border-style: solid;
    border-radius: 8px;
`

const StyledText = styled(Text as any)`
    & a {
        color: #4700E5;
        text-decoration: underline;
    }
`
