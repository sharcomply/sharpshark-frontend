import { useMutation, useQueryClient } from "react-query";
import { backend } from "api";
import { useCallback, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useSidebarCounts } from "features/Sidebar/hooks/useSidebarCounts";
import { DraftCreateFormState } from "./useDraftCreateForm";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";
import { useDraftStatus } from '../../DraftDetails/hooks/useDraftStatus';
import { useTranslation } from "react-i18next";


export const useDraftCreate = () => {
    const { t } = useTranslation()
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const { refetch } = useSidebarCounts();
    const alert = useDraftAlert();
    const [isLoading, setLoading] = useState(false);
    const status = useDraftStatus()

    const { mutateAsync: create, isLoading: createLoading } = useMutation(backend.drafts.saveDraft, {
        async onSuccess() {
            await Promise.all([
                queryClient.invalidateQueries([`DRAFT_LIST`]),
                queryClient.invalidateQueries(["DRAFT"])
            ]);
        },
    });

    const { mutateAsync: update, isLoading: updateLoading } = useMutation(backend.drafts.editDraft, {
        async onSuccess() {
            await Promise.all([
                queryClient.invalidateQueries([`DRAFT_LIST`]),
                queryClient.invalidateQueries(["DRAFT"])
            ]);
        },
    });

    const handleCreate = useCallback(
        async (form: DraftCreateFormState) => {
            const payload = {
                uploadFile: form.file || form.image,
                fileExt: "txt",
            }

            setLoading(true)

            if (form.type === 'text' && !form.file) {
                const blob = new Blob([form.text], { type: "text/plain" });
                payload.uploadFile = new File([blob], form.title);
            }

            if (form.type === 'image' && form.image) {
                payload.fileExt = form.image.type.split("/")[1]
            }

            try {
                status.actions.update({ type: undefined, message: t("statuses.uploading") })
                // Save Draft
                const { _id } = await create({
                    title: form.title,
                    type: form.type,
                    fullTitle: form.fullTitle,
                    authors: form.authors.filter(i => i.length),
                    coAuthors: form.coAuthors.filter(i => i.length),
                    participants: form.participants.filter(i => i.length),
                });

                // Get Links for Upload And Download
                const { uploadLink, downloadLink } = await backend.drafts.getFileUploadLink(_id, payload.fileExt);

                // Upload File
                await backend.drafts.uploadFile(uploadLink.url, payload.uploadFile!, uploadLink.fields);


                const uploadFile = async () => {
                    let additionalImages: string[] = [];
                    for (const item of form.additionalImages!) {
                        if (typeof item !== "string") {
                            const { uploadLink, downloadLink } = await backend.disputes.getImageUploadLink(_id);
                            additionalImages.push(downloadLink);
                            await backend.drafts.uploadFile(uploadLink.url, item, uploadLink.fields);
                        } else additionalImages.push(item);
                    }
                    return { additionalImages };
                };

                const { additionalImages } = await uploadFile();

                // update draft using link to file
                const draft = await update({
                    _id,
                    title: form.title,
                    linkUrl: downloadLink,
                    additionalImages
                });

                status.actions.update({ type: undefined, message: t("statuses.uploaded") })

                // Redirect to created draft
                await refetch();
                navigate(`/drafts/${draft._id}`, { replace: true });

                alert.actions.close()
                alert.actions.clear()

                setLoading(false)

            } catch (e) {
                console.log("[DRAFT_SAVE]", e);
                setLoading(false)
                status.actions.update({ type: 'warn', message: t("statuses.upload_failed") })
            }
        },
        [alert.actions, create, navigate, refetch, status.actions, update]
    );

    return {
        handleCreate,
        isLoading: createLoading || updateLoading || isLoading,
    };
};
