import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { validationSchema } from "../validationSchema";
import { useEffect } from "react";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";
import { useTranslation } from "react-i18next";

export interface DraftCreateFormState {
    title: string;
    fullTitle: string;
    text: string;
    file?: File;
    image?: File;
    type: "text" | "image";
    authors: string[];
    coAuthors: string[];
    participants: string[];
    additionalImages: string[]
}

export const useDraftCreateForm = () => {
    const alert = useDraftAlert();
    const { t } = useTranslation();

    const form = useForm<DraftCreateFormState>({
        resolver: yupResolver(validationSchema),
        defaultValues: {
            title: "",
            fullTitle: "",
            type: "text",
            text: "",
            file: undefined,
            image: undefined,
            authors: [],
            coAuthors: [],
            participants: [],
            additionalImages: []
        },
    });

    useEffect(() => {
        const error: any = Object.values(form.formState.errors)[0];

        if (!error) return;

        alert.actions.open();

        if (Array.isArray(error) && error.length) {
            error.map((item) => {
                if (item?.message) {
                    alert.actions.error(t("errors.titles.validation"), item.message || "");
                }
            });
        } else {
            alert.actions.error(t("errors.titles.validation"), error.message || "");
        }

        return () => {
            alert.actions.close();
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [alert.actions.error, alert.actions.close, alert.actions.open, alert.actions.error, form.formState.errors]);

    return {
        form,
    };
};
