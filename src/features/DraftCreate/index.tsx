import styled from "@emotion/styled";
import { Stack } from "@mantine/core";
import { SharkButton } from "components/SharkButton";
import PageTitle from "./components/PageTitle";
import { useDraftCreate } from "features/DraftCreate/hooks/useDraftCreate";
import { DraftCreateFormState, useDraftCreateForm } from "./hooks/useDraftCreateForm";
import { DraftCreateForm } from "./components/DraftCreateForm";
import { Alert } from "./components/Alert";
import { useCallback } from "react";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";
import useAuthStore from "features/Auth/hooks/useAuthStore";
import { useUnmount } from "hooks/useUnmount";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import { useTranslation } from "react-i18next";

export default function DraftCreate() {
    const { form } = useDraftCreateForm();
    const alert = useDraftAlert();
    const { clearStore } = useDraftStore();
    const { handleCreate, isLoading } = useDraftCreate();
    const { connected, isLoggedIn } = useAuthStore();

    const { t } = useTranslation();

    const onSubmit = useCallback(
        (data: DraftCreateFormState) => {
            if (!connected || !isLoggedIn) {
                alert.actions.open();
                alert.actions.error(t("errors.titles.auth"), t("errors.messages.auth"));
                return;
            }
            // if (data.type === "text" && data.text.replace(/<\/?\w+[^>]*\/?>/g, "").length < 50) {
            //     form.setError("text", { message: 'Draft "text" must be min 50 characters' });
            //     return;
            // }

            handleCreate(data);
        },
        [alert.actions, connected, handleCreate, isLoggedIn]
    );

    useUnmount(() => {
        alert.actions.clear();
        alert.actions.close();
        clearStore();
    });

    const actionItems = () => (
        <SharkButton type="submit" buttonType="secondary" variant="outlined" loading={isLoading} disabled={isLoading}>
            × {t("buttons.save_close_btn")}
        </SharkButton>
    );

    return (
        <form onSubmit={form.handleSubmit(onSubmit)}>
            <Stack>
                <PageTitle title={t("drafts.new-draft")} tooltipText={t("tooltips.drafts_arent_protected")} actions={actionItems()} />

                {alert.state.show && <Alert title={alert.state.title} text={alert.state.message} variant={alert.state.type} />}

                <FormRoot sx={{ minHeight: "380px" }}>
                    <DraftCreateForm control={form.control} setValue={form.setValue} />
                </FormRoot>
            </Stack>
        </form>
    );
}

const FormRoot = styled(Stack)`
    position: relative;
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    overflow: hidden;
    justify-content: space-between;

    img {
        max-width: 480px;
        height: auto !important;
    }
`;
