import { useQuery } from "react-query";
import { backend } from "api";
import { useParams } from "react-router-dom";

export default function usePublicDraft() {
    const { draftId } = useParams();
    const {
        data: draft,
        isError,
        ...options
    } = useQuery(["PUBLIC_DRAFT_ITEM", draftId], () => backend.drafts.getPublicDraft(draftId), {
        enabled: !!draftId,
        refetchInterval: data => (data ? false : 2000),
        retry: 0,
        keepPreviousData: true,
        refetchOnWindowFocus: false,
    });

    return {
        draft,
        isError,
        ...options,
    };
}
