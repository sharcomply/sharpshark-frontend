import getCertificateData, { getCertificateDataNear } from "../data/getCertificateData";
import { useQuery } from "react-query";
import { useNearContext } from "../../../providers/NearProvider";
import useDraftStore from "../../DraftDetails/hooks/useDraftStore";
import { useContractWithMintedNetwork } from "blockchain/hooks/useContractWithMintedNetwork";

interface error {
    message: string;
    code: string | number;
}

export default function useCertificate(draft: DraftDetails, isCertificateFromCache:boolean) {
    const contract = useContractWithMintedNetwork(isCertificateFromCache);
    const Near = useNearContext()
    const { mintedNetworks } = useDraftStore()

    const {
        data: certificate,
        isLoading,
        isError,
        isLoadingError,
        isFetching,
        error,
        ...options
    } = useQuery(["CERTIFICATE", draft?._id, contract.address, mintedNetworks, isCertificateFromCache],
        mintedNetworks! !== "near" ? () => getCertificateData(draft, contract, isCertificateFromCache) : () => getCertificateDataNear(draft, Near?.Contract!, contract, isCertificateFromCache), {
        enabled: !!draft?._id,
        retry: false,
        refetchOnWindowFocus: false,
        keepPreviousData: true,
        onError: (e: error) => e,
    });

    return {
        certificate,
        isLoading,
        isError,
        isLoadingError,
        isFetching,
        error,
        ...options,
    };
}
