import styled from "@emotion/styled";
import { Divider } from "@mantine/core";

export default function CertificateUnlocked() {
    return (
        <Root>
            <Title>An Introduction to the Principles of Morals and Legislation</Title>
            <p>Chapter I. The Inventor</p>
            <p>
                The man who made the Time Machine — the man I shall call the Time Traveler — was well known in scientific circles a few years since, and the
                fact of his disappearance is also well known. He was a mathematician of peculiar subtlety, and one of our most conspicuous investigators in
                molecular physics. He did not confine himself to abstract science. Several ingenious, and one or two profitable, patents were his: very
                profitable they were, these last, as his handsome house at Richmond testified. To those who were his intimates, however, his scientific
                investigations were as nothing to his gift of speech. In the after-dinner hours he was ever a vivid and variegated talker, and at times his
                fantastic, often paradoxical, conceptions came so thick and close as to form one continuous discourse.
            </p>
            <p>
                As he said: “At these times he was as unlike the popular conception of a scientific investigator as a man could be. His cheeks would flush, his
                eyes grow bright; and the stranger the ideas that sprang and crowded in his brain, the happier and the more animated would be his exposition.”
            </p>
            <p>
                Up to the last there was held at his house a kind of informal gathering, which it was my privilege to attend, and where, at one time or another,
                I have met most of our distinguished literary and scientific men. There was a plain dinner at seven. After that we would adjourn to a room of
                easy-chairs and little tables, and there, with libations of alcohol and reeking pipes, we would invoke the god. At first the conversation was
                mere fragmentary chatter, with some local lacunæ of digestive silence; but toward nine or half-past nine, if the god was favorable, some
                particular topic would triumph by a kind of natural selection, and would become the common interest. So it was, I remember, on the last Thursday
                but one of all—the Thursday when I first heard of the Time Machine.
            </p>
            <p>
                I had been jammed in a corner with a gentleman who shall be disguised as Filby. He had been running down Milton—the public neglects poor Filby's
                little verses shockingly; and as I could think of nothing but the relative status of Filby and the man he criticised, and was much too timid to
                discuss that, the arrival of that moment of fusion, when our several conversations were suddenly merged into a general discussion, was a great
                relief to me.
            </p>
            <p>"What's that is nonsense?" said a well-known Medical Man, speaking across Filby to the Psychologist.</p>
            <p>"He thinks," said the Psychologist, "that Time's only a kind of Space."</p>
            <Divider size={2} color={"black"} />
        </Root>
    );
}

const Root = styled.div`
    padding: 48px 96px 42px 96px;
    background: #ffffff;
    color: #0a001e;
    & p {
        margin-bottom: 20px;
        line-height: 20px;
        font-size: 18px;
        word-break: break-word;
    }
`;

const Title = styled.div`
    text-transform: uppercase;
    font-size: 24px;
    line-height: 30px;
    margin-bottom: 16px;
`;
