import styled from "@emotion/styled";
import { Stack } from "@mantine/core";
import React from "react";
import { useTranslation } from "react-i18next";

interface Props {
    isMinted: boolean;
}

export default function CertificateError({ isMinted }: Props) {
    const { t } = useTranslation();
    return (
        <React.Fragment>
            <Root align="center" justify="center">
                {isMinted
                    ? t("certificate.history.error")
                    : "The document was protected on a network that is not currently supported, protect the document again"}
            </Root>
        </React.Fragment>
    );
}

//Styling
const Root = styled(Stack)`
    height: 100vh;
    background: linear-gradient(92.08deg, #e5e6ff 1.32%, #ffffff 97.84%);
    font-weight: 500;
    font-size: 30px;
    padding: 50px;
    text-align: center;
`;
