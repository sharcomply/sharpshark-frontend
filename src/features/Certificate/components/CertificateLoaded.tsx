import styled from "@emotion/styled";
import {Box, Divider, Grid, Group, Modal, Stack, Text} from "@mantine/core";
import React, {Fragment, useCallback, useEffect, useState} from "react";
import {Link} from "react-router-dom";
import CertificateUnlocked from "features/Certificate/components/CertificateUnlocked";
import {CertificateTypes} from "../../../types/Certificate";
import CertificateVersions from "./Popup";
import CertificateSignBlack from "features/Icons/CertificateSignBlack";
import CertificateSignWhite from "features/Icons/CertificateSignWhite";
import CertificateSignOutlined from "features/Icons/CertificateSignOutlined";
import {useTranslation} from "react-i18next";
import {useCrossTabState} from "hooks/useCrossTbState";
import {useModal} from "features/Modals/hooks/useModal";
import {ModalContent, ModalType} from "features/Modals/interfaces";
import {useProtectedDraftStore} from "features/ProtectedDetails/hooks/useProtectedDraftStore";
import {getConvertFromWei} from "api/requests/drafts";
import {humanizeDate} from "helpers/humanizeDate";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import {useMediaQuery} from "@mantine/hooks";
import {Helmet} from "react-helmet";
import {isIos} from "../../../helpers/isIos";
import changeRightCC, {changeRightCCName} from "helpers/changeRightCC";

type componentProps = {
  certificate: CertificateTypes;
  linkUrl: string;
  fromCache: boolean;
};

export default function CertificateLoaded({certificate, linkUrl, fromCache}: componentProps) {
  const {mintedNetworks} = useDraftStore();
  const mobile = useMediaQuery("(max-width: 600px)");
  const [versionsOpened, setVersionsOpened] = useState<boolean>(false);
  const [price, setPrise] = useState<ConvertFromWeiResult>();
  const handleVersionsClose = () => setVersionsOpened(false);

  const {t} = useTranslation();

  const {
    state: {tokenId},
  } = useProtectedDraftStore();
  const { handleOpen, setModalContent, setModalStatus, handleClose } = useModal();
  const [name, setName] = useCrossTabState("stripeLicence", "");
  const [nameNear, setNearName] = useCrossTabState("nearLicence", "");
  const [nearConnectToPay] = useCrossTabState("nearConnectToPay", "");
  const [nearErrorCode, setNearErrorCode] = useCrossTabState("errorCode", "");

  const handleOpenModal = useCallback(() => {
    handleOpen({type: ModalType.SHARED, content: ModalContent.ChangePaymentLicense});
  }, [handleOpen]);

  useEffect(() => {
    if (nearErrorCode === "userRejected" && localStorage.getItem(`${window.location.href}`) === 'true') {
      handleOpen({type: ModalType.SHARED, content: ModalContent.CustomModal});
      setModalStatus("Cancel");
      setModalContent("License purchase has been canceled");
      localStorage.removeItem(`${window.location.href}`)
      setNearErrorCode("false");
    } else if (nearErrorCode !== "userRejected") {
      localStorage.removeItem(`${window.location.href}`)
    }
  }, [nearErrorCode, handleOpen, setNearErrorCode]);

    useEffect(() => {
        return () => {
            handleClose();
        }
    },[])

  useEffect(() => {
    if (nearConnectToPay === "true") {
      handleOpen({type: ModalType.SHARED, content: ModalContent.ChangePaymentLicense});
    }
  }, [nearConnectToPay, handleOpen]);

  useEffect(() => {
    if (name === "true") {
      handleOpen({type: ModalType.SHARED, content: ModalContent.CustomModal});
      setModalContent(t("certificate.purchase_success", {tokenId: tokenId}));
      setName("false");
    }
  }, [name, handleOpen, setName, setModalContent, tokenId, t]);

  useEffect(() => {
    if (nameNear === "true") {
      handleOpen({type: ModalType.SHARED, content: ModalContent.CustomModal});
      setModalContent(`The purchase for the ${tokenId} token was successful`);
      setNearName("false");
    }
  }, [nameNear, handleOpen, setNearName, setModalContent, tokenId, t]);

  useEffect(() => {
    if (!certificate) return;
    const getConvertFromWeiRequest = async () => {
      const response = await getConvertFromWei({amount: certificate.price?.toString()!});
      setPrise(response);
    };
    getConvertFromWeiRequest();
  }, [certificate]);
  useEffect(() => {
    if (mobile) {
      const html = document.querySelector("html") as any;
      if (!isIos()) {
        html.style.zoom = "30%";
      } else {
        html.style.zoom = "65%";
      }
    } else {
      const html = document.querySelector("html") as any;
      html.style.zoom = "0%";
    }
  }, [mobile]);

  const getUsageRights = (certificate: CertificateTypes): string | undefined => {
    if (certificate.usageRights === "") return "No distribution";
    return certificate.usageRights;
  };

  const getUsageRightsDescription = (usageRights: string | undefined): string => {
    if (usageRights === "Copyrighted (pay-to-use)") return t("certificate.copyrighted_rights");
    if (usageRights === "CC (free with attribution)" || usageRights === "100% free, no attribution") return t("certificate.free_rights");
    if (usageRights === "© all rights reserved, commercial distribution") return t("certificate.commercial_distribution_rights");
    if (usageRights === "© all rights reserved, no distribution") return t("certificate.reserved_no_distribution_rights");
    if (usageRights === "Free-to-use") return t("certificate.free_to_use_rights");
    if (usageRights === "") return t("certificate.no_distribution_rights");
    return "";
  };

  const getPriceText = (isSell: boolean | undefined, usageRights: string | undefined): string => {
    if (isSell) {
      if (!price) return "";
      switch (mintedNetworks) {
        case "polygon":
          return `$ ${price?.polygon}, ${t("certificate.attribution_neded")}`;
        case "eth":
          return `$ ${price?.eth}, ${t("certificate.attribution_neded")}`;
        case "bsc":
          return `$ ${price?.bsc}, ${t("certificate.attribution_neded")}`;
        case "near":
          return `$ ${price?.near}, ${t("certificate.attribution_neded")}`;
      }
    }
    if (usageRights === "Free-to-use") return `$0 (free), ${t("certificate.attribution_neded")}`;
    if (usageRights === "© all rights reserved, no distribution") return `${t("certificate.fair_use_attribution_needed")}`;
    return "";
  };
  // const [htmlString, setHtmlString] = useState("");

  const handleCopyClickText = () => {
    navigator.clipboard.writeText(
      `Title of Work: ${certificate.title!},\nCreator of Work: ${certificate.author!.map((item) => item)},\nLink to Work: ${window.location}.\n`
    );
  };

  const handleCopyClickHtml = () => {
    switch (certificate.usageRights) {
      case "Time Machine © 2022 by H. G. Wells is licensed under CC0 1.0: https://creativecommons.org/publicdomain/zero/1.0/":
        navigator.clipboard.writeText(
          `<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="${
            window.location
          }">${certificate.title}</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="${
            window.location
          }">${certificate.author?.join(
            ", "
          )}</a> is marked with <a href="http://creativecommons.org/publicdomain/zero/1.0?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC0 1.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/zero.svg?ref=chooser-v1"></a></p>
                `
        );
        break;
      case "Time Machine © 2022 by H. G. Wells is licensed under CC BY 4.0: https://creativecommons.org/licenses/by/4.0/":
        navigator.clipboard.writeText(
          `<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="${
            window.location
          }">${certificate.title}</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="${
            window.location
          }">${certificate.author?.join(
            ", "
          )}</a> is licensed under <a href="http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"></a></p>
                
                    `
        );
        break;

      case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/":
        navigator.clipboard.writeText(
          `<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="${
            window.location
          }" >${certificate.title}</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="${
            window.location
          }" >${certificate.author?.join(
            ", "
          )}</a> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>
                    `
        );
        break;

      case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-ND 4.0: https://creativecommons.org/licenses/by-nd/4.0/":
        navigator.clipboard.writeText(
          `<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="${
            window.location
          }" >${certificate.title}</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="${
            window.location
          }" >${certificate.author?.join(
            ", "
          )}</a> is licensed under <a href="http://creativecommons.org/licenses/by-nd/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-ND 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nd.svg?ref=chooser-v1"></a></p>
                

                        `
        );
        break;

      case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC 4.0: https://creativecommons.org/licenses/by-nc/4.0/":
        navigator.clipboard.writeText(
          `<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="${
            window.location
          }" >${certificate.title}</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="${
            window.location
          }" >${certificate.author?.join(
            ", "
          )}</a> is licensed under <a href="http://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"></a></p>

                        `
        );
        break;

      case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/":
        navigator.clipboard.writeText(
          `<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="${
            window.location
          }" >${certificate.title}</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="${
            window.location
          }" >${certificate.author?.join(
            ", "
          )}</a> is licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>

                        `
        );
        break;
      case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-ND 4.0: https://creativecommons.org/licenses/by-nc-nd/4.0/":
        navigator.clipboard.writeText(
          `<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/" href="${
            window.location
          }"><a property="dct:title" rel="cc:attributionURL">${
            certificate.title
          }</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="${window.location}">${certificate.author?.join(
            ", "
          )}</a> is licensed under <a href="http://creativecommons.org/licenses/by-nc-nd/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-ND 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nd.svg?ref=chooser-v1"></a></p>

                        `
        );
        break;
      default:
        break;
    }

    console.log(certificate.usageRights);
    // navigator.clipboard.writeText(
    //     ` <ul>
    //         <li>Title of Work: ${certificate.title!}</li>
    //         <li>Creator of Work: ${certificate.author!.map((item) => item)}</li>
    //         <li>Link to Work: ${window.location as any}</li>
    //     </ul>
    //     `
    // );
  };

  return (
    <React.Fragment>
      <Root>
        {isIos() && (
          <Helmet>
            <meta name="viewport" content="1280"/>
          </Helmet>
        )}
        {fromCache &&
            <Group align={'center'}>
                <Text weight={700} sx={{fontSize: "32px"}}>
                  {t("certificate.from_cache_title")}
                </Text>
                <Text>
                  {t("certificate.from_cache_subtitle")}
                </Text>
            </Group>
        }
        <Group position={"apart"} align={"center"} pr={84}>
          <Text weight={700} sx={{fontSize: "32px"}}>
            {t("certificate.proof_autorship")}
          </Text>
          <CertificateSignWhite width={60} height={60}/>
        </Group>
        <Divider size={6} color={"black"}/>
        <Divider
          size={2}
          color={"black"}
          sx={() => ({
            marginTop: "-13px",
          })}
        />
        <Box pr={84}>
          <Grid>
            <Grid.Col span={5}>
              <Text>{t("certificate.title")}</Text>
            </Grid.Col>
            <Grid.Col span={5}>
              <Text>{t("certificate.author")}</Text>
            </Grid.Col>
          </Grid>
          <Grid>
            <Grid.Col span={5}>
              <Text sx={{whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis"}}>{certificate.title}</Text>
            </Grid.Col>
            <Grid.Col span={7}>
              <Text>{certificate.author?.join(", ")}</Text>
            </Grid.Col>
          </Grid>
          <Divider
            size={2}
            color={"black"}
            sx={() => ({
              margin: "10px 0 14px 0",
            })}
          />
          <Grid style={{gap: "30px"}}>
            <Grid.Col span={5}>
              <Grid.Col>
                <Text sx={{fontSize: "18px", lineHeight: "20px"}}>
                  {t("certificate.usage_rights")}{" "}
                  {!!changeRightCC(getUsageRights(certificate)!) ? (
                    <span dangerouslySetInnerHTML={{__html: changeRightCC(getUsageRights(certificate)!) as any}}/>
                  ) : (
                    getUsageRights(certificate)!
                  )}
                </Text>
                <br/>
                <Text sx={{
                  fontSize: "18px",
                  lineHeight: "20px"
                }}>{getUsageRightsDescription(certificate.usageRights)}</Text>
              </Grid.Col>
            </Grid.Col>
            <Grid.Col span={5}>
              <Grid.Col>
                {!!changeRightCC(getUsageRights(certificate)!) && (
                  <Text
                    sx={{fontSize: "18px", lineHeight: "20px"}}
                    dangerouslySetInnerHTML={{__html: changeRightCCName(getUsageRights(certificate)!)}}
                  />
                )}
                <Text sx={{
                  fontSize: "18px",
                  lineHeight: "20px"
                }}>{getPriceText(certificate.isSell, certificate.usageRights)}</Text>
                {certificate.isSell && (
                  <Fragment>
                    {window.ethereum &&
                      <Button onClick={handleOpenModal}>{t("buttons.buy_licence_btn")}</Button>
                    }
                    <br/>
                    <OutlinedButton style={{backgroundColor: "transparent", color: "#000"}}
                                    onClick={() => handleCopyClickText()}
                    >
                      {t("certificate.coppy_attribution")}
                    </OutlinedButton>
                  </Fragment>
                )}
                {certificate.usageRights !== "" && certificate.price === "0" && (
                  <OutlinedButton
                    onClick={() =>
                      certificate.usageRights !== "© all rights reserved, no distribution" && certificate.usageRights !== "Free-to-use"
                        ? handleCopyClickHtml()
                        : handleCopyClickText()
                    }
                  >
                    {t("certificate.coppy_attribution")}
                  </OutlinedButton>
                )}
                {/*
                            {certificate.usageRights !== "" &&
                                certificate.usageRights !== "© all rights reserved, no distribution" &&
                                certificate.usageRights !== "Free-to-use" &&
                                certificate.price === "0" && (
                                    <OutlinedButton style={{ marginLeft: "16px" }} onClick={handleCopyClickHtml}>
                                        Copy metadata{" "}
                                    </OutlinedButton>
                                )} */}
              </Grid.Col>
            </Grid.Col>
          </Grid>
          <Divider
            size={2}
            color={"black"}
            sx={() => ({
              margin: "10px 0 14px 0",
            })}
          />
          <Grid>
            <Grid.Col span={5}>
              <Group align={"center"}>
                <img width={73} height={73} src={certificate.historyQR} alt=""/>
                <Stack justify="space-between" sx={() => ({marginLeft: "23px", height: "73px", gap: 0})}>
                  <Text>
                    {t("certificate.first_published", {dateTime: humanizeDate(certificate.createdAt).fullDate})}
                    <br/>
                    <Underline>
                      {t("certificate.publish_info.publish_updated", {
                        version: certificate?.versions[0]?.version,
                        dateTime: humanizeDate(certificate.updatedAt).fullDate,
                      })}
                    </Underline>{" "}
                  </Text>
                  <Text>
                    <Underline onClick={() => setVersionsOpened(true)}>{t("certificate.check_full_history")}</Underline>
                  </Text>
                </Stack>
              </Group>
            </Grid.Col>
            <Grid.Col span={5}>
              <Group align={"center"}>
                <img width={73} height={73} src={certificate.ipfsQR} alt=""/>
                <Stack justify="space-between" sx={() => ({marginLeft: "23px", height: "73px", gap: 0})}>
                  <Text>{t("certificate.content_immutable_form")}</Text>
                  <Text>
                    <a href={`https://ipfs.io/ipfs/${certificate.metadataHash}`} target="_blank" rel="noreferrer">
                      <Underline>{t("Check meta-data proof in the IPFS")}</Underline>
                    </a>
                  </Text>
                  <Text>
                    <a href={certificate.contentIpfsLink} target="_blank" rel="noreferrer">
                      <Underline>{t("certificate.check_ipsf")}</Underline>
                    </a>
                  </Text>
                </Stack>
              </Group>
            </Grid.Col>
          </Grid>
          <CertificateUnlocked title={certificate.title}
                               text={fromCache ? certificate.type === 'image' ? certificate.text : certificate.cachedSource || certificate.text : certificate.text}
                               imageUrl={linkUrl}/>
          <Grid pt={40} pb={25}>
            <Grid.Col span={2}>
              <Text>{t("certificate.faq_text")}</Text>
            </Grid.Col>
            <Grid.Col span={9}>
              {Object.keys(t("certificate.faq.questions", {returnObjects: true})).map((key, index) => (
                <Text key={index} sx={{marginTop: index > 0 ? "30px" : 0}}>
                  {t(`certificate.faq.questions.${key}`, {returnObjects: true})}
                </Text>
              ))}
              {changeRightCC(getUsageRights(certificate)!) != "" && (
                <>
                  <br/>
                  <br/>
                  <br/>
                  What is Creative Commons and what do you do?
                  <br/>
                  <br/>
                  <br/>
                  Creative Commons is a global nonprofit organization that enables sharing and reuse of creativity and
                  knowledge through the
                  provision of free legal tools. Our legal tools help those who want to encourage reuse of their works
                  by offering them for
                  use under generous, standardized terms; those who want to make creative uses of works; and those who
                  want to benefit from
                  this symbiosis. Our vision is to help others realize the full potential of the internet. CC has
                  affiliates all over the
                  world who help ensure our licenses work internationally and who raise awareness of our work.
                  <br/>
                  <br/>
                  <br/>
                  Is Creative Commons against copyright?
                  <br/>
                  <br/>
                  <br/>
                  Absolutely not. CC has responded to claims to the contrary. CC licenses are copyright licenses, and
                  depend on the existence
                  of copyright to work. CC licenses are legal tools that creators and other rights holders can use to
                  offer certain usage
                  rights to the public, while reserving other rights. Those who want to make their work available to the
                  public for limited
                  kinds of uses while preserving their copyright may want to consider using CC licenses. Others who want
                  to reserve all of
                  their rights under copyright law should not use CC licenses.
                  <br/>
                  <br/>
                  <br/>
                  What does “Some Rights Reserved” mean?
                  <br/>
                  <br/>
                  <br/>
                  Copyright grants to creators a bundle of exclusive rights over their creative works, which generally
                  include, at a minimum,
                  the right to reproduce, distribute, display, and make adaptations. The phrase “All Rights Reserved” is
                  often used by owners
                  to indicate that they reserve all of the rights granted to them under the law. When copyright expires,
                  the work enters the
                  public domain, and the rights holder can no longer stop others from engaging in those activities under
                  copyright, with the
                  exception of moral rights reserved to creators in some jurisdictions. Creative Commons licenses offer
                  creators a spectrum of
                  choices between retaining all rights and relinquishing all rights (public domain), an approach we call
                  “Some Rights
                  Reserved.”
                </>
              )}
            </Grid.Col>
          </Grid>
          <Divider
            size={2}
            color={"black"}
            sx={() => ({
              margin: "10px 0 14px 0",
            })}
          />
        </Box>
        <Group position={"apart"} pr={84}>
          <Stack justify="center">
            {" "}
            <div dangerouslySetInnerHTML={{__html: t("certificate.copyrights")}}/>
          </Stack>

          <FooterSigns>
            {changeRightCC(getUsageRights(certificate)!) != "" && (
              <img style={{height: "34px"}}
                   src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"></img>
            )}
            <CertificateSignBlack/>
            <CertificateSignWhite/>
            <CertificateSignOutlined/>
          </FooterSigns>
          <Box>
            {t("certificate.side.try_app_mobile")} <br/>{" "}
            <Link to="/">
              {" "}
              <Underline>{t("certificate.check_it_out")}</Underline>
            </Link>
          </Box>
        </Group>
      </Root>
      <Modal
        opened={versionsOpened}
        onClose={handleVersionsClose}
        withCloseButton={false}
        overlayOpacity={0.5}
        transition="slide-down"
        transitionDuration={400}
        closeOnClickOutside={true}
        size={"900px"}
        padding={0}
        radius={12}
        styles={{root: {overflow: "hidden"}, body: {position: "relative"}, modal: {margin: "0 auto !important"}}}
      >
        <CertificateVersions certificate={certificate} handleClose={handleVersionsClose}/>
      </Modal>
    </React.Fragment>
  );
}

//Styling
const Root = styled(Stack)`
  padding: 40px 0 48px 96px;
  background: linear-gradient(92.08deg, #e5e6ff 1.32%, #ffffff 97.84%);
  min-width: 1280px;
`;

const Underline = styled.span`
  text-decoration: underline;
  text-decoration-skip-ink: auto;
`;

const Button = styled.button`
  background: #0a001e;
  border-radius: 2px;
  padding: 10px 18px;
  line-height: 16px;
  color: #dde2ff;
  font-size: 18px;
  vertical-align: middle;
  margin-top: 20px;
  cursor: pointer;
`;

const OutlinedButton = styled(Button)`
  background: transparent;
  border: 2px solid #0a001e;
  color: #fff;
  padding: 8px 18px;
  background-color: #0a001e;
`;

const FooterSigns = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  & svg {
    margin: 0 2px;
  }
`;
