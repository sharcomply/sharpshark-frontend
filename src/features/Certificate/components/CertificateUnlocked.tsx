import styled from "@emotion/styled";
import { Divider, Text } from "@mantine/core";

type CertificateUnlockedProps = {
    title: string | undefined;
    text?: string | undefined;
    imageUrl?: string | undefined;
};

export default function CertificateUnlocked({ title, text, imageUrl }: CertificateUnlockedProps) {
    return (
        <Root>
            <Text
                transform={"uppercase"}
                style={{
                    fontSize: "24px",
                    marginBottom: "16px",
                    wordBreak: "break-all",
                }}
            >
                {title}
            </Text>
            <TextStyled dangerouslySetInnerHTML={{ __html: text! }} />
            {imageUrl && <img style={{ maxWidth: "100%" }} src={imageUrl} alt=""></img>}
            <Divider size={2} color={"black"} />
        </Root>
    );
}

const Root = styled.div`
    padding: 48px 96px 42px 96px;
    max-width: "100%";
    background: #ffffff;
    color: #0a001e;
    margin-top: 40px;
    & p {
        margin-bottom: 20px;
        line-height: 20px;
        font-size: 18px;
        word-break: break-word;
    }
`;

const TextStyled = styled.div`
    & img {
        height: 50%;
        width: 50%;
    }
`;
