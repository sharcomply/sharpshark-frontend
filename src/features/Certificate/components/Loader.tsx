import { Box, Skeleton } from "@mantine/core";

export default function Loader() {
    return (
        <Box sx={{ height: "100vh" }}>
            <Skeleton animate height={"10%"} width={"95%"} mt={6} radius={"md"}></Skeleton>
            <Skeleton animate height={"10%"} width={"90%"} mt={6} radius={"md"}></Skeleton>
            <Skeleton animate height={"15%"} width={"90%"} mt={6} radius={"md"}></Skeleton>
            <Skeleton animate height={"12%"} width={"90%"} mt={6} radius={"md"}></Skeleton>
            <Skeleton animate height={"20%"} width={"100%"} mt={6} radius={"md"}></Skeleton>
            <Skeleton animate height={"19%"} width={"90%"} mt={6} radius={"md"}></Skeleton>
            <Skeleton animate height={"12%"} width={"90%"} mt={6} radius={"md"}></Skeleton>
        </Box>
    );
}
