import styled from "@emotion/styled";
import { Stack } from "@mantine/core";
import React from "react";
import { useTranslation } from "react-i18next";

export default function CertificateNotFound() {
    const { t } = useTranslation();
    return (
        <React.Fragment>
            <Root align="center" justify="center">
                {t("certificate.history.not_found")}
            </Root>
        </React.Fragment>
    );
}

//Styling
const Root = styled(Stack)`
    height: 100vh;
    background: linear-gradient(92.08deg, #e5e6ff 1.32%, #ffffff 97.84%);
    font-weight: 500;
    font-size: 30px;
`;
