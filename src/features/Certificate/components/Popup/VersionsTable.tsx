import styled from "@emotion/styled";
import { Text } from "@mantine/core";
import Icon from "components/Icon";
import { useTranslation } from "react-i18next";
import { CertificateTypes, CertificateVersion } from "../../../../types/Certificate";

export type CeritificateVersionProps = {
    currentVersion: CertificateVersion;
    otherVersion: Draft[];
    certificate: CertificateTypes;
};

export default function VersionsTable({ certificate, currentVersion, otherVersion }: CeritificateVersionProps) {
    const { t } = useTranslation();

    return (
        <>
            <Table cellSpacing={0}>
                <thead>
                    <tr>
                        <th style={{ width: "50%" }}>{t("certificate.title")}</th>
                        <th style={{ width: "40%" }}>{t("certificate.history.version_date_title")}</th>
                        <th style={{ width: "10%" }}></th>
                    </tr>
                </thead>
                <TableBody>
                    <tr>
                        <td>
                            <TitleContainer>
                                <span>
                                    <Icon icon={"ic:baseline-lock"} color="#ffffff" hoverColor="#ffffff" />
                                </span>
                                {currentVersion.title}
                            </TitleContainer>
                        </td>
                        <td>v.{currentVersion.version.toString()}, locked</td>
                        <td>
                            <a href={certificate?.historyLink} target="_blank" rel="noreferrer">
                                <Underline>{t("certificate.history.check")}</Underline>
                            </a>
                        </td>
                    </tr>
                </TableBody>
            </Table>

            <Text transform={"uppercase"} sx={{ marginTop: "40px", fontSize: "24px", lineHeight: "30px" }}>
                {t("certificate.history.other_versions", { count: otherVersion.filter((item) => item.isMinted).length })}
            </Text>
            {!!otherVersion.length && (
                <Container>
                    <Table cellSpacing={0}>
                        <thead>
                            <tr>
                                <th style={{ width: "50%" }}></th>
                                <th style={{ width: "40%" }}></th>
                                <th style={{ width: "10%" }}></th>
                            </tr>
                        </thead>
                        <TableBody>
                            {otherVersion
                                .filter((item) => item.isMinted)
                                .map((elem, index) => (
                                    <tr key={index}>
                                        <td>
                                            <TitleContainer>
                                                <span>
                                                    <Icon icon={"ic:baseline-lock"} color="#ffffff" hoverColor="#ffffff" />
                                                </span>
                                                {elem.title}
                                            </TitleContainer>
                                        </td>
                                        <td>
                                            v.{elem.version}, {}
                                        </td>
                                        <td>
                                            <a href={elem?.mintedNetworks[0]?.blockchainTokenUrlLink} target="_blank" rel="noreferrer">
                                                <Underline>{t("certificate.history.check")}</Underline>
                                            </a>
                                        </td>
                                    </tr>
                                ))}
                        </TableBody>
                    </Table>
                </Container>
            )}
        </>
    );
}

const Table = styled.table`
    & th {
        font-weight: 400;
        text-align: start;
        padding-left: 5px;
    }

    & td {
        padding-top: 8px;
    }
    width: 100%;
    padding-right: 10px;
`;

const Container = styled.div`
    height: 400px;
    overflow-y: auto;
    &::-webkit-scrollbar {
        width: 4px;
        border-radius: 4px;
    }

    &::-webkit-scrollbar-thumb {
        background-color: #ffffff;
        border-radius: 4px;
        background-clip: padding-box;
        &:active {
            background-color: #938fff;
        }
    }
`;

const TableBody = styled.tbody`
    & tr {
        transition: 0.5s;
        &:hover {
            background-color: #454b7399;
        }
        & td {
            padding-left: 5px;
            padding-right: 40px;
            &:last-child {
                padding-right: unset;
            }
        }
    }
`;

const TitleContainer = styled.div`
    display: flex;
    & span {
        margin: 2px 5px 0 0;
    }
`;

const Underline = styled.span`
    text-decoration: underline;
    text-decoration-skip: ink;
    &:hover {
        color: #938fff;
    }
`;
