import styled from "@emotion/styled";
import { Text } from "@mantine/core";
import closeIcon from "assets/images/certificate/versions-popup-close.svg";
import { useTranslation } from "react-i18next";
import { CertificateTypes } from "types/Certificate";
import VersionsTable from "./VersionsTable";

type CertificateVersionsProps = {
    certificate: CertificateTypes;
    handleClose: any;
};

export default function CertificateVersions({ certificate, handleClose }: CertificateVersionsProps) {
    const { t } = useTranslation();
    if (!certificate) return null;

    return (
        <Root style={{ paddingBottom: certificate.versions.length < 2 ? "32px" : "0px" }}>
            <Header>
                {" "}
                <Text transform={"uppercase"} sx={{ fontSize: "24px", lineHeight: "30px" }}>
                    {t("certificate.history.current_version")}
                </Text>
                <img onClick={() => handleClose()} src={closeIcon} alt="" />
            </Header>
            {certificate.versions.length > 0 && (
                <VersionsTable certificate={certificate} currentVersion={certificate.currentVersion!} otherVersion={certificate.otherVersion!} />
            )}
        </Root>
    );
}

const Root = styled.div`
    padding: 32px 32px 0 32px;
    background: #0a001e;
    color: #ffffff;
    & p {
        margin-bottom: 16px;
        line-height: 20px;
        font-size: 18px;
        word-break: break-word;
    }
`;

const Header = styled.div`
    display: flex;
    justify-content: space-between;

    & img {
        cursor: pointer;
    }
`;
