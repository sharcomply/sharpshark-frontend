import { SharkContract } from "blockchain";
import { SharkKeeper } from "blockchain/contracts/SharkContract";
import { CertificateTypes } from "../../../types/Certificate";

export type CertificateData = {
    stateData: CertificateTypes;
    tokenInfo: SharkKeeper.TokenInfoStructOutput;
};
enum CertificateType {
    NEAR = 'near',
    NEAR_CACHE = 'near_cache',
    METAMASK = 'metamask',
    METAMASK_CACHE = 'metamask_cache'
}

let getMetadata = (metadataHash: string) => {
    if (!metadataHash) return;

    return fetch(`https://ipfs.io/ipfs/${metadataHash}`)
        .then((response) => response.json())
        .catch((err) => console.error(err));
};

function createStateDataMetamask(draft:DraftDetails, tokenInfo: any) {
    const stateData: CertificateTypes = {
        versions: [],
        title: draft.metadata.Title,
        author: draft.metadata.Authors,
        cachedSource: draft.cachedSource,
        usageRights: tokenInfo[7],
        historyQR: draft.mintedNetworks[0].blockchainTokenUrlQrCodeLink,
        historyLink: draft.mintedNetworks[0].blockchainTokenUrlLink,
        historyTransactionHash: draft.mintedNetworks[0].mintTransactionHash,
        ipfsQR: draft.linkUrlQrCodeLink,
        contentIpfsLink: draft.linkUrl,
        text: draft.text,
        type: draft.type,
        price: tokenInfo[0].toString(),
        isSell: tokenInfo[9],
        metadataHash: tokenInfo[5],
        contentHash: tokenInfo[6],
        licenseHash: tokenInfo[7],
        updatedAt: draft.updatedAt,
        createdAt: draft.createdAt,
        tokenId: draft.tokenId,
        currentVersion: {
            title: draft.metadata.Title,
            version: tokenInfo.version.toString(),
            tokenId: draft.tokenId,
        },
        otherVersion: Object.values(draft.versionsHistoryObjects as any).filter((item: any) => item?.tokenId !== draft.tokenId) as Draft[],
        minter: tokenInfo.minter
    };
    return stateData
}

const fillVersion = async(
        stateData: any,
        draft:DraftDetails,
        contracts: { contractMetamask: SharkContract, contractNear?:any },
        tokenInfo: any,
        typeCertificate: CertificateType,
        metadata?: any
    ) => {

    let haveParent = true;

    switch (typeCertificate) {
        case CertificateType.NEAR :
            while (haveParent) {
                stateData.versions.push({
                    title: metadata?.Title || draft.metadata.Title,
                    version: tokenInfo.version.toString(),
                    tokenId: tokenInfo?.parent_token_id?.toString() || "",
                });

                if (tokenInfo.parent_token_id) {
                    if (tokenInfo.parent_token_id.toString() === "0") {
                        haveParent = false;

                        break;
                    }
                }

                if (tokenInfo.parentTokenId) {
                    if (tokenInfo.parentTokenId.toString() === "0") {
                        haveParent = false;

                        break;
                    }
                }

                const newTokenInfo = await contracts.contractNear.get_token_info({ token_id: tokenInfo.parent_token_id });
                if (!newTokenInfo) {
                    console.log(tokenInfo)
                    if (tokenInfo.parent_token_id) {
                        tokenInfo = await contracts.contractMetamask.getTokenInfo(tokenInfo.parent_token_id)
                        metadata = await getMetadata(tokenInfo?.metadata_hash);
                    }
                    if (tokenInfo.parentTokenId) {
                        tokenInfo = await contracts.contractMetamask.getTokenInfo(tokenInfo.parentTokenId)
                        metadata = await getMetadata(tokenInfo?.metadataHash);
                    }
                } else {
                    tokenInfo = newTokenInfo
                    metadata = await getMetadata(tokenInfo?.metadata_hash);
                }

            }
            break;

        case CertificateType.NEAR_CACHE:
            Object.values(draft.versionsHistoryObjects as any).map((item: any)=>{
                stateData.versions.push({
                    title: item.title,
                    version: item.version.toString(),
                    tokenId: item.parentTokenId ? item.parentTokenId : '0'
                })
            })
            break;

        case CertificateType.METAMASK_CACHE:
            Object.values(draft.versionsHistoryObjects as any).map((item: any)=>{
                stateData.versions.push({
                    title: item.title,
                    version: item.version.toString(),
                    tokenId: item.parentTokenId ? item.parentTokenId : '0'
                })
            })
            break;
        default:
            let currentTokenInfo = tokenInfo;
            let currentMetadata = metadata;
            while (haveParent) {
                stateData.versions.push({
                    title: currentMetadata?.Title || draft.metadata.Title,
                    version: currentTokenInfo.version.toString(),
                    tokenId: currentTokenInfo.parentTokenId.toString(),
                });

                if (currentTokenInfo.parentTokenId.toString() === "0") {
                    haveParent = false;
                    break;
                }
                currentTokenInfo = await contracts.contractMetamask.getTokenInfo(tokenInfo.parentTokenId);
                currentMetadata = await getMetadata(tokenInfo.metadataHash);
            }
    }
}

export default async function getCertificateData(draft: DraftDetails, contract: SharkContract, fromCache: boolean): Promise<CertificateData> {
    let tokenInfo = await contract.getTokenInfo(draft.tokenId);
    const stateData = createStateDataMetamask(draft, tokenInfo);

    if (!fromCache) {
        let metadata = await getMetadata(tokenInfo.metadataHash);
        stateData.currentVersion = {
            title: metadata?.Title || draft.metadata.Title,
            version: tokenInfo.version.toString(),
            tokenId: draft.tokenId,
        }
        fillVersion(stateData, draft, {contractMetamask: contract}, tokenInfo, CertificateType.METAMASK, metadata);
    } else {
        fillVersion(stateData, draft, {contractMetamask: contract}, tokenInfo, CertificateType.METAMASK_CACHE);
    }

    return {
        stateData,
        tokenInfo: tokenInfo,
    };
}

function createStateDataNear(draft:DraftDetails, tokenInfo: any) {
    const stateData: CertificateTypes = {
        versions: [],
        title: draft.metadata.Title,
        author: draft.metadata.Authors,
        cachedSource: draft.cachedSource,
        usageRights: tokenInfo.license_hash,
        historyQR: draft.mintedNetworks[0].blockchainTokenUrlQrCodeLink,
        historyLink: draft.mintedNetworks[0].blockchainTokenUrlLink,
        historyTransactionHash: draft.mintedNetworks[0].mintTransactionHash,
        ipfsQR: draft.linkUrlQrCodeLink,
        contentIpfsLink: draft.linkUrl,
        text: draft.text,
        type: draft.type,
        price: tokenInfo.sale_amount,
        isSell: tokenInfo.sale_is_enabled,
        metadataHash: tokenInfo.metadata_hash,
        contentHash: tokenInfo.content_hash,
        licenseHash: tokenInfo.license_hash,
        updatedAt: draft.updatedAt,
        createdAt: draft.createdAt,
        tokenId: draft.tokenId,
        currentVersion: {
            title: draft.metadata.Title,
            version: tokenInfo?.version,
            tokenId: draft.tokenId,
        },
        otherVersion: Object.values(draft.versionsHistoryObjects as any).filter((item: any) => item?.tokenId !== draft.tokenId) as Draft[],
        minter: tokenInfo.minter
    };
    return stateData
}

export async function getCertificateDataNear(draft: DraftDetails, contract: any, contractMetamask: SharkContract, fromCache: boolean): Promise<CertificateData> {
    let tokenInfo = await contract.get_token_info({ token_id: draft.tokenId });
    const stateData = createStateDataNear(draft, tokenInfo)

    if (!fromCache){
        let metadata = await getMetadata(tokenInfo.metadata_hash);
        fillVersion(stateData, draft, {contractMetamask: contractMetamask, contractNear: contract}, tokenInfo, CertificateType.NEAR, metadata)
    } else {
        fillVersion(stateData, draft, {contractMetamask: contractMetamask, contractNear: contract}, tokenInfo, CertificateType.NEAR_CACHE)
    }

    return {
        stateData,
        tokenInfo: tokenInfo,
    };
}