import styled from "@emotion/styled";
import { Divider } from "@mantine/core";
import { useTranslation } from "react-i18next";

export default function CertificateLocked() {
    const {t} = useTranslation()

    return (
        <Root>
            <Title>{t("certificate.locked_content.title")}</Title>
            <p dangerouslySetInnerHTML={{__html: t("certificate.locked_content.content")}}/>
            <Divider size={2} color={"white"} />
        </Root>
    );
}

const Root = styled.div`
    padding: 48px 96px 42px 96px;
    background: #0a001e;
    color: #ffffff;
    & p {
        margin-bottom: 16px;
        line-height: 20px;
        font-size: 18px;
        word-break: break-word;
    }
`;

const Title = styled.div`
    text-transform: uppercase;
    font-size: 24px;
    line-height: 30px;
    margin-bottom: 16px;
`;

// const Underline = styled.span`
//     text-decoration: underline;
//     text-decoration-skip: ink;
// `;
