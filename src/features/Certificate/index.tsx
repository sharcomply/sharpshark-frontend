import React, {useEffect, useState} from "react";
import useCertificate from "./hooks/useCertificate";
import CertificateError from "./components/CertificateError";
import CertificateLoaded from "./components/CertificateLoaded";
import Loader from "./components/Loader";
import { Modal } from "@mantine/core";
import CertificateViewModal from "../Modals/components/CertificateViewModal";

type CeritificateProps = {
    draft: DraftDetails;
};

export default function Certificate({ draft }: CeritificateProps) {
    const [isCertificateFromCache, setIsCertificateFromCache] = useState(false);

    const { certificate, isLoading, isError, isLoadingError, isFetching, error} = useCertificate(draft, isCertificateFromCache);
    const [openModal, setOpenModal] = useState(false);
    const linkUrl = draft?.linkUrl;

    const handleCertificateFromCache = () => {
        setIsCertificateFromCache(!isCertificateFromCache)
        setOpenModal(!openModal)
    }

    useEffect(() => {
        if(!isFetching && error?.code === 'CALL_EXCEPTION'){
            setOpenModal(true)
        } else {
            setOpenModal(false)
        }
    }, [isError, isFetching])

    return (
        <React.Fragment>
            {(isLoading || isFetching) && <Loader />}
            {(!isLoading && !isFetching) && (isError || isLoadingError || !certificate) && <CertificateError isMinted={draft?.isMinted} />}
            {certificate && <CertificateLoaded certificate={certificate.stateData} linkUrl={linkUrl} fromCache={isCertificateFromCache}/>}
            <Modal
                opened={openModal}
                onClose={()=>setOpenModal(false)}
                withCloseButton={false}
                overlayOpacity={0.5}
                transition="slide-down"
                transitionDuration={400}
                closeOnClickOutside={false}
                size={"450px"}
                padding={0}
                radius={12}
                styles={{ root: { overflow: "hidden" }, body: { position: "relative" }, modal: { margin: "0 auto !important" } }}
            >
                <CertificateViewModal handleClose = {() => setOpenModal(false)} handleCertificateFromCache={handleCertificateFromCache} />
            </Modal>
        </React.Fragment>
    );
}
