import { Stack, Text } from "@mantine/core";
import styled from "@emotion/styled";
import { TableHead } from "../../components/DataTable/TableHead";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";
import { useDisputesGroupingList } from "./hooks/useDisputesGroupingList";
import { DisputeGroupingItem } from "./components/DisputeGroupingItem";

interface Props {
    status: string;
    keyStatus: string;
}

// Component
export default function DisputesTable({ status, keyStatus }: Props) {
    const { disputes, refetch } = useDisputesGroupingList(keyStatus, status);
    const { t } = useTranslation();

    return (
        <Root>
            <Table>
                {keyStatus.includes("DRAFT") && (
                    <TableHead
                        items={[
                            { title: t("table.titles.your_title"), width: "45%" },
                            { title: t("table.titles.appeared_on"), width: "35%" },
                            { title: t("table.titles.discovered"), width: "10%" },
                            { title: "", width: "10%" },
                            { title: "", width: "5%" },
                        ]}
                    />
                )}
                {keyStatus.includes("IN_PROGRESS") && (
                    <TableHead
                        items={[
                            { title: t("table.titles.your_title"), width: "33%" },
                            { title: t("table.titles.appeared_on"), width: "24%" },
                            { title: t("table.titles.discovered"), width: "10%" },
                            { title: "", width: "8%" },
                            { title: "", width: "10%" },
                            { title: "", width: "10%" },
                            { title: "", width: "5%" },
                        ]}
                    />
                )}

                {keyStatus.includes("CLAIM_SENT") && (
                    <TableHead
                        items={[
                            { title: t("table.titles.your_title"), width: "39%" },
                            { title: t("table.titles.appeared_on"), width: "18%" },
                            { title: t("table.titles.discovered"), width: "11%" },
                            { title: "", width: "11%" },
                            { title: "", width: "8%" },
                            { title: "", width: "8%" },
                            { title: "", width: "5%" },
                        ]}
                    />
                )}

                {keyStatus === "ENDED" && (
                    <TableHead
                        items={[
                            { title: t("table.titles.your_title"), width: "48%" },
                            { title: t("table.titles.appeared_on"), width: "14%" },
                            { title: t("table.titles.discovered"), width: "11%" },
                            { title: t("ENDED"), width: "11%" },
                            { title: "", width: "11%" },
                            { title: "", width: "5%" },
                        ]}
                    />
                )}
                {keyStatus === "NOTRELEVANT" && (
                    <TableHead
                        items={[
                            { title: t("table.titles.your_title"), width: "55%" },
                            { title: t("table.titles.appeared_on"), width: "20%" },
                            { title: t("table.titles.discovered"), width: "10%" },
                            { title: "", width: "10%" },
                            { title: "", width: "5%" },
                        ]}
                    />
                )}
                <tbody>
                    {!!disputes && !!disputes?.items.length && (
                        <>
                            <tr style={{ backgroundColor: "#000" }}>
                                <td colSpan={7} align="center" width={"100%"} style={{ padding: "5px", fontSize: "12px", fontWeight: 500, color: "#fff" }}>
                                    Are these cases violations? If yes, we can call for the website owner to remove/pay for it
                                </td>
                            </tr>
                            <Fragment>
                                {disputes.items.map((item) => {
                                    return (
                                        <DisputeGroupingItem key={keyStatus + `/${item.documentId._id}`} item={item} keyStatus={keyStatus} refetch={refetch} />
                                    );
                                })}
                            </Fragment>
                        </>
                    )}

                    {(!disputes || !disputes?.items.length) && (
                        <tr style={{ borderTop: "1px solid #DDDEEE" }}>
                            <td style={{ padding: 16 }}>
                                <Text size={"sm"} color={"#666892"} weight={"400"}>
                                    {t("table.empty-table")}
                                </Text>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    )}
                </tbody>
            </Table>
        </Root>
    );
}

// Styling
const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
`;

export const Table = styled.table`
    width: 100%;
    table-layout: fixed;
    border-collapse: collapse;
    overflow-wrap: anywhere;
`;

