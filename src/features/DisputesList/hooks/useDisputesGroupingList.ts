import { useQuery } from "react-query";
import { backend } from "api";

export const useDisputesGroupingList = (key: string, status: string) => {
    const {
        data: disputes,
        refetch,
        ...options
    } = useQuery([`DISPUTES_GROUPING_LIST_${key}`], () => backend.disputes.getDisputesGroupingList(status), {
        retry: 0,
    });
    const count = disputes?.count;

    return {
        disputes,
        count,
        refetch,
        ...options,
    };
};

export const useDisputesGroupingId = (key: string, status: string, id: string = "") => {
    const { data, ...rest } = useQuery([`DISPUTE_LIST_${key}`, id], () => backend.disputes.getDisputeIdGroupingList(status, id), {
        retry: 0,
        keepPreviousData: true,
    });

    return { disputes: data, ...rest };
};

