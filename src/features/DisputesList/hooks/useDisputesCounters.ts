import { backend } from "api";
import { useQuery } from "react-query";

export const useDisputesCounter = () => {
    const options = useQuery(["DISPUTES_COUNTERS"], () => backend.disputes.getDisputesCounters(), {
        retry: 0,
        keepPreviousData: true,
    });

    return options;
};