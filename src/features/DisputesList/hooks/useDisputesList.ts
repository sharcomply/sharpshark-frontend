import { useQuery } from "react-query";
import { backend } from "api";

export default function useDisputesList(key: string, status: string) {
    const {
        data: disputes,
        refetch,
        ...options
    } = useQuery([`DISPUTES_LIST_${key}`], () => backend.disputes.getDisputesList(status), {
        retry: 0,
        keepPreviousData: true,
    });
    const count = disputes?.count

    return {
        disputes,
        count,
        refetch,
        ...options,
    };
}

export function useDisputesListId(key: string, status: string, id: string) {
    const {
        data: disputes,
        refetch,
        ...options
    } = useQuery([`DISPUTES_LIST_${key}`, id], () => backend.disputes.getDisputesListId(status, id), {
        retry: 0,
        keepPreviousData: true,
    });

    return {
        disputes,
        refetch,
        ...options,
    };
}

export function useDisputDetail(id?: string) {
    const { data: disput, refetch, ...options } = useQuery(["DISPUT-ITEM", id], () => backend.disputes.getDisput(id), {
        enabled: !!id,
        retry: false,
        refetchOnWindowFocus: false,
        keepPreviousData: true,
    });

    return {
        disput,
        refetch,
        ...options,
    };
}
