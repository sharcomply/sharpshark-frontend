import { backend } from "api";
import { useCallback } from "react";
import { useMutation, useQueryClient } from "react-query";

export const useDisputeDelete = (keyStatus: string) => {
    const queryClient = useQueryClient();
    const { mutateAsync, ...rest } = useMutation({
        mutationFn: (id: string) => backend.disputes.deleteDisput(id),
    });

    const handleDelete = useCallback(
        async (id: string) => {
            console.debug(id, 'click')
            if (!id) return;
            try {
                await mutateAsync(id);
                await queryClient.invalidateQueries([`DISPUTES_GROUPING_LIST_${keyStatus}`]);
            } catch (error) {}
        },
        [mutateAsync, queryClient, keyStatus]
    );

    return { handleDelete, ...rest };
};
