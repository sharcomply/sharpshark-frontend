// Props
import { Group, Popover, Stack, Text } from "@mantine/core";
import Icon from "../../../components/Icon";
import { useState } from "react";
import styled from "@emotion/styled";
import { useTranslation } from "react-i18next";

// Variables
const rootStyles = {
    background: "#FFFFFF",
    border: "1px solid #DDDEEE",
    boxShadow: " 0px 2px 12px rgba(0, 0, 0, 0.1)",
    radius: 4,
    overflow: "hidden",
};

type ComponentProps = {
    protectId: string;
    onMoveToNotRelevant?: () => void;
    onMoveToRelevant?: () => void;
    handleDelete?: () => void;
};

// Component
export function ActionMenuDisput({ onMoveToNotRelevant, onMoveToRelevant, handleDelete }: ComponentProps) {
    const [opened, setOpened] = useState(false);
    const { t } = useTranslation();
    const control = (
        <Group align={"center"} position={"center"} px={4} onClick={() => setOpened((value) => !value)} sx={{ cursor: "pointer", pointerEvents: "all" }}>
            <Icon icon={"ic:sharp-more-vert"} size={24} color={"#b1b2c9"} hoverColor={"#b1b2c9"} />
        </Group>
    );
    const disabled = !onMoveToNotRelevant && !onMoveToRelevant && !handleDelete;

    return (
        <Popover
            opened={opened}
            target={control}
            onClose={() => setOpened(false)}
            width={160}
            radius={4}
            position="bottom"
            placement="end"
            disabled={disabled}
            transition={"pop-top-right"}
            styles={{ body: rootStyles, inner: { padding: 0 } }}
        >
            {/* View And Edit */}
            <Stack spacing={0}>
                {onMoveToNotRelevant && (
                    <MenuItem
                        px={16}
                        py={12}
                        onClick={() => {
                            onMoveToNotRelevant();
                            setOpened(false);
                        }}
                    >
                        <Text size={"sm"} weight={"500"}>
                            {t("table.row-options.move_not_relevant")}
                        </Text>
                    </MenuItem>
                )}
                {onMoveToRelevant && (
                    <MenuItem
                        px={16}
                        py={12}
                        onClick={() => {
                            onMoveToRelevant();
                            setOpened(false);
                        }}
                    >
                        <Text size={"sm"} weight={"500"}>
                            {t("table.row-options.move_to_relevant")}
                        </Text>
                    </MenuItem>
                )}
                {handleDelete && (
                    <DeleteItem
                        px={16}
                        py={12}
                        onClick={() => {
                            handleDelete();
                            setOpened(false);
                        }}
                    >
                        <Text size={"sm"} weight={"500"}>
                            × {t("table.row-options.delete")}
                        </Text>
                    </DeleteItem>
                )}
            </Stack>
            {/* Protect */}
            {/* <MenuItem px={16} py={12} className="disabled">
                <Text size={'sm'} weight={'500'}>
                    Add a new version
                    <br/>
                    (Coming Soon)
                </Text>
            </MenuItem> */}

            {/* Protect */}
            {/* <MenuItem px={16} py={12} className="disabled">
                <Text size={'sm'} weight={'500'}>
                    View certificate
                    <br/>
                    (Coming Soon)
                </Text>
            </MenuItem> */}

            {/* Delete */}
            {/* <MenuItem px={16} py={12} className="disabled">
                <Text size={'sm'} weight={'500'}>
                    × Stop monitoring
                    <br/>
                    (Coming Soon)
                </Text>
            </MenuItem> */}
        </Popover>
    );
}

const MenuItem = styled(Group)`
    cursor: pointer;
    color: #666892;

    &:hover {
        background-color: #4700e5;
        color: #ffffff;
    }

    &.disabled {
        color: #bbbed8;
    }

    &.disabled:hover {
        background-color: #f7f7fa;
        color: #8b8fad;
    }
`;

const DeleteItem = styled(Group)`
    cursor: pointer;
    color: #e50c54;

    &:hover {
        background-color: #e50c54;
        color: #ffffff;
    }
`;

