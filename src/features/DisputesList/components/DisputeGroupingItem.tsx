import { Group, Text } from "@mantine/core";
import styled from "@emotion/styled";
import { useState } from "react";
import { Icon } from "@iconify/react";
import ViewedStatus from "components/ViewedStatus";
import { AppearedOnDisputeItem } from "./AppearedOnDisputeItem";

// Props
type ComponentProps = {
    item: DisputesGroupingResponse;
    keyStatus: string;
    refetch: () => void;
};

// Component
export const DisputeGroupingItem = ({ item, keyStatus, refetch }: ComponentProps) => {
    const [opened, setOpen] = useState(false);
    const disputesCount = item?.appearedOns?.reduce((total, current) => total + current.disputes.length, 0) ?? 0;

    return (
        <>
            <Root>
                <td colSpan={7} style={{ overflow: "hidden" }}>
                    <Group onClick={() => setOpen(!opened)} spacing={0} style={{ cursor: "pointer", flexWrap: "nowrap" }}>
                        <Group>
                            <Group px={16} align="center" sx={{ height: 54 }}>
                                {opened && <Icon icon={`ic:round-keyboard-arrow-down`} rotate={"180deg"} color="#4b506d" />}
                                {!opened && <Icon icon={`ic:round-keyboard-arrow-down`} color="#4b506d" />}
                            </Group>
                        </Group>
                        <Group>
                            <ViewedStatus isRead={true} length={disputesCount} />
                        </Group>
                        <Group style={{ overflow: "hidden" }}>
                            <Group px={16} align="center" direction="row" sx={{ height: 54 }} style={{ overflow: "hidden" }}>
                                <Text
                                    size={"sm"}
                                    color={"#0A001E"}
                                    weight={"500"}
                                    style={{
                                        overflow: "hidden",
                                        textOverflow: "ellipsis",
                                        whiteSpace: "nowrap",
                                        // width: "99%",
                                    }}
                                >
                                    {item.documentId.title}
                                </Text>
                            </Group>
                        </Group>
                        <Group>
                            <Group px={16} align="center" sx={{ height: 54 }}>
                                <Text
                                    size={"sm"}
                                    color={"#666892"}
                                    weight={"500"}
                                    style={{
                                        overflow: "hidden",
                                        textOverflow: "ellipsis",
                                        whiteSpace: "nowrap",
                                        width: "90%",
                                    }}
                                >
                                    {/* {firstItemAlert.introduction} */}
                                </Text>
                            </Group>
                        </Group>
                    </Group>
                </td>
                <td></td>
            </Root>

            {opened &&
                item.appearedOns?.map((appearedOn, index) => (
                    <AppearedOnDisputeItem appearedOn={appearedOn} keyStatus={keyStatus} key={index} refetch={refetch} />
                ))}
        </>
    );
};

// Styling
const Root = styled.tr`
    transition: background-color 200ms linear;
    border-top: 1px solid #dddeee;

    &:hover {
        background-color: #edeeff;
    }
`;

