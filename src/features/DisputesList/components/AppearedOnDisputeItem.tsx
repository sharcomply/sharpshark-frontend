import styled from "@emotion/styled";
import { FC, useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDisputeDelete } from "../hooks/useDisputeDelete";
import { Group, Text } from "@mantine/core";
import { Icon } from "@iconify/react";
import ViewedStatus from "components/ViewedStatus";
import { humanizeDate } from "helpers/humanizeDate";
import { SharkButton } from "components/SharkButton";
import { Link, useNavigate } from "react-router-dom";
import { ActionMenuDisput } from "./ActionMenuDisput";
import { useAlertListStore } from "features/AlertsList/hooks/useAlertListStore";
import { useModal } from "features/Modals/hooks/useModal";
import useDisputesStore from "features/DisputesDetail/hooks/useDisputesStore";
import { useDisputeCreate } from "features/DisputesCollectingEvidence/hooks/useDisputeCreate";
import { editDisput } from "api/requests/disputes";
import { useQueryClient } from "react-query";
import { ModalContent, ModalType } from "features/Modals/interfaces";

type Props = {
    appearedOn: DisputeAppearedOns;
    keyStatus: string;
    refetch: () => void;
};

export const AppearedOnDisputeItem: FC<Props> = ({ appearedOn, keyStatus, refetch }) => {
    const [disputesOpened, setDisputesOpened] = useState(false);
    const { handleDelete } = useDisputeDelete(keyStatus);
    const canMoveToNotRelevant = keyStatus !== "NOTRELEVANT";
    const canMoveToRelevant = keyStatus === "NOTRELEVANT";
    const { t } = useTranslation();
    const { handleUpdate } = useDisputeCreate();
    const {
        actions: { setDisputPayload },
    } = useAlertListStore();
    const { handleOpen } = useModal();
    const [loadingId, setloadingId] = useState("");
    const [id, setId] = useState("");
    const { setDisputModalResponse, disputModalResponse } = useDisputesStore();

    const queryClient = useQueryClient();

    const onClosed = useCallback(
        async (id: string) => {
            try {
                await editDisput({ _id: id, status: "Closed" });
                await refetch();
                await queryClient.invalidateQueries([`DISPUTES_COUNTERS`]);
            } catch (error) {}
        },
        [refetch]
    );

    const handleNextStatusClick = async (alert: any) => {
        switch (alert.status) {
            case "In Progress: Website Owner":
                try {
                    setloadingId(alert._id);
                    await editDisput({ _id: alert._id, status: "Draft: Provider" });
                    await refetch();
                    await queryClient.invalidateQueries([`DISPUTES_COUNTERS`]);
                    setloadingId("");
                } catch (error) {}
                break;

            case "In Progress: Provider":
                try {
                    setloadingId(alert._id);
                    await editDisput({ _id: alert._id, status: "Draft: Google" });
                    await refetch();
                    await queryClient.invalidateQueries([`DISPUTES_COUNTERS`]);
                    setloadingId("");
                } catch (error) {}
                break;

            case "In Progress: Google":
                await onClosed(alert._id);
                await queryClient.invalidateQueries([`DISPUTES_COUNTERS`]);
                break;
        }
    };

    const checkKeyStatus = () => {
        switch (keyStatus) {
            case "DRAFT_WEBSITE_OWNER":
            case "DRAFT_PROVIDER":
            case "DRAFT_GOOGLE":
                return "DRAFT";
            case "IN_PROGRESS_WEBSITE_OWNER":
            case "IN_PROGRESS_PROVIDER":
            case "IN_PROGRESS_GOOGLE":
                return "IN_PROGRESS";
            case "CLAIM_SENT_WEBSITE_OWNER":
            case "CLAIM_SENT_PROVIDER":
            case "CLAIM_SENT_GOOGLE":
            case "LETTER_SENT_WEBSITE_OWNER":
            case "LETTER_SENT_PROVIDER":
            case "LETTER_SENT_GOOGLE":
                return "CLAIM_SENT";
            default:
                return keyStatus;
        }
    };
    useEffect(() => {
        checkKeyStatus();
    }, [keyStatus]);

    const checkStatus = useCallback((status: any) => {
        switch (status) {
            case undefined:
                return "Draft: Website Owner";

            case "Draft: Website Owner":
                return "Draft: Website Owner";

            case "In Progress: Website Owner":
                return "In Progress: Website Owner";

            case "Claim Sent: Website Owner":
                return "Draft: Provider";

            case "Draft: Provider":
                return "Draft: Provider";

            case "In Progress: Provider":
                return "In Progress: Provider";

            case "Claim Sent: Provider":
                return "Draft: Google";

            case "Draft: Google":
                return "Draft: Google";

            default:
                return status;
        }
    }, []);

    const navigate = useNavigate();

    const onHandleReact = async (values: DisputesListItem) => {
        setDisputPayload({ appearedOn: values.appearedOn, alertId: values.alertId._id });
        await handleUpdate({ ...values, status: checkStatus(values.status), alertId: values.alertId._id });
        navigate(`/disputes/${values._id}/collecting-evidence`, { replace: true });
    };

    const onMoveToNotRelevant = useCallback(
        async (disput: DisputesListItem) => {
            await handleUpdate({ ...disput, isNotRelevant: true, alertId: disput.alertId._id });
            await queryClient.invalidateQueries([`DISPUTES_GROUPING_LIST_${keyStatus}`]);
            await queryClient.invalidateQueries([`DISPUTES_LIST_NOTRELEVANT`]);
            await queryClient.invalidateQueries([`DISPUTES_COUNTERS`]);
            refetch();
        },
        [handleUpdate, queryClient, keyStatus, refetch]
    );

    const onMoveToRelevant = useCallback(
        async (disput: DisputesListItem) => {
            await handleUpdate({ ...disput, isNotRelevant: false, alertId: disput.alertId._id });
            console.debug(keyStatus)
            await queryClient.invalidateQueries([`DISPUTES_GROUPING_LIST_${keyStatus}`]);
            await queryClient.invalidateQueries([`DISPUTES_LIST_NOTRELEVANT`]);
            await queryClient.invalidateQueries([`DISPUTES_COUNTERS`]);
            refetch();
        },
        [handleUpdate, queryClient, keyStatus]
    );

    const onSaveAlertParam = useCallback(
        (appearedOn: string, alertId: string) => {
            setDisputPayload({ appearedOn, alertId });
        },
        [setDisputPayload]
    );

    const onClickUnderCourt = (id: string) => {
        setId(id);
        handleOpen({ type: ModalType.DISPUTE, content: ModalContent.TakeItToCourt });
    };

    const onUnderCourt = useCallback(
        async (id: string) => {
            try {
                await editDisput({ _id: id, status: "Under Court" });
                await refetch();
                await queryClient.invalidateQueries([`DISPUTES_COUNTERS`]);
            } catch (error) {}
        },
        [refetch]
    );
    useEffect(() => {
        const onUnderCourt = async (id: string) => {
            try {
                await editDisput({ _id: id, status: "Under Court" });
                await refetch();
                await queryClient.invalidateQueries([`DISPUTES_COUNTERS`]);
                setDisputModalResponse(undefined);
                setId("");
            } catch (error) {
                setDisputModalResponse(undefined);
                setId("");
            }
        };

        if (disputModalResponse === ModalContent.TakeItToCourt && id) {
            onUnderCourt(id);
        }
        return setDisputModalResponse(undefined);
    }, [disputModalResponse, id, onUnderCourt, refetch, setDisputModalResponse]);

    return (
        <>
            <Root>
                <td colSpan={7} style={{ overflow: "hidden" }}>
                    <Group ml={"5%"} onClick={() => setDisputesOpened(!disputesOpened)} spacing={0} style={{ cursor: "pointer", flexWrap: "nowrap" }}>
                        <Group>
                            <Group px={16} align="center" sx={{ height: 54 }}>
                                {disputesOpened && <Icon icon={`ic:round-keyboard-arrow-down`} rotate={"180deg"} color="#4b506d" />}
                                {!disputesOpened && <Icon icon={`ic:round-keyboard-arrow-down`} color="#4b506d" />}
                            </Group>
                        </Group>
                        <Group>
                            <ViewedStatus isRead={true} length={appearedOn.disputes.length} />
                        </Group>
                        <Group px={16} align="center" sx={{ height: 54 }} style={{ overflow: "hidden" }}>
                            <Text
                                size={"sm"}
                                color={"#666892"}
                                weight={"500"}
                                style={{
                                    overflow: "hidden",
                                    textOverflow: "ellipsis",
                                    whiteSpace: "nowrap",
                                }}
                            >
                                {appearedOn.appearedOn}
                            </Text>
                        </Group>
                        <Group>
                            <Group px={16} align="center" sx={{ height: 54 }}>
                                <Text
                                    size={"sm"}
                                    color={"#666892"}
                                    weight={"500"}
                                    style={{
                                        overflow: "hidden",
                                        textOverflow: "ellipsis",
                                        whiteSpace: "nowrap",
                                        width: "90%",
                                    }}
                                >
                                    {/* {firstItemAlert.introduction} */}
                                </Text>
                            </Group>
                        </Group>
                    </Group>
                </td>
            </Root>
            {disputesOpened &&
                appearedOn?.disputes?.map((dispute, index) => (
                    <Root key={index}>
                        <td></td>
                        <td>
                            {/* <a href={firstItemAlert.isSimilarContent ? firstItemAlert.url : "#"}> */}
                            <Group px={16} align="center" sx={{ height: 54 }} style={{ overflow: "hidden" }}>
                                <Text
                                    size={"sm"}
                                    color={"#666892"}
                                    weight={"500"}
                                    style={{
                                        overflow: "hidden",
                                        textOverflow: "ellipsis",
                                        whiteSpace: "nowrap",
                                    }}
                                >
                                    [{dispute.alertId.scanId.aggregatedScore}%] – {dispute.appearedOn}
                                </Text>
                            </Group>
                            {/* </a> */}
                        </td>

                        <td>
                            <Group px={16} align="center" sx={{ height: 54 }} style={{ overflow: "hidden" }}>
                                <Text size={"sm"} color={"#666892"} weight={"500"}>
                                    {humanizeDate(dispute.createdAt).shortDate}
                                </Text>
                            </Group>
                        </td>
                        {
                            {
                                DRAFT: (
                                    <td>
                                        <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                            <SharkButton
                                                as={Link}
                                                to={`/disputes/create/${dispute.alertId._id}/collecting-evidence`}
                                                variant={"filled"}
                                                buttonType="primary"
                                                onClick={() => onSaveAlertParam(dispute.appearedOn, dispute._id)}
                                                width={"100%"}
                                                style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                            >
                                                {t("dispute.dispute_item.edit")}
                                            </SharkButton>
                                        </Group>
                                    </td>
                                ),
                                IN_PROGRESS: (
                                    <>
                                        <td>
                                            <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                <SharkButton
                                                    as={Link}
                                                    to={`/disputes/create/${dispute.alertId._id}/collecting-evidence`}
                                                    variant={"filled"}
                                                    buttonType="primary"
                                                    onClick={() => onSaveAlertParam(dispute.appearedOn, dispute.alertId._id)}
                                                    width={"100%"}
                                                    style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                                >
                                                    {t("dispute.dispute_item.react")}
                                                </SharkButton>
                                            </Group>
                                        </td>
                                        <td>
                                            {dispute.status !== "Under Court" && (
                                                <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                    <SharkButton
                                                        loading={dispute._id === loadingId}
                                                        disabled={dispute._id === loadingId}
                                                        variant={"filled"}
                                                        buttonType="primary"
                                                        onClick={() => handleNextStatusClick(alert)}
                                                        width={"100%"}
                                                        style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                                    >
                                                        {t("dispute.dispute_item.next_status")}
                                                    </SharkButton>
                                                </Group>
                                            )}
                                        </td>
                                        <td>
                                            <Group px={16} position="center" align="center" sx={{ height: 54 }} onClick={() => onClosed(dispute._id)}>
                                                <Text size={"sm"} color={"#4700E5"} weight={"500"} style={{ cursor: "pointer" }}>
                                                    {t("dispute.dispute_item.closed")}
                                                </Text>
                                            </Group>
                                        </td>{" "}
                                    </>
                                ),
                                CLAIM_SENT: (
                                    <>
                                        <td>
                                            <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                <SharkButton
                                                    variant={"filled"}
                                                    buttonType="primary"
                                                    onClick={() => onHandleReact(dispute)}
                                                    width={"100%"}
                                                    style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                                >
                                                    {t("dispute.dispute_item.react")}
                                                </SharkButton>
                                            </Group>
                                        </td>
                                        <td>
                                            <Group px={16} position="center" align="center" sx={{ height: 54 }} onClick={() => onClickUnderCourt(dispute._id)}>
                                                <Text size={"sm"} color={"#4700E5"} weight={"500"} style={{ cursor: "pointer" }}>
                                                    Under <br /> Court
                                                </Text>
                                            </Group>
                                        </td>
                                        <td>
                                            <Group px={16} position="center" align="center" sx={{ height: 54 }} onClick={() => onClosed(dispute._id)}>
                                                <Text size={"sm"} color={"#4700E5"} weight={"500"} style={{ cursor: "pointer" }}>
                                                    {t("dispute.dispute_item.closed")}
                                                </Text>
                                            </Group>
                                        </td>{" "}
                                    </>
                                ),
                                ENDED: (
                                    <>
                                        <td>
                                            <Group px={16} align="center" sx={{ height: 54 }} style={{ overflow: "hidden" }}>
                                                <Text size={"sm"} color={"#666892"} weight={"500"}>
                                                    {humanizeDate(dispute.createdAt).shortDate}
                                                </Text>
                                            </Group>
                                        </td>
                                        <td>
                                            <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                <SharkButton
                                                    as={Link}
                                                    to={`/disputes/create/${dispute.alertId._id}/collecting-evidence`}
                                                    variant={"filled"}
                                                    buttonType="primary"
                                                    onClick={() => onSaveAlertParam(dispute.appearedOn, dispute.alertId._id)}
                                                    width={"100%"}
                                                    style={{
                                                        padding: "3px",
                                                        paddingLeft: "10px",
                                                        paddingRight: "10px",
                                                        cursor: "pointer",
                                                        overflow: "hidden",
                                                        textOverflow: "ellipsis",
                                                    }}
                                                >
                                                    {t("dispute.dispute_item.view_dispute")}
                                                </SharkButton>
                                            </Group>
                                        </td>
                                    </>
                                ),
                                NOTRELEVANT: <td></td>,
                            }[checkKeyStatus()]
                        }
                        <td>
                            <Group px={16} position="right" align="center" sx={{ height: 54, pointerEvents: "none" }}>
                                <ActionMenuDisput
                                    protectId={dispute._id}
                                    onMoveToNotRelevant={canMoveToNotRelevant ? () => onMoveToNotRelevant(dispute) : undefined}
                                    onMoveToRelevant={canMoveToRelevant ? () => onMoveToRelevant(dispute) : undefined}
                                    handleDelete={() => handleDelete(dispute.alertId._id)}
                                />
                            </Group>
                        </td>
                    </Root>
                ))}
        </>
    );
};

const Root = styled.tr`
    transition: background-color 200ms linear;
    border-top: 1px solid #dddeee;

    &:hover {
        background-color: #edeeff;
    }
`;

