import { Group, Text } from "@mantine/core";
import { humanizeDate } from "helpers/humanizeDate";
import styled from "@emotion/styled";
import { SharkButton } from "components/SharkButton";
import { useCallback, useEffect, useState } from "react";
import { Icon } from "@iconify/react";
import ViewedStatus from "components/ViewedStatus";
import { Link, useNavigate } from "react-router-dom";
import { useAlertListStore } from "features/AlertsList/hooks/useAlertListStore";
// import { ActionMenu } from "features/AlertsList/components/ActionMenu";
import { editDisput } from "api/requests/disputes";
import { ActionMenuDisput } from "./ActionMenuDisput";
import { useModal } from "../../Modals/hooks/useModal";
import useDisputesStore from "../../DisputesDetail/hooks/useDisputesStore";
import { ModalContent, ModalType } from "../../Modals/interfaces";
import { useDisputeCreate } from "../../DisputesCollectingEvidence/hooks/useDisputeCreate";
import { useQueryClient } from "react-query";
import { useDisputeDelete } from "../hooks/useDisputeDelete";
import { useTranslation } from "react-i18next";

// Props
type ComponentProps = {
    item: any[];
    keyStatus: string;
    refetch: () => void;
};

// Component
export const DisputItem = ({ item, keyStatus, refetch }: ComponentProps) => {
    const [opened, setOpen] = useState(false);
    const { t } = useTranslation();
    const { handleUpdate } = useDisputeCreate();
    const { handleDelete } = useDisputeDelete(keyStatus);
    const queryClient = useQueryClient();
    const canMoveToNotRelevant = keyStatus !== 'NOTRELEVANT'
    const canMoveToRelevant = keyStatus === "NOTRELEVANT";
    const {
        actions: { setDisputPayload },
    } = useAlertListStore();
    const { handleOpen } = useModal();
    const [itemAlert, setItemAlert] = useState<DisputesListItem[]>([]);
    const [firstItemAlert, setFirstItemAlert] = useState<DisputesListItem>();
    const [loadingId, setloadingId] = useState("");
    const [id, setId] = useState("");
    const { setDisputModalResponse, disputModalResponse } = useDisputesStore();
    useEffect(() => {
        if (!item) return;
        setItemAlert(item);
    }, [item, setItemAlert]);

    useEffect(() => {
        setFirstItemAlert(itemAlert[0]);
    }, [itemAlert, setFirstItemAlert]);
    
    const checkKeyStatus = () => {
        switch (keyStatus){
            case "DRAFT_WEBSITE_OWNER":
            case "DRAFT_PROVIDER":
            case "DRAFT_GOOGLE":
                return "DRAFT"
            case "IN_PROGRESS_WEBSITE_OWNER":
            case "IN_PROGRESS_PROVIDER":
            case "IN_PROGRESS_GOOGLE":
                return "IN_PROGRESS"
            case "CLAIM_SENT_WEBSITE_OWNER":
            case "CLAIM_SENT_PROVIDER":
            case "CLAIM_SENT_GOOGLE":
            case "LETTER_SENT_WEBSITE_OWNER":
            case "LETTER_SENT_PROVIDER":
            case "LETTER_SENT_GOOGLE":
                return "CLAIM_SENT"
            default:
                return  keyStatus
        }
    }
    useEffect(()=>{
        checkKeyStatus();
    }, [keyStatus])

    const checkStatus = useCallback((status: any) => {
        switch (status) {
            case undefined:
                return "Draft: Website Owner";

            case "Draft: Website Owner":
                return "Draft: Website Owner";

            case "In Progress: Website Owner":
                return "In Progress: Website Owner";

            case "Claim Sent: Website Owner":
                return "Draft: Provider";

            case "Draft: Provider":
                return "Draft: Provider";

            case "In Progress: Provider":
                return "In Progress: Provider";

            case "Claim Sent: Provider":
                return "Draft: Google";

            case "Draft: Google":
                return "Draft: Google";

            default:
                return status;
        }
    }, []);
    const navigate = useNavigate();

    const onHandleReact = async (values: DisputesListItem) => {
        setDisputPayload({ appearedOn: values.appearedOn, alertId: item[0]._id });
        await handleUpdate({ ...values, status: checkStatus(values.status), alertId: values.alertId._id });
        navigate(`/disputes/${values._id}/collecting-evidence`, { replace: true });
    };

    const onMoveToNotRelevant = useCallback(
        async (disput: DisputesListItem) => {
            await handleUpdate({ ...disput, isNotRelevant: true, alertId: disput.alertId._id });
            await queryClient.invalidateQueries([`DISPUTES_LIST_${keyStatus}`]);
            await queryClient.invalidateQueries([`DISPUTES_LIST_NOTRELEVANT`]);
        },
        [handleUpdate, queryClient, keyStatus]
    );

    const onMoveToRelevant = useCallback(
        async (disput: DisputesListItem) => {
            await handleUpdate({ ...disput, isNotRelevant: false, alertId: disput.alertId._id });
            await queryClient.invalidateQueries([`DISPUTES_LIST_${keyStatus}`]);
            await queryClient.invalidateQueries([`DISPUTES_LIST_NOTRELEVANT`]);
        },
        [handleUpdate, queryClient, keyStatus]
    );

    const onSaveAlertParam = useCallback(
        (appearedOn: string) => {
            setDisputPayload({ appearedOn, alertId: item[0]._id });
        },
        [item, setDisputPayload]
    );

    const onClickUnderCourt = (id: string) => {
        setId(id);
        handleOpen({ type: ModalType.DISPUTE, content: ModalContent.TakeItToCourt });
    };

    const onUnderCourt = useCallback(
        async (id: string) => {
            try {
                await editDisput({ _id: id, status: "Under Court" });
                await refetch();
            } catch (error) {}
        },
        [refetch]
    );
    useEffect(() => {
        const onUnderCourt = async (id: string) => {
            try {
                await editDisput({ _id: id, status: "Under Court" });
                await refetch();
                setDisputModalResponse(undefined);
                setId("");
            } catch (error) {
                setDisputModalResponse(undefined);
                setId("");
            }
        };

        if (disputModalResponse === ModalContent.TakeItToCourt && id) {
            onUnderCourt(id);
        }
        return setDisputModalResponse(undefined);
    }, [disputModalResponse, id, onUnderCourt, refetch, setDisputModalResponse]);

    const onClosed = useCallback(
        async (id: string) => {
            try {
                await editDisput({ _id: id, status: "Closed" });
                await refetch();
            } catch (error) {}
        },
        [refetch]
    );

    const handleNextStatusClick = async (alert: any) => {
        switch (alert.status) {
            case "In Progress: Website Owner":
                try {
                    setloadingId(alert._id);
                    await editDisput({ _id: alert._id, status: "Draft: Provider" });
                    await refetch();
                    setloadingId("");
                } catch (error) {}
                break;

            case "In Progress: Provider":
                try {
                    setloadingId(alert._id);
                    await editDisput({ _id: alert._id, status: "Draft: Google" });
                    await refetch();
                    setloadingId("");
                } catch (error) {}
                break;

            case "In Progress: Google":
                await onClosed(alert._id);
                break;
        }
    };

    if (!itemAlert.length || !firstItemAlert) return null;

    return (
        <>
            {itemAlert.length === 1 && (
                <>
                    <Root>
                        <td style={{ overflow: "hidden" }}>
                            <Group spacing={0} style={{ overflow: "hidden", width: "100%" }}>
                                <Group style={{ paddingLeft: "48px" }}>
                                    <ViewedStatus isRead={true} length={itemAlert.length} />
                                </Group>
                                <Group>
                                    <Group px={16} align="center" sx={{ height: 54 }} style={{ overflow: "hidden" }}>
                                        <Text
                                            size={"sm"}
                                            color={"#0A001E"}
                                            weight={"500"}
                                            style={{
                                                overflow: "hidden",
                                                textOverflow: "ellipsis",
                                                whiteSpace: "nowrap",
                                                // width: "99%",
                                            }}
                                        >
                                            {firstItemAlert.documentId.title}
                                        </Text>
                                    </Group>
                                </Group>
                                <Group>
                                    <Group px={16} align="center" sx={{ height: 54 }}>
                                        <Text
                                            size={"sm"}
                                            color={"#666892"}
                                            weight={"500"}
                                            style={{
                                                overflow: "hidden",
                                                textOverflow: "ellipsis",
                                                whiteSpace: "nowrap",
                                                width: "45%",
                                            }}
                                        >
                                            {/* {firstItemAlert.introduction} */}
                                        </Text>
                                    </Group>
                                </Group>
                            </Group>
                        </td>
                        <td>
                            {/* <a href={firstItemAlert.isSimilarContent ? firstItemAlert.url : "#"}> */}
                            <Group px={16} align="center" sx={{ height: 54 }} style={{ overflow: "hidden" }}>
                                <Text
                                    size={"sm"}
                                    color={"#666892"}
                                    weight={"500"}
                                    style={{
                                        overflow: "hidden",
                                        textOverflow: "ellipsis",
                                        whiteSpace: "nowrap",
                                    }}
                                >
                                    [{firstItemAlert.alertId.scanId.aggregatedScore}%] – {firstItemAlert.appearedOn}
                                </Text>
                            </Group>
                            {/* </a> */}
                        </td>
                        <td>
                            <Group px={16} align="center" sx={{ height: 54 }} style={{ overflow: "hidden" }}>
                                <Text size={"sm"} color={"#666892"} weight={"500"}>
                                    {humanizeDate(firstItemAlert.createdAt).shortDate}
                                </Text>
                            </Group>
                        </td>

                        {
                            {
                                DRAFT: (
                                    <td>
                                        <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                            <Link to={`/disputes/${firstItemAlert._id}/collecting-evidence`}>
                                                <SharkButton
                                                    variant={"filled"}
                                                    buttonType="primary"
                                                    onClick={() => onSaveAlertParam(firstItemAlert.appearedOn)}
                                                    width={"100%"}
                                                    style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                                >
                                                    {t("dispute.dispute_item.edit")}
                                                </SharkButton>
                                            </Link>
                                        </Group>
                                    </td>
                                ),
                                IN_PROGRESS: (
                                    <>
                                        <td>
                                            <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                <Link to={`/disputes/${firstItemAlert._id}/collecting-evidence`}>
                                                    <SharkButton
                                                        variant={"filled"}
                                                        buttonType="primary"
                                                        onClick={() => onSaveAlertParam(firstItemAlert.appearedOn)}
                                                        width={"100%"}
                                                        style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                                    >
                                                        {t("dispute.dispute_item.react")}
                                                    </SharkButton>
                                                </Link>
                                            </Group>
                                        </td>
                                        <td>
                                            {firstItemAlert.status !== "Under Court" && (
                                                <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                    <SharkButton
                                                        loading={firstItemAlert._id === loadingId}
                                                        disabled={firstItemAlert._id === loadingId}
                                                        variant={"filled"}
                                                        buttonType="primary"
                                                        onClick={() => handleNextStatusClick(firstItemAlert)}
                                                        width={"100%"}
                                                        style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                                    >
                                                        {t("dispute.dispute_item.next_status")}
                                                    </SharkButton>
                                                </Group>
                                            )}
                                        </td>
                                        <td>
                                            <Group px={16} position="center" align="center" sx={{ height: 54 }} onClick={() => onClosed(firstItemAlert._id)}>
                                                <Text size={"sm"} color={"#4700E5"} weight={"500"} style={{ cursor: "pointer" }}>
                                                    {t("dispute.dispute_item.closed")}
                                                </Text>
                                            </Group>
                                        </td>{" "}
                                    </>
                                ),
                                CLAIM_SENT: (
                                    <>
                                        <td>
                                            <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                <SharkButton
                                                    variant={"filled"}
                                                    buttonType="primary"
                                                    onClick={() => onHandleReact(firstItemAlert)}
                                                    width={"100%"}
                                                    style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                                >
                                                    {t("dispute.dispute_item.react")}
                                                </SharkButton>
                                            </Group>
                                        </td>
                                        <td>
                                            <Group
                                                px={16}
                                                position="center"
                                                align="center"
                                                sx={{ height: 54 }}
                                                onClick={() => onClickUnderCourt(firstItemAlert._id)}
                                            >
                                                <Text size={"sm"} color={"#4700E5"} weight={"500"} style={{ cursor: "pointer" }}>
                                                    Under <br /> Court
                                                </Text>
                                            </Group>
                                        </td>
                                        <td>
                                            <Group px={16} position="center" align="center" sx={{ height: 54 }} onClick={() => onClosed(firstItemAlert._id)}>
                                                <Text size={"sm"} color={"#4700E5"} weight={"500"} style={{ cursor: "pointer" }}>
                                                    {t("dispute.dispute_item.closed")}
                                                </Text>
                                            </Group>
                                        </td>{" "}
                                    </>
                                ),
                                ENDED: (
                                    <>
                                        <td>
                                            <Group px={16} align="center" sx={{ height: 54 }} style={{ overflow: "hidden" }}>
                                                <Text size={"sm"} color={"#666892"} weight={"500"}>
                                                    {humanizeDate(item[0].updatedAt).shortDate}
                                                </Text>
                                            </Group>
                                        </td>
                                        <td>
                                            <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                <Link to={`/disputes/${firstItemAlert._id}/collecting-evidence`}>
                                                    <SharkButton
                                                        variant={"filled"}
                                                        buttonType="primary"
                                                        onClick={() => onSaveAlertParam(firstItemAlert.appearedOn)}
                                                        width={"100%"}
                                                        style={{
                                                            padding: "3px",
                                                            paddingLeft: "10px",
                                                            paddingRight: "10px",
                                                            cursor: "pointer",
                                                            overflow: "hidden",
                                                            textOverflow: "ellipsis",
                                                        }}
                                                    >
                                                        {t("dispute.dispute_item.view_dispute")}
                                                    </SharkButton>
                                                </Link>
                                            </Group>
                                        </td>
                                    </>
                                ),
                                NOTRELEVANT: <td></td>,
                            }[checkKeyStatus()]
                        }
                        <td>
                            <Group px={16} position="right" align="center" sx={{ height: 54, pointerEvents: "none" }}>
                                <ActionMenuDisput
                                    protectId={firstItemAlert._id}
                                    onMoveToNotRelevant={canMoveToNotRelevant ? () => onMoveToNotRelevant(firstItemAlert) : undefined}
                                    onMoveToRelevant={canMoveToRelevant ? () => onMoveToRelevant(firstItemAlert) : undefined}
                                    handleDelete={() => handleDelete(firstItemAlert._id)}
                                />
                            </Group>
                        </td>
                    </Root>
                    {/* {itemAlert[0].slice(1).map((similarContent) => (
                        <Root className="children" style={{ border: "none" }}>
                            <td></td>
                            <td>
                                <tr>
                                    <td>
                                        <Group align="center" sx={{ height: 54 }}>
                                            <ViewedStatus isRead={similarContent.isRead} onlyCircle={true} />
                                        </Group>
                                    </td>
                                    <td>
                                        <a href={similarContent.url}>
                                            <Group px={16} align="center" sx={{ height: 54 }}>
                                                <Text
                                                    size={"sm"}
                                                    color={"#666892"}
                                                    weight={"500"}
                                                    style={{
                                                        overflow: "hidden",
                                                        textOverflow: "ellipsis",
                                                        whiteSpace: "nowrap",
                                                    }}
                                                >
                                                    {similarContent.url}
                                                </Text>
                                            </Group>
                                        </a>
                                    </td>
                                </tr>
                            </td>
                            <td colSpan={4}></td>
                        </Root>
                    ))} */}
                </>
            )}
            {itemAlert.length > 1 && (
                <>
                    <Root>
                        <td colSpan={4} style={{ overflow: "hidden" }}>
                            <Group onClick={() => setOpen(!opened)} spacing={0} style={{ cursor: "pointer" }}>
                                <Group>
                                    <Group px={16} align="center" sx={{ height: 54 }}>
                                        {opened && <Icon icon={`ic:round-keyboard-arrow-down`} rotate={"180deg"} color="#4b506d" />}
                                        {!opened && <Icon icon={`ic:round-keyboard-arrow-down`} color="#4b506d" />}
                                    </Group>
                                </Group>
                                <Group>
                                    <ViewedStatus isRead={true} length={itemAlert.length} />
                                </Group>
                                <Group style={{ overflow: "hidden" }}>
                                    <Group px={16} align="center" direction="row" sx={{ height: 54 }} style={{ overflow: "hidden" }}>
                                        <Text
                                            size={"sm"}
                                            color={"#0A001E"}
                                            weight={"500"}
                                            style={{
                                                overflow: "hidden",
                                                textOverflow: "ellipsis",
                                                whiteSpace: "nowrap",
                                                // width: "99%",
                                            }}
                                        >
                                            {firstItemAlert.documentId.title}
                                        </Text>
                                    </Group>
                                </Group>
                                <Group>
                                    <Group px={16} align="center" sx={{ height: 54 }}>
                                        <Text
                                            size={"sm"}
                                            color={"#666892"}
                                            weight={"500"}
                                            style={{
                                                overflow: "hidden",
                                                textOverflow: "ellipsis",
                                                whiteSpace: "nowrap",
                                                width: "90%",
                                            }}
                                        >
                                            {/* {firstItemAlert.introduction} */}
                                        </Text>
                                    </Group>
                                </Group>
                            </Group>
                        </td>
                        {
                            {
                                DRAFT: (
                                    <td>
                                        <Group px={16} position="right" align="center" sx={{ height: 54, pointerEvents: "none" }}>
                                            <ActionMenuDisput
                                                protectId={firstItemAlert._id}
                                                handleDelete={() => handleDelete(firstItemAlert._id)}
                                                onMoveToNotRelevant={() => onMoveToNotRelevant(firstItemAlert)}
                                            />
                                        </Group>
                                    </td>
                                ),
                                IN_PROGRESS: (
                                    <td colSpan={3}>
                                        <Group px={16} position="right" align="center" sx={{ height: 54, pointerEvents: "none" }}>
                                            <ActionMenuDisput
                                                protectId={firstItemAlert._id}
                                                onMoveToNotRelevant={() => onMoveToNotRelevant(firstItemAlert)}
                                                handleDelete={() => handleDelete(firstItemAlert._id)}
                                            />
                                        </Group>
                                    </td>
                                ),
                                CLAIM_SENT: (
                                    <td colSpan={3}>
                                        <Group px={16} position="right" align="center" sx={{ height: 54, pointerEvents: "none" }}>
                                            <ActionMenuDisput
                                                protectId={firstItemAlert._id}
                                                onMoveToNotRelevant={() => onMoveToNotRelevant(firstItemAlert)}
                                                handleDelete={() => handleDelete(firstItemAlert._id)}
                                            />
                                        </Group>
                                    </td>
                                ),
                                ENDED: (
                                    <>
                                        <td colSpan={2}>
                                            <Group px={16} position="right" align="center" sx={{ height: 54, pointerEvents: "none" }}>
                                                <ActionMenuDisput
                                                    protectId={firstItemAlert._id}
                                                    onMoveToNotRelevant={() => onMoveToNotRelevant(firstItemAlert)}
                                                    handleDelete={() => handleDelete(firstItemAlert._id)}
                                                />
                                            </Group>
                                        </td>
                                    </>
                                ),
                                NOTRELEVANT: (
                                    <>
                                        <td>
                                            <Group px={16} position="right" align="center" sx={{ height: 54, pointerEvents: "none" }}>
                                                <ActionMenuDisput
                                                    protectId={firstItemAlert._id}
                                                    onMoveToRelevant={() => onMoveToRelevant(firstItemAlert)}
                                                    handleDelete={() => handleDelete(firstItemAlert._id)}
                                                />
                                            </Group>
                                        </td>
                                    </>
                                ),
                            }[checkKeyStatus()]
                        }
                    </Root>

                    {opened &&
                        item.map((alert, index) => (
                            <Root key={index}>
                                <td></td>
                                <td>
                                    {/* <a href={firstItemAlert.isSimilarContent ? firstItemAlert.url : "#"}> */}
                                    <Group px={16} align="center" sx={{ height: 54 }} style={{ overflow: "hidden" }}>
                                        <Text
                                            size={"sm"}
                                            color={"#666892"}
                                            weight={"500"}
                                            style={{
                                                overflow: "hidden",
                                                textOverflow: "ellipsis",
                                                whiteSpace: "nowrap",
                                            }}
                                        >
                                            [{alert.alertId.scanId.aggregatedScore}%] – {alert.appearedOn}
                                        </Text>
                                    </Group>
                                    {/* </a> */}
                                </td>

                                <td>
                                    <Group px={16} align="center" sx={{ height: 54 }} style={{ overflow: "hidden" }}>
                                        <Text size={"sm"} color={"#666892"} weight={"500"}>
                                            {humanizeDate(alert.createdAt).shortDate}
                                        </Text>
                                    </Group>
                                </td>
                                {
                                    {
                                        DRAFT: (
                                            <td>
                                                <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                    <SharkButton
                                                        as={Link}
                                                        to={`/disputes/${alert._id}/collecting-evidence`}
                                                        variant={"filled"}
                                                        buttonType="primary"
                                                        // onClick={() => onSaveAlertParam(alert.url)}
                                                        width={"100%"}
                                                        style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                                    >
                                                        {t("dispute.dispute_item.edit")}
                                                    </SharkButton>
                                                </Group>
                                            </td>
                                        ),
                                        IN_PROGRESS: (
                                            <>
                                                <td>
                                                    <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                        <SharkButton
                                                            as={Link}
                                                            to={`/disputes/${alert._id}/collecting-evidence`}
                                                            variant={"filled"}
                                                            buttonType="primary"
                                                            // onClick={() => onSaveAlertParam(alert.url)}
                                                            width={"100%"}
                                                            style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                                        >
                                                            {t("dispute.dispute_item.react")}
                                                        </SharkButton>
                                                    </Group>
                                                </td>
                                                <td>
                                                    {alert.status !== "Under Court" && (
                                                        <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                            <SharkButton
                                                                loading={alert._id === loadingId}
                                                                disabled={alert._id === loadingId}
                                                                variant={"filled"}
                                                                buttonType="primary"
                                                                onClick={() => handleNextStatusClick(alert)}
                                                                width={"100%"}
                                                                style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                                            >
                                                                {t("dispute.dispute_item.next_status")}
                                                            </SharkButton>
                                                        </Group>
                                                    )}
                                                </td>
                                                <td>
                                                    <Group px={16} position="center" align="center" sx={{ height: 54 }} onClick={() => onClosed(alert._id)}>
                                                        <Text size={"sm"} color={"#4700E5"} weight={"500"} style={{ cursor: "pointer" }}>
                                                            {t("dispute.dispute_item.closed")}
                                                        </Text>
                                                    </Group>
                                                </td>{" "}
                                            </>
                                        ),
                                        CLAIM_SENT: (
                                            <>
                                                <td>
                                                    <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                        <SharkButton
                                                            variant={"filled"}
                                                            buttonType="primary"
                                                            onClick={() => onHandleReact(alert)}
                                                            width={"100%"}
                                                            style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                                        >
                                                            {t("dispute.dispute_item.react")}
                                                        </SharkButton>
                                                    </Group>
                                                </td>
                                                <td>
                                                    <Group
                                                        px={16}
                                                        position="center"
                                                        align="center"
                                                        sx={{ height: 54 }}
                                                        onClick={() => onClickUnderCourt(firstItemAlert._id)}
                                                    >
                                                        <Text size={"sm"} color={"#4700E5"} weight={"500"} style={{ cursor: "pointer" }}>
                                                            Under <br /> Court
                                                        </Text>
                                                    </Group>
                                                </td>
                                                <td>
                                                    <Group
                                                        px={16}
                                                        position="center"
                                                        align="center"
                                                        sx={{ height: 54 }}
                                                        onClick={() => onClosed(firstItemAlert._id)}
                                                    >
                                                        <Text size={"sm"} color={"#4700E5"} weight={"500"} style={{ cursor: "pointer" }}>
                                                            {t("dispute.dispute_item.closed")}
                                                        </Text>
                                                    </Group>
                                                </td>{" "}
                                            </>
                                        ),
                                        ENDED: (
                                            <>
                                                <td>
                                                    <Group px={16} align="center" sx={{ height: 54 }} style={{ overflow: "hidden" }}>
                                                        <Text size={"sm"} color={"#666892"} weight={"500"}>
                                                            {humanizeDate(alert.updatedAt).shortDate}
                                                        </Text>
                                                    </Group>
                                                </td>
                                                <td>
                                                    <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                        <SharkButton
                                                            as={Link}
                                                            to={`/disputes/${alert._id}/collecting-evidence`}
                                                            variant={"filled"}
                                                            buttonType="primary"
                                                            // onClick={() => onSaveAlertParam(alert.url)}
                                                            width={"100%"}
                                                            style={{
                                                                padding: "3px",
                                                                paddingLeft: "10px",
                                                                paddingRight: "10px",
                                                                cursor: "pointer",
                                                                overflow: "hidden",
                                                                textOverflow: "ellipsis",
                                                            }}
                                                        >
                                                            {t("dispute.dispute_item.view_dispute")}
                                                        </SharkButton>
                                                    </Group>
                                                </td>
                                            </>
                                        ),
                                        NOTRELEVANT: <td></td>,
                                    }[checkKeyStatus()]
                                }
                                <td>
                                    <Group px={16} position="right" align="center" sx={{ height: 54, pointerEvents: "none" }}>
                                        <ActionMenuDisput
                                            protectId={alert._id}
                                            onMoveToNotRelevant={canMoveToNotRelevant ? () => onMoveToNotRelevant(alert) : undefined}
                                            onMoveToRelevant={canMoveToRelevant ? () => onMoveToRelevant(alert) : undefined}
                                            handleDelete={() => handleDelete(alert._id)}
                                        />
                                    </Group>
                                </td>
                            </Root>
                        ))}
                </>
            )}
        </>
    );
};

// Styling
const Root = styled.tr`
    transition: background-color 200ms linear;
    border-top: 1px solid #dddeee;

    &:hover {
        background-color: #edeeff;
    }
`;

