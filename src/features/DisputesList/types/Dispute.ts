import { IAlert } from "./Alert";

export interface IDispute {
    _id:                                   string;
    alertId:                               IAlert;
    documentId:                            string;
    discoveredDate:                        string;
    appearedOn:                            string;
    demands:                               string;
    demandsProperly:                       boolean;
    demandsChange:                         boolean;
    demandsDont:                           boolean;
    demandsMakeSure:                       boolean;
    payAmount:                             number;
    publicationQuestionScreenshots:        string[];
    publicationQuestionLinks:              string[];
    publicationQuestionWebarchiveLinks:    string[];
    isPublicationQuestionWebarchiveFailed: boolean;
    publicationLinks:                      string[];
    publicationWebarchiveLinks:            string[];
    isPublicationWebarchiveFailed:         boolean;
    draftsScreenshots:                     string[];
    ownerClaimText:                        string;
    providerClaimText:                     string;
    googleClaimText:                       string;
    ownerClaimEmails:                      string[];
    providerClaimEmails:                   string[];
    status:                                string;
    ownerId:                               string;
    isNotRelevant:                         boolean;
    userContactEmail:                      string;
    createdAt:                             string;
    updatedAt:                             string;
    ownerClaimType:                        string;
}
