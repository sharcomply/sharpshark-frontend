export interface IDocument {
    _id:                   string;
    type:                  string;
    isMinted:              boolean;
    title:                 string;
    fullTitle:             string;
    ownerId:               string;
    authors:               string[];
    coAuthors:             string[];
    participants:          string[];
    commonId:              string;
    version:               number;
    parentTokenId:         string;
    additionalImages:      string[];
    images:                string[];
    sanitizedLinkUrl:      string;
    pagesCount:            number;
    cachedMetadata:        ICachedMetadata;
    cachedSource:          string;
    mintedNetworks:        IMintedNetwork[];
    createdAt:             string;
    updatedAt:             string;
    linkUrl:               string;
    metadataHash:          string;
    metadataUrl:           string;
    sourceHash:            string;
    tokenId:               string;
    linkUrlQrCodeLink:     string;
    metadataUrlQrCodeLink: string;
}

export interface ICachedMetadata {
    Title:          string;
    Authors:        string[];
    CoAuthors:      string[];
    Participants:   string[];
    MetamaskMinter: string;
    SourceIPFSHash: string;
    SourceIPFSUri:  string;
    _id:            string;
}

export interface IMintedNetwork {
    name:                         string;
    mintTransactionHash:          string;
    mintTransactionQrCodeLink:    string;
    mintTransactionLink:          string;
    blockchainTokenUrlQrCodeLink: string;
    blockchainTokenUrlLink:       string;
    _id:                          string;
}
