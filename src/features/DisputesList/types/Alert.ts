export interface IAlert {
    _id:        string;
    type:       string;
    scanId:     IScanID;
    ownerId:    string;
    documentId: string;
    date:       string;
    isRead:     boolean;
    isSkipped:  boolean;
    isReacted:  boolean;
    report:     string;
}

export interface IScanID {
    _id:                  string;
    ownerId:              string;
    documentId:           string;
    purpose:              string;
    aggregatedScore:      number;
    isRequestPayed:       boolean;
    pagesCount:           number;
    isCopyleaksWebhooked: boolean;
    isResultIgnored:      boolean;
    similarContent:       ISimilarContent[];
    nonce:                string;
    nonceEstimate:        string;
}

export interface ISimilarContent {
    url:          string;
    title:        string;
    introduction: string;
    similarity:   number;
    _id:          string;
}
