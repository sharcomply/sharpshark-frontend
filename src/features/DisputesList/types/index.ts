import { IDispute } from "./Dispute";
import { IDocument } from "./Document";

export interface DisputeNestedGroup {
    appearedOn: string;
    disputes: IDispute[];
}

export interface DisputeGroup {
    documentId: IDocument;
    appearedOns?: DisputeNestedGroup[];
    disputes?: IDispute[];
}

export enum DisputeStatus {
    DRAFT = "DRAFT",
    IN_PROGRESS = "IN_PROGRESS",
    CLAIM_SENT = "CLAIM_SENT",
    ENDED = "ENDED",
    NOT_RELEVANT = "NOTRELEVANT",
}