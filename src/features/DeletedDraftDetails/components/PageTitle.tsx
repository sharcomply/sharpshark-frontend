import { Group, Text, Tooltip } from "@mantine/core";
import Icon from "components/Icon";
import { ReactNode } from "react";

// Props
type ComponentProps = {
    title?: string;
    icon?: string;
    iconColor?: string;
    tooltipText?: ReactNode;
    actions?: ReactNode;
};

export const PageTitle = (props: ComponentProps) => {
    const { title, icon = "ep:question-filled", iconColor = "#E50C54", actions, tooltipText } = props;

    return (
        <Group position={"apart"} align={"center"} sx={{ height: 42, maxWidth: "calc(100vw - 356px)" }} spacing={0} noWrap={true}>
            {/* Title */}
            <Group spacing={8} position={"left"} sx={{ maxWidth: "40%" }} noWrap={true}>
                {tooltipText && (
                    <Tooltip
                        label={tooltipText}
                        position="bottom"
                        placement="center"
                        transition="fade"
                        transitionDuration={300}
                        transitionTimingFunction="ease"
                        withArrow={false}
                        styles={{ body: { backgroundColor: "#666892" } }}
                    >
                        <Icon icon={icon} color={iconColor} hoverColor={iconColor} />
                    </Tooltip>
                )}
                {!tooltipText && <Icon icon={icon} color={iconColor} hoverColor={iconColor} />}
                <Text
                    color={"#0a001e"}
                    weight="700"
                    sx={{
                        fontSize: 24,
                        lineHeight: "30px",
                        overflow: "hidden",
                        whiteSpace: "nowrap",
                        textOverflow: "ellipsis",
                    }}
                >
                    {title}
                </Text>
            </Group>

            {/* Actions */}
            <Group spacing={20} position={"right"} noWrap={true}>
                {actions}
            </Group>
        </Group>
    );
};
