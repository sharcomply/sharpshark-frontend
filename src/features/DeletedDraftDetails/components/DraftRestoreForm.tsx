import { Group, Image, Stack, Text } from "@mantine/core";
import { UseFormReturn, useWatch } from "react-hook-form";
import { DraftRestoreFormState } from "../hooks/useDraftRestoreForm";

interface FormProps {
    form: UseFormReturn<DraftRestoreFormState>;
}

export const DraftRestoreForm = ({ form }: FormProps) => {
    const { control } = form;

    const file = useWatch({ control, name: "file" });
    const image = useWatch({ control, name: "image" });
    const title = useWatch({ control, name: "title" });
    const text = useWatch({ control, name: "text" });
    
    return (
        <Stack pt={16} px={16} spacing={0} sx={{ flex: 1 }}>
            <Stack px={16} py={10}>
                <Text py={8} color={"#0A001E"} weight={"700"} sx={{ fontSize: 24, lineHeight: '30px', overflowWrap: 'anywhere' }}>
                    {String(title)}
                </Text>
            </Stack>

            <Stack px={16} py={8}>
                {!(file || image) && (
                    <Text color={"#0A001E"} weight={"400"} sx={{ fontSize: 16, lineHeight: '20px', overflowWrap: 'anywhere' }}>
                        {String(text)}
                    </Text>
                )}

                {!!image && (
                    <Group>
                        <Image width={480} height={360} src={URL.createObjectURL(image)} withPlaceholder radius={"sm"} />
                    </Group>
                )}
            </Stack>
        </Stack>
    );
};
