import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { validationSchema } from "../validationSchema";
import { useEffect } from "react";
import { useDraftAlert } from "features/DraftDetails/hooks/useDraftAlert";

export interface DraftRestoreFormState {
    title: string;
    text: string;
    file?: File;
    image?: any;
}

export const useDraftRestoreForm = (initialValues: DraftRestoreFormState) => {
    const alert = useDraftAlert()

    const form = useForm<DraftRestoreFormState>({
        resolver: yupResolver(validationSchema),
        defaultValues: initialValues
    })

    useEffect(() => {
        const error = Object.values(form.formState.errors)[0]

        if (!error) return
    
        alert.actions.open()
        alert.actions.error('Validation Error', error.message || "")

        return () => {
            alert.actions.close()
        }
        
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [alert.actions.error, alert.actions.close, alert.actions.open, alert.actions.error, form.formState.errors])

    return {
        form
    }
}
