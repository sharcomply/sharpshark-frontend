import { object, string, mixed } from "yup";

export const validationSchema = object().shape({
    title: string().required('Draft "title" not specified').trim().max(100),
    type: string().oneOf(["image", "text"]),
    text: string().when(["type", "file"], {
        is: (type: string, file?: File) => type === "text" && !file,
        then: string().required('Draft "text" not specified').min(50, 'Draft "text" must be min 50 characters').trim(),
        otherwise: string(),
    }),
    file: mixed().when(["type", "text"], {
        is: (type: string, text?: string) => type === "text" && !text?.length,
        then: mixed().required('Draft "text" not specified'),
        otherwise: mixed(),
    }),
    image: mixed().when("type", {
        is: "image",
        then: mixed().required("Image for upload not specified"),
        otherwise: mixed(),
    }),
}, [[ 'file', 'text']]);
