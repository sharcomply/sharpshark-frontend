import styled from "@emotion/styled";
import { Group, Stack, Text } from "@mantine/core";
import { useEffect } from "react";

import { useToolbarStore } from "features/Toolbar/hooks/useToolbarStore";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import { SharkButton } from "components/SharkButton";
import { Alert } from "./components/Alert";
import { PageTitle } from "./components/PageTitle";
import { DraftRestoreForm } from "./components/DraftRestoreForm";
import { humanizeDate } from "helpers/humanizeDate";
import { getDayAgo } from "helpers/getDayAgo";
import { useRestoreDraft } from "features/DeletedDraftList/hooks/useRestoreDraft";
import { useDraftRestoreForm } from "./hooks/useDraftRestoreForm";
import { useTranslation } from "react-i18next";

// Props
type ComponentProps = {
    draft: DraftDetails;
}

// Component
export default function DeletedDraftDetails({ draft }: ComponentProps) {
    const { setBackButton } = useToolbarStore();
    const { setDraftId, clearStore, ...draftStore } = useDraftStore();
    const { handleRestore, isLoading: restoreLoading } = useRestoreDraft(draft._id, true)
    const {t} = useTranslation()
    const { form } = useDraftRestoreForm({
        title: draft.title,
        text: draft.text|| "",
        file: undefined,
        image: draft.imageFile,
    })

    useEffect(() => {
        if (!draft) return 
        setDraftId(draft._id)
    }, [draft, setDraftId]);

    useEffect(() => {
        setBackButton(true);
        clearStore()
        return () => {
            setBackButton(false);
            clearStore()
        }
    }, [setBackButton, clearStore]);

    // Render
    const actionItems = () => (
        <Group>
            <SharkButton buttonType={"secondary"} variant={'outlined'} disabled={true}>
                × {t('buttons.delete_forever_btn')}
                <br/>
                {t('common.coming_soon')}
            </SharkButton>
            <SharkButton onClick={handleRestore} loading={restoreLoading} buttonType={"secondary"} variant={'filled'}>
                {t("buttons.restore_btn")}
            </SharkButton>
        </Group>
    )

    return (
        <Stack>
            <PageTitle icon="ic:round-delete" title={draft?.title || t('drafts.deleted_draft_title')} actions={actionItems()} />

            {draftStore.alert.show && (
                <Alert title={draftStore.alert.title} text={draftStore.alert.message} variant={draftStore.alert.type} />
            )}

            <FormRoot sx={{ minHeight: '380px' }}>
                <DraftRestoreForm form={form} />

                <Footer px={16}>
                    <Group>
                        <Text size={"sm"} weight={"400"} color={"#666892"}>
                            {t("drafts.deleted_text", {date: humanizeDate(draft.deleteDate, 'at').fullDate, daysAgo: getDayAgo(draft.deleteDate)})}
                        </Text>
                    </Group>
                </Footer>
            </FormRoot>
        </Stack>
    );
}

const FormRoot = styled(Stack)`
    position: relative;
    background-color: #fbfcff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    overflow: hidden;
    justify-content: space-between;
`;

const Footer = styled(Group)`
    position: relative;
    background-color: #ffffff;
    border-top: 1px solid #dddeee;
    height: 56px;
    overflow: hidden;
    justify-content: space-between;
`;
