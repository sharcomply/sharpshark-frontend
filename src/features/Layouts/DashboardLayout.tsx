import styled from "@emotion/styled";
import Sidebar from "features/Sidebar";
import Toolbar from "features/Toolbar";
import { Outlet } from "react-router-dom";
import { useScanClient } from "../Scan/ScanProvider";
import { useCallback, useEffect } from "react";
import { ScanCurrentStatus } from "../Scan/ScanClient";
import { useQueryClient } from "react-query";
// import { setUnreadAlerts, getUnreadAlerts } from "helpers/unreadAlertsStorage";
import { useSidebarCounts } from "features/Sidebar/hooks/useSidebarCounts";
import { useSidebarStore } from "features/Sidebar/hooks/useSidebarStore";
import Message from "features/DraftDetails/components/Message";
import { useDraftMessages } from "features/DraftDetails/hooks/useDraftMessages";
import { Collapse } from "@mantine/core";

// Component
export default function DashboardLayout() {
    const queryClient = useQueryClient();
    const { client, connected } = useScanClient();
    const { data } = useSidebarCounts();
    const { setAlertsIndicators, alerts } = useSidebarStore();
    const { draftMessage } = useDraftMessages();
    const onAlertCreated = useCallback(
        async (payload: any) => {
            const draftId = payload.draftId;
            setAlertsIndicators({ total: data?.counts.alerts, unread: alerts.unread! + 1 });
            await Promise.all([
                // queryClient.invalidateQueries(['PROTECTED_LIST']),F
                queryClient.invalidateQueries(["PROTECTED_ITEM", draftId]),
            ]);
        },
        [queryClient, setAlertsIndicators, alerts.unread, data?.counts.alerts]
    );

    // Listen WS by Status
    useEffect(() => {
        if (!connected) return;
        client.addStatusHandler(ScanCurrentStatus.ALERT_CREATED, onAlertCreated);
        return () => client.removeStatusHandler(ScanCurrentStatus.ALERT_CREATED, onAlertCreated);
    }, [connected, onAlertCreated, client, data?.counts.unreadAlerts]);

    return (
        <Layout>
            <Sidebar />
            <LayoutWrapper>
                <Toolbar />
                <Collapse in={draftMessage.show}>
                    <Message />
                </Collapse>
                <PageContent>
                    <Outlet />
                </PageContent>
            </LayoutWrapper>
        </Layout>
    );
}

// Styling
const Layout = styled.div`
    display: flex;
    flex-direction: row;
    height: 100vh;
`;

const LayoutWrapper = styled.div`
    display: flex;
    flex-direction: column;
    flex: auto;
`;

const PageContent = styled.div`
    display: flex;
    flex-direction: column;
    flex: auto;
    padding: 32px;
    overflow: auto;
`;
