import { useAppDispatch, useAppSelector } from "store";
import { useCallback } from "react";
import { disputeActions, DisputeState } from "../store";

export default function useDisputesStore() {
    const state = useAppSelector((state) => state.dispute);
    const dispatch = useAppDispatch();



    const setDisputeStatus = useCallback(
        (status: DisputeState["status"]) => {
            dispatch(disputeActions.setDisputeStatus(status));
        },
        [dispatch]
    );
    const setDisputModalResponse = useCallback(
        (disputModalResponse: DisputeState["disputModalResponse"]) => {
            dispatch(disputeActions.setDisputModalResponse(disputModalResponse));
        },
        [dispatch]
    );

    const setEmailFound = useCallback(
        (emailFound: DisputeState["emailFound"]) => {
            dispatch(disputeActions.setEmailFound(emailFound));
        },
        [dispatch]
    );


    const resetDisputeStatus = useCallback(() => {
        dispatch(disputeActions.resetDisputeStatus());
    }, [dispatch]);

    return {
        ...state,
        setDisputeStatus,
        resetDisputeStatus,
        setDisputModalResponse,
        setEmailFound
    };
}
