import useDisputeStore from "./useDisputesStore";
import { useCallback } from "react";
import { DisputeState } from "../store";

export const useDisputeStatus = () => {
    const { status: state, setDisputeStatus, resetDisputeStatus } = useDisputeStore();

    const update = useCallback(
        (status: DisputeState["status"]) => {
            setDisputeStatus(status);
        },
        [setDisputeStatus]
    );
    const reset = useCallback(() => {
        resetDisputeStatus();
    }, [resetDisputeStatus]);

    return {
        state,
        actions: {
            update,
            reset,
        },
    };
};
