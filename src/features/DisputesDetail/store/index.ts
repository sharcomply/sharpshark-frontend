import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Dispute Statuses

// Types
export interface DisputeState {
    status: {
        type?: "error" | "warn" | "success";
        message?: string;
    };
    disputModalResponse?: any;
    emailFound?: any
}

// Initial State
const initialState: DisputeState = {
    status: {
        type: undefined,
        message: undefined,
    },
    disputModalResponse: undefined,
    emailFound: undefined
};

// Slice
const disputeSlice = createSlice({
    name: "dispute",
    initialState,
    reducers: {
        setDisputeStatus: (state, action: PayloadAction<DisputeState["status"]>) => {
            state.status.type = action.payload.type;
            state.status.message = action.payload.message;
        },
        setDisputModalResponse: (state, action: PayloadAction<DisputeState["disputModalResponse"]>) => {
            state.disputModalResponse = action.payload
        },
        setEmailFound: (state, action: PayloadAction<DisputeState["emailFound"]>) => {
            state.emailFound = action.payload
        },
        resetDisputeStatus: (state) => {
            state.status.type = undefined;
            state.status.message = undefined;
        },
    },
});

export const disputeReducers = disputeSlice.reducer;
export const disputeActions = disputeSlice.actions;
