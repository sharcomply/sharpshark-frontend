export default function Plansh(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 250 250"
            fill="none"
            width="1em"
            height="1em"
            {...props}
        >
            <rect x="6" y="41" width="238" height="168" stroke="currentColor" strokeWidth="12"/>
            <rect x="224" y="95" width="10" height="60" fill="currentColor"/>
            <rect x="214" y="1" width="248" height="178" transform="rotate(90 214 1)" stroke="currentColor" strokeWidth="2"/>
            <rect x="154" y="225" width="8" height="58" transform="rotate(90 154 225)" stroke="currentColor" strokeWidth="2"/>
        </svg>
    );
}

