import styled from "@emotion/styled";
import { Group, Stack, Text } from "@mantine/core";
import { SharkButton } from "components/SharkButton";
import Icon from "../../components/Icon";

const DisputesSearchLevelSending = () => {

    return (
        <Root>
            <Wrapper>
                <Title>Step 3. Sending the claim to Google law office</Title>
                <div style={{ width: "380px" }}>
                    <div style={{ marginBottom: "16px" }}>
                        <div style={{ display: "flex", flexDirection: "column" }}>
                            <SubTitle>at First, </SubTitle>
                            <Description>
                                Log into your Google account and follow the link:
                            </Description>
                        </div>
                    </div>
                    <div style={{ marginBottom: "16px"}}>
                        <div style={{ display: "flex", alignItems: "center"}}>
                            <Icon icon={"ic:link"} color={"#979CBC"} rotate={-30} style={{marginRight: "8px"}}/>
                            <a target='_blank' href="https://www.google.com/webmasters/tools/legal-removal-request?complaint_type=dmca&visit_id=637932436777923240-611449655&hl=en&rd=1"
                               style={{
                                marginRight: "8px",
                                fontWeight: "400",
                                fontSize: "16px",
                                lineHeight: "20px",
                                color: "#4700E5",
                            }}> Legal removal request </a>
                            <div style={{
                                border: "1px solid #DDDEEE",
                                borderRadius: "4px",
                                padding: "5px",
                            }}>
                                <Icon icon={"ic:open-in-new"} color={"#666892"}/>
                            </div>
                        </div>
                    </div>
                    <div style={{ marginBottom: "16px" }}>
                        <div style={{ display: "flex", flexDirection: "column" }}>
                            <SubTitle>Then </SubTitle>
                            <Description>
                                Identify and describe the copyrighted material,
                            </Description>
                        </div>
                    </div>
                    <div style={{ marginBottom: "16px" }}>
                        <div style={{ display: "flex", flexDirection: "column" }}>
                            <SubTitle>And last </SubTitle>
                            <Description>
                                And tell us you did it!
                            </Description>
                        </div>
                    </div>
                </div>
            </Wrapper>

            <Footer>
                <Group>
                    <Text size={"sm"} weight={"400"} color={"#666892"}>
                        Document “Document long title, this is it’s long, long, very long title”
                    </Text>
                </Group>
                <Group spacing={12}>
                    <Group spacing={8}></Group>
                    <SharkButton
                        // onClick={handleUpdate}
                        variant="filled"
                        buttonType="secondary"
                        size="small"
                        px={8}
                        py={4}
                    >
                        Confirm you’ve sent the report
                    </SharkButton>
                </Group>
            </Footer>
        </Root>
    );
};

export default DisputesSearchLevelSending;

const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
`;

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    margin: 32px 32px 72px 48px;
`;

const Footer = styled(Group)`
    position: relative;
    background-color: #ffffff;
    border-top: 1px solid #dddeee;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
    padding: 14px 16px;
    height: 56px;
    overflow: hidden;
    justify-content: space-between;
`;

const Title = styled.span`
    font-weight: 700;
    font-size: 20px;
    line-height: 24px;
    margin-bottom: 16px;
`;

const SubTitle = styled.span`
    font-weight: 700;
    font-size: 12px;
    line-height: 16px;
    text-transform: uppercase;
    margin-bottom: 6px;
`;

const Description = styled.span`
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;
    color: #666892;
    margin-bottom: 0;
`;
