import { backend } from "api";
import { editDisput, saveDispute } from "api/requests/disputes";
import { useAlertListStore } from "features/AlertsList/hooks/useAlertListStore";
import { useCallback, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDisputeStatus } from "../../DisputesDetail/hooks/useDisputeStatus";
import { useTranslation } from "react-i18next";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";

export const useDisputeCreate = () => {
    const { t } = useTranslation();
    const { state } = useAlertListStore();
    const navigate = useNavigate();
    const [loading, setLoading] = useState<boolean>(false);
    const status = useDisputeStatus();
    const draftStore = useDraftStore();
    const handleUpdate = useCallback(
        async (form: EditDispute) => {
            try {
                draftStore.showDraftAlert(false);
                draftStore.setDraftAlert({
                    type: undefined,
                    title: undefined,
                    message: undefined,
                });
                setLoading(true);
                status.actions.update({ type: undefined, message: t("statuses.uploading") });
                await editDisput({
                    _id: form._id,
                    alertId: form.alertId,
                    appearedOn: form.appearedOn,
                    demands: form.demands,
                    payAmount: form.payAmount,
                    publicationQuestionLinks: form.publicationQuestionLinks,
                    publicationQuestionWebarchiveLinks: form.publicationQuestionWebarchiveLinks,
                    isPublicationQuestionWebarchiveFailed: form.isPublicationQuestionWebarchiveFailed,
                    publicationLinks: form.publicationLinks,
                    publicationWebarchiveLinks: form.publicationWebarchiveLinks,
                    isPublicationWebarchiveFailed: form.isPublicationWebarchiveFailed,
                    providerClaimEmails: form.providerClaimEmails,
                    ownerClaimEmails: form.ownerClaimEmails,
                    googleClaimText: form.googleClaimText,
                    providerClaimText: form.providerClaimText,
                    // ownerClaimType: form.ownerClaimType,
                    status: form.status,
                });

                const uploadFile = async () => {
                    let draftsScreenshots: string[] = [];
                    let publicationQuestionScreenshots: string[] = [];
                    for (const item of form.draftsScreenshots!) {
                        if (typeof item !== "string") {
                            const { uploadLink, downloadLink } = await backend.disputes.getImageUploadLink(form._id);
                            draftsScreenshots.push(downloadLink);
                            await backend.drafts.uploadFile(uploadLink.url, item, uploadLink.fields);
                        } else draftsScreenshots.push(item);
                    }
                    for (const item of form.publicationQuestionScreenshots!) {
                        if (typeof item !== "string") {
                            const { uploadLink, downloadLink } = await backend.disputes.getImageUploadLink(form._id);
                            publicationQuestionScreenshots.push(downloadLink);
                            await backend.drafts.uploadFile(uploadLink.url, item, uploadLink.fields);
                        } else publicationQuestionScreenshots.push(item);
                    }
                    return { draftsScreenshots, publicationQuestionScreenshots };
                };

                const downloadImg = async () => {
                    const { draftsScreenshots, publicationQuestionScreenshots } = await uploadFile();
                    console.log(draftsScreenshots);

                    await editDisput({
                        ...form,
                        draftsScreenshots,
                        publicationQuestionScreenshots,
                    });
                };

                await downloadImg();

                // Get Links for Upload And Download
                // const { uploadLink, downloadLink } = await backend.disputes.getImageUploadLink(_id);

                // Upload File
                // await backend.drafts.uploadFile(uploadLink.url, payload.uploadFile!, uploadLink.fields);

                // // update draft using link to file
                // const draft = await update({
                //     _id,
                //     title: form.title,
                //     linkUrl: downloadLink,
                // });

                status.actions.update({ type: "success", message: t("statuses.saved") });
                // // Redirect to created draft
                // await refetch();
                // setLoading(false)
                setLoading(false);
                // navigate(`/disputes/${form._id}/sending`, { replace: true });
                return form._id;
            } catch (e) {
                status.actions.update({ type: "warn", message: t("statuses.upload_failed") });
                console.log("[DISPUTE_UPDATE]", e);
                setLoading(false);
            }
        },
        [t, status.actions]
    );

    const handleCreate = useCallback(
        async (form: SaveDispute) => {
            try {
                setLoading(true);
                draftStore.showDraftAlert(false);
                draftStore.setDraftAlert({
                    type: undefined,
                    title: undefined,
                    message: undefined,
                });
                status.actions.update({ type: "success", message: t("statuses.uploading") });

                const { _id } = await saveDispute({
                    alertId: state.disputPayload.alertId,
                    appearedOn: state.disputPayload.appearedOn,
                    demands: form.demands || undefined,
                    status: form.status,
                    payAmount: form.payAmount,
                    publicationQuestionLinks: form.publicationQuestionLinks,
                    publicationQuestionWebarchiveLinks: form.publicationQuestionWebarchiveLinks,
                    isPublicationQuestionWebarchiveFailed: form.isPublicationQuestionWebarchiveFailed,
                    publicationLinks: form.publicationLinks,
                    publicationWebarchiveLinks: form.publicationWebarchiveLinks,
                    isPublicationWebarchiveFailed: form.isPublicationWebarchiveFailed,
                    ownerClaimEmails: form.ownerClaimEmails,
                    ownerClaimText: form.ownerClaimText,
                    googleClaimText: form.googleClaimText,
                    providerClaimText: form.providerClaimText,
                    // ownerClaimType: form.ownerClaimType,
                });

                const uploadFile = async () => {
                    let draftsScreenshots: string[] = [];
                    let publicationQuestionScreenshots: string[] = [];
                    for (const item of form.draftsScreenshots!) {
                        const { uploadLink, downloadLink } = await backend.disputes.getImageUploadLink(_id);
                        draftsScreenshots.push(downloadLink);
                        await backend.drafts.uploadFile(uploadLink.url, item, uploadLink.fields);
                    }
                    for (const item of form.publicationQuestionScreenshots!) {
                        const { uploadLink, downloadLink } = await backend.disputes.getImageUploadLink(_id);
                        publicationQuestionScreenshots.push(downloadLink);
                        await backend.drafts.uploadFile(uploadLink.url, item, uploadLink.fields);
                    }
                    return { draftsScreenshots, publicationQuestionScreenshots };
                };

                const downloadImg = async () => {
                    const { draftsScreenshots, publicationQuestionScreenshots } = await uploadFile();
                    console.log(draftsScreenshots);

                    await editDisput({
                        _id,
                        draftsScreenshots,
                        publicationQuestionScreenshots,
                    });
                };

                await downloadImg();

                status.actions.update({ type: "success", message: t("statuses.uploaded") });

                // // Redirect to created draft
                // await refetch();
                if (window.location.pathname.includes("/collecting-evidence")) {
                    navigate(`/disputes/${_id}/collecting-evidence`, { replace: true });
                } else if (window.location.pathname.includes("/generating-claim")) {
                    navigate(`/disputes/${_id}/generating-claim`, { replace: true });
                } else if (window.location.pathname.includes("/sending")) {
                    navigate(`/disputes/${_id}/sending`, { replace: true });
                }

                setLoading(false);
                // navigate(`/disputes/${_id}/sending`, { replace: true });
                return _id;
            } catch (e) {
                status.actions.update({ type: "warn", message: t("statuses.upload_failed") });
                console.log("[DISPUTE_SAVE]", e);
                setLoading(false);
            }
        },
        [t, status.actions, state.disputPayload, navigate]
    );

    return {
        handleCreate,
        handleUpdate,
        loading,
    };
};
