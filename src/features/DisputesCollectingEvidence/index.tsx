import styled from "@emotion/styled";
import { Checkbox, CloseButton, Group, Input, Select, Stack, Text } from "@mantine/core";
import { Dropzone } from "@mantine/dropzone";
import Icon from "components/Icon";
import { SharkButton } from "components/SharkButton";
import { useCallback, useEffect, useRef, useState } from "react";
import { Control, FieldValues, useFieldArray, UseFormRegister, useWatch } from "react-hook-form";
import AddButton from "./components/AddButton";
import RadioButton from "./components/RadioButton";
import RemoveButton from "./components/RemoveButton";
import { useNavigate, useParams } from "react-router-dom";
import { useDraftAlert } from "../DraftDetails/hooks/useDraftAlert";
import { useContract } from "../../blockchain/hooks/useContract";
import { useTranslation } from "react-i18next";
import useAlert from "features/AlertsList/hooks/useAlert";
import clsx from "clsx";

interface Props {
    control: Control<FieldValues>;
    register: UseFormRegister<FieldValues>;
    getValues: (props?: string) => string;
    setValue: (name: string, value: unknown, config?: Object) => void;
}

const DisputesCollectingEvidence = ({ control, register, getValues, setValue }: Props) => {
    const navigate = useNavigate();
    const status = getValues("status");
    const [disable, setDisable] = useState(false);
    const [disableDemands, setDisableDemands] = useState(false);
    const [token_Id, setTokenId] = useState("");
    const tokenIdValue = getValues("documentId.tokenId");
    const { alertId } = useParams();
    const alert = useDraftAlert();
    const contract = useContract();
    const [typeLicense, setTypeLicense] = useState("");
    const { t } = useTranslation();
    const { alert: alertItem } = useAlert(alertId);

    const getUsageRights = useCallback(async () => {
        let tokenInfo = await contract.getTokenInfo(token_Id);
        setTypeLicense(tokenInfo[7]);
    }, [contract, token_Id]);
    getUsageRights();

    alert.actions.close();
    useEffect(() => {
        if (tokenIdValue === undefined) {
            setTokenId(alertItem?.documentId.tokenId as string);
        } else {
            setTokenId(tokenIdValue);
        }
    }, [alertItem, tokenIdValue]);

    useEffect(() => {
        if (status) {
            if (
                status === "Draft: Provider" ||
                status === "In Progress: Provider" ||
                status === "Claim Sent: Provider" ||
                status === "Draft: Google" ||
                status === "In Progress: Google" ||
                status === "Claim Sent: Google"
            ) {
                setDisableDemands(true);
            } else {
                setDisableDemands(false);
            }
        }
    }, [status]);

    useEffect(() => {
        if (status) {
            if (
                status === "In Progress: Website Owner" ||
                status === "In Progress: Provider" ||
                status === "In Progress: Google" ||
                status === "Closed" ||
                status === "Under Court"
            ) {
                setDisable(true);
            } else {
                setDisable(false);
            }
        }
    }, [status]);

    const {
        fields: fieldsPublicationQuestionLinks,
        append: appendPublicationQuestionLinks,
        remove: removePublicationQuestionLinks,
    } = useFieldArray({
        control,
        name: "publicationQuestionLinks",
    });

    const {
        fields: fieldsPublicationQuestionWebarchiveLinks,
        append: appendPublicationQuestionWebarchiveLinks,
        remove: removePublicationQuestionWebarchiveLinks,
    } = useFieldArray({
        control,
        name: "publicationQuestionWebarchiveLinks",
    });

    const {
        fields: fieldsPublicationLinks,
        append: appendPublicationLinks,
        remove: removePublicationLinks,
    } = useFieldArray({
        control,
        name: "publicationLinks",
    });

    const {
        fields: fieldsPublicationWebarchiveLinks,
        append: appendPublicationWebarchiveLinks,
        remove: removePublicationWebarchiveLinks,
    } = useFieldArray({
        control,
        name: "publicationWebarchiveLinks",
    });

    const {
        // fields: fieldsDraftsScreenshots,
        append: appendDraftsScreenshots,
        remove: removeDraftsScreenshots,
    } = useFieldArray({
        control,
        name: "draftsScreenshots",
    });

    const {
        // fields: fieldsPublicationQuestionScreenshots,
        append: appendPublicationQuestionScreenshots,
        remove: removePublicationQuestionScreenshots,
    } = useFieldArray({
        control,
        name: "publicationQuestionScreenshots",
    });

    const imageRefQuestionScreenshots = useRef<any>(null);

    const imageRefDraftsScreenshots = useRef<any>(null);

    const selectDraftsScreenshots = useCallback(() => {
        if (!imageRefDraftsScreenshots.current) return;
        imageRefDraftsScreenshots.current();
    }, []);

    const selectPublicationQuestionScreenshots = useCallback(() => {
        if (!imageRefQuestionScreenshots.current) return;
        imageRefQuestionScreenshots.current();
    }, []);

    const publicationQuestionScreenshots = useWatch({ control, name: "publicationQuestionScreenshots" });
    const draftsScreenshots = useWatch({ control, name: "draftsScreenshots" });
    const demands = useWatch({ control, name: "demands" });
    
    const typeLicenseCC0 = typeLicense === "Time Machine © 2022 by H. G. Wells is licensed under CC0 1.0: https://creativecommons.org/publicdomain/zero/1.0/" && 
        (status === undefined || status === 'Draft: Website Owner')
  
    const changeDemandsCC = () => {
        switch (typeLicense) {
            case "Time Machine © 2022 by H. G. Wells is licensed under CC BY 4.0: https://creativecommons.org/licenses/by/4.0/":
                return (
                    <Section>
                        <Checkbox disabled={disable} {...register("demandsProperly")} label="Properly attribute this material to you" />
                    </Section>
                );
            case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/":
                return (
                    <>
                        <Section>
                            <Checkbox disabled={disable} {...register("demandsProperly")} label="Properly attribute this material to you" />
                        </Section>
                        <Section>
                            <Checkbox disabled={disable} {...register("demandsChange")} label="Change it to its original form" />
                        </Section>
                        <Section>
                            <Checkbox disabled={disable} {...register("demandsMakeSure")} label="Make sure it’s redistributed under identical terms" />
                        </Section>
                    </>
                );
            case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-ND 4.0: https://creativecommons.org/licenses/by-nd/4.0/":
                return (
                    <>
                        <Section>
                            <Checkbox disabled={disable} {...register("demandsProperly")} label="Properly attribute this material to you" />
                        </Section>
                        <Section>
                            <Checkbox disabled={disable} {...register("demandsChange")} label="Change it to its original form" />
                        </Section>
                    </>
                );
            case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC 4.0: https://creativecommons.org/licenses/by-nc/4.0/":
                return (
                    <>
                        <Section>
                            <Checkbox disabled={disable} {...register("demandsProperly")} label="Properly attribute this material to you" />
                        </Section>
                        <Section>
                            <Checkbox disabled={disable} {...register("demandsDont")} label="Don’t use it for commercial purposes" />
                        </Section>
                    </>
                );
            case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/":
                return (
                    <>
                        <Section>
                            <Checkbox disabled={disable} {...register("demandsProperly")} label="Properly attribute this material to you" />
                        </Section>
                        <Section>
                            <Checkbox disabled={disable} {...register("demandsDont")} label="Don’t use it for commercial purposes" />
                        </Section>
                        <Section>
                            <Checkbox disabled={disable} {...register("demandsChange")} label="Change it to its original form" />
                        </Section>
                        <Section>
                            <Checkbox disabled={disable} {...register("demandsMakeSure")} label="Make sure it’s redistributed under identical terms" />
                        </Section>
                    </>
                );
            case "Time Machine © 2022 by H. G. Wells is licensed under CC BY-NC-ND 4.0: https://creativecommons.org/licenses/by-nc-nd/4.0/":
                return (
                    <>
                        <Section>
                            <Checkbox disabled={disable} {...register("demandsProperly")} label="Properly attribute this material to you" />
                        </Section>
                        <Section>
                            <Checkbox disabled={disable} {...register("demandsDont")} label="Don’t use it for commercial purposes" />
                        </Section>
                        <Section>
                            <Checkbox disabled={disable} {...register("demandsChange")} label="Change it to its original form" />
                        </Section>
                    </>
                );
        }
    };

    useEffect(() => {
        if (fieldsPublicationQuestionWebarchiveLinks.length > 0) {
            setValue("isPublicationQuestionWebarchiveFailed", false);
        }
    }, [fieldsPublicationQuestionWebarchiveLinks, setValue]);

    useEffect(() => {
        if (fieldsPublicationWebarchiveLinks.length > 0) {
            setValue("isPublicationWebarchiveFailed", false);
        }
    }, [fieldsPublicationWebarchiveLinks, setValue]);

    return (
        <>
            <Root>
                <Wrapper>
                    <Title>Step 1. Collecting evidence</Title>
                    <Title>Recipient</Title>
                    <SubTitle>who this claim is for</SubTitle>
                    <Description>
                        The first claim should be sent to the website owner.
                        <a href={"/coming-soon-page"} target={"_blank"} style={{ color: "blue", textDecoration: "underline" }} rel="noreferrer">
                            {" "}
                            Why?{" "}
                        </a>
                        But if they ignore it, you can later send it to the provider or/and search engine
                    </Description>
                    <Select data={[]} placeholder={getValues("status") || "Draft: Website Owner"} disabled={true} />
                    <Section>
                        <Title>Case details</Title>
                        <SubTitle>Time of case discovery</SubTitle>
                        <Input {...register("discoveredTime")} disabled={true}></Input>
                    </Section>
                    <Section>
                        <SubTitle>Screenshots of the publication</SubTitle>
                        <Description>
                            Capture and attach it below. Make sure that the date, time, and website URL are clearly visible/clear and visible in the image
                        </Description>

                        <div style={{ display: "flex" }}>
                            {publicationQuestionScreenshots?.length > 0 &&
                                publicationQuestionScreenshots.map((field: any, index: number) => (
                                    <div key={index} style={{ display: "flex", alignItems: "flex-start", marginBottom: "8px" }}>
                                        <img alt="" width={"200px"} src={typeof field !== "string" ? URL.createObjectURL(field) : field} />
                                        <CloseButton disabled={disable} onClick={() => removePublicationQuestionScreenshots(index)} />
                                    </div>
                                ))}
                        </div>

                        <Dropzone
                            style={{ display: "none" }}
                            openRef={imageRefQuestionScreenshots}
                            accept={["image/jpeg", "image/png"]}
                            onDrop={(files) => appendPublicationQuestionScreenshots(files[0])}
                            onReject={(files) => console.log("rejected image", files)}
                            maxSize={104857600} // 100mb limit
                            disabled={disable}
                        >
                            {() => null}
                        </Dropzone>

                        <AddButton disabled={disable} onClick={selectPublicationQuestionScreenshots}>
                            Add screenshot
                        </AddButton>
                    </Section>
                    <Section>
                        <SubTitle>Link to the publication</SubTitle>
                        {fieldsPublicationQuestionLinks.map((field, index) => (
                            <div key={field.id} style={{ display: "flex", alignItems: "center", marginBottom: "8px" }}>
                                {disable ? (
                                    <Input disabled={true} {...register(`publicationQuestionLinks.${index}`)} style={{ width: "350px", marginRight: "10px" }} />
                                ) : (
                                    <Input
                                        {...register(`publicationQuestionLinks.${index}`)}
                                        variant="unstyled"
                                        style={{
                                            marginRight: "10px",
                                            width: "350px",
                                            border: "1px solid #29CCB1",
                                            borderRadius: "4px",
                                            paddingLeft: "12px",
                                        }}
                                    />
                                )}
                                <RemoveButton disabled={disable} onClick={() => removePublicationQuestionLinks(index)} />
                            </div>
                        ))}
                        <AddButton disabled={disable} onClick={() => appendPublicationQuestionLinks("")}>
                            Add more
                        </AddButton>
                    </Section>

                    <Section>
                        <SubTitle>Publication in question on Web archive</SubTitle>
                        <Description>
                            This is important. Go to{" "}
                            <a style={{ color: "blue", textDecoration: "underline" }} href="http://web.archive.org/" target={"_blank"} rel="noreferrer">
                                {" "}
                                web.archive.org
                            </a>
                            , paste the website URL and find a page with the publication. Search carefully, it’s chaotic there.
                        </Description>

                        {fieldsPublicationQuestionWebarchiveLinks.map((field, index) => (
                            <div key={field.id} style={{ display: "flex", alignItems: "center", marginBottom: "8px" }}>
                                {disable ? (
                                    <Input
                                        disabled={true}
                                        {...register(`publicationQuestionWebarchiveLinks.${index}`)}
                                        style={{ width: "350px", marginRight: "10px" }}
                                    />
                                ) : (
                                    <Input
                                        {...register(`publicationQuestionWebarchiveLinks.${index}`)}
                                        variant="unstyled"
                                        style={{
                                            marginRight: "10px",
                                            width: "350px",
                                            border: "1px solid #29CCB1",
                                            borderRadius: "4px",
                                            paddingLeft: "12px",
                                        }}
                                    />
                                )}
                                <RemoveButton disabled={disable} onClick={() => removePublicationQuestionWebarchiveLinks(index)} />
                            </div>
                        ))}
                        <AddButton disabled={disable} onClick={() => appendPublicationQuestionWebarchiveLinks("")}>
                            Add more
                        </AddButton>
                    </Section>
                    <Section>
                        <Checkbox
                            disabled={disable || fieldsPublicationQuestionWebarchiveLinks.length > 0}
                            {...register("isPublicationQuestionWebarchiveFailed")}
                            label={t("dispute.collecting_evidence.web_archive_failed")}
                        />
                    </Section>

                    <Section>
                        <Title>Your demands</Title>
                        <div>
                            <div style={{ display: "flex", alignItems: "center", marginBottom: "16px" }}>
                                <RadioButton disabled={disable} {...register("demands")} type="radio" value="Remove" />
                                <Label>Remove the content</Label>
                            </div>
                            <div>
                                <div style={{ display: "flex", alignItems: "center", marginBottom: "16px" }}>
                                    <RadioButton disabled={disable || disableDemands} {...register("demands")} type="radio" value="Pay and/or credit" />
                                    <Label className={ clsx([disableDemands && 'disable'])}>Pay and/or credit</Label>
                                </div>
                                {demands === "Pay and/or credit" && (
                                    <div style={{ marginLeft: "36px" }}>
                                        <Checkbox
                                            {...register("test1")}
                                            style={{ alignItems: "flex-start" }}
                                            label="Ask to acknowledge your authorship and include a source link"
                                            checked
                                            disabled
                                        />
                                        <Checkbox
                                            {...register("test2")}
                                            label="Ask to pay you for using the content"
                                            style={{ alignItems: "flex-start", marginTop: "16px" }}
                                            checked={!typeLicenseCC0}
                                            disabled
                                        />
                                        {typeLicenseCC0 &&
                                            <Description>
                                                {t("dispute.collecting_evidence.description_ask_to_pay")}
                                            </Description>
                                        }
                                       
                                        <Input
                                            disabled={disable}
                                            type={"number"}
                                            {...register("payAmount")}
                                            defaultValue={undefined}
                                            placeholder="Amount"
                                            styles={{ rightSection: { marginRight: "15px", color: "#666892" } }}
                                            style={{ width: "250px", margin: "8px 0px 8px 0px" }}
                                            rightSection={<Text>US$</Text>}
                                        />
                                        <Stack spacing={0}>
                                            <Group spacing={8}>
                                                <Text color="#666892" size="sm" underline>
                                                    How to set a fair price?
                                                </Text>
                                                <Icon icon={"mdi:launch"} color={"#666892"} hoverColor={"#666892"} />
                                            </Group>
                                            <Text color="#9a9cbd" size="xs">
                                                Coming Soon
                                            </Text>
                                        </Stack>
                                    </div>
                                )}
                            </div>
                            <div>
                                <div style={{ display: "flex", alignItems: "center", marginBottom: "16px" }}>
                                    <RadioButton disabled={disable || disableDemands} {...register("demands")} type="radio" value="Pay and remove" />
                                    <Label className={ clsx([disableDemands && 'disable'])}>Pay and remove</Label>
                                </div>
                                {demands === "Pay and remove" && (
                                    <div style={{ marginLeft: "36px" }}>
                                        <Input
                                            disabled={disable}
                                            type={"number"}
                                            {...register("payAmount")}
                                            placeholder="Amount"
                                            styles={{ rightSection: { marginRight: "15px", color: "#666892" } }}
                                            style={{ width: "250px", margin: "8px 0px 8px 0px" }}
                                            rightSection={<Text>US$</Text>}
                                        />
                                        <Stack spacing={0}>
                                            <Group spacing={8}>
                                                <Text color="#666892" size="sm" underline>
                                                    How to set a fair price?
                                                </Text>
                                                <Icon icon={"mdi:launch"} color={"#666892"} hoverColor={"#666892"} />
                                            </Group>
                                            <Text color="#9a9cbd" size="xs">
                                                Coming Soon
                                            </Text>
                                        </Stack>
                                    </div>
                                )}
                            </div>
                            <div style={{ display: "flex", alignItems: "center", marginBottom: "16px" }}>
                                <RadioButton disabled={disable || disableDemands} {...register("demands")} type="radio" value="Pay and attribute" />
                                <Label className={ clsx([disableDemands && 'disable'])}>Pay and attribute</Label>
                            </div>
                            {disableDemands &&
                                <Description>
                                    {t("dispute.collecting_evidence.demands_description")}
                                </Description>
                            }
                        </div>
                    </Section>

                    <Section>
                        <Checkbox disabled={disable} {...register("test4")} label="Make conditions default for this document" />
                    </Section>
                    {changeDemandsCC()}

                    <Section>
                        <Title>Your proofs</Title>
                        <SubTitle>certificate of authorship</SubTitle>
                        <Input value={window.location.origin + `/certificate/${token_Id}`} disabled={true}></Input>
                    </Section>

                    <Section>
                        <SubTitle>link to your publication </SubTitle>
                        {fieldsPublicationLinks.map((field, index) => (
                            <div key={field.id} style={{ display: "flex", alignItems: "center", marginBottom: "8px" }}>
                                {disable ? (
                                    <Input disabled {...register(`publicationLinks.${index}`)} style={{ width: "350px", marginRight: "10px" }} />
                                ) : (
                                    <Input
                                        disabled={disable}
                                        {...register(`publicationLinks.${index}`)}
                                        variant="unstyled"
                                        style={{
                                            marginRight: "10px",
                                            width: "350px",
                                            border: "1px solid #29CCB1",
                                            borderRadius: "4px",
                                            paddingLeft: "12px",
                                        }}
                                    />
                                )}
                                <RemoveButton disabled={disable} onClick={() => removePublicationLinks(index)} />
                            </div>
                        ))}
                        <AddButton disabled={disable} onClick={() => appendPublicationLinks("")}>
                            Add more
                        </AddButton>
                    </Section>

                    <Section>
                        <SubTitle>Link to your publication on Web archive</SubTitle>
                        <Description>
                            Again, go to{" "}
                            <a style={{ color: "blue", textDecoration: "underline" }} href="http://web.archive.org/" target={"_blank"} rel="noreferrer">
                                {" "}
                                web.archive.org
                            </a>
                            , paste your website URL and find the page with your publication
                        </Description>
                        {fieldsPublicationWebarchiveLinks.map((field, index) => (
                            <div key={field.id} style={{ display: "flex", alignItems: "center", marginBottom: "8px" }}>
                                {disable ? (
                                    <Input disabled {...register(`publicationWebarchiveLinks.${index}`)} style={{ width: "350px", marginRight: "10px" }} />
                                ) : (
                                    <Input
                                        disabled={disable}
                                        {...register(`publicationWebarchiveLinks.${index}`)}
                                        variant="unstyled"
                                        style={{
                                            marginRight: "10px",
                                            width: "350px",
                                            border: "1px solid #29CCB1",
                                            borderRadius: "4px",
                                            paddingLeft: "12px",
                                        }}
                                    />
                                )}
                                <RemoveButton disabled={disable} onClick={() => removePublicationWebarchiveLinks(index)} />
                            </div>
                        ))}
                        <AddButton disabled={disable} onClick={() => appendPublicationWebarchiveLinks("")}>
                            Add more
                        </AddButton>
                    </Section>

                    <Section>
                        <Checkbox
                            disabled={disable || fieldsPublicationWebarchiveLinks.length > 0}
                            {...register("isPublicationWebarchiveFailed")}
                            label={t("dispute.collecting_evidence.web_archive_failed")}
                        />
                    </Section>

                    <Section>
                        <SubTitle>Screenshots of your drafts (optional)</SubTitle>
                        <Description>
                            Should you have any proof that you worked on this content, capture and add it below. It can be the history of edits in your Google
                            doc, paper drafts, etc.
                        </Description>

                        <div style={{ display: "flex" }}>
                            {draftsScreenshots?.length > 0 &&
                                draftsScreenshots.map((field: any, index: number) => (
                                    <div key={index} style={{ display: "flex", alignItems: "flex-start", marginBottom: "8px" }}>
                                        <img alt="" width={"200px"} src={typeof field !== "string" ? URL.createObjectURL(field) : field} />
                                        <CloseButton disabled={disable} onClick={() => removeDraftsScreenshots(index)} />
                                    </div>
                                ))}
                        </div>

                        <Dropzone
                            disabled={disable}
                            style={{ display: "none" }}
                            openRef={imageRefDraftsScreenshots}
                            accept={["image/jpeg", "image/png"]}
                            onDrop={(files) => appendDraftsScreenshots(files[0])}
                            onReject={(files) => console.log("rejected image", files)}
                            maxSize={104857600} // 100mb limit
                        >
                            {() => null}
                        </Dropzone>

                        <AddButton disabled={disable} onClick={selectDraftsScreenshots}>
                            Add screenshot
                        </AddButton>
                    </Section>
                </Wrapper>

                <Footer>
                    <Group>
                        <Text size={"sm"} weight={"400"} color={"#666892"}>
                            Document {getValues("documentName")}
                        </Text>
                    </Group>
                    <Group spacing={12}>
                        <Group spacing={8}></Group>
                        <SharkButton
                            onClick={() => navigate(window.location.pathname.replace(/[^/]*$/, "generating-claim"), { replace: true })}
                            variant="filled"
                            buttonType="secondary"
                            size="small"
                            px={8}
                            py={4}
                        >
                            Generate claim & proceed
                        </SharkButton>
                    </Group>
                </Footer>
            </Root>
        </>
    );
};

export default DisputesCollectingEvidence;

const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
`;

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 383px;
    margin: 32px 32px 72px 48px;
`;

const Section = styled.div`
    display: flex;
    flex-direction: column;
    margin-top: 16px;
`;

const Title = styled.span`
    font-weight: 700;
    font-size: 20px;
    line-height: 24px;
    margin-bottom: 16px;
`;

const SubTitle = styled.span`
    font-weight: 700;
    font-size: 12px;
    line-height: 16px;
    text-transform: uppercase;
    margin-bottom: 6px;
`;

const Description = styled.span`
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;
    color: #666892;
    margin-bottom: 8px;
`;

const Label = styled.span`
    font-weight: 500;
    font-size: 14px;
    line-height: 20px;
    margin-left: 8px;
    &.disable {
        color: #d4d4d4;
    }
`;

const Footer = styled(Group)`
    position: relative;
    background-color: #ffffff;
    border-top: 1px solid #dddeee;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
    padding: 14px 16px;
    height: 56px;
    overflow: hidden;
    justify-content: space-between;
`;
