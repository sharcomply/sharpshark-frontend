import styled from "@emotion/styled";
import Icon from "../../../components/Icon";
import { Group } from "@mantine/core";

interface Props {
    onClick?: (index?: number) => void;
    disabled?: boolean;
}

const RemoveButton = ({ onClick, disabled }: Props) => {
    return (
        <>
            <Button disabled={disabled} onClick={onClick}>
                <Group spacing={10}>
                    <Icon icon={"ic:round-remove-circle"} color={disabled ? "#DADADA" : "#e50c54"} width="20px" height="20px" />
                </Group>
            </Button>
        </>
    );
};

export default RemoveButton;

const Button = styled.button<any>`
    width: 20px;
    height: 20px;
    background-image: url(${(props) => props.img});
`;
