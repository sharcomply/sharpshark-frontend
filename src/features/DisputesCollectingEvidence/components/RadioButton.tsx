import styled from "@emotion/styled";

const RadioButton = styled.input`
    display: flex;
    align-items: center;
    justify-content: center;
    margin-right: 7px;
    position: relative;
    border-radius: 50%;
    width: 20px;
    height: 20px;
    border: 2px solid #979cbc;
    padding: 6px;
    box-shadow: inset 0 0 1px rgb(0 0 0 / 10%);

    &:checked {
        border: 2px solid #4700e5;

        &::before {
            content: "";
            position: absolute;
            border-radius: 50%;
            width: 10px;
            height: 10px;
            background-color: #4700e5;
        }
    };
    &:disabled {
        background-color: #ededed;
    }
`;

export default RadioButton;
