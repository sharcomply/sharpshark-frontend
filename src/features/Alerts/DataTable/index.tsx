import { Grid } from "@mantine/core";
import { Table, TableContainer, TableHead, TableRow, TableData, Text, Title, DateRow } from "components/DataTable";
import { formatDate, RowType } from "components/DataTable/shared";
import { ReactNode, useEffect, useState } from "react";

// Props
interface DataTableProps {
    data: Array<any>;
}

// Component
export default function AlertsTable(props: DataTableProps) {
    const currentDate = new Date();
    const currentDay = currentDate.getDay();
    const currentMonth = currentDate.getMonth();

    const [todayList, setTodayList] = useState<Array<any>>([]);
    const [thisWeekList, setThisWeekList] = useState<Array<any>>([]);
    const [earlierThisMonthList, setEarlierThisMonthList] = useState<Array<any>>([]);
    const [lastMonthList, setLastMonthList] = useState<Array<any>>([]);
    const [earlierList, setEarlierList] = useState<Array<any>>([]);

    const compareRows = (a: any, b: any): number => {
        const date1 = new Date(a.appearedDate);
        const date2 = new Date(b.appearedDate);
        if (date1 < date2) {
            return 1;
        }
        if (date1 > date2) {
            return -1;
        }
        return 0;
    };

    useEffect(() => {
        const today = [];
        const thisWeek = [];
        const earlierThisMonth = [];
        const lastMonth = [];
        const earlier = [];

        props.data.sort(compareRows);

        for (let i = 0; i < props.data.length; ++i) {
            const checkedDate = new Date(props.data[i].appearedDate);
            const checkedDay = checkedDate.getDay();
            const checkedMonth = checkedDate.getMonth();

            if (checkedMonth === currentMonth - 1) {
                lastMonth.push(props.data[i]);
                continue;
            }
            if (checkedMonth === currentMonth && checkedDay === currentDay) {
                today.push(props.data[i]);
                continue;
            }
            if (checkedMonth === currentMonth && currentDay - checkedDay <= 7) {
                thisWeek.push(props.data[i]);
                continue;
            }
            if (checkedMonth === currentMonth) {
                earlierThisMonth.push(props.data[i]);
            }

            earlier.push(props.data[i]);
        }
        setTodayList(today);
        setThisWeekList(thisWeek);
        setEarlierThisMonthList(earlierThisMonth);
        setLastMonthList(lastMonth);
        setEarlierList(earlier);
    }, [props.data, currentDay, currentMonth]);

    const renderRows = (type: RowType): ReactNode => {
        let rowList = [];
        let header = "";

        switch (type) {
            case RowType.Today:
                rowList = todayList;
                header = "TODAY";
                break;
            case RowType.ThisWeek:
                rowList = thisWeekList;
                header = "THIS WEEK";
                break;
            case RowType.LastMonth:
                rowList = lastMonthList;
                header = "LAST MONTH";
                break;
            case RowType.EarlierThisMonth:
                rowList = earlierThisMonthList;
                header = "EARLIER THIS MONTH";
                break;
            case RowType.Earlier:
                rowList = earlierList;
                header = "EARLIER";
                break;
        }

        if (rowList.length === 0) {
            return;
        }

        return (
            <>
                <DateRow>
                    <td></td>
                    <td>{header}</td>
                    <td></td>
                </DateRow>
                {rowList.map((row, index) => {
                    return (
                        <TableRow key={index}>
                            <TableData>
                                <Text>
                                    <Title>{row.title}</Title>
                                    {row.text}
                                </Text>
                            </TableData>
                            <TableData>{formatDate(row.appearedDate)}</TableData>
                            <TableData>
                                <Grid justify="space-between" align="center">
                                    {formatDate(row.discoveredDate)}
                                </Grid>
                            </TableData>
                        </TableRow>
                    );
                })}{" "}
            </>
        );
    };

    return (
        <TableContainer>
            <Table>
                <TableHead>
                    <tr>
                        <th>TITLE</th>
                        <th>APPEARED ON</th>
                        <th>DISCOVERED</th>
                    </tr>
                </TableHead>
                {props.data.length > 0 ? (
                    <tbody>
                        {renderRows(RowType.Today)}
                        {renderRows(RowType.ThisWeek)}
                        {renderRows(RowType.EarlierThisMonth)}
                        {renderRows(RowType.LastMonth)}
                        {renderRows(RowType.Earlier)}
                    </tbody>
                ) : (
                    <tbody>
                        <TableRow>
                            <TableData>
                                <Text>This is a blank page... Cases will appear here</Text>
                            </TableData>
                            <td></td>
                        </TableRow>
                    </tbody>
                )}
            </Table>
        </TableContainer>
    );
}
