import { useCallback } from "react";
import { useAppDispatch, useAppSelector } from "store";
import { protectedListActions, ProtectedListState } from "../store";

export function useProtectedListStore() {
    const state = useAppSelector((state) => state.protectedList);
    const dispatch = useAppDispatch()

    const setCount = useCallback((type: keyof ProtectedListState["counts"], value: number) => {
        const action = protectedListActions.setCount({ type, value });
        dispatch(action)
    }, [dispatch]);

    const clearStore = useCallback(() => {
        const action = protectedListActions.clear()
        dispatch(action)
    }, [dispatch])

    return {
        state,
        actions: {
            setCount,
            clearStore
        },
    };
}
