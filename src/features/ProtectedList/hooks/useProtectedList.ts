import { useQuery } from "react-query";
import { backend } from "api";

export default function useProtectedList() {
    const { data: protects, ...options } = useQuery(["PROTECTED_LIST"], () => backend.drafts.getDraftList({ isMinted: true }), {
        keepPreviousData: true,
    });

    return {
        protects,
        ...options,
    };
}
