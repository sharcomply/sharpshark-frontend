import { backend } from "api";
import { useDraftVersionStore } from "features/NewDraftVersion/hooks/useDraftVersionStore";
import { useCallback } from "react";
import { useNavigate } from "react-router-dom";

export const useProtectedDraft = () => {
    const { setParent, setInitialForm } = useDraftVersionStore();
    const navigate = useNavigate()

    const getLastParent = useCallback((draft: DraftDetails) => {
        if (!draft) return
        if (!draft.versionsHistory) return draft;

        const versionSize = Object.keys(draft.versionsHistory).length;
        const lastParentId = draft.versionsHistory[versionSize];

        return { ...draft, _id: lastParentId };
    }, []);

    const handleUpdate = useCallback(async (draft: DraftDetails) => {
        if (!draft) return
        const lastParent = getLastParent(draft)
        if (!lastParent) return
        console.log('update')
        setParent(lastParent);
        setInitialForm({
            title: draft.title,
            fullTitle: draft.metadata?.Title || "",
            type: draft.type || "text",
            text: draft.text || "",
            image: draft.imageFile,
            authors: draft.authors,
            coAuthors: draft.coAuthors,
            participants: draft.participants,
            additionalImages: draft.additionalImages

        });
    }, [
        setInitialForm,
        setParent,
    ]);

    const createNewVersion = useCallback(async (draftId: string) => {
        try {
            const draft = await backend.drafts.getDraft(draftId)
            await handleUpdate(draft)
            navigate("/protected/new")
        } catch (error) {
            console.log(error)
        }

    }, [navigate, handleUpdate])



    return {
        createNewVersion
    }
}