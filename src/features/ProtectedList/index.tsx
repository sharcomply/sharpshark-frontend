import { Stack, Text } from '@mantine/core';
import styled from '@emotion/styled';
import { TableHead } from '../../components/DataTable/TableHead';
import { Fragment } from 'react';
import { getListByDate } from 'helpers/getListByDate';
import { TableDate } from '../../components/DataTable/TableDate';
import { ProtectItem } from './components/ProtectItem';
import useProtectedList from './hooks/useProtectedList';
import { useTranslation } from 'react-i18next';

// Component
export default function ProtectedList() {
    const { protects } = useProtectedList()
    const {t} = useTranslation()
    const itemsByDate = getListByDate(protects?.items || []);
    
    return (
        <Root>
            <Table>
                <TableHead items={[
                    { title: t("table.header-title"), width: '50%' },
                    { title: t("table.header-updated"), width: '20%' },
                    { title: t("table.header-protected"), width: '20%' },
                    { title: '', width: '10%' }
                ]}/>
                
                <tbody>
                {!!protects?.items.length && (
                    <Fragment>
                            {renderListByDate(t("table.row-by-date.today"), itemsByDate.today)}
                            {renderListByDate(t("table.row-by-date.this-week"), itemsByDate.week)}
                            {renderListByDate(t("table.row-by-date.last-month"), itemsByDate.lastMonth)}
                            {renderListByDate(t("table.row-by-date.earlier-this-month"), itemsByDate.month)}
                            {renderListByDate(t("table.row-by-date.earlier"), itemsByDate.earlier)}
                    </Fragment>
                )}
                
                {!protects?.items.length && (
                    <tr style={{ borderTop: '1px solid #DDDEEE' }}>
                        <td style={{ padding: 16 }}>
                            <Text size={'sm'} color={'#666892'} weight={'400'}>{t("table.empty-table")}</Text>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                )}
                </tbody>
            </Table>
        </Root>
    );
}

// List By Date
function renderListByDate(title: string, items: Draft[]) {
    if (!items.length) return null;

    return (
        <Fragment>
            <TableDate title={title} cols={4} titlePos={3}/>
            {items.map((item) => {
                return <ProtectItem key={item._id} item={item} />;
            })}
        </Fragment>
    );
}

// Styling
const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
`;

export const Table = styled.table`
    width: 100%;
    table-layout: fixed;
    border-collapse: collapse;
    overflow-wrap: anywhere;
`;
