// Props
import { Group, Popover, Text } from "@mantine/core";
import Icon from "../../../components/Icon";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { useProtectedDraft } from "../hooks/useProtectedDraft";

// Variables
const rootStyles = {
    background: "#FFFFFF",
    border: "1px solid #DDDEEE",
    boxShadow: " 0px 2px 12px rgba(0, 0, 0, 0.1)",
    radius: 4,
    overflow: "hidden",
};

type ComponentProps = {
    protectId: string;
    tokenId?: string;
};

// Component
export function ActionMenu({ protectId , tokenId}: ComponentProps) {
    const [opened, setOpened] = useState(false);
    const { t } = useTranslation();

    const { createNewVersion } = useProtectedDraft();

    const control = (
        <Group align={"center"} position={"center"} px={4} onClick={() => setOpened((value) => !value)}>
            <Icon icon={"ic:sharp-more-vert"} size={24} color={"#666892"} hoverColor={"#666892"} />
        </Group>
    );

    return (
        <Popover
            opened={opened}
            target={control}
            onClose={() => setOpened(false)}
            width={160}
            radius={4}
            position="bottom"
            placement="end"
            transition={"pop-top-right"}
            styles={{ body: rootStyles, inner: { padding: 0 } }}
        >
            <Link to={`/protected/${protectId}`}>
                <MenuItem px={16} py={12}>
                    <Text size={"sm"} weight={"500"}>
                        {t("buttons.view_document_btn")}
                    </Text>
                </MenuItem>
            </Link>

            <Link to={`/protected/${protectId}`} onClick={() => createNewVersion(protectId)}>
                <MenuItem px={16} py={12}>
                    <Text size={"sm"} weight={"500"}>
                        {t("buttons.add_new_version_btn")}
                    </Text>
                </MenuItem>
            </Link>

            <Link to={`/certificate/${tokenId}`}>
                <MenuItem px={16} py={12}>
                    <Text size={"sm"} weight={"500"}>
                        {t("buttons.view_certificate_btn")}
                    </Text>
                </MenuItem>
            </Link>
        </Popover>
    );
}

// Styling
const MenuItem = styled(Group)`
    cursor: pointer;
    color: #666892;

    &:hover {
        background-color: #4700e5;
        color: #ffffff;
    }

    &.disabled {
        color: #bbbed8;
    }

    &.disabled:hover {
        background-color: #f7f7fa;
        color: #8b8fad;
    }
`;

// const DeleteItem = styled(Group)`
//     cursor: pointer;
//     color: #E50C54;

//     &:hover {
//         background-color: #E50C54;
//         color: #FFFFFF;
//     }
// `;
