import { Group, Text } from "@mantine/core";
import { humanizeDate } from "helpers/humanizeDate";
import styled from "@emotion/styled";
import { ActionMenu } from "./ActionMenu";
import { Link } from "react-router-dom";

// Props
type ComponentProps = {
    item: Draft;
};

// Component
export const ProtectItem = ({ item }: ComponentProps) => {
    // const link = `/protected/${item.isMinted ? item._id : item.parentId ? item.parentId : item._id}`;
    const link = `/protected/${item.isMinted || !item.parentId ? item._id : item.parentId}`;

    return (
        <Root>
            <td>
                <Link to={link}>
                    <Group px={16} align="center" sx={{ height: 54 }}>
                        <Text
                            size={"sm"}
                            color={"#0A001E"}
                            weight={"500"}
                            style={{
                                overflow: "hidden",
                                textOverflow: "ellipsis",
                                whiteSpace: "nowrap",
                            }}
                        >
                            {item.title}
                        </Text>
                    </Group>
                </Link>
            </td>

            <td>
                <Link to={link}>
                    <Group px={16} align="center" sx={{ height: 54 }}>
                        <Text size={"sm"} color={"#666892"} weight={"500"}>
                            {humanizeDate(item.updatedAt).shortDate}
                        </Text>
                    </Group>
                </Link>
            </td>

            <td>
                {item.isMinted && (
                    <Link to={link}>
                        <Group px={16} align="center" sx={{ height: 54 }}>
                            <Text size={"sm"} color={"#666892"} weight={"500"}>
                                {humanizeDate(item.updatedAt).shortDate}
                            </Text>
                        </Group>
                    </Link>
                )}
            </td>

            <td>
                <Group px={16} position="right" align="center" sx={{ height: 54 }}>
                    <ActionMenu protectId={item._id} tokenId={item.tokenId} />
                </Group>
            </td>
        </Root>
    );
};

// Styling
const Root = styled.tr`
    transition: background-color 200ms linear;
    border-top: 1px solid #dddeee;

    &:hover {
        background-color: #edeeff;
    }
`;
