import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Types
export interface ProtectedListState {
    counts: {
        locked: number;
        unlocked: number;
    }
}

// Initial State
const initialState: ProtectedListState = {
    counts: {
        locked: 0,
        unlocked: 0
    }
};

// Slice
const protectedListSlice = createSlice({
    name: "protectedList",
    initialState,
    reducers: {
        clear: () => initialState,
        setCount: (state, action: PayloadAction<{ type: keyof ProtectedListState["counts"], value: number }>) => {
            state.counts[action.payload.type] = action.payload.value
        },
    },
});

export const protectedListReducers = protectedListSlice.reducer;
export const protectedListActions = protectedListSlice.actions;
