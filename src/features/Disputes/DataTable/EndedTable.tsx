import { Grid } from "@mantine/core";
import { Table, TableContainer, TableHead, TableRow, TableData, Text, Title, DateRow } from "components/DataTable";
import { ReactNode, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

enum RowType {
    Today,
    ThisWeek,
    EarlierThisMonth,
    LastMonth,
    Earlier,
}

// Props
interface DataTableProps {
    data: Array<any>;
}

// Component
export default function EndedTable(props: DataTableProps) {
    const currentDate = new Date();
    const currentDay = currentDate.getDay();
    const currentMonth = currentDate.getMonth();
    const {t} = useTranslation()

    const [todayList, setTodayList] = useState<Array<any>>([]);
    const [thisWeekList, setThisWeekList] = useState<Array<any>>([]);
    const [earlierThisMonthList, setEarlierThisMonthList] = useState<Array<any>>([]);
    const [lastMonthList, setLastMonthList] = useState<Array<any>>([]);
    const [earlierList, setEarlierList] = useState<Array<any>>([]);

    const compareRows = (a: any, b: any): number => {
        const date1 = new Date(a.appearedDate);
        const date2 = new Date(b.appearedDate);
        if (date1 < date2) {
            return 1;
        }
        if (date1 > date2) {
            return -1;
        }
        return 0;
    };

    useEffect(() => {
        const today = [];
        const thisWeek = [];
        const earlierThisMonth = [];
        const lastMonth = [];
        const earlier = [];

        props.data.sort(compareRows);

        for (let i = 0; i < props.data.length; ++i) {
            const checkedDate = new Date(props.data[i].appearedDate);
            const checkedDay = checkedDate.getDay();
            const checkedMonth = checkedDate.getMonth();

            if (checkedMonth === currentMonth - 1) {
                lastMonth.push(props.data[i]);
                continue;
            }
            if (checkedMonth === currentMonth && checkedDay === currentDay) {
                today.push(props.data[i]);
                continue;
            }
            if (checkedMonth === currentMonth && currentDay - checkedDay <= 7) {
                thisWeek.push(props.data[i]);
                continue;
            }
            if (checkedMonth === currentMonth) {
                earlierThisMonth.push(props.data[i]);
            }

            earlier.push(props.data[i]);
        }
        setTodayList(today);
        setThisWeekList(thisWeek);
        setEarlierThisMonthList(earlierThisMonth);
        setLastMonthList(lastMonth);
        setEarlierList(earlier);
    }, [props.data, currentDay, currentMonth]);

    const formatDate = (d: string): string => {
        const date = new Date(d);
        const months = [t("month.jan"), t("month.feb"), t("month.mar"), t("month.apr"), t("month.may"), t("month.jun"), t("month.jul"), t("month.aug"), t("month.sep"), t("month.oct"), t("month.nov"), t("month.dec")];
        const day = ("0" + date.getDate()).slice(-2);
        const month = months[date.getMonth()];
        const year = date.getFullYear();
        const hours = ("0" + date.getHours()).slice(-2);
        const minutes = ("0" + date.getMinutes()).slice(-2);
        const formatedDate = `${day} ${month} ${year}, ${hours}:${minutes}`;

        return formatedDate;
    };

    const renderRows = (type: RowType): ReactNode => {
        let rowList = [];
        let header = "";

        switch (type) {
            case RowType.Today:
                rowList = todayList;
                header = t("table.row-by-date.today");
                break;
            case RowType.ThisWeek:
                rowList = thisWeekList;
                header =  t("table.row-by-date.this-week");
                break;
            case RowType.LastMonth:
                rowList = lastMonthList;
                header = t("table.row-by-date.last-month");
                break;
            case RowType.EarlierThisMonth:
                rowList = earlierThisMonthList;
                header = t("table.row-by-date.earlier-this-month");
                break;
            case RowType.Earlier:
                rowList = earlierList;
                header = t("table.row-by-date.earlier");
                break;
        }

        if (rowList.length === 0) {
            return;
        }

        return (
            <>
                <DateRow>
                    <td></td>
                    <td>{header}</td>
                    <td></td>
                </DateRow>
                {rowList.map((row) => {
                    return (
                        <TableRow>
                            <TableData>
                                <Text>
                                    <Title>{row.title}</Title>
                                    {row.text}
                                </Text>
                            </TableData>
                            <TableData>{formatDate(row.appearedDate)}</TableData>
                            <TableData>{formatDate(row.discoveredDate)}</TableData>
                            <TableData>
                                <Grid justify="space-between" align="center">
                                    {formatDate(row.tableEndedDate)}
                                </Grid>
                            </TableData>
                        </TableRow>
                    );
                })}{" "}
            </>
        );
    };

    return (
        <TableContainer>
            <Table>
                <TableHead>
                    <tr>
                        <th>{t("table.titles.title")}</th>
                        <th>{t('table.titles.appeared_on')}</th>
                        <th>{t("table.titles.discovered")}</th>
                        <th>{t("table.titles.table_ended")}</th>
                    </tr>
                </TableHead>
                {props.data.length > 0 ? (
                    <tbody>
                        {renderRows(RowType.Today)}
                        {renderRows(RowType.ThisWeek)}
                        {renderRows(RowType.EarlierThisMonth)}
                        {renderRows(RowType.LastMonth)}
                        {renderRows(RowType.Earlier)}
                    </tbody>
                ) : (
                    <tbody>
                        <TableRow>
                            <TableData>
                                <Text>{t("table.empty-table")}</Text>
                            </TableData>
                            <td></td>
                        </TableRow>
                    </tbody>
                )}
            </Table>
        </TableContainer>
    );
}
