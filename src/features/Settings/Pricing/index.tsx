import styled from "@emotion/styled";
import { Box, Group, Stack, Text } from "@mantine/core";
import { useTranslation } from "react-i18next";
import { ComingSoonOverlay } from "../components/helpers";

export default function PricingTab() {
    const { t } = useTranslation();
    return (
        <Root>
            <ComingSoonOverlay />
            <Box px={32} pt={32} pb={48}>
                <Text weight={"bold"} size={"xl"} sx={{ lineHeight: "24px" }}>
                    {t("settings.pricing.title")}
                </Text>
                <Text color={"#FF00FF"} sx={{ lineHeight: "20px" }} mt={8} dangerouslySetInnerHTML={{__html: t("settings.pricing.description")}}/>
                <Box sx={{ width: "350px" }} mt={32}>
                    <Group position={"apart"}>
                        <Text color={"#FF00FF"} sx={{ lineHeight: "20px" }} mt={8}>
                            {t("common.something")}
                        </Text>
                        <Text>$54</Text>
                    </Group>
                    <Group position={"apart"}>
                        <Text color={"#FF00FF"} sx={{ lineHeight: "20px" }} mt={8}>
                            {t("common.something")}
                        </Text>
                        <Text>$54</Text>
                    </Group>
                    <Group position={"apart"}>
                        <Text color={"#FF00FF"} sx={{ lineHeight: "20px" }} mt={8}>
                            {t("common.something")}
                        </Text>
                        <Text>$54</Text>
                    </Group>
                    <Group position={"apart"}>
                        <Text color={"#FF00FF"} sx={{ lineHeight: "20px" }} mt={8}>
                            {t("common.something")}
                        </Text>
                        <Text>$54</Text>
                    </Group>
                </Box>
            </Box>
        </Root>
    );
}

const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    height: 492px;
    position: relative;
`;
