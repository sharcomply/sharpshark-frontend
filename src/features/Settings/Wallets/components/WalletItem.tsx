import styled from "@emotion/styled";
import { Group, Text } from "@mantine/core";
import Icon from "components/Icon";
import { useTranslation } from "react-i18next";

type ComponentsProps = {
    isActive?: boolean;
};

export const WalletItem = ({ isActive }: ComponentsProps) => {
    const {t} = useTranslation()

    return (
        <Root>
            <td style={{ paddingLeft: "16px" }}>
                <Group noWrap align={"center"} sx={{ gap: "14px" }}>
                    <Icon icon={"ic:round-open-in-new"} color={"#666892"} hoverColor={"#DADADA"} size={20} />
                    <Text
                        weight={500}
                        color={"#0A001E"}
                        size={"sm"}
                        sx={{ textOverflow: "ellipsis", maxWidth: "90%", whiteSpace: "nowrap", overflow: "hidden" }}
                    >
                        0xC580880A...f24Bb40d3490
                    </Text>
                </Group>
            </td>
            <td style={{ paddingLeft: "16px" }}>
                <Text color={"#666892"} size={"sm"}>
                    10 Jun 2022
                </Text>
            </td>
            <td style={{ paddingLeft: "16px" }}>
                <Text color={"#666892"} size={"sm"}>
                    3 Aug 17:45
                </Text>
            </td>
            <td>
                {isActive && (
                    <Group position={"right"}>
                        <Group position={"center"} px={12} sx={{ borderRadius: 100, backgroundColor: "#29CCB1", height: 30, width: "fit-content" }}>
                            <Text weight={500} color={"#0A001E"} size={"xs"} transform={"uppercase"} sx={{ whiteSpace: "nowrap" }}>
                                {t("buttons.active_btn")}
                            </Text>
                        </Group>
                    </Group>
                )}
            </td>
            <td style={{ padding: "0 31px 0 14px" }}>
                {isActive ? (
                    <Text size={"sm"} color={"#E50C54"} weight={500} align={"right"} pr={8}>
                        x {t("buttons.disconnect_btn")}
                    </Text>
                ) : (
                    <Group position={"right"}>
                        <SwitchButton>
                            <Text color={"white"} size={"sm"} sx={{ lineHeight: "20px" }}>
                                {t("buttons.switch_too_btn")}
                            </Text>
                        </SwitchButton>
                    </Group>
                )}
            </td>
        </Root>
    );
};

const Root = styled.tr`
    transition: background-color 200ms linear;
    border-top: 1px solid #dddeee;
    height: 54px;

    &:hover {
        background-color: #edeeff;
    }

    & td {
        padding-top: 12px;
        padding-bottom: 12px;
    }
`;

const SwitchButton = styled.button`
    padding: 5px 8px;
    background-color: #4700e5;
    transition: background-color 200ms linear;
    box-shadow: 0px 4px 12px rgba(55, 81, 255, 0.24);
    border-radius: 4px;
    cursor: pointer;

    &:hover {
        background-color: #1c00a6;
    }
`;
