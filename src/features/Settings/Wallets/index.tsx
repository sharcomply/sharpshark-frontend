import styled from "@emotion/styled";
import { Group, Stack, Text } from "@mantine/core";
import { TableHead } from "components/DataTable/TableHead";
import { WalletItem } from "./components/WalletItem";
import { ComingSoonOverlay } from "../components/helpers";
import { useTranslation } from "react-i18next";

export default function WalletsTab() {
    const {t} = useTranslation()
    return (
        <Root justify={"space-between"}>
            <ComingSoonOverlay />
            <Table>
                {" "}
                <TableHead
                    items={[
                        { title: t("table.titles.wallet_address"), width: "30%" },
                        { title: t("table.titles.connected_on"), width: "18%" },
                        { title: t("table.titles.last_active"), width: "18%" },
                        { title: "", width: "20%" },
                        { title: "", width: "20%" },
                    ]}
                />
                <tbody>
                    <WalletItem isActive />
                    <WalletItem />
                    <WalletItem />
                    <WalletItem />
                </tbody>
            </Table>
            <Group px={16} py={13} position={"right"} sx={{ borderTop: "1px solid #DDDEEE" }}>
                <Group px={8} py={5} sx={{ width: "fit-content", border: "1px solid #DDDEEE", borderRadius: "4px" }}>
                    <Text weight={500} color={"#666892"} size={"sm"}>
                        {t("buttons.connect_another_wallet")}
                    </Text>
                </Group>
            </Group>
        </Root>
    );
}

const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    position: relative;
    height: 485px;
`;

const Table = styled.table`
    width: 100%;
    table-layout: fixed;
    border-collapse: collapse;
    overflow-wrap: anywhere;
    max-height: 429px;
`;
