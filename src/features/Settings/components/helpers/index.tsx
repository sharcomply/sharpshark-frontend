import { Group, Overlay as MantineOverlay, Text } from '@mantine/core';
import { useTranslation } from 'react-i18next';

export const ComingSoonOverlay = () => {
    const {t} = useTranslation()
    return (
        <Group position={"center"} align={"center"} sx={{ position: "absolute", height: "100%", width: "100%" }}>
            <Text weight={500} sx={{ zIndex: 1 }}>{t("common.coming_soon")}</Text>
            <MantineOverlay opacity={0.6} blur={4} zIndex={0} sx={{ height: "100%" }} color={"#E5E6FF"}></MantineOverlay>
        </Group>
    );
};
