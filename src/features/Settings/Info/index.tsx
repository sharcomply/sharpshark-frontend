import styled from "@emotion/styled";
import { Box, Input, Select, Stack, Text } from "@mantine/core";
import Icon from "components/Icon";
import { SharkButton } from "components/SharkButton";
import useAuthStore from "features/Auth/hooks/useAuthStore";
import { useModal } from "features/Modals/hooks/useModal";
import { ModalContent, ModalType } from "features/Modals/interfaces";
import { getLanguage, setLanguage } from "helpers/localizationLanguageStorage";
import { useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import useSettingStore from "../hooks/useSettingStore";
import { useLocales } from "../../../hooks/useLocales";
import { useWhoAmi } from "../../Auth/hooks/useWhoAmi";
import { useUnmount } from "../../../hooks/useUnmount";
import { useDraftAlert } from "../../DraftDetails/hooks/useDraftAlert";
import { useNearContext } from "../../../providers/NearProvider";
// import Section from "../../components/Section";
import useDraftStore from "../../DraftDetails/hooks/useDraftStore";
import { NEAR_CONTRACT } from "../../../constants/providers";
import { changeMethods, viewMethods } from "../../../constants/NearContract";
import { useCrossTabState } from "hooks/useCrossTbState";
import { changeNearAddressRequest } from "../../../api/requests/auth";

export default function InfoTab() {
    const { handleOpen } = useModal();
    const { i18n, t } = useTranslation();
    const { user } = useAuthStore();
    const { email, setEmailAddress } = useSettingStore();
    const { data } = useWhoAmi();
    const alert = useDraftAlert();
    const { localesData } = useLocales();
    const Near = useNearContext();
    const { canMint, overlay } = useDraftStore();
    const [nearConnect, setNearConnect] = useCrossTabState("nearConnect", "");
    const [loading, setLoading] = useState(false);

    const changeLanguage = useCallback(
        (language: any) => {
            console.log(language);
            setLanguage(language);
            i18n.changeLanguage(language);
        },
        [i18n]
    );
    useUnmount(() => {
        alert.actions.close();
    });

    useEffect(() => {
        setEmailAddress({ address: user?.email, isEmailVerified: user?.isEmailVerified });

        if (nearConnect === "true") {
            window.location.reload();
            setNearConnect(false);
        }

        if (data?.user.walletAddressNear === "" && Near?.WalletConnection.account().accountId) {
            changeNearAddressRequest({ walletAddressNear: Near?.WalletConnection.account().accountId });
        }
    }, [setEmailAddress, user, setNearConnect, nearConnect, Near?.WalletConnection, data?.user.walletAddressNear]);

    const NearLogin = useCallback(async () => {
        try {
            Near?.WalletConnection.requestSignIn({
                contractId: NEAR_CONTRACT,
                methodNames: [...viewMethods, ...changeMethods],
                successUrl: `${window.location.href}?nearConnect=true`,
            });
        } catch (e) {
            console.log(e);
        } finally {
            setLoading(false);
        }
    }, [Near]);

    const handleNearLogout = useCallback(async () => {
        try {
            setLoading(true);

            await Near?.WalletConnection.signOut();
            localStorage.setItem("NETWORK_ID", window.ethereum.networkVersion);

            await changeNearAddressRequest({ walletAddressNear: "" });

            window.location.reload();
        } catch (e) {
            console.log(e);
        } finally {
            setLoading(false);
        }
    }, [Near]);

    return (
        <Root>
            <Box>
                <Text weight={"bold"} size={"xl"} sx={{ lineHeight: "24px" }}>
                    {t("settings.info.title")}
                </Text>
                <Text size={"sm"} color={"#666892"} sx={{ lineHeight: "20px" }} mt={8}>
                    {t("settings.info.description")}
                </Text>
            </Box>

            <Box>
                <Text transform={"uppercase"} weight={"bold"} size={"xs"} mb={3}>
                    {t("settings.info.language_title")}
                </Text>
                <Select
                    styles={{
                        input: {
                            padding: "11px 12px",
                        },
                        selected: {
                            background: "transparent",
                        },
                        rightSection: {
                            pointerEvents: "none",
                        },
                    }}
                    sx={{ width: "250px" }}
                    value={getLanguage() || "en"}
                    placeholder="English"
                    onChange={(e) => changeLanguage(e)}
                    data={localesData}
                    rightSection={<Icon icon={"ic:round-arrow-drop-down"} size={20} color={"#666892"} hoverColor={"#666892"} />}
                />
            </Box>

            <Box>
                <Text transform={"uppercase"} weight={"bold"} size={"xs"} mb={3}>
                    {t("settings.info.email_title")}
                </Text>

                {!user?.isEmailVerified && user?.email && (
                    <Text color={"red"} size={"sm"} weight={"600"}>
                        Email not verified. Verification email sent to email
                    </Text>
                )}

                <Input value={email.address} sx={{ width: "250px" }} mb={16} />
                <div style={{ display: "flex" }}>
                    <SharkButton variant="outlined" buttonType="secondary" px={5} py={8}>
                        <Text size={"sm"} weight={"600"} onClick={() => handleOpen({ type: ModalType.SETTINGS, content: ModalContent.ChangeEmail })}>
                            {email.address ? t("buttons.change_email_btn") : t("buttons.add_email_btn")}
                        </Text>
                    </SharkButton>
                </div>
            </Box>

            {Near?.WalletConnection.isSignedIn() ? (
                <Box>
                    <Text transform={"uppercase"} weight={"bold"} size={"xs"} mb={3}>
                        Linked near wallet
                    </Text>

                    <Input value={Near?.WalletConnection.account().accountId} disabled sx={{ width: "250px" }} mb={16} />

                    <SharkButton
                        onClick={handleNearLogout}
                        disabled={canMint || overlay.show || loading}
                        variant={"filled"}
                        buttonType="primary"
                        width={"190px"}
                    >
                        Disconnect Near
                    </SharkButton>
                </Box>
            ) : (
                <Box>
                    <Text transform={"uppercase"} weight={"bold"} size={"xs"} mb={3}>
                        Near
                    </Text>
                    <SharkButton onClick={NearLogin} disabled={canMint || overlay.show || loading} variant={"filled"} buttonType="primary" width={"190px"}>
                        Connect Near
                    </SharkButton>
                </Box>
            )}

            <Box>
                <Text transform={"uppercase"} weight={"bold"} size={"xs"} mb={3}>
                    {t("settings.info.manager_title")}
                </Text>
                <Text sx={{ lineHeight: "20px" }}>{t("settings.info.manager_description")}</Text>
                <Input placeholder={"0x00000..."} sx={{ width: "250px" }} mt={6} />
                <ConnectButton>{t("buttons.manager_btn")}</ConnectButton>
            </Box>
            <Box>
                <SharkButton variant="outlined" buttonType="secondary" px={5} py={8}>
                    <Text size={"sm"} weight={"600"} onClick={() => handleOpen({ type: ModalType.AUTH, content: ModalContent.KYCVerification })}>
                        {t("buttons.pass_kyc_btn")}
                    </Text>
                </SharkButton>
            </Box>
        </Root>
    );
}

const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    padding: 32px 32px 32px 48px;
    gap: 32px;
`;

const Button = styled.button`
    border: 1px solid #dddeee;
    border-radius: 4px;
    padding: 5px 8px;
    line-height: 20px;
    color: #666892;
    cursor: pointer;
`;

const ConnectButton = styled(Button)`
    color: white;
    background: #b8bde0;
    border: 1px solid #b8bde0;
    margin-top: 16px;
`;


