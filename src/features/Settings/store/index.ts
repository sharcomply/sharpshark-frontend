import {createSlice, PayloadAction} from "@reduxjs/toolkit";

// Types

export interface Setting {
    email: {
        address?: string;
        isEmailVerified?: boolean;
    },
    walletAddressNear?: string
}

// Initial State
const initialState: Setting = {
    email: {
        address: undefined,
        isEmailVerified: undefined
    },
    walletAddressNear: undefined
};

// Slice
const settingSlice = createSlice({
    name: "setting",
    initialState,
    reducers: {

        setEmailAddress: (state, action: PayloadAction<Setting["email"]>) => {
            state.email.address = action.payload?.address
            state.email.isEmailVerified = action.payload?.isEmailVerified
        },
        setNearAddress: (state, action: PayloadAction<Setting["walletAddressNear"]>) => {
          state.walletAddressNear = action.payload
        },

        clearStore: () => ({
            ...initialState
        }),
    },
});

export const settingReducers = settingSlice.reducer;
export const settingActions = settingSlice.actions;
