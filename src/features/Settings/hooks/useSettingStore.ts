import {useAppDispatch, useAppSelector} from "store";
import {useCallback} from "react";
import {Setting, settingActions} from "../store";


export default function useSettingStore() {
    const state = useAppSelector((state) => state.setting);
    const dispatch = useAppDispatch();

    const setEmailAddress = useCallback((address: Setting["email"]) => {
        dispatch(settingActions.setEmailAddress(address));
    }, [dispatch]);

    const setNearAddress = useCallback((address: Setting["walletAddressNear"]) => {
        console.log(address)
        dispatch(settingActions.setNearAddress(address))
    }, [])


    return {
        ...state,
        setEmailAddress,
        setNearAddress
    };
}
