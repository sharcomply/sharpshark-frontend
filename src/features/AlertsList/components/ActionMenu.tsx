// Props
import { Group, Popover, Text } from "@mantine/core";
import Icon from "../../../components/Icon";
import { useCallback, useState } from "react";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { closeAlert } from "../../../api/requests/alerts";
import useAlertNotActual from "../../AlertsNotActualList/hooks/useAlertList";
import { useAlertListStore } from "../hooks/useAlertListStore";

// Variables
const rootStyles = {
    background: "#FFFFFF",
    border: "1px solid #DDDEEE",
    boxShadow: " 0px 2px 12px rgba(0, 0, 0, 0.1)",
    radius: 4,
    overflow: "hidden",
};

type ComponentProps = {
    alertId: string;
    refetch: () => void;
    url: string;
};

// Component
export function ActionMenu({ alertId, refetch, url }: ComponentProps) {
    const [opened, setOpened] = useState(false);
    const {t} = useTranslation();
    const {refetch: refetchAlerts, alerts} = useAlertNotActual({isSkipped: true}, "ALERTS_NOT_ACTUAL_LIST");
    const {
        actions: {setCount},
    } = useAlertListStore();

    const onDelete = useCallback(
        async (id: string) => {
            if (!id) return;
            await closeAlert(id);
            refetchAlerts()
            setCount('notActual', alerts?.count!)
            refetch();
        },
        [refetch, closeAlert, refetchAlerts, setCount]
    );

    const control = (
        <Group align={"center"} position={"center"} px={4} onClick={() => setOpened((value) => !value)}>
            <Icon icon={"ic:sharp-more-vert"} size={24} color={"#666892"} hoverColor={"#666892"} />
        </Group>
    );

    return (
        <Popover
            opened={opened}
            target={control}
            onClose={() => setOpened(false)}
            width={160}
            radius={4}
            position="bottom"
            placement="end"
            transition={"pop-top-right"}
            styles={{ body: rootStyles, inner: { padding: 0 } }}
        >
            {url !== "Similar content not found" && url !== undefined && (
                <a href={url} target={"_blank"} rel="noreferrer">
                    <MenuItem px={16} py={12}>
                        <Text size={"sm"} weight={"500"}>
                            {t("See on the website")}
                        </Text>
                    </MenuItem>
                </a>
            )}

            <MenuItem px={16} py={12} onClick={() => onDelete(alertId)}>
                <Text size={"sm"} weight={"500"}>
                    Not relevant
                </Text>
            </MenuItem>

            {url !== "Similar content not found" && url !== undefined && (
                <Link to={`/disputes/create/${alertId}/collecting-evidence`}>
                    <MenuItem px={16} py={12}>
                        <Text size={"sm"} weight={"500"}>
                            React
                        </Text>
                    </MenuItem>
                </Link>
            )}
        </Popover>
    );
}

// Styling
const MenuItem = styled(Group)`
    cursor: pointer;
    color: #666892;

    &:hover {
        background-color: #4700e5;
        color: #ffffff;
    }

    &.disabled {
        color: #bbbed8;
    }

    &.disabled:hover {
        background-color: #f7f7fa;
        color: #8b8fad;
    }
`;

