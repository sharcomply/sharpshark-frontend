import { Group, Text } from "@mantine/core";
import { humanizeDate } from "helpers/humanizeDate";
import styled from "@emotion/styled";
import { ActionMenu } from "./ActionMenu";
import { SharkButton } from "components/SharkButton";
import { useCallback, useEffect, useState } from "react";
import { closeAlert } from "api/requests/alerts";
import { Icon } from "@iconify/react";
import ViewedStatus from "components/ViewedStatus";
import { useUpdateList } from "../hooks/useAlertList";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { useAlertListStore } from "../hooks/useAlertListStore";
import { useQueryClient } from "react-query";

interface SimilarContentProps extends SimilarContent {
    alertId: string;
    alertDate: string;
    isRead: boolean;
    isSimilarContent: boolean;
    tokenId: string;
}
// Props
type ComponentProps = {
    item: any[];
};

// Component
export const AlertItem = ({ item }: ComponentProps) => {
    const [opened, setOpen] = useState(false);
    const [isRead, setIsRead] = useState(true);
    const { refetch } = useUpdateList();
    const {
        actions: { setDisputPayload },
    } = useAlertListStore();
    const queryClient = useQueryClient()
    const [itemAlert, setItemAlert] = useState<SimilarContentProps[][]>([]);
    const [firstItemAlert, setFirstItemAlert] = useState<SimilarContentProps>();

    const onDelete = useCallback(
        async (id: string) => {
            if (!id) return;
            await closeAlert(id);
            refetch();
            queryClient.refetchQueries(['ALERTS_NOT_ACTUAL_LIST'])
        },
        [refetch, queryClient]
    );

    const { t } = useTranslation();

    useEffect(() => {
        item.map((a: Alert) => !a.isRead && setIsRead(false));
        return () => setIsRead(true);
    }, [item]);

    useEffect(() => {
        if (!item) return;
        setItemAlert(
            item.map((k: Alert) =>
                !!k.scanId.similarContent.length
                    ? k.scanId.similarContent.map((l: SimilarContent) =>
                          Object.assign(l, {
                              alertId: k._id,
                              alertDate: k.date,
                              isRead: k.isRead,
                              isSimilarContent: true,
                              tokenId: k.documentId.tokenId,
                              title: k.documentId.title,
                          })
                      )
                    : [
                          Object.assign({
                              alertId: k._id,
                              alertDate: k.date,
                              isRead: k.isRead,
                              title: k.documentId.title,
                              isSimilarContent: false,
                              url: "Similar content not found",
                              tokenId: k.documentId.tokenId,
                          }),
                      ]
            )
        );
    }, [item, setItemAlert]);

    useEffect(() => {
        setFirstItemAlert(itemAlert?.find(Boolean)!?.find(Boolean));
    }, [itemAlert, setFirstItemAlert]);

    const onSaveAlertParam = useCallback(
        (appearedOn: string) => {
            setDisputPayload({ appearedOn, alertId: item[0]._id });
        },
        [setDisputPayload, item]
    );

    if (!itemAlert.length || !firstItemAlert) return null;

    return (
        <>
            {itemAlert.length === 1 && (
                <>
                    <Root>
                        <td>
                            <Group direction={"row"} spacing={0} style={{ overflow: "hidden" }} noWrap={true}>
                                {!isRead && (
                                    <Group>
                                        <ViewedStatus isRead={false} />
                                    </Group>
                                )}

                                <Group px={16} align="center" style={{ overflow: "hidden", minWidth: "40%", maxWidth: "50%" }} sx={{ height: 54 }}>
                                    <Text
                                        size={"sm"}
                                        color={"#0A001E"}
                                        weight={"500"}
                                        style={{
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",
                                            whiteSpace: "nowrap",
                                            // width: "99%",
                                        }}
                                    >
                                        {firstItemAlert.title}
                                    </Text>
                                </Group>
                                <Group px={16} align="center" style={{ overflow: "hidden" }} sx={{ height: 54 }}>
                                    <Text
                                        size={"sm"}
                                        color={"#666892"}
                                        weight={"500"}
                                        style={{
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",
                                            whiteSpace: "nowrap",
                                            // width: "45%",
                                        }}
                                    >
                                        {firstItemAlert.introduction}
                                    </Text>
                                </Group>
                            </Group>
                        </td>

                        <td>
                            <a href={firstItemAlert.isSimilarContent ? firstItemAlert.url : "#"}>
                                <Group px={16} align="center" sx={{ height: 54 }}>
                                    <Text
                                        size={"sm"}
                                        color={"#666892"}
                                        weight={"500"}
                                        style={{
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        {firstItemAlert.url}
                                    </Text>
                                </Group>
                            </a>
                        </td>

                        <td>
                            <Group px={16} align="center" sx={{ height: 54 }}>
                                <Text size={"sm"} color={"#666892"} weight={"500"}>
                                    {humanizeDate(firstItemAlert.alertDate).shortDate}
                                </Text>
                            </Group>
                        </td>

                        <td>
                            {firstItemAlert.url !== "Similar content not found" && (
                                <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                    <Link to={`/disputes/create/${itemAlert[0][0].alertId}/collecting-evidence`}>
                                        <SharkButton
                                            variant={"filled"}
                                            buttonType="primary"
                                            onClick={() => onSaveAlertParam(firstItemAlert.url)}
                                            width={"100%"}
                                            style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                        >
                                            React...
                                        </SharkButton>
                                    </Link>
                                </Group>
                            )}
                        </td>

                        <td>
                            <Group px={16} position="center" align="center" sx={{ height: 54 }} onClick={() => onDelete(firstItemAlert.alertId)}>
                                <Text size={"sm"} color={"#4700E5"} weight={"500"} style={{ cursor: "pointer" }}>
                                    Skip
                                </Text>
                            </Group>
                        </td>

                        <td>
                            <Group px={16} position="right" align="center" sx={{ height: 54 }}>
                                <ActionMenu alertId={firstItemAlert.alertId} refetch={refetch} url={firstItemAlert.url} />
                            </Group>
                        </td>
                    </Root>
                    {itemAlert[0].slice(1).map((similarContent, index) => (
                        <Root key={index} className="children" style={{ border: "none" }}>
                            <td></td>
                            <td>
                                <Group direction={"row"} spacing={0} noWrap={true}>
                                    <Group align="center" sx={{ height: 54 }}>
                                        <ViewedStatus isRead={similarContent.isRead} onlyCircle={true} />
                                    </Group>
                                    <a href={similarContent.url} style={{ overflow: "hidden" }}>
                                        <Group px={16} align="center" sx={{ height: 54 }}>
                                            <Text
                                                size={"sm"}
                                                color={"#666892"}
                                                weight={"500"}
                                                style={{
                                                    overflow: "hidden",
                                                    textOverflow: "ellipsis",
                                                    whiteSpace: "nowrap",
                                                }}
                                            >
                                                {similarContent.url}
                                            </Text>
                                        </Group>
                                    </a>
                                </Group>
                            </td>
                            <td colSpan={4}></td>
                        </Root>
                    ))}
                </>
            )}
            {itemAlert.length > 1 && (
                <>
                    <Root>
                        <td colSpan={5}>
                            <Group direction={"row"} spacing={0} onClick={() => setOpen(!opened)} style={{ cursor: "pointer" }} noWrap={true}>
                                <Group px={16} align="center" sx={{ height: 54 }}>
                                    {opened && <Icon icon={`ic:round-keyboard-arrow-down`} rotate={"180deg"} color="#4b506d" />}
                                    {!opened && <Icon icon={`ic:round-keyboard-arrow-down`} color="#4b506d" />}
                                </Group>

                                <Group>
                                    <ViewedStatus isRead={isRead} length={itemAlert.length} />
                                </Group>

                                <Group px={16} align="center" style={{ overflow: "hidden", minWidth: "10%", maxWidth: "50%" }} sx={{ height: 54 }}>
                                    <Text
                                        size={"sm"}
                                        color={"#0A001E"}
                                        weight={"500"}
                                        style={{
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",
                                            whiteSpace: "nowrap",
                                            // width: "99%",
                                        }}
                                    >
                                        {firstItemAlert.title}
                                    </Text>
                                </Group>

                                <Group px={16} align="center" style={{ overflow: "hidden" }} sx={{ height: 54 }}>
                                    <Text
                                        size={"sm"}
                                        color={"#666892"}
                                        weight={"500"}
                                        style={{
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",
                                            whiteSpace: "nowrap",
                                            // width: "90%",
                                        }}
                                    >
                                        {firstItemAlert.introduction}
                                    </Text>
                                </Group>
                            </Group>
                        </td>
                        <td>
                            <Group px={16} position="right" align="center" sx={{ height: 54 }}>
                                <ActionMenu alertId={firstItemAlert.alertId} refetch={refetch} url={firstItemAlert.url} />
                            </Group>
                        </td>
                    </Root>

                    {opened &&
                        itemAlert.map((item, index) => (
                            <>
                                {item.length === 1 &&
                                    item.map((alert) => (
                                        <Root key={index}>
                                            <td colSpan={2}>
                                                <Group direction={"row"} noWrap={true}>
                                                    <Group align="center" pl={alert.isRead ? 0 : 86} sx={{ height: 54 }}>
                                                        <ViewedStatus isRead={alert.isRead} onlyCircle={true} />
                                                    </Group>
                                                    <a href={firstItemAlert.isSimilarContent ? firstItemAlert.url : "#"} style={{ overflow: "hidden" }}>
                                                        <Group align="center" pl={alert.isRead ? 113 : 0} sx={{ height: 54 }}>
                                                            <Text
                                                                size={"sm"}
                                                                color={"#666892"}
                                                                weight={"500"}
                                                                style={{
                                                                    overflow: "hidden",
                                                                    textOverflow: "ellipsis",
                                                                    whiteSpace: "nowrap",
                                                                }}
                                                            >
                                                                {alert.url}
                                                            </Text>
                                                        </Group>
                                                    </a>
                                                </Group>
                                            </td>

                                            <td>
                                                <Group px={16} align="center" sx={{ height: 54 }}>
                                                    <Text size={"sm"} color={"#666892"} weight={"500"}>
                                                        {humanizeDate(alert.alertDate).shortDate}
                                                    </Text>
                                                </Group>
                                            </td>

                                            <td>
                                                {alert.url !== "Similar content not found" && (
                                                    <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                        {/* <ComingSoonWrapper orientation="vertical"> */}
                                                        <Link to={`/disputes/create/${alert.alertId}/collecting-evidence`}>
                                                            <SharkButton
                                                                variant={"filled"}
                                                                buttonType="primary"
                                                                onClick={() => onSaveAlertParam(alert.url)}
                                                                width={"100%"}
                                                                style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                                            >
                                                                React...
                                                            </SharkButton>
                                                        </Link>

                                                        {/* </ComingSoonWrapper> */}
                                                    </Group>
                                                )}
                                            </td>

                                            <td>
                                                <Group px={16} position="center" align="center" sx={{ height: 54 }} onClick={() => onDelete(alert.alertId)}>
                                                    <Text size={"sm"} color={"#4700E5"} weight={"500"} style={{ cursor: "pointer" }}>
                                                        {t("buttons.skip_btn")}
                                                    </Text>
                                                </Group>
                                            </td>

                                            <td>
                                                <Group px={16} position="right" align="center" sx={{ height: 54 }}>
                                                    <ActionMenu alertId={alert.alertId} refetch={refetch} url={alert.url} />
                                                </Group>
                                            </td>
                                        </Root>
                                    ))}

                                {item.length && item.length > 1 && (
                                    <>
                                        <Root>
                                            {/* <tr> */}
                                            <td colSpan={2}>
                                                <Group direction={"row"} noWrap={true}>
                                                    <Group align="center" pl={item[0].isRead ? 0 : 86} sx={{ height: 54 }}>
                                                        <ViewedStatus isRead={item[0].isRead} onlyCircle={true} />
                                                    </Group>
                                                    <a href={item[0].url} style={{ overflow: "hidden" }}>
                                                        <Group align="center" pl={item[0].isRead ? 113 : 0} sx={{ height: 54 }}>
                                                            <Text
                                                                size={"sm"}
                                                                color={"#666892"}
                                                                weight={"500"}
                                                                style={{
                                                                    overflow: "hidden",
                                                                    textOverflow: "ellipsis",
                                                                    whiteSpace: "nowrap",
                                                                }}
                                                            >
                                                                {item[0].url}
                                                            </Text>
                                                        </Group>
                                                    </a>
                                                </Group>
                                            </td>

                                            <td>
                                                <Group px={16} align="center" sx={{ height: 54 }}>
                                                    <Text size={"sm"} color={"#666892"} weight={"500"}>
                                                        {humanizeDate(item[0].alertDate).shortDate}
                                                    </Text>
                                                </Group>
                                            </td>

                                            <td>
                                                {item[0].url !== "Similar content not found" && (
                                                    <Group px={16} position="center" align="center" sx={{ height: 80 }}>
                                                        {/*<ComingSoonWrapper orientation="vertical"> */}
                                                        <Link to={`/disputes/create/${item[0].alertId}/collecting-evidence`}>
                                                            <SharkButton
                                                                variant={"filled"}
                                                                buttonType="primary"
                                                                onClick={() => onSaveAlertParam(item[0].url)}
                                                                width={"100%"}
                                                                style={{ padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer" }}
                                                            >
                                                                React...
                                                            </SharkButton>
                                                        </Link>
                                                        {/*</ComingSoonWrapper> */}
                                                    </Group>
                                                )}
                                            </td>

                                            <td>
                                                <Group px={16} position="center" align="center" sx={{ height: 54 }} onClick={() => onDelete(item[0].alertId)}>
                                                    <Text size={"sm"} color={"#4700E5"} weight={"500"} style={{ cursor: "pointer" }}>
                                                        {t("buttons.skip_btn")}
                                                    </Text>
                                                </Group>
                                            </td>

                                            <td>
                                                <Group px={16} position="right" align="center" sx={{ height: 54 }}>
                                                    <ActionMenu alertId={item[0].alertId} refetch={refetch} url={item[0].url} />
                                                </Group>
                                            </td>
                                            {/* </tr> */}
                                        </Root>
                                        {item.slice(1).map((similarContent, index) => (
                                            <Root key={index} style={{ border: "none" }}>
                                                <td colSpan={6}>
                                                    <Group direction={"row"} spacing={0} noWrap={true}>
                                                        <Group align="center" pl={similarContent.isRead ? 16 : 113} sx={{ height: 54 }}>
                                                            <ViewedStatus isRead={similarContent.isRead} onlyCircle={true} />
                                                        </Group>
                                                        <a href={similarContent.url} style={{ overflow: "hidden" }}>
                                                            <Group align="center" pl={similarContent.isRead ? 113 : 16} sx={{ height: 54 }}>
                                                                <Text
                                                                    size={"sm"}
                                                                    color={"#666892"}
                                                                    weight={"500"}
                                                                    style={{
                                                                        overflow: "hidden",
                                                                        textOverflow: "ellipsis",
                                                                        whiteSpace: "nowrap",
                                                                    }}
                                                                >
                                                                    {similarContent.url}
                                                                </Text>
                                                            </Group>
                                                        </a>
                                                    </Group>
                                                </td>
                                            </Root>
                                        ))}
                                    </>
                                )}
                            </>
                        ))}
                </>
            )}
        </>
    );
};

// Styling
const Root = styled.tr`
    transition: background-color 200ms linear;
    border-top: 1px solid #dddeee;

    &:hover {
        background-color: #edeeff;
    }
`;
