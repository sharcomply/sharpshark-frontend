import { Stack, Text } from "@mantine/core";
import styled from "@emotion/styled";
import { TableHead } from "../../components/DataTable/TableHead";
import { Fragment, useEffect, useState } from "react";
import { getListByDate } from "helpers/getListByDate";
import { TableDate } from "../../components/DataTable/TableDate";
import { AlertItem } from "./components/AlertItem";
import useAlertList from "./hooks/useAlertList";
import useChangeUnreadAlerts from "./hooks/useChangeUnreadAlerts";
import { useTranslation } from "react-i18next";

// Component
export default function AlertsList() {
    const [groupAlerts, setGroupAlerts] = useState<Alert[] | undefined>([]);
    const { alerts, refetch } = useAlertList({ isSkipped: false });
    const { t } = useTranslation();
    function groupByDocumentId(arr: any) {
        const temp = arr?.reduce((acc: any, elem: any) => {
            const documentId = elem.documentId._id;

            if (!acc[documentId]) {
                acc[documentId] = [];
            }

            acc[documentId].push(elem);
            return acc;
        }, {});
        if (temp === undefined) return;
        return Object.getOwnPropertyNames(temp)?.map((k) => temp[k]);
    }

    useChangeUnreadAlerts({
        alerts,
        refetch,
    });

    useEffect(() => {
        setGroupAlerts(groupByDocumentId(alerts?.items!));
    }, [alerts?.items]);

    const itemsByDate = getListByDate(groupAlerts || []);

    return (
        <Root>
            <Table>
                <TableHead
                    items={[
                        { title: t("table.titles.your_title"), width: "48%" },
                        { title: t("table.titles.appeared_on"), width: "18%" },
                        { title: t("table.titles.discovered"), width: "11%" },
                        { title: "", width: "10%" },
                        { title: "", width: "8%" },
                        { title: "", width: "5%" },
                    ]}
                />

                <tbody>
                    {!!alerts?.items.length && (
                        <>
                            <tr style={{ backgroundColor: "#000" }}>
                                <td colSpan={6} align="center" width={"100%"} style={{ padding: "5px", fontSize: "12px", fontWeight: 500, color: "#fff" }}>
                                    Are these cases violations? If yes, we can call for the website owner to remove/pay for it
                                </td>
                            </tr>
                            <Fragment>
                                {renderListByDate(t("table.row-by-date.today"), itemsByDate.today)}
                                {renderListByDate(t("table.row-by-date.this-week"), itemsByDate.week)}
                                {renderListByDate(t("table.row-by-date.last-month"), itemsByDate.lastMonth)}
                                {renderListByDate(t("table.row-by-date.earlier-this-month"), itemsByDate.month)}
                                {renderListByDate(t("table.row-by-date.earlier"), itemsByDate.earlier)}
                            </Fragment>
                        </>
                    )}

                    {!alerts?.items.length && (
                        <tr style={{ borderTop: "1px solid #DDDEEE" }}>
                            <td style={{ padding: 16 }}>
                                <Text size={"sm"} color={"#666892"} weight={"400"}>
                                    {t("table.empty-table")}
                                </Text>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    )}
                </tbody>
            </Table>
        </Root>
    );
}

// List By Date
function renderListByDate(title: string, items: any) {
    if (!items.length) return null;

    return (
        <Fragment>
            <TableDate title={title} cols={6} titlePos={3} />
            {items.map((item: Alert[]) => {
                return <AlertItem key={item[0]._id} item={item} />;
            })}
        </Fragment>
    );
}

// Styling
const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
`;

export const Table = styled.table`
    width: 100%;
    table-layout: fixed;
    border-collapse: collapse;
    overflow-wrap: anywhere;
`;
