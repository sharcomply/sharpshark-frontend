import { useSidebarStore } from "features/Sidebar/hooks/useSidebarStore";
import { useEffect, useState } from "react"

interface Props {
  alerts: { items: Alert[] | undefined } | undefined;
  refetch: () => void
}

const useChangeUnreadAlerts = ({ alerts, refetch }: Props) => {

  const [unreadCount, setUnreadCount] = useState<number>();
  const { setAlertsIndicators, alerts: alertsCount } = useSidebarStore();

  useEffect(() => {
    setUnreadCount(alerts!?.items!.filter((item) => !item.isRead).length);
  }, [alerts]);

  useEffect(() => {
    if (!unreadCount) return;
    return () => {
      setAlertsIndicators({ unread: alertsCount.unread! - unreadCount!, total: alertsCount.total });
      refetch();
      setUnreadCount(0)
    };
  }, [setAlertsIndicators, alertsCount.total, alertsCount.unread, unreadCount, refetch]);


}

export default useChangeUnreadAlerts