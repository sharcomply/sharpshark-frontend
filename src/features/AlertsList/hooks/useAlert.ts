import { useQuery } from "react-query";
import { backend } from "api";

export default function useAlert(id?: string) {
    const { data: alert, ...options } = useQuery(["ALERT-ITEM", id], () => backend.alerts.getAlertDetails(id), {
        keepPreviousData: true,
        refetchOnWindowFocus: false,
    });

    return {
        alert,
        ...options,
    };
}
