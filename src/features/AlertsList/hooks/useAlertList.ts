import { useQuery } from "react-query";
import { backend } from "api";

export default function useAlertList(params?: GetAlertParams) {
  const { data: alerts, ...options } = useQuery(["ALERTS_LIST"], () => backend.alerts.getAlertsList(params), {
    keepPreviousData: true,
    refetchOnWindowFocus: false,
  });

  return {
    alerts,
    ...options,
  };
}

export function useUpdateList(params?: GetAlertParams) {
  const { data: alerts, ...options } = useQuery(["ALERTS_LIST"], () => backend.alerts.getAlertsList(params), {
    enabled: false
  });

  return {
    alerts,
    ...options,
  };
}