import { useCallback } from "react";
import { useAppDispatch, useAppSelector } from "store";
import { alertListActions, AlertListState } from "../store";

export function useAlertListStore() {
  const state = useAppSelector((state) => state.alertList);
  const dispatch = useAppDispatch()

  const setCount = useCallback((type: keyof AlertListState["counts"], value: number) => {
    const action = alertListActions.setCount({ type, value });
    dispatch(action)
  }, [dispatch]);

  const setDisputPayload = useCallback((disputPayload: AlertListState["disputPayload"]) => {
    const action = alertListActions.setDisputPayload(disputPayload);
    dispatch(action)
  }, [dispatch]);



  const clearStore = useCallback(() => {
    const action = alertListActions.clear()
    dispatch(action)
  }, [dispatch])

  return {
    state,
    actions: {
      setCount,
      clearStore,
      setDisputPayload
    },
  };
}
