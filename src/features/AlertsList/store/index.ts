import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Types
export interface AlertListState {
  counts: {
    new: number;
    notActual: number;
  }
  disputPayload: {
    alertId: string;
    appearedOn: string;
  }
}

// Initial State
const initialState: AlertListState = {
  counts: {
    new: 0,
    notActual: 0
  },
  disputPayload: {
    alertId: "",
    appearedOn: "",
  }
};

// Slice
const alertListSlice = createSlice({
  name: "alertList",
  initialState,
  reducers: {
    clear: () => initialState,
    setCount: (state, action: PayloadAction<{ type: keyof AlertListState["counts"], value: number }>) => {
      state.counts[action.payload.type] = action.payload.value
    },

    setDisputPayload: (state, action: PayloadAction<AlertListState["disputPayload"]>) => {
      state.disputPayload.alertId = action.payload.alertId;
      state.disputPayload.appearedOn = action.payload.appearedOn
    },
  },
});

export const alertListReducers = alertListSlice.reducer;
export const alertListActions = alertListSlice.actions;