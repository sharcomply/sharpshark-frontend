import { PropsWithChildren, Fragment } from 'react';
import { useAutoConnect } from '../../blockchain/hooks/useAutoConnect';
import { useMetaMaskListener } from '../../blockchain/hooks/useMetaMaskListener';


// Component
export default function AuthProvider({ children }: PropsWithChildren<{}>) {
    
    // Сюда кроме метамаска ничего не пихать!!!
    const connected = useAutoConnect()
    useMetaMaskListener(connected)
    
    return (
        <Fragment>
            {children}
        </Fragment>
    );
}
