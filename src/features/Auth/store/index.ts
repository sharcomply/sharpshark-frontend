import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { getToken } from "helpers/tokenStorage";
import { User } from "types/user";
import { ChainId, getNetworkById } from "../../../constants/networks";

// Types
const networkID = localStorage.getItem("NETWORK_ID");

if (!networkID || networkID === String(ChainId.GOERLI_TESTNET) || networkID === String(ChainId.BSC_TESTNET)) {
    localStorage.setItem("NETWORK_ID", window.ethereum?.networkVersion);
}

export interface AuthState {
    isLoggedIn: boolean;
    connected: boolean;
    user: User | null;
    network: any;
}

// Initial State
const initialState: AuthState = {
    isLoggedIn: !!getToken(),
    connected: false,
    user: null,
    network: getNetworkById(networkID || "")?.value,
};

// Slice
const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        setLoggedIn: (state, action: PayloadAction<boolean>) => {
            state.isLoggedIn = action.payload;
        },
        setUser: (state, action: PayloadAction<User | null>) => {
            state.user = action.payload;
        },
        setConnect: (state, action: PayloadAction<boolean>) => {
            state.connected = action.payload;
        },
        setCurrentNetwork: (state, action) => {
            state.network = action.payload;
        },
    },
});

export const authReducers = authSlice.reducer;
export const authActions = authSlice.actions;
