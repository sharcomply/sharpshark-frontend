import {useAppDispatch, useAppSelector} from 'store';
import {useCallback} from 'react';
import {authActions} from '../store';
import {User} from 'types/user';


export default function useAuthStore() {
    const state = useAppSelector((state) => state.auth)
    const dispatch = useAppDispatch()

    const setLoggedIn = useCallback((status: boolean) => {
        dispatch(authActions.setLoggedIn(status))
    }, [dispatch])

    const setUser = useCallback((user: User | null) => {
        dispatch(authActions.setUser(user))
    }, [dispatch])

    const setConnect = useCallback((status: boolean) => {
        dispatch(authActions.setConnect(status))
    }, [dispatch])

    const setCurrentNetwork = useCallback((network: string) => {
        dispatch(authActions.setCurrentNetwork(network))
    }, [dispatch])


    return {
        ...state,
        setLoggedIn,
        setUser,
        setConnect,
        setCurrentNetwork
    }
}