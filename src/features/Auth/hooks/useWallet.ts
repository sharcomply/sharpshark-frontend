import {backend} from 'api';
import {useCallback, useEffect, useState} from 'react';
import {useNavigate} from 'react-router-dom';
import useAuthStore from './useAuthStore';
import useDraftStore from "../../DraftDetails/hooks/useDraftStore";
import {useSidebarStore} from "../../Sidebar/hooks/useSidebarStore";
import {useModal} from "../../Modals/hooks/useModal";
import {ModalContent, ModalType} from "../../Modals/interfaces";

export function useWallet() {
    const { setConnect, setUser, user, setLoggedIn } = useAuthStore();
    const {clearStore} = useDraftStore()
    const {clearStore: cleaSideBarStore} = useSidebarStore()
    const {handleOpen} = useModal(ModalType.GENERAL)
    const [isError, setError] = useState(false)
    const navigate = useNavigate()
    const connect = useCallback(async () => {
        if (!window.ethereum) {
            return console.log('[USE_WALLET]', 'MetaMask not detected');
        }

        try {
            const [ address ] = await window.ethereum.request({ method: 'eth_requestAccounts' }).catch((e: any) => console.error(e));
            if (!address) return;

            // Get login code by user address & network
            const loginCode = await backend.auth.getLoginCode({ address });

            if(!loginCode.isSuccess || !loginCode.code) {
                setError(true)
                return
            }
            window.ethereum.on('accountsChanged', (accounts: string[]) => {
                if (!accounts.length) {
                    setLoggedIn(false)
                    clearStore()

                    const timer = setTimeout(() => {
                        cleaSideBarStore()
                        clearTimeout(timer)
                        window.location.reload()
                    }, 1000)

                    // window.history.go(-1)
                    console.log(`DISCONNECTED_WALLET`)
                    // window.location.reload()
                }

            })

            // Get user signature from MetaMask Extension
            const signature = await window.ethereum.request({
                method: 'personal_sign',
                params: [ loginCode.code, address ]
            });

            setConnect(true);

            return {
                code: loginCode.code,
                address,
                signature,
            };
        } catch (e: any) {
            console.log(e.code)
            if(e.code === -32603) return
            setError(true)
        }

        // Get account from MetaMask Extension

    }, [ setConnect, navigate, setError ]);

    useEffect(() => {
        if(isError) {
            handleOpen({
                type: ModalType.GENERAL,
                content: ModalContent.Error
            })
        }
    }, [isError])

    const disconnect = useCallback(() => {
        setUser(null);
        setConnect(false);
    }, [ setConnect, setUser ]);

    return {
        user,
        connect,
        disconnect
    };
}
