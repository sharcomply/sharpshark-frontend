import { useCallback, useMemo } from "react";
import { removeToken, setToken } from "helpers/tokenStorage";
import useAuthStore from "./useAuthStore";
import { useWallet } from "./useWallet";
import { useWhoAmi } from "./useWhoAmi";
import { backend } from "api";
import { useWeb3React } from "@web3-react/core";

export const useAuth = () => {
    const wallet = useWallet();
    const { data, remove, refetch, isError } = useWhoAmi();
    const { connected, isLoggedIn, setLoggedIn, setUser, setConnect } = useAuthStore();
    const { deactivate } = useWeb3React();

    // window.onbeforeunload = function () {
    //     return localStorage.clear();
    // };

    const isWrongWallet = useMemo(() => {
        if (!data?.user) return false;

        const addressByToken = data?.user.walletAddress.toLowerCase();
        const walletAddress = wallet.user?.address.toLowerCase();

        return addressByToken !== walletAddress;
    }, [data?.user, wallet]);

    const userByWallet = useMemo(() => {
        return isWrongWallet ? null : data?.user;
    }, [data?.user, isWrongWallet]);

    const connectWallet = useCallback(async () => {
        try {
            const payload = await wallet.connect();
            if (!payload) return;

            const { token } = await backend.auth.confirmLoginCode({
                ...payload,
                network: "ethereum",
            });

            setToken(token);

            const response = await refetch();
            const user = response.data?.user;

            setUser(
                user
                    ? {
                          _id: user._id,
                          firstName: user.firstName,
                          lastName: user.lastName,
                          address: user.walletAddress,
                          isVerification: user.isVerification,
                          nonce: user.nonce,
                          networkType: user.networkType,
                          email: user.email,
                          isEmailVerified: user.isEmailVerified,
                          disputesPrefillEmail: user.disputesPrefillEmail,
                          country: user.country,
                      }
                    : null
            );
            setLoggedIn(true);
        } catch (e) {
            console.log("[USE_AUTH:CONNECT_WALLET]:", e);
            wallet.disconnect();
        }
    }, [refetch, setLoggedIn, setUser, wallet]);

    const disconnectWallet = useCallback(async () => {
        wallet.disconnect();
        deactivate();
    }, [deactivate, wallet]);

    const logout = useCallback(async () => {
        removeToken();
        remove();

        if (connected) {
            await disconnectWallet();
        }

        setLoggedIn(false);
        setConnect(false);
        setUser(null);
    }, [remove, connected, setLoggedIn, setConnect, setUser, disconnectWallet]);

    return {
        user: data?.user,
        logout,
        wallet,
        isError,
        connected,
        isLoggedIn,
        userByWallet,
        connectWallet,
        isWrongWallet,
        disconnectWallet,
    };
};

