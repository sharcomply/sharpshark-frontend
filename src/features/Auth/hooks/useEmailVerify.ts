import { backend } from "api";
import { useQuery } from "react-query";
import { useSearchParams } from "react-router-dom";

export const useEmailVerify = () => {
    const [searchParams] = useSearchParams();
    const token = searchParams.get("token") || "";

    const { data, isError, isSuccess, isLoading } = useQuery(["VERIFY_RESULT"], () => backend.auth.confirmEmail({ token }), {
        enabled: !!token,
        retry: false,
        keepPreviousData: true,
    })

    return {
        data,
        token,
        isError,
        isSuccess,
        isLoading,
    }
}
