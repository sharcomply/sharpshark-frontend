import { useQuery } from 'react-query';
import { backend } from 'api';


export const useWhoAmi = () => {
    const { data, refetch, isLoading, isError, remove } = useQuery([ 'WHO_AM_I' ], () => backend.auth.getWhoAmi(), {
        enabled: false,
        retry: false,
        keepPreviousData: true,
    });
    
    return {
        data,
        refetch,
        isLoading,
        isError,
        remove
    };
};
