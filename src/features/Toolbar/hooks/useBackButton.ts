import { useToolbarStore } from './useToolbarStore';
import { useEffect } from 'react';

// Обработчик который переключает отображаение кнопки назад для страниц
export const useBackButton = (options = { onOpen: true, onClose: false }) => {
    const { setBackButton } = useToolbarStore();
    
    useEffect(() => {
        setBackButton(options.onOpen);
        return () => setBackButton(options.onClose);
    }, [options.onClose, options.onOpen, setBackButton]);
};