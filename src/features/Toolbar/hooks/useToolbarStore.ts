import { useAppDispatch, useAppSelector } from '../../../store';
import { useCallback } from 'react';
import { toolbarActions } from '../store';


export function useToolbarStore() {
    const state = useAppSelector((state) => state.toolbar);
    const dispatch = useAppDispatch();
    
    const setBackButton = useCallback((status: boolean) => {
        const action = toolbarActions.updateBackStatus(status);
        
        dispatch(action);
    }, [ dispatch ]);
    
    return {
        ...state,
        setBackButton
    };
}