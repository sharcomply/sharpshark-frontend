import { useCallback, useEffect } from "react";
import styled from "@emotion/styled";
import { useWeb3React } from "@web3-react/core";
import { useTranslation } from "react-i18next";
import { Group, Select, Text, Tooltip } from "@mantine/core";
import { useViewportSize } from "@mantine/hooks";
import { Link, useNavigate } from "react-router-dom";

import useDraftStore from "../DraftDetails/hooks/useDraftStore";
import { useWhoAmi } from "../Auth/hooks/useWhoAmi";
import { useModal } from "../Modals/hooks/useModal";
import { useToolbarStore } from "./hooks/useToolbarStore";
import useAuthStore from "../Auth/hooks/useAuthStore";
import { shortedWalletAddress } from "../../helpers/shortenWalletAddress";
import { useCrossTabState } from "hooks/useCrossTbState";

import Icon from "components/Icon";
import { SharkButton } from "components/SharkButton";
import Button from "./components/Button";
import { ModalContent, ModalType } from "../Modals/interfaces";

import { useNearContext } from "../../providers/NearProvider";
import { getNetworkById, NetworksList } from "constants/networks";
import { NEAR_CONTRACT } from "../../constants/providers";
import { changeMethods, viewMethods } from "../../constants/NearContract";
import {changeNearAddressRequest} from "../../api/requests/auth";

// const SelectStyles = {
//     item: {
//         'hover': {
//             color: "#ffffff !important"
//         }
//
//     }
// }

// Component
export default function Toolbar() {
    const { width } = useViewportSize();
    const navigate = useNavigate();
    const { canBack } = useToolbarStore();
    const { isLoggedIn, connected, user, network, setUser, setCurrentNetwork } = useAuthStore();
    const { handleOpen } = useModal();
    const { t } = useTranslation();
    const { data } = useWhoAmi();
    const { canMint, overlay } = useDraftStore();
    const { activate } = useWeb3React();
    const Near = useNearContext();

    const [nearConnect, setNearConnect] = useCrossTabState("nearConnect", "");
    const networkMetamask = window.ethereum?.networkVersion

    useEffect(() => {
        if (network !== "near" && networkMetamask) {
            switch (window.ethereum?.networkVersion) {
                case "5":
                    setCurrentNetwork("5");
                    localStorage.setItem("NETWORK_ID", "5");
                    break;
                case "80001":
                    setCurrentNetwork("80001");
                    localStorage.setItem("NETWORK_ID", "80001");
                    break;
                case "137":
                    setCurrentNetwork("137");
                    localStorage.setItem("NETWORK_ID", "137");
                    break;
                // case "56":
                //     setCurrentNetwork("56");
                //     localStorage.setItem("NETWORK_ID", "56");
                //     break;
                case "97":
                    setCurrentNetwork("97");
                    localStorage.setItem("NETWORK_ID", "97");
                    break;
                default:
                    setUser(null);
                    break;
            }
        }

        if (nearConnect === "true") {
            window.location.reload();
            setNearConnect(false);
        }
    }, [setCurrentNetwork, setUser, activate, setNearConnect, nearConnect, networkMetamask]);

    useEffect(() => {
        if (data?.user.walletAddressNear === "" && Near?.WalletConnection.account().accountId) {
            changeNearAddressRequest({ walletAddressNear: Near?.WalletConnection.account().accountId });
        }
    }, [Near?.WalletConnection, data?.user.walletAddressNear]);

    const NearLogin = useCallback(async () => {
        await Near?.WalletConnection.requestSignIn({
            contractId: NEAR_CONTRACT,
            methodNames: [...viewMethods, ...changeMethods],
            successUrl: `${window.location.href}?nearConnect=true`,
        });
    }, [Near]);

    const handleChangeNetwork = useCallback(
        async (value: any) => {
            const selectedNetwork = getNetworkById(value);
            if (!value) return;

            if (!selectedNetwork?.chainId) {
                if (!Near?.WalletConnection.isSignedIn()) NearLogin();
                setCurrentNetwork(value);
                localStorage.setItem("NETWORK_ID", value);
                return;
            }
            try {
                await window.ethereum.request({
                    method: "wallet_switchEthereumChain",
                    params: [{ chainId: selectedNetwork?.chainId }],
                });
                setCurrentNetwork(value);
                localStorage.setItem("NETWORK_ID", value);
            } catch (e) {
                console.log(e);
            }
        },
        [setCurrentNetwork, Near?.WalletConnection, NearLogin]
    );

    const selectData = () => {
        const res: any = [];
        if (Near?.WalletConnection.isSignedIn()) {
            NetworksList.forEach((item) => {
                res.push({ label: item.label, value: item.value });
            });
        } else {
            NetworksList.forEach((item) => {
                if (item.value !== "near") res.push({ label: item.label, value: item.value });
            });
            res.push({ label: "Connect NEAR", value: "near" });
        }
        return res;
    };

    return (
        <ToolbarRoot id="toolbar" px={width > 1240 ? 32 : 6} py={12} position={"apart"}>
            <Group align={"center"} spacing={24}>
                {canBack && (
                    <HoverGroup align={"center"} spacing={8} sx={{ cursor: "pointer" }} onClick={() => navigate(-1)}>
                        <Icon icon={"ic:round-arrow-back"} color={"#B6B4CD"} hoverColor={"#B6B4CD"} size={24} />
                        <Text size={"sm"} color={"#B6B4CD"} weight={"500"}>
                            {t("header.back_btn")}
                        </Text>
                    </HoverGroup>
                )}
            </Group>

            <Group align={"center"} spacing={width > 1240 ? 24 : 6}>
                {/* SEARCH */}
                <TempContainer>
                    <Icon icon={"ic:round-search"} size={24} hoverColor={"#3751ff"} />
                    <TempText>{t("header.coming_soon")}</TempText>
                </TempContainer>
                {isLoggedIn && (!connected || !user) && (
                    <Group spacing={width > 1240 ? 24 : 6}>
                        {!user && data ? <Icon icon={"ic:round-settings"} size={20} hoverColor={"#3751ff"} container={<Link to={"/settings"} />} /> : null}
                        <SharkButton px={width > 1240 ? 24 : 6} disabled buttonType="primary" variant={"outlined"}>
                            {t("errors.titles.network_not_supported")}
                        </SharkButton>
                        <Tooltip
                            label={
                                <Text style={{ fontSize: 14, textAlign: "center" }} dangerouslySetInnerHTML={{ __html: t("tooltips.change_network") }}></Text>
                            }
                        >
                            <SharkButton buttonType="secondary" variant="filled">
                                {t("buttons.change_network_btn")}
                            </SharkButton>
                        </Tooltip>
                        {!user && data ? (
                            <SharkButton buttonType="primary" variant={"outlined"} disabled>
                                {shortedWalletAddress(data.user.walletAddress)}
                            </SharkButton>
                        ) : null}
                    </Group>
                )}
                {/* CONNECTED */}
                {isLoggedIn && connected && user && (
                    <Group spacing={width > 1240 ? 24 : 6}>
                        <Icon icon={"ic:round-settings"} size={20} hoverColor={"#3751ff"} container={<Link to={"/settings"} />} />
                        <Select
                            styles={{
                                root: {
                                    maxWidth: 200,
                                },
                                rightSection: { pointerEvents: "none" },
                                item: { background: "transparent !important", color: "#0A001E !important" },
                            }}
                            value={network}
                            onChange={handleChangeNetwork}
                            data={selectData()}
                            disabled={canMint || overlay.show}
                        />

                        <SharkButton buttonType="primary" variant={"outlined"} disabled>
                            {shortedWalletAddress(user.address)}
                        </SharkButton>
                    </Group>
                )}

                {/*  NOT CONNECTED  */}
                {(!isLoggedIn || !connected) && (
                    <Group spacing={width > 1240 ? 24 : 6}>
                        <Button
                            title={t("buttons.connect_wallet_btn")}
                            variant={"filled"}
                            onClick={() => {
                                handleOpen({ type: ModalType.AUTH, content: ModalContent.SignIn });
                            }}
                        />

                        <Button
                            title={t("buttons.replenish_balance_btn")}
                            subtitle={t("common.test_network_only")}
                            variant={"filled"}
                            href={"https://rinkebyfaucet.com/"}
                            target="_blank"
                        />
                    </Group>
                )}
            </Group>
        </ToolbarRoot>
    );
}

// Styling
const ToolbarRoot = styled(Group)`
    height: 64px;
    min-height: 64px;
    background-color: #0a001e;
`;

const HoverGroup = styled(Group)`
    & > * {
        transition: color 200ms linear;
    }

    &:hover > * {
        color: #4700e5;
    }
`;

const TempContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const TempText = styled.div`
    font-size: 10px;
    color: #ffffff;
    margin-left: 5px;
`;
