import { css } from "@emotion/react";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";

// Props
interface Props {
    title: string;
    to?: string;
    onClick?: () => void;
    variant?: "filled" | "outlined";
    target?: "_blank" | "_parent" | "_self" | "_top";
    href?: string;
    subtitle?: string
}

// Component
export default function Button({ title, to, onClick, variant = "filled", target = "_self", href, subtitle }: Props) {
    if (href) {
        return (
            <ExternalLinkRoot href={href} target={target} className={`${variant} ${subtitle ? "small": ""}`}>
                <span>{title}</span>
                {subtitle && <span>{subtitle}</span>}
            </ExternalLinkRoot>
        );
    }
    if (to) {
        return (
            <NavLinkRoot target={target} to={to} className={`${variant} ${subtitle ? "small": ""}`}>
                <span>{title}</span>
                {subtitle && <span>{subtitle}</span>}
            </NavLinkRoot>
        );
    }

    return (
        <ButtonRoot onClick={onClick} className={`${variant} ${subtitle ? "small": ""}`}>
            <span>{title}</span>
            {subtitle && <span>{subtitle}</span>}
        </ButtonRoot>
    );
}

// Styling
const RootCSS = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: relative;
    max-height: 40px;
    width: 160px;

    @media (max-width: 1150px) {
        min-width: 120px;
        width: unset;
    }

    padding: 10px 5px;
    border-radius: 4px;
    cursor: pointer;

    font-weight: 500;
    font-size: 14px;
    line-height: 20px;
    text-align: center;

    transition-duration: 200ms;
    transition-property: color, background-color, box-shadow, border;
    transition-timing-function: linear;

    &.filled {
        color: #fff;
        background-color: #4700e5;
        box-shadow: 0 4px 12px rgba(55, 81, 255, 0.24);
    }

    &.filled:hover {
        color: #fff;
        background-color: #1c00a6;
        box-shadow: 0 6px 24px rgba(37, 61, 217, 0.32);
    }

    &.filled:focus {
        color: #fff;
        background-color: #4700e5;
        box-shadow: 0 2px 6px rgba(37, 61, 217, 0.32);
    }

    &.outlined {
        color: #938fff;
        background-color: transparent;
        border: 1px solid #938fff;
    }

    &.outlined:hover {
        color: #ffffff;
        background-color: #4700e5;
        border: 1px solid #4700e5;
    }

    &.outlined:focus {
        color: #ffffff;
        background-color: #938fff;
        border: 1px solid #938fff;
    }

    &.small {
        span {
            line-height: 1;
            font-size: 12px;
            &:first-child {
                font-size: 14px;
            }
        }
    }
`;

const ButtonRoot = styled.button`
    ${RootCSS};
`;

const NavLinkRoot = styled(Link)`
    ${RootCSS};
`;

const ExternalLinkRoot = styled.a`
    ${RootCSS};
`;
