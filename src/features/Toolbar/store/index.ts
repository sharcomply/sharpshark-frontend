import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Types
export interface ToolbarState {
    canBack: boolean;
}

// Initial State
const initialState: ToolbarState = {
    canBack: false
};

// Slice
const toolbarSlice = createSlice({
    name: "toolbar",
    initialState,
    reducers: {
        updateBackStatus: (state, action: PayloadAction<boolean>) => ({
            ...state,
            canBack: action.payload
        })
    },
});

export const toolbarReducers = toolbarSlice.reducer
export const toolbarActions = toolbarSlice.actions
