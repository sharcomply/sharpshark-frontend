import styled from "@emotion/styled";
import { Group, Input, Stack, Text } from "@mantine/core";
import { findEmails, sendEmailDispute } from "api/requests/disputes";
import Icon from "components/Icon";
import { SharkButton } from "components/SharkButton";
import AddButton from "features/DisputesCollectingEvidence/components/AddButton";
import RadioButton from "features/DisputesCollectingEvidence/components/RadioButton";
import RemoveButton from "features/DisputesCollectingEvidence/components/RemoveButton";
import { useDisputeCreate } from "features/DisputesCollectingEvidence/hooks/useDisputeCreate";
import { useDisputDetail } from "features/DisputesList/hooks/useDisputesList";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";
import { useModal } from "features/Modals/hooks/useModal";
import { ModalContent, ModalType } from "features/Modals/interfaces";
import { useCallback, useEffect, useState } from "react";
import { Control, FieldValues, useFieldArray, UseFormRegister, UseFormSetError, useWatch } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useNavigate, useParams } from "react-router-dom";
import useDisputesStore from "../DisputesDetail/hooks/useDisputesStore";
import jsPDF from "jspdf";
import { useDraftAlert } from "../DraftDetails/hooks/useDraftAlert";
import { validateEmail } from "helpers/validateEmail";
import {getFont} from "../../helpers/FontFoPDF";

interface Props {
    control: Control;
    register: UseFormRegister<FieldValues>;
    getValues: (props?: string) => string;
    setError: UseFormSetError<any>;
}

const DisputesSending = ({ control, register, getValues }: Props) => {
    const { disputId } = useParams();
    const { handleOpen, handleSwitch, handleClose } = useModal();
    const navigate = useNavigate();
    const { status, disputModalResponse, setDisputModalResponse, emailFound, setEmailFound } = useDisputesStore();
    const draftStore = useDraftStore();
    const disputeStatus = getValues("status");
    const { t } = useTranslation();
    const [closeForm, setCloseForm] = useState(false);
    const alert = useDraftAlert();

    const [disable, setDisable] = useState(false);
    const [loading, setLoading] = useState(false);
    const { refetch } = useDisputDetail(disputId);
    const demands = getValues("demands");

    const ownerClaimType = useWatch({ control, name: "ownerClaimType" });
    // const fieldsOwnerClaimEmailsWatch = useWatch({ control, name: "ownerClaimEmails" });

    useEffect(() => {
        if (disputeStatus) {
            if (
                disputeStatus === "In Progress: Website Owner" ||
                disputeStatus === "In Progress: Provider" ||
                disputeStatus === "In Progress: Google" ||
                disputeStatus === "Closed" ||
                disputeStatus === "Under Court"
            ) {
                setDisable(true);
            } else {
                setDisable(false);
            }
        }
    }, [getValues, disputeStatus]);

    useEffect(() => {
        if (status.message === "Saved" && closeForm) {
            navigate("/disputes/draft-website-owner");
        }
        return () => {
            setCloseForm(false);
        };
    }, [closeForm, navigate, status.message]);

    const { handleUpdate, handleCreate } = useDisputeCreate();

    const {
        fields: fieldsOwnerClaimEmails,
        append: appendOwnerClaimEmails,
        remove: removeOwnerClaimEmails,
    } = useFieldArray({
        control,
        name: "ownerClaimEmails",
    });

    const {
        fields: fieldsProviderClaimEmails,
        append: appendProviderClaimEmails,
        remove: removeProviderClaimEmails,
    } = useFieldArray({
        control,
        name: "providerClaimEmails",
    });

    const checkStatus = useCallback((disputeStatus: any) => {
        switch (disputeStatus) {
            case undefined:
                return "In Progress: Website Owner";

            case "Draft: Website Owner":
                return "In Progress: Website Owner";

            case "Claim Sent: Website Owner":
                return "In Progress: Provider";

            case "Draft: Provider":
                return "In Progress: Provider";

            case "Claim Sent: Provider":
                return "In Progress: Google";

            case "Draft: Google":
                return "In Progress: Google";

            default:
                return disputeStatus;
        }
    }, []);

    const checkOwnerClaimType = useCallback(
        (ownerClaimType: string) => {
            switch (ownerClaimType) {
                case "By email":
                    return handleOpen({ type: ModalType.DISPUTE, content: ModalContent.ClaimBeSent });
                case "Via form":
                    return handleOpen({ type: ModalType.DISPUTE, content: ModalContent.SentTheClaim });
                case "Nothing found":
                    return handleOpen({ type: ModalType.DISPUTE, content: ModalContent.NoTraceOfContacts });
                default:
                    break;
            }
        },
        [handleOpen]
    );

    const onUpdateStatus = useCallback(async () => {
        try {
            let disputIdLocal: any = undefined;
            let checkCreate: any = undefined;
            if (disputId) {
                disputIdLocal = await handleUpdate({ ...(getValues() as any), status: checkStatus(getValues("status")) });
            } else {
                disputIdLocal = await handleCreate({ ...(getValues() as any), status: checkStatus(getValues("status")) });
                checkCreate = disputIdLocal;
            }
            if (!checkCreate) {
                await refetch();
            }
            if (disputModalResponse === ModalContent.ClaimBeSent) {
                try {
                    if (getValues("providerClaimEmail")) {
                        await sendEmailDispute(disputIdLocal);

                        setDisputModalResponse(undefined);
                        if (!checkCreate) {
                            await refetch();
                        }
                    }
                } catch (error) {
                    setDisputModalResponse(undefined);
                    handleClose();
                }
            }
            handleSwitch(ModalContent.ClaimIsSent);
            setDisputModalResponse(undefined);
        } catch (error) {
            handleClose();
            setDisputModalResponse(undefined);
            console.log(error);
        }
    }, [disputId, handleUpdate, getValues, checkStatus, handleCreate, refetch, disputModalResponse, setDisputModalResponse]);

    const onOpenModal = useCallback(async () => {
        if ((demands === "Pay and/or credit" || demands === "Pay and remove") && (!getValues("payAmount") || Number(getValues("payAmount")) === 0)) {
            alert.actions.open();
            alert.actions.error(t("Validation Error!"), t('The amount in the "Your demands" must be greater than zero'));
        } else {
            if (
                disputeStatus === "Draft: Website Owner" ||
                disputeStatus === "In Progress: Website Owner" ||
                disputeStatus === undefined ||
                "Letter Sent: Website Owner"
            ) {
                checkOwnerClaimType(ownerClaimType);
            }
            if (
                disputeStatus === "Claim Sent: Website Owner" ||
                disputeStatus === "Draft: Provider" ||
                disputeStatus === "In Progress: Provider" ||
                disputeStatus === "Letter Sent: Provider"
            ) {
                handleOpen({ type: ModalType.DISPUTE, content: ModalContent.ClaimBeSent });
            }
            if (
                disputeStatus === "Claim Sent: Provider" ||
                disputeStatus === "Draft: Google" ||
                disputeStatus === "In Progress: Google" ||
                disputeStatus === "Letter Sent: Google"
            ) {
                await handleUpdate({ ...(getValues() as any), status: checkStatus(getValues("status")) });
                if (status.type === "success") {
                    handleOpen({ type: ModalType.DISPUTE, content: ModalContent.ClaimIsSent });
                }
            }
        }
    }, [ownerClaimType, checkOwnerClaimType]);

    const onFindEmails = useCallback(async () => {
        try {
            setLoading(true);
            let disputIdLocal: any = undefined;

            if (disputId) {
                disputIdLocal = await handleUpdate({ ...(getValues() as any) });
            } else {
                disputIdLocal = await handleCreate({ ...(getValues() as any) });
            }
            const response = await findEmails(disputIdLocal);
            appendOwnerClaimEmails(response.emails);
            setEmailFound(response.emails.length);
            setLoading(false);
        } catch (error) {
            console.log(error);
            setLoading(false);
        }
    }, [disputId, appendOwnerClaimEmails, getValues, handleCreate, handleUpdate]);

    const onFindEmailsProvider = useCallback(async () => {
        try {
            setLoading(true);

            const response = await findEmails(disputId!);
            appendProviderClaimEmails(response.emails);
            setEmailFound(response.emails.length);
            setLoading(false);
        } catch (error) {
            console.log(error);
            setLoading(false);
        }
    }, [disputId, appendProviderClaimEmails, getValues, handleCreate, handleUpdate]);

    useEffect(() => {
        if (!disputModalResponse) return;

        if (
            ownerClaimType === "By email" &&
            (disputeStatus === "Draft: Website Owner" ||
                disputeStatus === "In Progress: Website Owner" ||
                disputeStatus === "Letter Sent: Website Owner" ||
                disputeStatus === undefined)
        ) {
            if (!getValues("ownerClaimEmails")?.length || getValues("ownerClaimEmails")[0]?.length === 0) {
                draftStore.showDraftAlert(true);
                draftStore.setDraftAlert({
                    type: "error",
                    title: t("errors.titles.didnt_add_any_email"),
                    message: t("errors.messages.plz_add_website_owner_email"),
                });
                setDisputModalResponse(undefined);
                handleClose();
                return;
            }

            if (!getValues("userContactEmail")) {
                draftStore.showDraftAlert(true);
                draftStore.setDraftAlert({
                    type: "error",
                    title: t("errors.titles.smth_wrong"),
                    message: t("dispute.errors.your_contacts_required"),
                });
                setDisputModalResponse(undefined);
                handleClose();
                return;
            }

            if (!validateEmail(getValues("userContactEmail"))) {
                draftStore.showDraftAlert(true);
                draftStore.setDraftAlert({
                    type: "error",
                    title: t("errors.titles.smth_wrong"),
                    message: t("dispute.errors.your_contacts_validate"),
                });
                setDisputModalResponse(undefined);
                handleClose();
                return;
            }
        }

        if (
            (disputeStatus === "Draft: Website Owner" || disputeStatus === "In Progress: Website Owner" || disputeStatus === undefined) &&
            !getValues("ownerClaimText")?.length
        ) {
            draftStore.showDraftAlert(true);
            draftStore.setDraftAlert({
                type: "error",
                title: t("errors.titles.smth_wrong"),
                message: "You didn't generate a claim",
            });
            setDisputModalResponse(undefined);
            handleClose();
            return;
        }

        if (
            disputeStatus === "Draft: Provider" ||
            disputeStatus === "In Progress: Provider" ||
            disputeStatus === "Letter Sent: Provider" ||
            disputeStatus === "Draft: Google" ||
            disputeStatus === "In Progress: Google" ||
            disputeStatus === "Letter Sent: Google"
        ) {
            if (!getValues("userContactEmail")) {
                draftStore.showDraftAlert(true);
                draftStore.setDraftAlert({
                    type: "error",
                    title: t("errors.titles.smth_wrong"),
                    message: t("dispute.errors.your_contacts_required"),
                });
                setDisputModalResponse(undefined);
                handleClose();
                return;
            }

            if (!validateEmail(getValues("userContactEmail"))) {
                draftStore.showDraftAlert(true);
                draftStore.setDraftAlert({
                    type: "error",
                    title: t("errors.titles.smth_wrong"),
                    message: t("dispute.errors.your_contacts_validate"),
                });
                setDisputModalResponse(undefined);
                handleClose();
                return;
            }
        }

        if (
            (disputeStatus === "Claim Sent: Website Owner" || disputeStatus === "Draft: Provider" || disputeStatus === "In Progress: Provider") &&
            !getValues("providerClaimText")?.length
        ) {
            draftStore.showDraftAlert(true);
            draftStore.setDraftAlert({
                type: "error",
                title: t("errors.titles.smth_wrong"),
                message: "You didn't generate a claim",
            });
            setDisputModalResponse(undefined);
            handleClose();
            return;
        }

        if (
            (disputeStatus === "Claim Sent: Website Owner" || disputeStatus === "Draft: Provider" || disputeStatus === "In Progress: Provider") &&
            (!getValues("providerClaimEmails")?.length || getValues("providerClaimEmails")[0]?.length === 0)
        ) {
            draftStore.showDraftAlert(true);
            draftStore.setDraftAlert({
                type: "error",
                title: t("erros.titles.didnt_add_any_email"),
                message: t("errors.messages.plz_add_website_owner_email"),
            });
            setDisputModalResponse(undefined);
            handleClose();
            return;
        }

        disputModalResponse && onUpdateStatus();
    }, [disputModalResponse, setDisputModalResponse, t]);

    useEffect(() => {
        return () => {
            draftStore.showDraftAlert(false);
            draftStore.setDraftAlert({
                type: undefined,
                title: undefined,
                message: undefined,
            });
            setEmailFound(undefined);
        };
    }, []);

    const pdfGenerate = async () => {
        const font = getFont();
        const doc = new jsPDF("portrait", "px", "a4", false);
        doc.addFileToVFS("AvrileSans-Black-normal.ttf", font);
        doc.addFont("AvrileSans-Black-normal.ttf", "AvrileSans-Black", "normal");
        const node = document.createElement("div");
        const claimText = getValues("ownerClaimText");
        node.innerHTML = claimText;
        const a = Array.from(node.getElementsByTagName("a"));
        a.map((item) => {
            item.style.letterSpacing = "0.2px";
            const href = item.getAttribute("href");
            item.textContent = href;
        });

        node.style.fontSize = "11px";
        node.style.wordSpacing = "normal";
        node.style.fontFamily = "AvrileSans-Black";
        node.style.wordSpacing = "1px";
        await doc.html(node, {
            margin: [10, 10, 10, 10],
            autoPaging: "text",
            width: 750,
            windowWidth: 650,
            html2canvas: {
                scale: 0.65,
            },
        });
        doc.save("download.pdf");
    };

    return (
        <Root>
            <Wrapper>
                {(disputeStatus === "Draft: Website Owner" ||
                    disputeStatus === "In Progress: Website Owner" ||
                    disputeStatus === "Claim Sent: Website Owner" ||
                    disputeStatus === "Letter Sent: Website Owner" ||
                    disputeStatus === undefined) && (
                    <>
                        <Title>Step 3. Sending the claim to the website owner</Title>
                        <div style={{ width: "380px" }}>
                            <div style={{ marginBottom: "32px" }}>
                                <div style={{ display: "flex", alignItems: "center", marginBottom: "16px" }}>
                                    <RadioButton disabled={disable} {...register("ownerClaimType")} type="radio" value="By email" />
                                    <Label>By email</Label>
                                </div>

                                <div style={{ display: "flex", flexDirection: "column" }}>
                                    <SubTitle>{t("dispute.disputes_sending.your_contacts.subtitle")}</SubTitle>
                                    <Description>{t("dispute.disputes_sending.your_contacts.description")}</Description>
                                    <div style={{ display: "flex", alignItems: "center" }}></div>
                                    <Input {...register(`userContactEmail`)} style={{ width: "350px", marginRight: "10px" }} />
                                </div>

                                <div style={{ display: "flex", flexDirection: "column", marginTop: "10px" }}>
                                    <SubTitle>contact email</SubTitle>
                                    <Description>
                                        Go to the contact section, view cookies and privacy policy, check the website on{" "}
                                        <a style={{ color: "blue", textDecoration: "underline" }} href="https://who.is/" target={"_blank"} rel="noreferrer">
                                            {" "}
                                            who.is
                                        </a>
                                        , or check their social media profiles.{" "}
                                        <span style={{ color: "blue", textDecoration: "underline" }}>
                                            <a href={"https://medium.com/@256_43885/how-to-find-contacts-62e2aea92b64"} target={"_blank"} rel={"noreferrer"}>
                                                More tips on how to find contacts
                                            </a>
                                        </span>
                                    </Description>
                                    {fieldsOwnerClaimEmails.map((field, index) => (
                                        <div key={field.id} style={{ display: "flex", alignItems: "center", marginBottom: "8px" }}>
                                            {disable ? (
                                                <Input
                                                    disabled={true}
                                                    {...register(`ownerClaimEmails.${index}`)}
                                                    style={{ width: "350px", marginRight: "10px" }}
                                                />
                                            ) : (
                                                <Input
                                                    {...register(`ownerClaimEmails.${index}`)}
                                                    variant="unstyled"
                                                    style={{
                                                        marginRight: "10px",
                                                        width: "350px",
                                                        border: "1px solid #29CCB1",
                                                        borderRadius: "4px",
                                                        paddingLeft: "12px",
                                                    }}
                                                />
                                            )}
                                            <RemoveButton disabled={disable} onClick={() => removeOwnerClaimEmails(index)} />
                                        </div>
                                    ))}

                                    <AddButton disabled={disable} onClick={() => appendOwnerClaimEmails("")}>
                                        Add more
                                    </AddButton>
                                </div>
                                <div style={{ marginTop: "20px" }}>
                                    <Title>Find emails using a third-party service</Title>

                                    <Description> If the platform finds emails, it will automatically add them to the form</Description>
                                    <div style={{ marginTop: "10px", marginBottom: "10px" }}>
                                        <SharkButton
                                            type="button"
                                            px={8}
                                            py={4}
                                            size="small"
                                            variant="outlined"
                                            buttonType="secondary"
                                            onClick={onFindEmails}
                                            loading={loading}
                                            disabled={loading || disable}
                                        >
                                            {/* <Icon icon={`fluent:add-circle-16-filled`} color={"#666892"} /> */}
                                            Find
                                        </SharkButton>
                                    </div>
                                    {emailFound !== undefined && <Description style={{ color: "red" }}>{` Platform found ${emailFound} emails`}</Description>}
                                </div>
                            </div>
                            <div style={{ marginBottom: "32px" }}>
                                <div style={{ display: "flex", alignItems: "center", marginBottom: "16px" }}>
                                    <RadioButton disabled={disable} {...register("ownerClaimType")} type="radio" value="Via form" />
                                    <Label>By contact form</Label>
                                </div>
                                <Description>If they hid their email but have a contact form, send the text of the claim there and let us know.</Description>
                                <div style={{ display: "flex", marginTop: "8px" }}>
                                    <div style={{ marginRight: "8px" }}>
                                        <SharkButton
                                            type={"button"}
                                            disabled={disable || getValues("ownerClaimType") !== "Via form"}
                                            onClick={pdfGenerate}
                                            variant="outlined"
                                            buttonType="secondary"
                                            size="small"
                                            px={8}
                                            py={4}
                                        >
                                            <Icon icon={"charm:download"} color={"#666892"} hoverColor={"#666892"} />
                                            Download .pdf
                                        </SharkButton>
                                    </div>
                                    <SharkButton
                                        type={"button"}
                                        disabled={disable || getValues("ownerClaimType") !== "Via form"}
                                        onClick={() => {
                                            navigator.clipboard.writeText(getValues("ownerClaimText"));
                                        }}
                                        variant="filled"
                                        buttonType="secondary"
                                        size="small"
                                        px={8}
                                        py={4}
                                    >
                                        <Icon icon={"clarity:copy-line"} color={"#ffff"} hoverColor={"#666892"} />
                                        Copy the claim’s text
                                    </SharkButton>
                                </div>
                            </div>
                            <div style={{ marginBottom: "32px" }}>
                                <div style={{ display: "flex", alignItems: "center", marginBottom: "16px" }}>
                                    <RadioButton disabled={disable} {...register("ownerClaimType")} type="radio" value="Nothing found" />
                                    <Label>Nothing found</Label>
                                </div>
                                <Description>
                                    Sometimes it happens. So, make sure you’ve tried everything below and let us know — we will prepare an immediate claim for
                                    the provider.
                                </Description>
                            </div>
                        </div>
                    </>
                )}

                {(disputeStatus === "Draft: Provider" ||
                    disputeStatus === "In Progress: Provider" ||
                    disputeStatus === "Claim Sent: Provider" ||
                    disputeStatus === "Letter Sent: Provider") && (
                    <>
                        <Title>Step 3. Sending the claim to the provider</Title>
                        <Title>By email</Title>

                        <div style={{ marginBottom: "32px", width: "380px" }}>
                            <div style={{ display: "flex", flexDirection: "column" }}>
                                <SubTitle>{t("dispute.disputes_sending.your_contacts.subtitle")}</SubTitle>
                                <Description>{t("dispute.disputes_sending.your_contacts.description")}</Description>
                                <div style={{ display: "flex", alignItems: "center" }}></div>
                                <Input {...register(`userContactEmail`)} style={{ width: "350px", marginRight: "10px" }} />
                            </div>
                            <div style={{ display: "flex", flexDirection: "column", marginTop: "10px" }}>
                                <SubTitle>contact email</SubTitle>
                                <Description
                                    dangerouslySetInnerHTML={{
                                        __html: t("dispute.disputes_sending.provider.description", {
                                            name: "[Provider’s name]",
                                            link: '<a style="color: blue, textDecoration: underline" href="https://who.is/" target={"_blank"} rel="noreferrer"> who.is </a>',
                                        }),
                                    }}
                                />
                                {fieldsProviderClaimEmails.map((field, index) => (
                                    <div key={field.id} style={{ display: "flex", alignItems: "center", marginBottom: "8px" }}>
                                        {disable ? (
                                            <Input
                                                disabled={true}
                                                {...register(`providerClaimEmails.${index}`)}
                                                style={{ width: "350px", marginRight: "10px" }}
                                            />
                                        ) : (
                                            <Input
                                                {...register(`providerClaimEmails.${index}`)}
                                                variant="unstyled"
                                                required={true}
                                                style={{
                                                    marginRight: "10px",
                                                    width: "350px",
                                                    border: "1px solid #29CCB1",
                                                    borderRadius: "4px",
                                                    paddingLeft: "12px",
                                                }}
                                            />
                                        )}
                                        <RemoveButton disabled={disable} onClick={() => removeProviderClaimEmails(index)} />
                                    </div>
                                ))}

                                <AddButton disabled={disable} onClick={() => appendProviderClaimEmails("")}>
                                    Add more
                                </AddButton>
                            </div>
                            <div style={{ marginTop: "20px" }}>
                                <Title>Find emails using a third-party service</Title>

                                <Description> If the platform finds emails, it will automatically add them to the form</Description>
                                <div style={{ marginTop: "10px", marginBottom: "10px" }}>
                                    <SharkButton
                                        type="button"
                                        px={8}
                                        py={4}
                                        size="small"
                                        variant="outlined"
                                        buttonType="secondary"
                                        onClick={onFindEmailsProvider}
                                        loading={loading}
                                        disabled={loading || disable}
                                    >
                                        {/* <Icon icon={`fluent:add-circle-16-filled`} color={"#666892"} /> */}
                                        Find
                                    </SharkButton>
                                </div>
                                {emailFound !== undefined && <Description style={{ color: "red" }}>{` Platform found ${emailFound} emails`}</Description>}
                            </div>
                        </div>
                    </>
                )}
                {(disputeStatus === "Draft: Google" ||
                    disputeStatus === "In Progress: Google" ||
                    disputeStatus === "Claim Sent: Google" ||
                    disputeStatus === "Under Court" ||
                    disputeStatus === "Closed") && (
                    <>
                        <Title>Step 3. Sending the claim to Google law office</Title>
                        <div style={{ width: "380px" }}>
                            <div style={{ display: "flex", flexDirection: "column" }}>
                                <SubTitle>{t("dispute.disputes_sending.your_contacts.subtitle")}</SubTitle>
                                <Description>{t("dispute.disputes_sending.your_contacts.description")}</Description>
                                <div style={{ display: "flex", alignItems: "center" }}></div>
                                <Input {...register(`userContactEmail`)} style={{ width: "350px", marginRight: "10px" }} />
                            </div>
                            <div style={{ marginBottom: "16px", marginTop: 10 }}>
                                <div style={{ display: "flex", flexDirection: "column" }}>
                                    <SubTitle>at First, </SubTitle>
                                    <Description>Log into your Google account and follow the link:</Description>
                                </div>
                            </div>
                            <div style={{ marginBottom: "16px" }}>
                                <div style={{ display: "flex", alignItems: "center" }}>
                                    <Icon icon={"ic:link"} color={"#979CBC"} rotate={-30} style={{ marginRight: "8px" }} />
                                    <a
                                        href="https://www.google.com/webmasters/tools/legal-removal-request?complaint_type=dmca&visit_id=637932436777923240-611449655&hl=en&rd=1"
                                        style={{
                                            marginRight: "8px",
                                            fontWeight: "400",
                                            fontSize: "16px",
                                            lineHeight: "20px",
                                            color: "#4700E5",
                                        }}
                                        target="_blank"
                                    >
                                        {" "}
                                        Legal removal request{" "}
                                    </a>
                                    <div
                                        style={{
                                            border: "1px solid #DDDEEE",
                                            borderRadius: "4px",
                                            padding: "5px",
                                        }}
                                    >
                                        <Icon icon={"ic:open-in-new"} color={"#666892"} />
                                    </div>
                                </div>
                            </div>
                            <div style={{ marginBottom: "16px" }}>
                                <div style={{ display: "flex", flexDirection: "column" }}>
                                    <SubTitle>Then </SubTitle>
                                    <Description>Identify and describe the copyrighted material,</Description>
                                </div>
                            </div>
                            <div style={{ marginBottom: "16px" }}>
                                <div style={{ display: "flex", flexDirection: "column" }}>
                                    <SubTitle>And last </SubTitle>
                                    <Description>And tell us you did it!</Description>
                                </div>
                            </div>
                        </div>
                    </>
                )}
            </Wrapper>

            <Footer>
                <Group>
                    <Text size={"sm"} weight={"400"} color={"#666892"}>
                        Document {getValues("documentName")}
                    </Text>
                </Group>
                <Group spacing={12}>
                    <Group spacing={8}></Group>
                    {getValues("status") === "Claim Sent: Google" || getValues("status") === "Under Court" || getValues("status") === "Closed" ? null : (
                        <SharkButton type="button" onClick={onOpenModal} variant="filled" buttonType="secondary" size="small" px={8} py={4}>
                            Send the claim!
                        </SharkButton>
                    )}
                </Group>
            </Footer>
        </Root>
    );
};

export default DisputesSending;

const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
`;

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    margin: 32px 32px 72px 48px;
`;

const Footer = styled(Group)`
    position: relative;
    background-color: #ffffff;
    border-top: 1px solid #dddeee;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
    padding: 14px 16px;
    height: 56px;
    overflow: hidden;
    justify-content: space-between;
`;

const Title = styled.span`
    font-weight: 700;
    font-size: 20px;
    line-height: 24px;
    margin-bottom: 16px;
`;

const SubTitle = styled.span`
    font-weight: 700;
    font-size: 12px;
    line-height: 16px;
    text-transform: uppercase;
    margin-bottom: 6px;
`;

const Description = styled.div`
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;
    color: #666892;
    margin-bottom: 8px;
    a {
        color: blue;
        text-decoration: underline;
    }
`;

const Label = styled.span`
    font-weight: 700;
    font-size: 20px;
    line-height: 24px;
    margin-left: 8px;
`;

