import React, {
    PropsWithChildren,
    useContext,
    useEffect,
    useMemo,
    useState,
} from "react";
import { ScanClient } from "./ScanClient";
import { useWhoAmi } from '../Auth/hooks/useWhoAmi';

const ChatContext = React.createContext<{
    client: ScanClient;
    connected: boolean;
} | null>(null);

const client = new ScanClient();

export const ScanClientProvider = (props: PropsWithChildren<{}>) => {
    const [connected, setConnected] = useState(false);
    const { data } = useWhoAmi()
    
    useEffect(() => {
        const openListener = () => {
            setConnected(true);
        };
        const closeListener = () => setConnected(false);
        client.openEvent.addListener(openListener);
        client.closeEvent.addListener(closeListener);
        return () => client.close();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    
    useEffect(() => {
        if (!data) return
        
        client.connectToServer(data.websocket);
        
        return () => client.close();
    }, [data]);
    
    const value = useMemo(() => ({ client, connected }), [connected]);
    
    return <ChatContext.Provider value={value} {...props} />;
};

export const useScanClient = () => {
    const context = useContext(ChatContext);
    if (context === null) {
        throw new Error("No client in context");
    }
    return context;
};
