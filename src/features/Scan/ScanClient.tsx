import CustomEvent from "features/Websocket/CustomEvent";
import { WsClient } from "features/Websocket";

export class ScanClient extends WsClient {
    newMessageEvent: CustomEvent<void> = new CustomEvent<void>();

    connectToServer(url: string) {
        super.connect(url);
    }
}

export enum ScanEvent {
    SCAN_CURRENT_STATUS = "scanCurrentStatus",
}

export enum ScanCurrentStatus {
    PAYMENT_RECEIVED = "paymentReceived",
    PREPARING_WORK = "preparingWork",
    SEND_TO_CHECK_SUCCESS = "sendToCheckSuccess",
    SEND_TO_CHECK_ERROR = "sendToCheckError",
    METADATA_UPLOADED = "metadataUploaded",
    SCAN_FAILED = "scanFailed",
    SCAN_SUCCESS = "scanSuccess",
    MINTED = "minted",
    ALERT_CREATED = "alertCreated",
    SEND_TO_ESTIMATE_ERROR = "sendToEstimateError",
    SEND_TO_ESTIMATE_SUCCESS = "sendToEstimateSuccess",
    ESTIMATE_SUCCESS = "estimatedSuccess",
}
