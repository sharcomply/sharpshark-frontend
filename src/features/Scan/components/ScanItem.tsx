import styled from "@emotion/styled";
import { Group, Text } from "@mantine/core";

// Props
type ComponentProps = {
    item: ScanSimilarContent;
};

// Component
export const ScanItem = ({ item }: ComponentProps) => (
    <Root>
        <td>
            <Group pr={8} pl={32} align="center" sx={{ height: 42 }} spacing={12}>
                <Text
                    size={"sm"}
                    color={"#0A001E"}
                    weight={"500"}
                    transform={"capitalize"}
                    style={{
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                    }}
                >
                    {item.title}
                </Text>
            </Group>
        </td>

        <td>
            <Group px={8} align="center" sx={{ height: 42 }} spacing={12}>
                <Text
                    size={"sm"}
                    color={"#0A001E"}
                    weight={"400"}
                    transform={"capitalize"}
                    style={{
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                    }}
                >
                    {item.introduction}
                </Text>
            </Group>
        </td>

        <td>
            <Group px={8} position="apart" align="center" sx={{ height: 42 }}>
                    <Text size={"sm"} color={"#666892"} weight={"400"} style={{
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                    }}>
                <a href={item.url} target="_blank" rel="noreferrer">
                        {item.url}
                </a>
                    </Text>
            </Group>
        </td>

        <td>
            <Group pl={8} pr={32} position="right" align="center" sx={{ height: 42 }}>
                <Text size={"sm"} color={"#666892"} weight={"400"}>
                    {(item.similarity.toFixed(2))}%
                </Text>
            </Group>
        </td>
    </Root>
);

// Styling
const Root = styled.tr`
    transition: background-color 200ms linear;
    border-top: 1px solid #dddeee;

    &:hover {
        background-color: #edeeff;
    }
`;
