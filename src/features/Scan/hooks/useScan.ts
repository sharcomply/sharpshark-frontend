import { useQuery } from 'react-query';
import { backend } from 'api';


export default function useScan(id?: string) {
    const { data: scan, ...options } = useQuery([ 'SCAN', id], () => backend.scans.getScan(id), {
        enabled: !!id,
        retry: false,
        keepPreviousData: true
    });
    
    return {
        scan,
        ...options
    };
}
