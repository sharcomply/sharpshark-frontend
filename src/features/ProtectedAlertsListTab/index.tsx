import { Group, Stack } from "@mantine/core";
import InfoButton from "components/InfoButton";
import { TabLink } from "components/TabLink";
import useCurrentProtected from "features/ProtectedDetails/hooks/useCurrentProtected";
import { useTranslation } from "react-i18next";
import { Outlet, useParams } from 'react-router-dom';
import useAlertList from '../ProtectedItemAlertsList/hooks/useAlertList';
import { AlertBox } from '../../components/AlertBox';
import { useMemo } from 'react';

//Component
export default function ProtectedAlertsListTab() {
    const { protect } = useCurrentProtected();
    const { protectId } = useParams();
    const { alerts } = useAlertList({ isSkipped: false, documentId: protectId }, protectId, {
        refetchOnMount: false
    });
    const {t} = useTranslation();
    
    const hasUnreadAlerts = useMemo(() => {
        const list = alerts?.items.filter((item) => !item.isRead) || []
        return list.length > 0
    }, [alerts])
    
    return (
        <Stack>
            <Group position={"apart"} spacing={16}>
                <Group spacing={12}>
                    <TabLink variant="outlined" to={`/protected/${protect?._id}/alerts/new`} label={t("alerts.tabs.new")} />
                    <TabLink variant="outlined" to={`/protected/${protect?._id}/alerts/not-actual`} label={t("alerts.tabs.not-actual")} />
                </Group>
                <InfoButton />
            </Group>
    
            {hasUnreadAlerts && (
                <AlertBox variant={'warning'} text={t("alerts.new_allert")} />
            )}
            
            <Outlet />
        </Stack>
    );
}
