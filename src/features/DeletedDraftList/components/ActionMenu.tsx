// Props
import { Group, Popover, Text } from '@mantine/core';
import Icon from '../../../components/Icon';
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

// Variables
const rootStyles = {
    background: '#FFFFFF',
    border: '1px solid #DDDEEE',
    boxShadow: ' 0px 2px 12px rgba(0, 0, 0, 0.1)',
    radius: 4,
    overflow: 'hidden'
};


type ComponentProps = {
    draftId: string;
    onRestore?: () => void;
}

// Component
export function ActionMenu({ draftId, onRestore }: ComponentProps) {
    const [ opened, setOpened ] = useState(false);
    const {t} = useTranslation()
    const control = (
        <Group align={'center'} position={'center'} px={4} onClick={() => setOpened(value => !value)}>
            <Icon icon={'ic:sharp-more-vert'} size={24} color={'#666892'} hoverColor={'#666892'}/>
        </Group>
    );
    
    return (
        <Popover
            opened={opened}
            target={control}
            onClose={() => setOpened(false)}
            width={160}
            radius={4}
            position="bottom"
            placement="end"
            transition={'pop-top-right'}
            styles={{ body: rootStyles, inner: { padding: 0 } }}
        >
            {/* View And Edit */}
            <Link to={`/drafts/${draftId}`}>
                <MenuItem px={16} py={12}>
                    <Text size={'sm'} weight={'500'}>{t('buttons.view_document_btn')}</Text>
                </MenuItem>
            </Link>
            
            {/* Protect */}
            <MenuItem px={16} py={12} onClick={onRestore}>
                <Text size={'sm'} weight={'500'}>
                    {t("buttons.restore_btn")}
                </Text>
            </MenuItem>
            
            {/* Delete */}
            <MenuItem px={16} py={12} className="disabled">
                <Text size={'sm'} weight={'500'}>
                    × {t('buttons.delete_forever_btn')}
                    <br />
                    {t('common.coming_soon')}
                </Text>
            </MenuItem>
        </Popover>
    );
}

// Styling
const MenuItem = styled(Group)`
    cursor: pointer;
    color: #666892;
    
    &:hover {
        background-color: #4700E5;
        color: #FFFFFF;
    }
    &.disabled {
        color: #bbbed8;
    }

    &.disabled:hover {
        background-color: #f7f7fa;
        color: #8b8fad;
    }
`;

// const DeleteItem = styled(Group)`
//     cursor: pointer;
//     color: #E50C54;
    
//     &:hover {
//         background-color: #E50C54;
//         color: #FFFFFF;
//     }
// `;
