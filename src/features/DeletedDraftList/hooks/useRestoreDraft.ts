import { backend } from "api";
import { useSidebarCounts } from "features/Sidebar/hooks/useSidebarCounts";
import { useCallback } from "react";
import { useMutation, useQueryClient } from "react-query";
import { useNavigate } from "react-router-dom";

export const useRestoreDraft = (draftId?: string, redirect = false) => {
    const queryClient = useQueryClient();
    const navigate = useNavigate()
    const {refetch} = useSidebarCounts()

    const { mutateAsync, isLoading } = useMutation(() => backend.drafts.recoverDraft(draftId), {
        onSuccess: async () => {
            await Promise.all([
                queryClient.invalidateQueries(['DELETED_DRAFT_LIST']),
                queryClient.invalidateQueries(['DRAFT_LIST']),
                queryClient.invalidateQueries(['DRAFT']),
            ]);
        }
    })

    const handleRestore = useCallback(async () => {
        if (!draftId) return;
        await mutateAsync();
        refetch()
        if (!redirect) return
        navigate('/drafts/deleted', { replace: true });
    }, [draftId, mutateAsync, redirect, navigate, refetch]);

    return {
        handleRestore,
        isLoading
    }
}
