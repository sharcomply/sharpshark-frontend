import { useQuery } from "react-query";
import { backend } from "api";

export default function useDeletedDraftList() {
    const { data: drafts, ...options } = useQuery(["DELETED_DRAFT_LIST"], () => backend.drafts.getDeletedDraftList(), {
        keepPreviousData: true,
    });

    return {
        drafts,
        ...options,
    };
}
