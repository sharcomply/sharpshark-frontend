import styled from "@emotion/styled";
import { useMediaQuery } from "@mantine/hooks";
import Plansh from "features/Icons/Plansh";
import Smaills from "features/Icons/Smaills";
import React from "react";
import { useTranslation } from "react-i18next";

// Props
// Component
export default function PlatformWarning() {
    const tablet = useMediaQuery("(max-width: 1030px)");
    const mobile = useMediaQuery("(max-width: 600px)");
    const {t} = useTranslation()
    // SHOW DESKTOP
    if (!tablet && !mobile) {
        return <div></div>
    }


    const TabletContent = () => (
        <React.Fragment>
            <TabletImage>
                <Plansh />
            </TabletImage>
            <TabletText>
                <span> {t("warnings.rotate_horizontally")} </span>
            </TabletText>
        </React.Fragment>
    );

    const MobileContent = () => (
        <React.Fragment>
            <MobileImage>
                <Smaills />
            </MobileImage>
            <MobileText>
                <p> {t("warnings.dont_support_mobile")} </p>
                <p> {t("warnings.enjoy_desktop")}</p>
            </MobileText>
        </React.Fragment>
    );

    return (
        <Root>
            {/* TABLET CONTENT */}
            {tablet && !mobile && TabletContent()}

            {/* MOBILE CONTENT */}
            {tablet && mobile && MobileContent()}
        </Root>
    );
}

// Styling
const Root = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 100vh;
    padding: 20px;
`;

const TabletImage = styled.div`
    font-size: 250px;
    color: #32313e;
    display: flex;
`;

const TabletText = styled.div`
    text-align: center;
    margin-top: 60px;
    font-size: 24px;
    color: #666892;
    font-weight: 500;
`;

const MobileImage = styled.div`
    font-size: 55px;
    color: #32313e;
    display: flex;
`;

const MobileText = styled.div`
    text-align: center;
    margin-top: 60px;
    font-size: 24px;
    color: #666892;
    font-family: SuisseIntl, sans-serif;
    font-weight: 500;

    & p:not(:last-child) {
        display: block;
        text-align: center;
        margin-bottom: 25px;
    }
`;
