import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import DashboardLayout from "features/Layouts/DashboardLayout";
import SettingsPage from "pages/Settings.page";
import ProtectedPage from "pages/protected/[protectedId]";
import VerifyPage from "pages/auth/Verify";
import { ProtectedContent } from "features/ProtectedDetails/contents/ProtectedContent";
import { Redirect } from "components/Redirect";
import DraftPage from "pages/drafts/[draftId]";
import DraftListPage from "pages/drafts";
import DraftList from "features/DraftList";
import DeletedDraftList from "features/DeletedDraftList";
import ProtectedListPage from "pages/protected";
import ProtectedList from "features/ProtectedList";
import DraftCreatePage from "pages/drafts/create";
import DraftCreate from "features/DraftCreate";
import AlertsListPage from "pages/alerts";
import AlertsList from "features/AlertsList";
import AlertsNotActualList from "features/AlertsNotActualList";
import ProtectedAlertsListTab from "features/ProtectedAlertsListTab";
import ProtectedItemAlertsList from "features/ProtectedItemAlertsList";
import CertificatePage from "pages/Certificate.page";
import ProtectedRights from "features/ProtectedRights";
import { NewDraftVersion } from "features/NewDraftVersion";
import { ProtectedDetails } from "features/ProtectedDetails";
import WalletsTab from "features/Settings/Wallets";
import InfoTab from "features/Settings/Info";
import PricingTab from "features/Settings/Pricing";
import DisputCreatePage from "pages/disputes/create";
import DisputListPage from "pages/disputes";
import DisputClaimTextPage from "pages/disputes/claimText";
import DisputesTable from "features/DisputesList/DisputesTable";
import ProtectedDisputesListTab from "../ProtectedDisputesListTab";
import DisputesTableProtect from "../DisputesList/DisputesTableProtect";
import ComingSoonPage from "../../pages/ComingSoonPage";

export default function SecureRoutes() {
    return (
        <Routes>
            <Route path="/" element={<DashboardLayout />}>
                <Route index element={<Navigate replace to="/drafts" />} />

                <Route path="drafts" element={<Outlet />}>
                    <Route index element={<Navigate replace to="/drafts/working-on" />} />
                    <Route path="create" element={<DraftCreatePage />}>
                        <Route path="" element={<DraftCreate />} />
                    </Route>
                    <Route path=":draftId" element={<DraftPage />} />
                    <Route path="" element={<DraftListPage />}>
                        <Route path="working-on" element={<DraftList />} />
                        <Route path="deleted" element={<DeletedDraftList />} />
                    </Route>
                </Route>

                <Route path="protected" element={<Outlet />}>
                    <Route index element={<Navigate replace to="/protected/locked" />} />
                    <Route path="new" element={<NewDraftVersion />} />
                    <Route path=":protectId" element={<ProtectedPage />}>
                        <Route index element={<Redirect to={({ protectId }) => `/protected/${protectId}/content`} />} />
                        <Route element={<ProtectedDetails />}>
                            <Route path="content" element={<ProtectedContent />} />
                            <Route path="about" element={<ProtectedRights />} />
                            <Route path="alerts" element={<ProtectedAlertsListTab />}>
                                <Route index element={<Redirect to={({ protectId }) => `/protected/${protectId}/alerts/new`} />} />
                                <Route path="new" element={<ProtectedItemAlertsList />} />
                                <Route path="not-actual" element={<AlertsNotActualList />} />
                            </Route>
                            <Route path="disputes" element={<ProtectedDisputesListTab />}>
                                <Route index element={<Redirect to={({ protectId }) => `/protected/${protectId}/disputes/draft`} />} />
                                <Route
                                    path="draft"
                                    element={<DisputesTableProtect keyStatus="DRAFT" status="Draft: Website Owner,Draft: Provider,Draft: Google" />}
                                />
                                <Route
                                    path="in-progress"
                                    element={
                                        <DisputesTableProtect
                                            keyStatus="IN_PROGRESS"
                                            status="In Progress: Website Owner,In Progress: Provider,In Progress: Google,Under Court"
                                        />
                                    }
                                />
                                <Route
                                    path="claim-sent"
                                    element={
                                        <DisputesTableProtect
                                            keyStatus="CLAIM_SENT"
                                            status="Claim Sent: Website Owner,Claim Sent: Provider,Claim Sent: Google"
                                        />
                                    }
                                />
                                <Route path="ended" element={<DisputesTableProtect keyStatus="ENDED" status="Closed" />} />
                            </Route>
                        </Route>
                    </Route>
                    <Route path="" element={<ProtectedListPage />}>
                        <Route path="locked" element={<ProtectedList />} />
                    </Route>
                </Route>

                <Route path="alerts" element={<Outlet />}>
                    <Route index element={<Navigate replace to="/alerts/new" />} />
                    <Route path="" element={<AlertsListPage />}>
                        <Route path="new" element={<AlertsList />} />
                        <Route path="not-actual" element={<AlertsNotActualList />} />
                    </Route>
                </Route>

                <Route path="settings" element={<Outlet />}>
                    <Route index element={<Navigate replace to="/settings/wallets" />} />
                    <Route path="" element={<SettingsPage />}>
                        <Route path="wallets" element={<WalletsTab />} />
                        <Route path="info" element={<InfoTab />} />
                        <Route path="pricing" element={<PricingTab />} />
                    </Route>
                </Route>

                <Route path="disputes" element={<Outlet />}>
                    <Route index element={<Navigate replace to="/disputes/draft-website-owner" />} />

                    <Route path="" element={<DisputListPage />}>
                        <Route path="draft-website-owner" element={<DisputesTable keyStatus="DRAFT_WEBSITE_OWNER" status="Draft: Website Owner" />} />
                        <Route path="draft-provider" element={<DisputesTable keyStatus="DRAFT_PROVIDER" status="Draft: Provider" />} />
                        <Route path="draft-google" element={<DisputesTable keyStatus="DRAFT_GOOGLE" status="Draft: Google" />} />
                        <Route
                            path="in-progress-website-owner"
                            element={
                                <DisputesTable
                                    keyStatus="IN_PROGRESS_WEBSITE_OWNER"
                                    status="In Progress: Website Owner"
                                />
                            }
                        />
                        <Route
                            path="in-progress-provider"
                            element={
                                <DisputesTable
                                    keyStatus="IN_PROGRESS_PROVIDER"
                                    status="In Progress: Provider"
                                />
                            }
                        />
                        <Route
                            path="in-progress-google"
                            element={
                                <DisputesTable
                                    keyStatus="IN_PROGRESS_GOOGLE"
                                    status="In Progress: Google,Under Court"
                                />
                            }
                        />
                        <Route
                            path="claim-sent-website-owner"
                            element={<DisputesTable keyStatus="CLAIM_SENT_WEBSITE_OWNER" status="Claim Sent: Website Owner" />}
                        />
                        <Route
                            path="claim-sent-provider"
                            element={<DisputesTable keyStatus="CLAIM_SENT_PROVIDER" status="Claim Sent: Provider" />}
                        />
                        <Route
                            path="claim-sent-google"
                            element={<DisputesTable keyStatus="CLAIM_SENT_GOOGLE" status="Claim Sent: Google" />}
                        />

                        <Route
                            path="letter-sent-website-owner"
                            element={<DisputesTable keyStatus="LETTER_SENT_WEBSITE_OWNER" status="Letter Sent: Website Owner" />}
                        />
                        <Route
                            path="letter-sent-provider"
                            element={<DisputesTable keyStatus="LETTER_SENT_PROVIDER" status="Letter Sent: Provider" />}
                        />
                        <Route
                            path="letter-sent-google"
                            element={<DisputesTable keyStatus="LETTER_SENT_GOOGLE" status="Letter Sent: Google" />}
                        />
                        
                        <Route path="ended" element={<DisputesTable keyStatus="ENDED" status="Closed" />} />
                        <Route path="not-relevant" element={<DisputesTable keyStatus="NOTRELEVANT" status="NotRelevant" />} />
                    </Route>
                    <Route path=":disputId" element={<DisputCreatePage />}>
                        <Route path="" element={<DisputCreatePage />}>
                            <Route path="collecting-evidence" element={<DisputCreatePage />} />
                            <Route path="generating-claim" element={<DisputCreatePage />} />
                            <Route path="sending" element={<DisputCreatePage />} />
                        </Route>
                    </Route>
                    <Route path="create" element={<Outlet />}>
                        <Route path=":alertId" element={<DisputCreatePage />}>
                            <Route path="" element={<DisputCreatePage />}>
                                <Route path="collecting-evidence" element={<DisputCreatePage />} />
                                <Route path="generating-claim" element={<DisputCreatePage />} />
                                <Route path="sending" element={<DisputCreatePage />} />
                            </Route>
                        </Route>
                    </Route>
                </Route>
            </Route>

            <Route path="/auth" element={<Outlet />}>
                <Route path="verify" element={<VerifyPage />} />
            </Route>

            <Route path="/certificate" element={<Outlet />}>
                <Route path=":draftId" element={<CertificatePage />} />
            </Route>
            <Route path="/claim_text" element={<Outlet />}>
                <Route path=":disputId" element={<DisputClaimTextPage />} />
            </Route>
            <Route path="/coming-soon-page" element={<ComingSoonPage />} />

            <Route path="*" element={<Navigate replace to="/drafts" />} />
        </Routes>
    );
}
