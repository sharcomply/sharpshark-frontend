import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import DashboardLayout from "features/Layouts/DashboardLayout";

import DisputesPage from "pages/Disputes.page";
import VerifyPage from "pages/auth/Verify";
import DraftListPage from "pages/drafts";
import DraftList from "features/DraftList";
import DeletedDraftList from "features/DeletedDraftList";
import ProtectedListPage from "pages/protected";
import ProtectedList from "features/ProtectedList";
import DraftCreatePage from "pages/drafts/create";
import DraftCreate from "features/DraftCreate";
import AlertsListPage from "pages/alerts";
import AlertsList from "features/AlertsList";
import AlertsNotActualList from "features/AlertsNotActualList";
import CertificatePage from "pages/Certificate.page";

export default function PublicRoutes() {
    return (
        <Routes>
            <Route path="/" element={<DashboardLayout />}>
                <Route index element={<Navigate replace to="/drafts" />} />

                <Route path="drafts" element={<Outlet />}>
                    <Route index element={<Navigate replace to="/drafts/working-on" />} />
                    <Route path="" element={<DraftListPage />}>
                        <Route path="working-on" element={<DraftList />} />
                        <Route path="deleted" element={<DeletedDraftList />} />
                    </Route>
                    <Route path="create" element={<DraftCreatePage />}>
                        <Route path="" element={<DraftCreate />} />
                    </Route>
                </Route>

                <Route path="protected" element={<Outlet />}>
                    <Route index element={<Navigate replace to="/protected/locked" />} />
                    <Route path="" element={<ProtectedListPage />}>
                        <Route path="locked" element={<ProtectedList />} />
                    </Route>
                </Route>

                <Route path="alerts" element={<Outlet />}>
                    <Route index element={<Navigate replace to="/alerts/new" />} />
                    <Route path="" element={<AlertsListPage />}>
                        <Route path="new" element={<AlertsList />} />
                        <Route path="not-actual" element={<AlertsNotActualList />} />
                    </Route>
                </Route>
                <Route path="disputes" element={<DisputesPage />} />
            </Route>

            <Route path="/auth" element={<Outlet />}>
                <Route path="verify" element={<VerifyPage />} />
            </Route>

            <Route path="/certificate" element={<Outlet />}>
                <Route path=":draftId" element={<CertificatePage />} />
            </Route>

            <Route path="*" element={<Navigate replace to="/drafts" />} />
        </Routes>
    );
}
