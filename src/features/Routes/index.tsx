import SecureRoutes from "./SecureRoutes";
import PublicRoutes from "./PublicRoutes";
import useAuthStore from '../Auth/hooks/useAuthStore';

export default function AppRouter() {
    const { isLoggedIn } = useAuthStore()
    
    return !isLoggedIn ? <PublicRoutes /> : <SecureRoutes />
}
