import React, { useEffect, useMemo, useRef } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import { useModal } from "features/Modals/hooks/useModal";
import { ModalContent, ModalType } from "features/Modals/interfaces";
import useDraftStore from "features/DraftDetails/hooks/useDraftStore";

interface Props {
    newEditorState: string;
    onChangeState?: (props: string) => void;
    placeholder?: string;
    disabled?: boolean;
}

const DraftJs = ({ newEditorState, onChangeState, placeholder, disabled }: Props) => {
    const [editorState, setEditorState] = React.useState(newEditorState);
    const quillRef = useRef<any>(null);
    const { handleOpen } = useModal();
    const { imageTextEditor, setImageInTextEditor } = useDraftStore();
    const onChange = (editorState: any) => {
        setEditorState(editorState);
        onChangeState && onChangeState(editorState);
    };

    const addImage = () => {
        handleOpen({ type: ModalType.SHARED, content: ModalContent.TextEditorAddImage });
    };

    useEffect(() => {
        if (!imageTextEditor) return;
        const editor = quillRef.current.getEditor();
        editor.insertEmbed(editor.getSelection(), "image", imageTextEditor);
        setImageInTextEditor(undefined);
    }, [imageTextEditor]);

    const modules = useMemo(
        () => ({
            toolbar: {
                container: [
                    [{ header: [1, 2, 3, 4, 5, 6, false] }],
                    ["bold", "italic", "underline", "strike"],
                    [{ list: "ordered" }, { list: "bullet" }, { indent: "-1" }, { indent: "+1" }],
                    ["image", "link"],
                    [
                        {
                            color: [
                                "#000000",
                                "#e60000",
                                "#ff9900",
                                "#ffff00",
                                "#008a00",
                                "#0066cc",
                                "#9933ff",
                                "#ffffff",
                                "#facccc",
                                "#ffebcc",
                                "#ffffcc",
                                "#cce8cc",
                                "#cce0f5",
                                "#ebd6ff",
                                "#bbbbbb",
                                "#f06666",
                                "#ffc266",
                                "#ffff66",
                                "#66b966",
                                "#66a3e0",
                                "#c285ff",
                                "#888888",
                                "#a10000",
                                "#b26b00",
                                "#b2b200",
                                "#006100",
                                "#0047b2",
                                "#6b24b2",
                                "#444444",
                                "#5c0000",
                                "#663d00",
                                "#666600",
                                "#003700",
                                "#002966",
                                "#3d1466",
                            ],
                        },
                    ],
                ],
                handlers: {
                    image: addImage,
                },
            },
        }),
        []
    );

    return (
        <div style={{ paddingLeft: "16px", paddingRight: "16px", maxWidth: "100%" }}>
            <ReactQuill
                readOnly={disabled}
                theme="snow"
                value={editorState}
                ref={quillRef}
                onChange={onChange}
                style={{ minHeight: "300", height: "300px", wordBreak: "break-word" }}
                modules={modules}
                placeholder={placeholder}
            />
        </div>
    );
};

export default DraftJs;
