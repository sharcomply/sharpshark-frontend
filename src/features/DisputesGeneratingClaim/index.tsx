import styled from "@emotion/styled";
import { Group, Stack, Text } from "@mantine/core";
import { SharkButton } from "components/SharkButton";
import { Control, FieldValues, UseFormRegister, UseFormStateReturn } from "react-hook-form";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Icon from "../../components/Icon";
import { useTranslation } from "react-i18next";
import useDraftStore from "../DraftDetails/hooks/useDraftStore";
import { useUnmount } from "../../hooks/useUnmount";

interface Props {
    control: Control;
    getValues: (props?: string) => string;
    register: UseFormRegister<FieldValues>;
    formState: UseFormStateReturn<FieldValues>;
}

const DisputesGeneratingClaim = ({ getValues, register, formState }: Props) => {
    const { t } = useTranslation();
    const navigate = useNavigate();
    const [disable, setDisable] = useState(false);
    const status = getValues("status");
    const draftStore = useDraftStore();
    const demands = getValues("demands");
    const [claimText, setClaimText] = useState("");
    const ownerClaimText = getValues("ownerClaimText");
    const providerClaimText = getValues("providerClaimText");
    const googleClaimText = getValues("googleClaimText");

    useEffect(() => {
        switch (status) {
            case undefined:
            case "Draft: Website Owner":
            case "In Progress: Website Owner":
            case "Claim Sent: Website Owner":
                setClaimText(ownerClaimText);
                break;
            case "Draft: Provider":
            case "In Progress: Provider":
            case "Claim Sent: Provider":
                setClaimText(providerClaimText);
                break;
            case "Draft: Google":
            case "In Progress: Google":
            case "Claim Sent: Google":
            case "Closed":
            case "Under Court":
                setClaimText(googleClaimText);
                break;

            default:
                setClaimText(ownerClaimText);
                break;
        }
    }, [getValues, googleClaimText, ownerClaimText, providerClaimText, status]);

    useEffect(() => {
        if (formState.isDirty || (demands && !claimText)) {
            draftStore.showDraftAlert(true);
            draftStore.setDraftAlert({
                type: "warn",
                title: t("The data has changed!"),
                message: t('Click the "Update claim" button to update the claim text'),
            });
        } else {
            draftStore.showDraftAlert(false);
            draftStore.setDraftAlert({
                type: undefined,
                title: undefined,
                message: undefined,
            });
        }
    }, [claimText, demands, draftStore, formState.isDirty, t]);
    useUnmount(() => {
        draftStore.showDraftAlert(false);
        draftStore.setDraftAlert({
            type: undefined,
            title: undefined,
            message: undefined,
        });
    });

    useEffect(() => {
        if (status) {
            if (
                status === "In Progress: Website Owner" ||
                status === "In Progress: Provider" ||
                status === "In Progress: Google" ||
                status === "Closed" ||
                status === "Under Court"
            ) {
                setDisable(true);
            } else {
                setDisable(false);
            }
        }
    }, [status]);

    return (
        <Root>
            <Wrapper>
                {(status === "Draft: Website Owner" ||
                    status === "In Progress: Website Owner" ||
                    status === "Claim Sent: Website Owner" ||
                    status === "Letter Sent: Website Owner" ||
                    status === undefined) && (
                    <>
                        <Title>Step 2. Generating your claim to the website owner</Title>
                        {!getValues("ownerClaimText")?.length ? (
                            <Group
                                style={{
                                    border: "1px solid #dddeee",
                                    borderRadius: "8px",
                                    padding: "16px 16px",
                                }}
                            >
                                {!getValues("demands") && <Title>First, select the conflict resolution option in the "Your demand" field</Title>}
                            </Group>
                        ) : (
                            <>
                                <TextArea {...register(`ownerClaimText`)} dangerouslySetInnerHTML={{ __html: getValues("ownerClaimText") }} />
                            </>
                        )}
                    </>
                )}

                {(status === "Draft: Provider" ||
                    status === "In Progress: Provider" ||
                    status === "Claim Sent: Provider" ||
                    status === "Letter Sent: Provider") && (
                    <>
                        <Title>Step 2. Preparing your claim for the provider</Title>
                        {!getValues("providerClaimText")?.length ? (
                            <Group
                                style={{
                                    border: "1px solid #dddeee",
                                    borderRadius: "8px",
                                    padding: "16px 16px",
                                }}
                            ></Group>
                        ) : (
                            <TextArea {...register(`providerClaimText`)} dangerouslySetInnerHTML={{ __html: getValues("providerClaimText") }} />
                        )}
                    </>
                )}

                {(status === "Draft: Google" ||
                    status === "In Progress: Google" ||
                    status === "Claim Sent: Google" ||
                    status === "Letter Sent: Google" ||
                    status === "Under Court" ||
                    status === "Closed") && (
                    <>
                        <Title>Step 2. Preparing your legal reguest for Google</Title>
                        {!getValues("googleClaimText")?.length ? (
                            <Group
                                style={{
                                    border: "1px solid #dddeee",
                                    borderRadius: "8px",
                                    padding: "16px 16px",
                                }}
                            ></Group>
                        ) : (
                            <TextArea {...register(`googleClaimText`)} dangerouslySetInnerHTML={{ __html: getValues("googleClaimText") }} />
                        )}
                    </>
                )}
            </Wrapper>

            <Footer>
                <Group>
                    <SharkButton variant={"outlined"} buttonType={"secondary"} size={"small"} type={"submit"} disabled={disable}>
                        <Group spacing={9}>
                            <Icon icon={"ic:replay"} color={"#666892"} size={16} style={{ cursor: "default" }} />
                            Update claim
                        </Group>
                    </SharkButton>
                    <Text size={"sm"} weight={"400"} color={"#666892"}>
                        Document {getValues("documentName")}
                    </Text>
                </Group>
                <Group spacing={12}>
                    <Group spacing={8}></Group>
                    <SharkButton
                        onClick={() => navigate(window.location.pathname.replace(/[^/]*$/, "sending"), { replace: true })}
                        variant="filled"
                        buttonType="secondary"
                        size="small"
                        px={8}
                        py={4}
                    >
                        Seems good, next
                    </SharkButton>
                </Group>
            </Footer>
        </Root>
    );
};

export default DisputesGeneratingClaim;

const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
`;

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    margin: 32px 32px 72px 48px;
`;

const Footer = styled(Group)`
    position: relative;
    background-color: #ffffff;
    border-top: 1px solid #dddeee;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
    padding: 14px 16px;
    height: 56px;
    overflow: hidden;
    justify-content: space-between;
`;

const Title = styled.span`
    font-weight: 700;
    font-size: 20px;
    line-height: 24px;
    margin-bottom: 16px;
`;
const TextArea = styled.div`
    border: 1px solid #dddeee;
    border-radius: 8px;
    background-color: #f1f3f5;
    padding: 8px 16px;
`;

