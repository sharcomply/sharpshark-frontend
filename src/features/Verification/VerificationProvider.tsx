import { useWhoAmi } from "features/Auth/hooks/useWhoAmi";
import { useModal } from "features/Modals/hooks/useModal";
import { ModalContent, ModalType } from "features/Modals/interfaces";
import { useScanClient } from "features/Scan/ScanProvider";
import { getVerificationStorage } from "helpers/verificationStorage";
import { PropsWithChildren, Fragment, useEffect, useCallback } from "react";

export enum VerificationEvent {
    VERIFICATION_EVENT = "verificationWebhookEvent",
}

export enum VerificationStatus {
    VERIFICATION_SUCCES = "verification.accepted",
    VERIFICATION_DECLINED = "verification.declined",
    VERIFICATION_PENDING = "request.pending",
}

// Component
export default function VerificationProvider({ children }: PropsWithChildren<{}>) {
    const { data } = useWhoAmi();
    const { handleOpen, handleClose, setModalContent, setModalStatus } = useModal();
    const { client, connected } = useScanClient();

    const onVerificationPending = useCallback(() => {
        handleClose();
        handleOpen({ type: ModalType.SHARED, content: ModalContent.CustomModal });
        setModalStatus("Waiting");
        setModalContent("Continue verification on the page");
    }, [handleOpen, setModalContent, setModalStatus, handleClose]);

    const onVerificationSucces = useCallback(() => {
        handleClose();
        handleOpen({ type: ModalType.SHARED, content: ModalContent.CustomModal });
        setModalStatus("Success");
        setModalContent("Verification was successful");
    }, [handleOpen, setModalContent, setModalStatus, handleClose]);

    const onVerificationDeclined = useCallback(() => {
        handleClose();
        handleOpen({ type: ModalType.SHARED, content: ModalContent.CustomModal });
        setModalStatus("Failed");
        setModalContent("Verification failed. Please try again");
    }, [handleOpen, setModalContent, setModalStatus, handleClose]);

    // Listen WS by Status
    useEffect(() => {
        if (!connected) return;
        client.addEventHandler(VerificationStatus.VERIFICATION_PENDING, onVerificationPending);
        client.addEventHandler(VerificationStatus.VERIFICATION_SUCCES, onVerificationSucces);
        client.addEventHandler(VerificationStatus.VERIFICATION_DECLINED, onVerificationDeclined);

        return () => {
            client.removeEventHandler(VerificationStatus.VERIFICATION_PENDING, onVerificationPending);
            client.removeEventHandler(VerificationStatus.VERIFICATION_SUCCES, onVerificationSucces);
            client.removeEventHandler(VerificationStatus.VERIFICATION_DECLINED, onVerificationDeclined);
        };
    }, [client.removeStatusHandler, client.addStatusHandler, connected, client, onVerificationDeclined, onVerificationSucces, onVerificationPending]);

    useEffect(() => {
        if (!data || data?.user?.isVerification || !!getVerificationStorage()) return;
        if (window.location.pathname.includes("certificate")) return;
        handleOpen({ type: ModalType.AUTH, content: ModalContent.KYCVerification });
    }, [data?.user?.isVerification, handleOpen, data]);

    return <Fragment>{children}</Fragment>;
}
