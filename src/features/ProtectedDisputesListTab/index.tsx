import { Group, Stack } from "@mantine/core";
import InfoButton from "components/InfoButton";
import { TabLink } from "components/TabLink";
import useCurrentProtected from "features/ProtectedDetails/hooks/useCurrentProtected";
import { useTranslation } from "react-i18next";
import { Outlet } from "react-router-dom";

//Component
export default function ProtectedDisputesListTab() {
    const { protect } = useCurrentProtected();

    const { t } = useTranslation();

    return (
        <Stack>
            <Group position={"apart"} spacing={16}>
                <Group spacing={12}>
                    <TabLink variant="outlined" to={`/protected/${protect?._id}/disputes/draft`} label={t("Draft")} />
                    <TabLink variant="outlined" to={`/protected/${protect?._id}/disputes/in-progress`} label={t("In progress")} />
                    <TabLink variant="outlined" to={`/protected/${protect?._id}/disputes/claim-sent`} label={t("Claim Sent")} />
                    <TabLink variant="outlined" to={`/protected/${protect?._id}/disputes/ended`} label={t("Ended")} />
                </Group>
                <InfoButton />
            </Group>
            <Outlet />
        </Stack>
    );
}
