import styled from "@emotion/styled";
import { Tooltip } from "@mantine/core";
import Icon from "components/Icon";
import { ReactNode } from "react";

// Props
interface PageTitleProps {
    title?: string;
    icon?: string;
    iconColor?: string;
    tooltipText?: ReactNode;
    actions?: ReactNode;
}

// Component
export default function PageTitle(props: PageTitleProps) {
    const { title, icon = "ep:question-filled", iconColor = "#9FA2B4", actions, tooltipText } = props;

    return (
        <Root>
            <LeftSide>
                {props.tooltipText && (
                    <Tooltip
                        label={tooltipText}
                        position="bottom"
                        placement="center"
                        transition="fade"
                        transitionDuration={300}
                        transitionTimingFunction="ease"
                        withArrow={false}
                        styles={{ body: { backgroundColor: "#666892" } }}
                    >
                        <Icon icon={icon} color={iconColor} hoverColor={iconColor} />
                    </Tooltip>
                )}

                <Text>{title}</Text>
            </LeftSide>

            <RightSide>{actions}</RightSide>
        </Root>
    );
}

// Styling
const Root = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    height: 42px;
    flex-shrink: 0;
`;

const LeftSide = styled.div`
    display: flex;

    & > * {
        display: flex;
        margin-right: 8px;
    }
`;

const RightSide = styled.div`
    display: flex;

    & > * {
        display: flex;
        margin-left: 20px;
    }
`;

const Text = styled.h1`
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    font-size: 24px;
    font-weight: 700;
    line-height: 30px;
    color: #0a001e;
`;
