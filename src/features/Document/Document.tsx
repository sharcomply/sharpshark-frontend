import styled from '@emotion/styled';
import { Group, Stack, Text, Textarea } from '@mantine/core';
import Icon from '../../components/Icon';
import { Fragment, useCallback, useRef } from 'react';
import { Dropzone } from '@mantine/dropzone';
import { formatBytes } from '../../helpers/formatBytes';
import ComingSoonWrapper from 'components/ComingSoonWrapper';
import { useTranslation } from 'react-i18next';


interface DocumentProps {
    title?: string;
    text?: string;
    file?: File | undefined;
    onTitle?: (str: string) => void;
    onText?: (str: string) => void;
    onFile?: (file: File | undefined) => void;
}


export const Document = ({ title, text, file, onTitle, onText, onFile }: DocumentProps) => {
    const fileRef = useRef<any>(null);
    const {t} = useTranslation()
    const selectFile = useCallback(() => {
        if (!fileRef.current) return;
        fileRef.current();
    }, []);
    
    const handleText = useCallback((text: string) => {
        if (file && onFile) onFile(undefined);
        
        onText && onText(text);
    }, [ file, onFile, onText ]);
    
    const handleFile = useCallback((file: File) => {
        if (text && onText) onText('');
        
        onFile && onFile(file);
    }, [ onFile, onText, text ]);
    
    const clearFile = useCallback(() => {
        if (!onFile) return;
        onFile(undefined);
    }, [ onFile ]);
    
    return (
        <Fragment>
            <Stack pt={16} px={16} spacing={0} sx={{ flex: 1 }}>
                <Input
                    id={'title'}
                    placeholder={t("drafts.title_placeholder")}
                    variant={'unstyled'}
                    value={title}
                    autosize
                    onChange={(e) => onTitle && onTitle(e.target.value)}
                    px={16}
                    py={8}
                />
                
                <Stack sx={{ flex: 1, position: 'relative' }}>
                    {!file && (
                        <TextArea
                            id={'text'}
                            value={text}
                            onChange={(e) => handleText(e.target.value)}
                            placeholder={t("drafts.text_palaceholder")}
                            autosize
                            variant={'unstyled'}
                            styles={{
                                root: { flex: 1 },
                                wrapper: { height: '100%' },
                                input: { minHeight: '100%!important' },
                            }}
                            px={16}
                            py={8}
                        />
                    )}
                    
                    {!!file && (
                        <Group px={16} py={8} spacing={6}>
                            <Text color={'#666892'} size={'md'}>{t("drafts.file-select-text.text_1")}</Text>
                            <Text color={'#5A3DD1'} size={'md'} weight={'500'}>{file.name}</Text>
                            <Text color={'#666892'} size={'md'}>({formatBytes(file.size)})</Text>
                            <Text color={'#E50C54'} size={'md'} onClick={clearFile} style={{ cursor: 'pointer', opacity: 0.75 }}>{t("drafts.file-select-text.text_2")}</Text>
                        </Group>
                    )}
                    
                    {!(text || file) && (
                        <Group px={16} py={0} spacing={6} sx={{ position: 'absolute', left: 0, top: 30 }}>
                            <Text color={'#666892'} size={'md'}>{t("drafts.file-upload-text.text_1")}</Text>
                            <Text onClick={selectFile} color={'#5A3DD1'} size={'md'} underline={true} weight={'500'} style={{ cursor: 'pointer' }}>{t("drafts.file-upload-text.text_2")}</Text>
                            <Text color={'#666892'} size={'md'}>{t('drafts.file-upload-text.text_3')}</Text>
                        </Group>
                    )}
                </Stack>
                
                <Dropzone
                    style={{ display: 'none' }}
                    openRef={fileRef}
                    accept={["text/plain"]}
                    onDrop={(files) => handleFile(files[0])}
                    onReject={(files) => console.log('rejected file', files[0])}
                    maxSize={104857600} // 100mb limit
                >
                    {() => null}
                </Dropzone>
            
            </Stack>
            <Group px={16} style={{ borderTop: '1px solid #DDDEEE', height: 56 }} spacing={0}>
                <Group spacing={16}>
                    <ComingSoonWrapper orientation="vertical">
                        <Icon icon={'ic:baseline-insert-link'} size={20} color={'#DDDEEE'} rotate={-30}/>
                    </ComingSoonWrapper>

                    <ComingSoonWrapper orientation="vertical">
                        <Icon icon={'ic:round-insert-photo'} size={20} color={'#DDDEEE'}/>
                    </ComingSoonWrapper>

                    <Divider/>

                    <ComingSoonWrapper orientation="vertical">
                        <Icon icon={'ic:round-visibility-off'} size={20} color={'#DDDEEE'}/>
                    </ComingSoonWrapper>
                </Group>
            </Group>
        </Fragment>
    );
};

const Divider = styled.div`
    height: 24px;
    background-color: #DDDEEE;
    width: 1px;
`;

const Input = styled(Textarea)`
    #title {
        font-weight: 700;
        font-size: 24px;
        line-height: 30px;
        color: #0A001E;
    }

    #title::placeholder {
        font-weight: 700;
        font-size: 24px;
        line-height: 30px;
        color: #666892;
    }
`;

const TextArea = styled(Textarea)`
    #text {
        font-weight: 400;
        font-size: 16px;
        line-height: 20px;
        color: #0A001E;
        padding: 0;
    }


    #text::placeholder {
        font-weight: 400;
        font-size: 16px;
        line-height: 20px;
        color: #666892;
    }
`;
