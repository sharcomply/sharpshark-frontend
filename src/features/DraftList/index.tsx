import { Stack, Text } from '@mantine/core';
import styled from '@emotion/styled';
import { TableHead } from '../../components/DataTable/TableHead';
import { Fragment } from 'react';
import { getListByDate } from 'helpers/getListByDate';
import { TableDate } from '../../components/DataTable/TableDate';
import { DraftItem } from './components/DraftItem';
import useDraftList from './hooks/useDraftList';
import { useTranslation } from 'react-i18next';

// Component
export default function DraftList() {
    const { drafts } = useDraftList()
    const { t } = useTranslation()

    const itemsByDate = getListByDate(drafts?.items || []);
    
    return (
        <Root>
            <Table>
                <TableHead items={[
                    { title: t("table.header-title"), width: '70%' },
                    { title: t("table.header-updated"), width: '20%' },
                    { title: '', width: '10%' }
                ]}/>
                
                <tbody>
                {!!drafts?.items.length && (
                    <Fragment>
                        {renderListByDate(t("table.row-by-date.today"), itemsByDate.today)}
                        {renderListByDate(t("table.row-by-date.this-week"), itemsByDate.week)}
                        {renderListByDate(t("table.row-by-date.last-month"), itemsByDate.lastMonth)}
                        {renderListByDate(t("table.row-by-date.earlier-this-month"), itemsByDate.month)}
                        {renderListByDate(t("table.row-by-date.earlier"), itemsByDate.earlier)}
                    </Fragment>
                )}
                
                {!drafts?.items.length && (
                    <tr style={{ borderTop: '1px solid #DDDEEE' }}>
                        <td style={{ padding: 16 }}>
                            <Text size={'sm'} color={'#666892'} weight={'400'}>{t("table.empty-table")}</Text>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                )}
                </tbody>
            </Table>
        </Root>
    );
}

// List By Date
function renderListByDate(title: string, items: Draft[]) {
    if (!items.length) return null;

    return (
        <Fragment>
            <TableDate title={title} cols={3} titlePos={2}/>
            {items.map((item) => {
                return <DraftItem key={item._id} item={item} />;
            })}
        </Fragment>
    );
}

// Styling
const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
`;

export const Table = styled.table`
    width: 100%;
    table-layout: fixed;
    border-collapse: collapse;
    overflow-wrap: anywhere;
`;
