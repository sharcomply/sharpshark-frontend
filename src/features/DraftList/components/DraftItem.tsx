import { Group, Loader, Text } from "@mantine/core";
import { humanizeDate } from "helpers/humanizeDate";
import styled from "@emotion/styled";
import { ActionMenu } from "./ActionMenu";
import { useDraftDelete } from "../hooks/useDraftDelete";
import { Link } from "react-router-dom";

// Props
type ComponentProps = {
    item: Draft;
};

// Component
export const DraftItem = ({ item }: ComponentProps) => {
    const { handleDelete, isLoading } = useDraftDelete(item._id);
    
    return (
        <Root>
            <td>
                <Link to={`/drafts/${item._id}`}>
                    <Group px={16} align="center" sx={{ height: 54 }} spacing={12}>
                        <Text size={"sm"} color={"#0A001E"} weight={"500"} style={{
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            whiteSpace: "nowrap",
                        }}>
                            {item.title}
                        </Text>
                    </Group>
                </Link>
            </td>

            <td>
                <Group px={16} align="center" sx={{ height: 54 }}>
                    <Link to={`/drafts/${item._id}`}>
                        <Text size={"sm"} color={"#666892"} weight={"500"}>
                            {humanizeDate(item.updatedAt).shortDate}
                        </Text>
                    </Link>
                </Group>
            </td>

            <td>
                <Group px={16} align="center"  sx={{ height: 54, justifyContent: 'flex-end' }}>
                    { isLoading 
                        ? <Loader color={"#4700e5"} size="sm" />
                        : <ActionMenu draftId={item._id} onDelete={handleDelete} />
                    }
                </Group>
            </td>
        </Root>
    );
};

// Styling
const Root = styled.tr`
    transition: background-color 200ms linear;
    border-top: 1px solid #dddeee;

    &:hover {
        background-color: #edeeff;
    }
`;
