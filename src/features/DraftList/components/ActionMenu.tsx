// Props
import { Group, Popover, Text } from "@mantine/core";
import Icon from "components/Icon";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";
import { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { ModalContent, ModalType } from "features/Modals/interfaces";
import { useModal } from "features/Modals/hooks/useModal";

// Variables
const rootStyles = {
    background: "#FFFFFF",
    border: "1px solid #DDDEEE",
    boxShadow: " 0px 2px 12px rgba(0, 0, 0, 0.1)",
    radius: 4,
    overflow: "hidden",
};

type ComponentProps = {
    draftId: string;
    onDelete?: () => void;
};

// Component
export function ActionMenu({ draftId, onDelete }: ComponentProps) {
    const [opened, setOpened] = useState(false);
    const { t } = useTranslation();
    const { handleOpen } = useModal();

    const onProtect = useCallback(() => {
        handleOpen({ type: ModalType.PROTECTED, content: ModalContent.LicenseAndMode });
    }, [handleOpen]);

    const control = (
        <Group align={"center"} position={"center"} px={4} onClick={() => setOpened((value) => !value)}>
            <Icon icon={"ic:sharp-more-vert"} size={24} color={"#666892"} hoverColor={"#666892"} />
        </Group>
    );

    return (
        <Popover
            opened={opened}
            target={control}
            onClose={() => setOpened(false)}
            width={160}
            radius={4}
            position="bottom"
            placement="end"
            transition={"pop-top-right"}
            styles={{ body: rootStyles, inner: { padding: 0 } }}
        >
            {/* View And Edit */}
            <Link to={`/drafts/${draftId}`}>
                <MenuItem px={16} py={12}>
                    <Text size={"sm"} weight={"500"}>
                        {t("table.row-options.view")}
                    </Text>
                </MenuItem>
            </Link>

            {/* Protect */}
            <Link to={`/drafts/${draftId}`}>
                <MenuItem onClick={onProtect} px={16} py={12}>
                    <Text size={"sm"} weight={"500"}>
                        {t("buttons.protect_btn")}
                    </Text>
                </MenuItem>
            </Link>

            {/* Delete */}
            <DeleteItem px={16} py={12} onClick={onDelete}>
                <Text size={"sm"} weight={"500"}>
                    × {t("table.row-options.delete")}
                </Text>
            </DeleteItem>
        </Popover>
    );
}

// Styling
const MenuItem = styled(Group)`
    cursor: pointer;
    color: #666892;

    &:hover {
        background-color: #4700e5;
        color: #ffffff;
    }
    &.disabled {
        color: #bbbed8;
    }

    &.disabled:hover {
        background-color: #f7f7fa;
        color: #8b8fad;
    }
`;

const DeleteItem = styled(Group)`
    cursor: pointer;
    color: #e50c54;

    &:hover {
        background-color: #e50c54;
        color: #ffffff;
    }
`;
