import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Types
export interface DraftListState {
    counts: {
        working: number;
        deleted: number;
    }
}

// Initial State
const initialState: DraftListState = {
    counts: {
        working: 0,
        deleted: 0
    }
};

// Slice
const draftListSlice = createSlice({
    name: "draftList",
    initialState,
    reducers: {
        clear: () => initialState,
        setCount: (state, action: PayloadAction<{ type: keyof DraftListState["counts"], value: number }>) => {
            state.counts[action.payload.type] = action.payload.value
        },
    },
});

export const draftListReducers = draftListSlice.reducer;
export const draftListActions = draftListSlice.actions;
