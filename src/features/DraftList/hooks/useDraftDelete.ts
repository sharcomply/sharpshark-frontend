import { useSidebarCounts } from 'features/Sidebar/hooks/useSidebarCounts';
import { useCallback } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { useNavigate } from 'react-router-dom';
import { backend } from '../../../api';

export const useDraftDelete = (id?: string, redirect = false) => {
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const {refetch} = useSidebarCounts()

    // Remove Location
    const { mutateAsync, ...options } = useMutation(() => backend.drafts.deleteDraft(id), {
        onSuccess: async () => {
            await Promise.all([
                queryClient.invalidateQueries(['DELETED_DRAFT_LIST']),
                queryClient.invalidateQueries(['DRAFT_LIST']),
                queryClient.invalidateQueries(['DRAFT'])
            ]);
        }
    });

    const handleDelete = useCallback(async () => {
        if (!id) return;
        await mutateAsync();
        refetch()
        if (!redirect) return
        navigate('/drafts', { replace: true });
    }, [id, mutateAsync, redirect, navigate, refetch]);

    return {
        handleDelete,
        ...options
    }
};
