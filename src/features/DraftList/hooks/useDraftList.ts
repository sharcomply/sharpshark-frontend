import { useQuery } from "react-query";
import { backend } from "api";

export default function useDraftList() {
    const { data: drafts, ...options } = useQuery(["DRAFT_LIST"], () => backend.drafts.getDraftList({ isMinted: false }), {
        keepPreviousData: true,
    });

    return {
        drafts,
        ...options,
    };
}
