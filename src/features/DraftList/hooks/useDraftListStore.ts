import { useCallback } from "react";
import { useAppDispatch, useAppSelector } from "store";
import { draftListActions, DraftListState } from "../store";

export function useDraftListStore() {
    const state = useAppSelector((state) => state.draftList);
    const dispatch = useAppDispatch()

    const setCount = useCallback((type: keyof DraftListState["counts"], value: number) => {
        const action = draftListActions.setCount({ type, value });
        dispatch(action)
    }, [dispatch]);

    const clearStore = useCallback(() => {
        const action = draftListActions.clear()
        dispatch(action)
    }, [dispatch])

    return {
        state,
        actions: {
            setCount,
            clearStore
        },
    };
}
